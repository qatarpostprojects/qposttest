﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = default!;
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
        public string? Name { get; set; }
        public string DocumentType { get; set; } = default!;
        public string? DocumentReference { get; set; }
        public string MobileNumber { get; set; } = default!;
        public string? AlternativeMobileNumber { get; set; }
        public string Nationality { get; set; } = default!;
        public DateTime? DateOfBirth { get; set; }
        public string PoBoxNumber { get; set; } = default!;
        public string ConnectedVpNumber { get; set; } = default!;
        public string? SmartBoxNumber { get; set; }
        public DateTime? PoBoxExpiry { get; set; }
        public DateTime? SmartBoxExpiry { get; set; }
        public string RecordStatus { get; set; } = default!;
        public int? WalletId { get; set; }
        public decimal? WalletBalanceAmount { get; set; }
        public string? WalletReferenceNumber { get; set; }
        public string? HDS { get; set; }
        public int? HDSPackageId { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? Reference3 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? EmailAddress { get; set; }
        public string? ProfileImage { get; set; }
        public string? FirstNameAr { get; set; }
        public string? MiddleNameAr { get; set; }
        public string? LastNameAr { get; set; }
        public string? NameAr { get; set; }

        public static Customer FromModel(CustomerModel model)
        {
            return new Customer
            {
                Id = model.Id,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                Name = model.Name,
                DocumentType = model.DocumentType,
                DocumentReference = model.DocumentReference,
                MobileNumber = model.MobileNumber,
                AlternativeMobileNumber = model.AlternativeMobileNumber,
                ConnectedVpNumber = model.ConnectedVpNumber,
                CreatedDate = model.CreatedDate,
                ModifiedDate = model.ModifiedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                DateOfBirth = model.DateOfBirth,
                HDS = model.HDS,
                HDSPackageId = model.HDSPackageId,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                Nationality = model.Nationality,
                PoBoxExpiry = model.PoBoxExpiry,
                PoBoxNumber = model.PoBoxNumber,
                RecordStatus = model.RecordStatus.ToString(),
                Reference1 = model.Reference1,
                Reference2 = model.Reference2,
                Reference3 = model.Reference3,
                SmartBoxExpiry = model.SmartBoxExpiry,
                SmartBoxNumber = model.SmartBoxNumber,
                WalletBalanceAmount = model.WalletBalanceAmount,
                WalletId = model.WalletId,
                WalletReferenceNumber = model.WalletReferenceNumber,
                EmailAddress = model.EmailAddress,
                ProfileImage = model.ProfileImage,
                FirstNameAr = model.FirstNameAr,
                MiddleNameAr = model.MiddleNameAr,
                LastNameAr = model.LastNameAr,
                NameAr = model.NameAr
            };
        }
    }
}
