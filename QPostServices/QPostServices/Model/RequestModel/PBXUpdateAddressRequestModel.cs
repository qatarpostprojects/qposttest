﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PBXUpdateAddressRequestModel
    {
        [Required]
       // [StringLength(6, MinimumLength = 1)]
        public string POBoxNumber { get; set; } = default!;

        [Required]
        public PBXAddressRequest Address { get; set; } = default!;
    }
}
