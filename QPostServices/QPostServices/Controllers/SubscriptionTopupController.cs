﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubscriptionTopupController : ControllerBase
    {
        private readonly ISubscriptionTopupService _subscriptionTopupService;

        public SubscriptionTopupController(ISubscriptionTopupService subscriptionTopupService)
        {
            _subscriptionTopupService = subscriptionTopupService;
        }

        #region Create SubscriptionTopup
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateSubscriptionTopup))]
        public async Task<IActionResult> CreateSubscriptionTopup([FromBody] SubscriptionTopupRequest request)
        {
            try
            {
                var result = await _subscriptionTopupService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get SubscriptionTopup by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SubscriptionTopup))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetSubscriptionTopup))]
        public async Task<IActionResult> GetSubscriptionTopup(int id)
        {
            try
            {
                var result = await _subscriptionTopupService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(SubscriptionTopup.FromModel((SubscriptionTopupModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get SubscriptionTopups By ServiceId
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SubscriptionTopup>))]
        [HttpGet("service/{serviceId}", Name = nameof(GetSubscriptionTopupListByServiceId))]
        public async Task<IActionResult> GetSubscriptionTopupListByServiceId(int serviceId)
        {
            try
            {
                var results = await _subscriptionTopupService.GetListByServiceIdAsync(serviceId);

                return Ok(results.Select(x => SubscriptionTopup.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get SubscriptionTopups
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SubscriptionTopup>))]
        [HttpGet(Name = nameof(GetSubscriptionTopupList))]
        public async Task<IActionResult> GetSubscriptionTopupList()
        {
            try
            {
                var results = await _subscriptionTopupService.GetListAsync();

                return Ok(results.Select(x => SubscriptionTopup.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete SubscriptionTopup
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteSubscriptionTopup))]
        public async Task<IActionResult> DeleteSubscriptionTopup(int id)
        {
            try
            {
                var result = await _subscriptionTopupService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update SubscriptionTopup
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateSubscriptionTopup))]
        public async Task<IActionResult> UpdateSubscriptionTopup(int id, [FromBody] SubscriptionTopupRequest request)
        {
            try
            {
                var result = await _subscriptionTopupService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
