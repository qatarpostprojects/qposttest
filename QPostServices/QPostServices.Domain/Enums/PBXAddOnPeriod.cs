﻿namespace QPostServices.Domain.Enums
{
    public enum PBXAddOnPeriod
    {
        OneYear = 0,
        TwoYear = 1,
        BoxRentalPeriod = 3
    }
}
