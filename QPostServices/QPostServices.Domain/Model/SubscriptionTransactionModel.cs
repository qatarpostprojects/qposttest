﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class SubscriptionTransactionModel
    {
        public int Id { get; set; }
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public string TransactionReferenceNumber { get; set; } = string.Empty;
        public decimal TransactionAmount { get; set; }
        public string TransactionType { get; set; } = string.Empty;
        public DateTime TransactionDate { get; set; }
        public string? PaymentType { get; set; }
        public int? WalletTransactioId { get; set; }
        public int? CardPaymentId { get; set; }
        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int ServiceId { get; set; }
        public string? AWBNumber { get; set; }
        public string? ServiceName { get; set; }
    }
}
