﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class ProfileNotificationSettings
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool RenewalReminders { get; set; }
        public bool OrderStatusUpdate { get; set; }
        public bool WalletbalanceAlert { get; set; }
        public bool DiscountOffers { get; set; }
        public bool Status { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static ProfileNotificationSettings FromModel(ProfileNotificationSettingsModel model)
        {
            return new ProfileNotificationSettings()
            {
                Id = model.Id,
                UserId = model.UserId,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CreatedDate = model.CreatedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                WalletbalanceAlert = model.WalletbalanceAlert,
                DiscountOffers = model.DiscountOffers,
                Status = model.Status,
                ModifiedUserName = model.ModifiedUserName,
                OrderStatusUpdate = model.OrderStatusUpdate,
                RenewalReminders = model.RenewalReminders,
            };
        }
    }
}
