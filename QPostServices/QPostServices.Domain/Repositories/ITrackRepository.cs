﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ITrackRepository
    {
        Task<List<TrackMobileResponseModel>> TrackAndTraceByMobile(TrackMobileModel model);
        Task AddLogAsync(LogModel log);
        Task<string?> GetMobileNumberAsync(string searchString);
    }
}
