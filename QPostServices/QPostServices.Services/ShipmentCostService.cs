﻿using QPostServices.Domain.Helper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class ShipmentCostService : IShipmentCostService
    {
        private readonly AppSettings _appSettings;
        private readonly IShipmentCostRepository _shipmentCostRepository;
        private readonly string _urlSuffix = "DigitalService/rest/calculatePrice";
        public ShipmentCostService(IShipmentCostRepository shipmentCostRepository, AppSettings appSettings)
        {
            _appSettings = appSettings;
            _urlSuffix = _appSettings.ExternalApiUrl + _urlSuffix;
            _shipmentCostRepository = shipmentCostRepository;
        }
        #region GetCostDetailsAsync
        public async Task<ResponseModel> GetCostDetailsAsync(PriceCalculatorModel model)
        {
            var masterList = await _shipmentCostRepository.GetListAsync();
            var shipmentCost = new List<ShipmentCostModel>();

            var priceTypes = new List<string>() { "priceStandardParcel", "priceGlobalMail", "priceEMS" };
            foreach (var item in priceTypes)
            {
                var shipment = masterList.FirstOrDefault(x => x.Type == item & x.ShipmentType.ToString().ToLower() == model.ShipmentType.ToLower());
                if (shipment != null)
                {
                    shipmentCost.Add(new ShipmentCostModel
                    {
                        Price = await GetPrice(model.WeightInKg, model.Destination, item),
                        SLA = shipment?.SLA ?? string.Empty,
                        Type = shipment?.TypeName ?? string.Empty,
                        TypeAr = shipment?.TypeAr
                    });
                }
            }
            return new ResponseModel
            {
                Error = false,
                Response = shipmentCost,
                StatusCode = System.Net.HttpStatusCode.OK
            };
        }
        #endregion

        #region GetPrice
        private async Task<string> GetPrice(decimal weightInKg, string destination, string priceType)
        {
            var req = $"{_urlSuffix}/getPrice?weightInKg={weightInKg}&destination={destination}&priceType={priceType}";
            try
            {
                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Get, req);
                var response = await client.SendAsync(request);
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        } 
        #endregion
    }
}
