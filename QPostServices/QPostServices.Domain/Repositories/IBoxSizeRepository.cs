﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IBoxSizeRepository
    {
        Task<List<BoxSizeModel>> GetListAsync();
        Task<BoxSizeModel> GetByIdAsync(int id);
        Task<int> CreateAsync(BoxSizeModel model);
        Task<int> UpdateAsync(BoxSizeModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
