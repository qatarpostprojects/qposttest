﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerNotificationService
    {
        Task<ResponseModel> CreateNotificationAsync(CustomerNotificationModel model);
        Task<ResponseModel> UpdateReadStatusAsync(int id, bool isRead);
        Task<List<CustomerNotificationModel>> GetAllNotificationsAsync(CustomerNotificationFetchModel model);
    }
}
