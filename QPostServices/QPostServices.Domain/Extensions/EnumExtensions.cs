﻿using QPostServices.Domain.Model;
using System.ComponentModel;
using System.Reflection;

namespace QPostServices.Domain.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                var _Attribs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (_Attribs != null && _Attribs.Count() > 0)
                {
                    return ((DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                }
            }
            return GenericEnum.ToString();
        }

        public static string GetString<T>(this Enum GenericEnum) where T : Enum
        {
            return Enum.GetName(typeof(T), GenericEnum);
        }

        public static int GetEnumFromDescription(string description, Type enumType)
        {
            foreach (var field in enumType.GetFields())
            {
                DescriptionAttribute attribute
                    = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute == null)
                    continue;
                if (attribute.Description == description)
                {
                    return (int)field.GetValue(null);
                }
            }
            return 0;
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static List<IdValueModel> ToIdValueList<T>() where T : Enum
        {
            var enumType = typeof(T);
            var idValueList = new List<IdValueModel>();

            foreach (var value in Enum.GetValues(enumType))
            {
                var id = (int)value;
                var name = Enum.GetName(enumType, value);
                idValueList.Add(new IdValueModel { Id = id, Value = name ?? "" });
            }

            return idValueList;
        }
    }
}
