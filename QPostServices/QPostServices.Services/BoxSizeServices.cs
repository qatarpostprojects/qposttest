﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class BoxSizeServices : IBoxSizeServices
    {
        private readonly IBoxSizeRepository _boxSizeRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Box size created successfully";
        private const string _successUpdate = "Box size updated successfully";
        private const string _failedCreation = "Box size creation failed";
        private const string _failedUpdation = "Box size updation failed";
        private const string _successDelete = "Box size deleted successfully";
        private const string _failedDelete = "Box size deletion failed";
        private const string _nameAlreadyExist = "Box size with name : {0} already exist";
        private const string _boxSizeDoesNotExist = "Box size with id : {0} Does not exist";


        public BoxSizeServices(IBoxSizeRepository boxSizeRepository, ILoginService loginService)
        {
            _boxSizeRepository = boxSizeRepository;
            _loginService = loginService;
        }

        #region Create BoxSize
        public async Task<ResponseModel> CreateAsync(BoxSizeModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _boxSizeRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get BoxSize by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _boxSizeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxSizeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of BoxSize
        public async Task<List<BoxSizeModel>> GetListAsync()
        {
            return await _boxSizeRepository.GetListAsync();
        }
        #endregion

        #region Update BoxSize
        public async Task<ResponseModel> UpdateAsync(BoxSizeModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            var result = await _boxSizeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxSizeDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete BoxSize
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _boxSizeRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxSizeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
