﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly ICustomerServicesService _customerServicesService;
        public ServiceController(ICustomerServicesService customerServicesService)
        {
            _customerServicesService = customerServicesService;
        }

        #region Create Service
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateService))]
        public async Task<IActionResult> CreateService([FromBody] ServiceRequest request)
        {
            try
            {
                var result = await _customerServicesService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Service by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Service))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetService))]
        public async Task<IActionResult> GetService(int id)
        {
            try
            {
                var result = await _customerServicesService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(Service.FromModel((ServiceModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Services
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Service>))]
        [HttpGet(Name = nameof(GetServiceList))]
        public async Task<IActionResult> GetServiceList()
        {
            try
            {
                var results = await _customerServicesService.GetAllActiveAsync();

                return Ok(results.Select(x => Service.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Service
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteService))]
        public async Task<IActionResult> DeleteService(int id)
        {
            try
            {
                var result = await _customerServicesService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Service
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateService))]
        public async Task<IActionResult> UpdateService(int id, [FromBody] ServiceRequest request)
        {
            try
            {
                var result = await _customerServicesService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Service Amount
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceAmount))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("GetServiceAmount/{id}", Name = nameof(GetServiceAmount))]
        public async Task<IActionResult> GetServiceAmount(int id)
        {
            try
            {
                var result = await _customerServicesService.GetServiceAmountAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(ServiceAmount.FromModel((ServiceAmountModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region GetServiceDetailsByCustomerId
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Service>))]
        [HttpGet("GetServiceDetailsByCustomerId", Name = nameof(GetServiceDetailsByCustomerId))]
        public async Task<IActionResult> GetServiceDetailsByCustomerId()
        {
            try
            {
                var results = await _customerServicesService.GetDetailsByCustomerIdAsync();

                return Ok(results.Select(x => Service.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Lookup
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<IdValue>))]
        [HttpGet("Lookup/{type}",Name = nameof(GetLookup))]
        public async Task<IActionResult> GetLookup(ServiceType type)
        {
            try
            {
                var results = await _customerServicesService.GetLookupAsync(type);

                return Ok(results.Select(x => IdValue.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
