﻿namespace QPostServices.Domain.Model
{
    public class CreateOrderResponse
    {
        public int OrderId { get; set; }
        public string TrackingNumber { get; set; } = string.Empty;
        public string MerchantTrackingNo { get; set; } = string.Empty;

    }
}
