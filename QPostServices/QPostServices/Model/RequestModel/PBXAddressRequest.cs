﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PBXAddressRequest
    {
        public string Address { get; set; } = default!;
        public string? BuildingNumber { get; set; }
        public string? ZoneNumber { get; set; }
        public string? StreetNumber { get; set; }
        public string? City { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Invalid phone number.")]
        public string MobileNumber { get; set; } = default!;
        public string? OfficeTelephoneNo { get; set; }
        [Required]
        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string Email { get; set; } = default!;
        public PBXAddressModel ToModel()
        {
            return new PBXAddressModel()
            {
                Address = Address,
                BuildingNumber = BuildingNumber,
                ZoneNumber = ZoneNumber,
                StreetNumber = StreetNumber,
                City = City,
                MobileNumber = MobileNumber,
                Email = Email,
                OfficeTelephoneNo = OfficeTelephoneNo
            };
        }
    }
}
