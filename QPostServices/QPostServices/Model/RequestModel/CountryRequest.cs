﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CountryRequest
    {
        [Required]
        public string CountryName { get; set; } = string.Empty;
        public string? CountryCode { get; set; }
        public string? Flag { get; set; }
        public IFormFile? FlagFile { get; set; }
        public string? CountryNameAr { get; set; }

        public CountryModel ToCreateModel()
        {
            return new CountryModel
            {
                CountryCode = CountryCode,
                Flag = Flag,
                CountryName = CountryName,
                FlagFile = FlagFile,
                CountryNameAr = CountryNameAr
            };
        }

        public CountryModel ToUpdateModel(int id)
        {
            return new CountryModel
            {
                CountryCode = CountryCode,
                Flag = Flag,
                FlagFile = FlagFile,
                CountryName = CountryName,
                Id = id,
                CountryNameAr = CountryNameAr
            };
        }
    }
}