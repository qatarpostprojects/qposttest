﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class CustomerProfileImageService : ICustomerProfileImageService
    {
        private const string _successCreation = "Profile picture created successfully";
        private const string _successUpdate = "Profile picture updated successfully";
        private const string _failedCreation = "Profile picture creation failed";
        private const string _failedUpdation = "Profile picture updation failed";
        private const string _failedToGet = "Profile picture could not be found";

        private readonly ICustomerProfileImageRepository _customerProfileImageRepository;
        private readonly IFileService _fileServices;
        public CustomerProfileImageService(ICustomerProfileImageRepository customerProfileImageRepository, IFileService fileServices)
        {
            _customerProfileImageRepository = customerProfileImageRepository;
            _fileServices = fileServices;
        }

        #region Create
        public async Task<ResponseModel> CreateImage(CustomerProfileImageModel model)
        {
            var response = new ResponseModel();
            model.Image = await _fileServices.ConvertImageToBase64(model.ImageFile);

            var result = await _customerProfileImageRepository.CreateImageAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get profile image
        public async Task<ResponseModel> GetCustomerProfileImage(int customerId)
        {
            var response = new ResponseModel();

            var result = await _customerProfileImageRepository.GetCustomerProfileImageAsync(customerId);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedToGet;
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;

        }
        #endregion

        #region Update profile image
        public async Task<ResponseModel> UpdateImage(CustomerProfileImageModel model)
        {
            var response = new ResponseModel();
            model.Image = await _fileServices.ConvertImageToBase64(model.ImageFile);

            var result = await _customerProfileImageRepository.UpdateImageAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedUpdation;
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
