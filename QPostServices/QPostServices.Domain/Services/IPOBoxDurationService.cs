﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPOBoxDurationService
    {
        Task<List<POBoxDurationModel>> GetListByServiceIdAsync(int serviceId);
        Task<List<POBoxDurationModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(POBoxDurationModel model);
        Task<ResponseModel> UpdateAsync(POBoxDurationModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
