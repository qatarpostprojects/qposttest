﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PaymentRequest
    {
        [Required]
        public string PaymentSessionId { get; set; } = string.Empty;
        [Required]
        public int PaymentProviderId { get; set; }
        [Required]
        public string PaymentProviderName { get; set; } = string.Empty;
        [Required]
        public int PaymentServiceId { get; set; }
        [Required]
        public string PaymentServiceName { get; set; } = string.Empty;
        public decimal? PaymentRequestAmount { get; set; }
        public decimal? PaymentReceiveAmount { get; set; }
        public int? PaymentTransactionId { get; set; }
        public string? PaymentBankReferenceNumber { get; set; }
        [Required]
        public int PaymentCustomerId { get; set; }
        [Required]
        public DateTime PaymentDate { get; set; }
        public string? PaymentReference1 { get; set; }
        public PaymentModel ToCreateModel()
        {
            return new PaymentModel()
            {
                PaymentReceiveAmount = PaymentReceiveAmount,
                PaymentTransactionId = PaymentTransactionId,
                PaymentBankReferenceNumber = PaymentBankReferenceNumber,
                PaymentCustomerId = PaymentCustomerId,
                PaymentServiceId = PaymentServiceId,
                PaymentServiceName = PaymentServiceName,
                PaymentRequestAmount = PaymentRequestAmount,
                PaymentReference1= PaymentReference1,
                PaymentSessionId = PaymentSessionId,
                PaymentProviderId = PaymentProviderId,
                PaymentDate = PaymentDate,
                PaymentProviderName = PaymentProviderName
            };
        }
        public PaymentModel ToUpdateModel(int id) {
            return new PaymentModel()
            {
                Id = id,
                PaymentReceiveAmount= PaymentReceiveAmount,
                PaymentTransactionId= PaymentTransactionId,
                PaymentBankReferenceNumber = PaymentBankReferenceNumber,
                PaymentReference1 = PaymentReference1,
            };
        }
    }
}
