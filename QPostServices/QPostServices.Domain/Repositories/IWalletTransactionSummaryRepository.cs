﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IWalletTransactionSummaryRepository
    {
        Task<int> CreateAsync(WalletTransactionSummaryModel model, TransactionType transactionType);
        Task<WalletTransactionSummaryModel?> GetByIdAsync(int id);
        Task<List<WalletTransactionSummaryModel>> GetByWalletIdAsync(int walletId);
        Task<List<WalletTransactionSummaryModel>> GetListByCustomerId(int customerId);
        Task<decimal> GetWalletBalanceAmountAsync(int customerId, int walletId);
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(WalletTransactionSummaryModel model);
    }
}

