﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IBoxTypeRepository
    {
        Task<List<BoxTypeModel>> GetListAsync();
        Task<BoxTypeModel> GetByIdAsync(int id);
        Task<int> CreateAsync(BoxTypeModel model);
        Task<int> UpdateAsync(BoxTypeModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
