﻿namespace QPostServices.Domain.Model
{
    public class ExternalCreateOrderModel
    {
        public float TotalWeight { get; set; }
        public string? BranchID { get; set; }
        public string? PostingDate { get; set; }
        public string? shipmentType { get; set; }
        public string? Source { get; set; }
        public string? airWayBillNumber { get; set; }
        public string? remarks { get; set; }
        public OrderItemModel? item { get; set; } = new();
        public SenderModel? sender { get; set; } = new();
        public LabelDataModel? LabelData { get; set; } = new();
    }

    public class OrderItemModel
    {
        public FinancedetailsModel? financeDetails { get; set; } = new();
        public RecipientModel? recipient { get; set; } = new();
        public PickupaddressModel? pickupaddress { get; set; } = new();
        public string? ProductServiceCategory { get; set; }
        public string? TrackingNumber { get; set; }
        public string? transactionDate { get; set; }
        public string? PostingDate { get; set; }
        public string? deliveryDate { get; set; }
        public string? PickupDate { get; set; }
        public string? PickupMode { get; set; }
        public string? pickupModeValue { get; set; }
        public string? deliveryMode { get; set; }
        public string? deliveryModeValue { get; set; }
        public string? deliveryTimeSlot_from { get; set; }
        public string? deliveryTimeSlot_to { get; set; }
        public string? PickupTimeSlot_from { get; set; }
        public string? PickupTimeSlot_to { get; set; }
        public float itemWeight { get; set; }
        public int quantity { get; set; }
        public string? TotalAmount { get; set; }
        public bool codRequired { get; set; }
        public bool podRequired { get; set; }
        public int volumetricWeight { get; set; }
        public string? ShipmentContent { get; set; }
        public string? safeplace { get; set; }
        public string? PaymentType { get; set; }
        public string? chequeNo { get; set; }
        public string? paymentCurrency { get; set; }
        public string? OrginBranch { get; set; }
        public string? Priority { get; set; }
        public int POD { get; set; }
    }

    public class SenderModel
    {
        public string? salutation { get; set; }
        public string? firstName { get; set; }
        public string? LastName { get; set; }
        public string? middleName { get; set; }
        public string? estNo { get; set; }
        public string? companyName { get; set; }
        public string? Address { get; set; }
        public string? buildingNo { get; set; }
        public string? City { get; set; }
        public string? streetNumber { get; set; }
        public string? streetName { get; set; }
        public string? streetType { get; set; }
        public string? floor { get; set; }
        public string? town { get; set; }
        public string? region { get; set; }
        public string? zone { get; set; }
        public string? poBoxNumber { get; set; }
        public string? postalCode { get; set; }
        public string? isoCountryCode { get; set; }
        public string? IdentifierType { get; set; }
        public string? IdentifierNumber { get; set; }
        public string? PhoneNumber { get; set; }
        public string? MobileNumber { get; set; }
        public string? Email { get; set; }
        public string? UnitNo { get; set; }
    }

    public class LabelDataModel
    {
        public string? BarCodeDimention { get; set; }
        public string? CopyCount { get; set; }
        public string? LabelType { get; set; }
        public string? OutputType { get; set; }
        public string? Quantity { get; set; }
        public string? Suffix { get; set; }
        public string? SuffixCode { get; set; }
    }

    public class FinancedetailsModel
    {
        public string? InvoiceAmount { get; set; }
        public string? InsuranceAmount { get; set; }
        public string? customsDuty { get; set; }
        public string? DeliveryCharges { get; set; }
        public string? discountAmount { get; set; }
        public string? ExtraAmount { get; set; }
        public string? PostageStatus { get; set; }
        public string? PostageAmount { get; set; }
    }

    public class RecipientModel
    {
        public string? salutation { get; set; }
        public string? firstName { get; set; }
        public string? LastName { get; set; }
        public string? middleName { get; set; }
        public string? Address { get; set; }
        public string? buildingNo { get; set; }
        public string? City { get; set; }
        public string? streetNumber { get; set; }
        public string? streetName { get; set; }
        public string? streetType { get; set; }
        public string? floor { get; set; }
        public string? zone { get; set; }
        public string? poBoxNumber { get; set; }
        public string? PostalCode { get; set; }
        public string? isoCountryCode { get; set; }
        public string? email { get; set; }
        public string? MobileNumber { get; set; }
        public string? ArabicName { get; set; }
        public string? UnitNumber { get; set; }
        public string? Type { get; set; }
        public string? typeOfIdentification { get; set; }
        public string? identificationNumber { get; set; }
        public string? PhoneNumber { get; set; }
    }

    public class PickupaddressModel
    {
        public string? salutation { get; set; }
        public string? firstName { get; set; }
        public string? LastName { get; set; }
        public string? middleName { get; set; }
        public string? Address { get; set; }
        public string? buildingNo { get; set; }
        public string? City { get; set; }
        public string? streetNumber { get; set; }
        public string? streetName { get; set; }
        public string? streetType { get; set; }
        public string? floor { get; set; }
        public string? region { get; set; }
        public string? zone { get; set; }
        public string? poBoxNumber { get; set; }
        public string? isoCountryCode { get; set; }
        public string? email { get; set; }
        public string? MobileNumber { get; set; }
        public string? ArabicName { get; set; }
        public string? UnitNumber { get; set; }
        public string? typeOfIdentification { get; set; }
        public string? identificationNumber { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
