﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PickupTypeRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; } 
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Logo { get; set; }
        public IFormFile? LogoImageFile { get; set; }
        public PickupTypeModel ToCreateModel()
        {
            return new PickupTypeModel 
            { 
                Name = Name, 
                Description = Description, 
                Logo = Logo , 
                LogoImageFile = LogoImageFile, 
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
        public PickupTypeModel ToUpdateModel(int id)
        {
            return new PickupTypeModel { 
                Id=id,
                Name = Name, 
                Description = Description, 
                Logo = Logo , 
                LogoImageFile = LogoImageFile,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
