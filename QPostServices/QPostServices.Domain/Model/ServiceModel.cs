﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class ServiceModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string SubscriptionType { get; set; } = string.Empty;
        public string? ApplicableFor { get; set; } 
        public string SubscriptionPaymentType { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; } 
        public string? SubscriptionTypeAr { get; set; } 
        public string? ApplicableForAr { get; set; } 
        public string? SubscriptionPaymentTypeAr { get; set; } 
        public bool IsUpgradeEligible { get; set; }
        public int? UpgradeID { get; set; }
        public bool IsAddonServiceAvailable { get; set; }
        public bool IsAddonService { get; set; }
        public int? ParentServiceId { get; set; }
        public int? LoyaltyID { get; set; }
        public int ServiceBaseValue { get; set; }
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }
        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ServiceBaseOn { get; set; }
        public int? ServiceTypeId { get; set; }

        public List<ServiceModel>? AddOnServices { get; set; }

    }
}
