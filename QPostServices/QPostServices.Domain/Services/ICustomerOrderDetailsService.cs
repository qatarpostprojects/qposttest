﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerOrderDetailsService
    {
        Task<ResponseModel> CreateAsync(CustomerOrdersDetailsModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<CustomerOrdersDetailsModel>> GetAllActiveAsync();
        Task<List<CustomerOrdersDetailsModel>> GetByCustomerIdAsync(int customerId);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(CustomerOrdersDetailsModel model);
        Task<ResponseModel> GetByTrackingNumberAsync(string trackingNumber);
        Task<ResponseModel> CheckOrderDetailAsync(string trackingNumber, string? phoneNumber, string? POBoxNumber);

        Task<ResponseModel> CreateOrderAsync(CreateOrderModel model);
        Task<ResponseModel> GetOrderByIdOrTrackingNumberAsync(string reqInput);
        Task<ResponseModel> UpdateOrderAsync(int id, CreateOrderModel model);
        Task<List<GetOrderDetails>> GetMyOrdersAsync(int customerId, string? type);
        Task<List<GetOrderDetails>> GetOrdersByFilterAsync(string? phoneNumber, string? poBoxNo, string? type, FilterDateTypes? filterDate);
        Task<ResponseModel> GetLatestTrackingDetails(string? type);
        Task<ResponseModel> CreateInboundRequestAsync(InboundModel model);
    }
}
