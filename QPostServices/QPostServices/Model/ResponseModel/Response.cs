﻿namespace QPostServices.Model.ResponseModel
{
    public class PBXResponse
    {
        public string? ErrorCode { get; set; }
        public object? Entry { get; set; }
        public string? Message { get; set; }
    }
}
