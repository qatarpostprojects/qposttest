﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class BoxSizeModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
