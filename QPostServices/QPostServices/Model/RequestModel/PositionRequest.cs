﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PositionRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        
        [Required]
        public string Description { get; set; } = string.Empty;

        public PositionModel ToCreateModel()
        {
            return new PositionModel
            {
                Name = Name,
                Description = Description
            };
        }

        public PositionModel ToUpdateModel(int id)
        {
            return new PositionModel
            {
                Id = id,
                Name = Name,
                Description = Description
            };
        }
    }
}
