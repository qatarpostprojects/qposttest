﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class ValidAddressRequest
    {
        [Required]
        public int BuildingNumber { get; set; }
        [Required]
        public int StreetNumber { get; set; }
        [Required]
        public int ZoneNumber { get; set; }

        public ValidAddressModel ToModel()
        {
            return new ValidAddressModel()
            {
                BuildingNumber = BuildingNumber,
                StreetNumber = StreetNumber,
                ZoneNumber = ZoneNumber
            };
        }
    }
}
