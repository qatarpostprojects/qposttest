﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class BoxSizeRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        
        [Required]
        public string Description { get; set; } = string.Empty;

        public BoxSizeModel ToCreateModel()
        {
            return new BoxSizeModel
            {
                Name = Name,
                Description = Description
            };
        }

        public BoxSizeModel ToUpdateModel(int id)
        {
            return new BoxSizeModel
            {
                Id = id,
                Name = Name,
                Description = Description
            };
        }
    }
}
