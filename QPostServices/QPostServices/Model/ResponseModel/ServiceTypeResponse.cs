﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class ServiceTypeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }
        public static ServiceTypeResponse FromModel(ServiceTypeModel model)
        {
            return new ServiceTypeResponse()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                RecordStatus = model.RecordStatus,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                CreatedUserName = model.CreatedUserName,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr
            };
        }
    }
}
