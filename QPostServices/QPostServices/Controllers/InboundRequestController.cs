﻿using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Services;
using QPostServices.Model.ResponseModel;

namespace QPostServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InboundRequestController : ControllerBase
    {
        private readonly IInboundRequestService _inboundRequestService;
        public InboundRequestController(IInboundRequestService inboundRequestService)
        {
            _inboundRequestService = inboundRequestService;
        }

        #region Get MasterLookup
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<IdValue>))]
        [HttpGet("Lookup", Name = nameof(GetMasterLookupList))]
        public async Task<IActionResult> GetMasterLookupList()
        {
            try
            {
                var results = await _inboundRequestService.GetMasterLookupAsync();

                return Ok(results.Select(x => IdValue.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
