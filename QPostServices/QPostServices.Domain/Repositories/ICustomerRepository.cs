﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerRepository
    {
        Task<int> CreateAsync(CustomerModel model);
        Task<int> RegistrationAsync(CustomerRegistraionModel model);
        Task<CustomerModel?> GetByIdAsync(int id);
        Task<List<CustomerModel>> GetAllActiveAsync();
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(CustomerModel model);
        Task SaveCustomerVerificationOTP(OtpModel model);
        Task<bool> VerifyOTPAsync(string value, string otp);
        Task<int> UpdateProfileAsync(CustomerProfileModel model);
        Task<bool> CheckPasswordValidAsync(int customerId,string password);
        Task<int> ChangePasswordAsync(int id, string password);
        Task<int> UpdateLanguagePreference(int customerId,string language, int modifiedUser);
        Task<int> MapOrderCustomerAsync(MapOrderCustomerModel model);

    }
}
