﻿namespace QPostServices.Domain.Model
{
    public class SubscriptionRenewModel
    {
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public int SubscriptionTypeValue { get; set; }
        public int? CardPaymentId { get; set; }
        public int? WalletTransactioId { get; set; }
        public string PaymentType { get; set; } = string.Empty;
        public string AWBNumber { get; set; } = string.Empty;
        public PBXRenewPostBoxModel ExternalRenewModel { get; set; } = new PBXRenewPostBoxModel();
    }
}
