﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PaymentMethodRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? Image { get; set; }
        public IFormFile? ImageFile { get; set; }
        public int ListingOrder { get; set; }
        public string? RedirectingURL { get; set; }
        public string? NameAr { get; set; }

        public PaymentMethodModel ToCreateModel()
        {
            return new PaymentMethodModel()
            {
                Name = Name,
                Image = Image,
                ImageFile = ImageFile,
                ListingOrder = ListingOrder,
                RedirectingURL = RedirectingURL,
                NameAr = NameAr
            };
        }
        public PaymentMethodModel ToUpdateModel(int id)
        {
            return new PaymentMethodModel()
            {
                Id = id,
                Name = Name,
                Image = Image,
                ImageFile = ImageFile,
                ListingOrder = ListingOrder,
                RedirectingURL = RedirectingURL,
                NameAr = NameAr
            };
        }
    }
}
