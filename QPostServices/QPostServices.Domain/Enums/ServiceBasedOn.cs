﻿namespace QPostServices.Domain.Enums
{
    public enum ServiceBasedOn
    {
        Transaction = 1,
        Amount = 2,
        Weight = 3,
        Day = 4,
        Month = 5,
        Year = 6
    }
}
