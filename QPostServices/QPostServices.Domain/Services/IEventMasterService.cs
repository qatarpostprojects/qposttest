﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IEventMasterService
    {
        Task<List<EventMasterModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(EventMasterModel model);
        Task<ResponseModel> UpdateAsync(EventMasterModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
