﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class HSCODESearchResponse
    {
        public int Id { get; set; }
        public string HSCODE { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string DisplayKeyWord { get; set; } = string.Empty;
        public string? DescriptionAr { get; set; }
        public string? DisplayKeyWordAr { get; set; }

        public static HSCODESearchResponse FromModel(HSCODEModel model)
        {
            return new HSCODESearchResponse
            { 
                Id = model.Id, 
                HSCODE = model.HSCODE, 
                Description = model.Description,
                DisplayKeyWord = model.DisplayKeyWord,
                DescriptionAr = model.DescriptionAr,
                DisplayKeyWordAr = model.DisplayKeyWordAr 
            };
        }
    }
}
