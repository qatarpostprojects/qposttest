﻿namespace QPostServices.Domain.Enums
{
    public enum PBXCustomerIdType
    {
        QatarID = 1,
        Passport = 2
    }
}
