﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPOBoxService
    {
        Task<List<POBoxModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(POBoxModel model);
        Task<ResponseModel> UpdateAsync(POBoxModel model);
        Task<ResponseModel> DeleteAsync(int id);
        Task<PBXResponseModel> GetPOBoxEnqiryAsync(POBoxEnquiryModel model);
        Task<PBXResponseModel> RenewPostBoxAsync(string POBoxNo, POBoxRenewalPeriod Period);
        Task<PBXResponseModel> UpdateAddressAsync(PBXAddressModel model, string poBoxNumber);
        Task<PBXResponseModel> GetAddOnServicesAsync(string pOBoxNo, PBXAddOnPeriod addOnPeriod);
        Task<PBXResponseModel> GetAvailablePOBoxesAsync(string pOBoxNo, string costCenter);
        Task<PBXResponseModel> AddAddOnServicesAsync(string pOBoxNo, PBXAddOnPeriod addOnPeriod, PBXAddOnServiceType addOnServiceType);
        Task<PBXResponseModel> SubscribePOBoxAsync(PBXSubscribeModel subscribeModel);
        Task<PBXResponseModel> HoldPOBoxAsync(string poBoxNo, string CostCenter);
        Task<List<POBoxServiceModel>> GetAllLandingPageServicesAsync();
    }
}
