﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class FileServices : IFileService
    {
        #region Comvert image to base 64
        public async Task<string> ConvertImageToBase64(IFormFile? image)
        {
            if (image != null)
            {
                using var memoryStream = new MemoryStream();
                await image.CopyToAsync(memoryStream);
                byte[] bytes = memoryStream.ToArray();
                return Convert.ToBase64String(bytes);
            }
            return string.Empty;
        }
        #endregion
    }
}
