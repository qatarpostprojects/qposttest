﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class DeliveryType
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public string PriceType { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public int ListingOrder { get; set; }
        public int DeliveryProductId { get; set; }
        public string? Image { get; set; }
        public string DeliveryProductName { get; set; } = string.Empty;
        public string? ProductServiceCategory { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public static DeliveryType FromModel(DeliveryTypeModel model)
        {
            return new DeliveryType
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                RecordStatus = model.RecordStatus,
                CreatedDate = model.CreatedDate,
                CreatedUserId = model.CreatedUserId,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserId = model.ModifiedUserId,
                ModifiedUserName = model.ModifiedUserName,
                ListingOrder = model.ListingOrder,
                DeliveryProductId = model.DeliveryProductId,
                PriceType = model.PriceType,
                Image = model.Image,
                DeliveryProductName = model.DeliveryProductName,
                ProductServiceCategory = model.ProductServiceCategory,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr
            };
        }
    }
}
