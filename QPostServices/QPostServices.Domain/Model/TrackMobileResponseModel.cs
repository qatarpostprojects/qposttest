﻿namespace QPostServices.Domain.Model
{
    public class TrackMobileResponseModel
    {
        public string TrackingNumber { get; set; } = string.Empty;
        public string CurrentStatus { get; set; } = string.Empty;
        public DateTime CurrentStatusDate { get; set; }
        public string Origin { get; set; } = string.Empty;
        public string ProductServiceCode { get; set; } = string.Empty;
        public DateTime ReceivedDate { get; set; }
    }
}
