﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;
        private const string _DoesNotExist = "Address with ZoneNo : {0}  and StreetNo : {1} and BuildingNo: {2} Does not exist";

        public LocationService(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }

        #region GetBuildings
        public Task<List<string>> GetBuildingsAsync(int zoneNumber, int streetNumber)
        {
            return _locationRepository.GetBuildingsAsync(zoneNumber, streetNumber);
        }
        #endregion

        #region GetStreets
        public Task<List<string>> GetStreetsAsync(int zoneNumber)
        {
            return _locationRepository.GetStreetsAsync(zoneNumber);
        }
        #endregion

        #region GetZones
        public Task<List<string>> GetZonesAsync(int zoneNumber)
        {
            return _locationRepository.GetZonesAsync(zoneNumber);
        }

        #endregion

        #region ValidateAddress
        public async Task<ResponseModel> ValidateAddressAsync(ValidAddressModel model)
        {
            var response = new ResponseModel();
            var result = await _locationRepository.ValidateAddressAsync(model);

            if (result.ValidAddress)
            {
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
                response.Response = result;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, model.ZoneNumber, model.StreetNumber, model.BuildingNumber);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
