﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Services
{
    public class ProfileNotificationsService : IProfileNotificationSettingsService
    {
        private const string _DoesNotExist = "Settings for user id {0} does not exists";
        private const string _successCreation = "Settings created successfully";
        private const string _failedCreation = "Settings creation failed";
        private const string _successUpdate = "Settings for user id {0} updated successfully";


        private readonly IProfileNotificationSettingsRepository _profileNotificationSettingsRepository;
        private readonly ILoginService _loginService;
        public ProfileNotificationsService(IProfileNotificationSettingsRepository profileNotificationSettingsRepository, ILoginService loginService)
        {
            _profileNotificationSettingsRepository = profileNotificationSettingsRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(ProfileNotificationSettingsModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _profileNotificationSettingsRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get By User ID
        public async Task<ResponseModel> GetByUserId(int userId)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _profileNotificationSettingsRepository.GetByUserId(userId);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_DoesNotExist, userId);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update By User id
        public async Task<ResponseModel> UpdateAsync(ProfileNotificationSettingsModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _profileNotificationSettingsRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successUpdate, model.UserId);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_DoesNotExist, model.UserId);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        } 
        #endregion
    }
}
