﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class WalletTransactionRequest
    {
        public int CustomerId { get; set; }
        public int WalletId { get; set; }
        public string BillNo { get; set; } = default!;
        public decimal Amount { get; set; }
        public string ReferenceId { get; set; } = default!;
        public string BankTransactionId { get; set; } = default!;
        public DateTime TransactionDate { get; set; }
        public int PaymentId { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public WalletTransactionModel ToCreateModel()
        {
            return new WalletTransactionModel 
            { 
                CustomerId = CustomerId, 
                WalletId = WalletId, 
                BillNo = BillNo, 
                Amount = Amount, 
                ReferenceId = ReferenceId,
                BankTransactionId = BankTransactionId,
                TransactionDate = TransactionDate,
                PaymentId = PaymentId,
                Reference1 = Reference1,
                Remarks = Remarks
            };
        }
    }
}
