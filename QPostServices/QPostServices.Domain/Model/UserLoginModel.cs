﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class UserLoginModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string SecurityQuestion { get; set; } = string.Empty;
        public string SecurityAnswer { get; set; } = string.Empty;
        public int RoleId { get; set; }
        public string RoleName { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? CreatedUserName { get; set; }
        public string? Token { get; set; }
        public string? POBOXNumber { get; set;}
        public int? ServiceId { get; set; }
        public string MobileNumber { get; set; } = string.Empty;
        public string EmailAddress { get; set; } = string.Empty;
        public string? PreferedLanguage { get; set; }
    }
}
