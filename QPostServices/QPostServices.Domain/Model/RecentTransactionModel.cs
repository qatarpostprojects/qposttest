﻿namespace QPostServices.Domain.Model
{
    public class RecentTransactionModel
    {
        public int PaymentId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal? PaymentRequestAmount { get; set; }
        public decimal? PaymentReceiveAmount { get; set; }
        public int? OrderId { get; set; }
        public int? SubscriptionTransactionId { get; set; }
        public string? SubscriptionReferenceNumber { get; set; }
        public string? OrderTrackingNumber { get; set; }
        public string? TransactionReferenceId { get; set; }
        public int? WalletTransactionDetailsId { get; set; }

    }
}
