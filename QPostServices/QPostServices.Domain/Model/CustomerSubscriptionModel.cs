﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CustomerSubscriptionModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceType { get; set; } = string.Empty;
        public int SubscriptionTypeValue { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string? QatarIdFrontSide { get; set; }
        public IFormFile? QatarIdFrontSideFile { get; set; }
        public string? QatarIdBackSide { get; set; }
        public IFormFile? QatarIdBackSideFile { get; set; }

        public string? CompanyDocFrontSide { get; set; }
        public IFormFile? CompanyDocFrontSideFile { get; set; }
        public string? CompanyDocBackSide { get; set; }
        public IFormFile? CompanyDocBackSideFile { get; set; }



        public int? ServiceProductId { get; set; }
        public string PaymentChannel { get; set; } = string.Empty;
        public bool IsEligibleForRenew { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public int? LastPaymentTransactionId { get; set; }
        public bool ActiveStatus { get; set; }
        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? MaximumNumberOfTransaction { get; set; }
        public int? NumberOfTransactionPending { get; set; }
        public int? NumberOfTransactionCompleted { get; set; }
        public decimal? ServiceAmount { get; set; }
        public decimal? ServiceAmountPending { get; set; }
        public decimal? ServiceAmountConsumed { get; set; }
        public int? CardPaymentId { get; set; }
        public string? PaymentType { get; set; }
        public int? WalletTransactioId { get; set; }
        public string AWBNumber { get; set; } = string.Empty;

        public PBXSubscribeModel? ExternalSubscriptionModel { get; set; } = new PBXSubscribeModel();
        
    }

    public class CustomerSubscriptionRenewModel
    {
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public DateTime EndDate { get; set; }
        public int? ModifiedUser { get; set; }
        public int? MaximumNumberOfTransaction { get; set; }
        public int? NumberOfTransactionPending { get; set; }
        public decimal? ServiceAmount { get; set; }
        public decimal? ServiceAmountPending { get; set; }
    }
}
