﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public PaymentRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(PaymentModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentDetails_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@paymentSessionId", model.PaymentSessionId);
                parameters.Add("@paymentProviderId", model.PaymentProviderId);
                parameters.Add("@paymentProviderName", model.PaymentProviderName);
                parameters.Add("@paymentServiceId", model.PaymentServiceId);
                parameters.Add("@paymentServiceName", model.PaymentServiceName);
                parameters.Add("@paymentRequestAmount", model.PaymentRequestAmount);
                parameters.Add("@paymentReceiveAmount", model.PaymentReceiveAmount);
                parameters.Add("@paymentTransactionId", model.PaymentTransactionId);
                parameters.Add("@paymentBankReferenceNumber", model.PaymentBankReferenceNumber);
                parameters.Add("@paymentCustomerId", model.PaymentCustomerId);
                parameters.Add("@paymentDate", model.PaymentDate);
                parameters.Add("@paymentReference1", model.PaymentReference1);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By CustomerId
        public async Task<List<PaymentModel>> GetAllActiveByCustomerIdAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentDetails_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<PaymentModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<PaymentModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentDetails_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@paymentId", id);
                var result = await connection.QueryAsync<PaymentModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(PaymentModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentDetails_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@paymentReceiveAmount", model.PaymentReceiveAmount);
                parameters.Add("@paymentTransactionId", model.PaymentTransactionId);
                parameters.Add("@paymentBankReferenceNumber", model.PaymentBankReferenceNumber);
                parameters.Add("@paymentReference1", model.PaymentReference1);
                parameters.Add("@paymentId", model.Id);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentDetails_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@paymentId", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Recent Transactions by customer
        public async Task<List<RecentTransactionModel>> GetRecentTransactionsByCustomerIdAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_PaymentDetails_GetCustomerRecentTransactions";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<RecentTransactionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
