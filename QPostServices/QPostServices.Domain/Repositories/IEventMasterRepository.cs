﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IEventMasterRepository
    {
        Task<List<EventMasterModel>> GetListAsync();
        Task<EventMasterModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(EventMasterModel model);
        Task<int> UpdateAsync(EventMasterModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
