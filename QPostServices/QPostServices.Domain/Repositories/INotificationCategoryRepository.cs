﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface INotificationCategoryRepository
    {
        Task<int> CreateAsync(NotificationCategoryModel model);
        Task<List<NotificationCategoryModel>> GetListAsync();
    }
}
