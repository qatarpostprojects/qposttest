﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerOrdersDetailsRequest
    {
        public string? SubOrderReference { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public string DeliveryProductCode { get; set; } = string.Empty;

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public string OrderAWBNumber { get; set; } = string.Empty;

        [Required]
        public string TrackingNumber { get; set; } = string.Empty;

        [Required]
        public string PickupSlot { get; set; } = string.Empty;
        [Required]
        public string DeliverySlot { get; set; } = string.Empty;

        [Required]
        public DateTime DeliveryDate { get; set; }

        [Required]
        public int DeliveryTypeId { get; set; }

        [Required]
        public string Source { get; set; } = string.Empty;

        [Required]
        public string OriginCountry { get; set; } = string.Empty;

        [Required]
        public string DestinationCountry { get; set; } = string.Empty;

        [Required]
        public string MailFlow { get; set; } = string.Empty;

        [Required]
        public string MerchantName { get; set; } = string.Empty;

        [Required]
        public string MerchantTrackingNo { get; set; } = string.Empty;

        [Required]
        public decimal TotalWeight { get; set; }

        [Required]
        public decimal VolumetricWeight { get; set; }

        [Required]
        public string ShipmentContent { get; set; } = string.Empty;

        [Required]
        public string BranchId { get; set; } = string.Empty;

        [Required]
        public decimal TotalAmountToPaid { get; set; }

        [Required]
        public decimal TotalAmountToCollect { get; set; }
        public bool CODRequired { get; set; }

        [Required]
        public int Quantity { get; set; }
        public string? HSCode { get; set; }

        [Required]
        public int CurrentStatusCode { get; set; }


        [Required]
        public string CurrentStatusDescription { get; set; } = string.Empty;
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int PaymentId { get; set; }
        public int? WalletTransactionId { get; set; }
        public bool? IsHDS { get; set; }
        public bool? IsHold { get; set; }
        public bool? IsBranch { get; set; }

        public CustomerOrdersDetailsModel ToCreateModel()
        {
            return new CustomerOrdersDetailsModel
            {
                CustomerId = CustomerId,
                DeliveryTypeId = DeliveryTypeId,
                PickupSlot = PickupSlot,
                DeliverySlot = DeliverySlot,
                DeliveryProductCode = DeliveryProductCode,
                DeliveryDate = DeliveryDate,
                CurrentStatusDescription = CurrentStatusDescription,
                CurrentStatusCode = CurrentStatusCode,
                BranchId = BranchId,
                CODRequired = CODRequired,
                DestinationCountry = DestinationCountry,
                HSCode = HSCode,
                MailFlow = MailFlow,
                MerchantName = MerchantName,
                MerchantTrackingNo = MerchantTrackingNo,
                OrderAWBNumber = OrderAWBNumber,
                OrderDate = OrderDate,
                OriginCountry = OriginCountry,
                Quantity = Quantity,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Remarks = Remarks,
                ShipmentContent = ShipmentContent,
                Source = Source,
                SubOrderReference = SubOrderReference,
                TotalAmountToCollect = TotalAmountToCollect,
                TotalAmountToPaid = TotalAmountToPaid,
                TotalWeight = TotalWeight,
                VolumetricWeight = VolumetricWeight,
                TrackingNumber = TrackingNumber,
                PaymentId = PaymentId,
                WalletTransactionId = WalletTransactionId,
                IsBranch = IsBranch,
                IsHDS = IsHDS,
                IsHold = IsHold,
            };
        }

        public CustomerOrdersDetailsModel ToUpdateModel(int id)
        {
            return new CustomerOrdersDetailsModel
            {
                Id = id,
                CustomerId = CustomerId,
                DeliveryTypeId = DeliveryTypeId,
                DeliverySlot = DeliverySlot,
                PickupSlot = PickupSlot,
                DeliveryProductCode = DeliveryProductCode,
                DeliveryDate = DeliveryDate,
                CurrentStatusDescription = CurrentStatusDescription,
                CurrentStatusCode = CurrentStatusCode,
                BranchId = BranchId,
                CODRequired = CODRequired,
                DestinationCountry = DestinationCountry,
                HSCode = HSCode,
                MailFlow = MailFlow,
                MerchantName = MerchantName,
                MerchantTrackingNo = MerchantTrackingNo,
                OrderAWBNumber = OrderAWBNumber,
                OrderDate = OrderDate,
                OriginCountry = OriginCountry,
                Quantity = Quantity,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Remarks = Remarks,
                ShipmentContent = ShipmentContent,
                Source = Source,
                SubOrderReference = SubOrderReference,
                TotalAmountToCollect = TotalAmountToCollect,
                TotalAmountToPaid = TotalAmountToPaid,
                TotalWeight = TotalWeight,
                VolumetricWeight = VolumetricWeight,
                TrackingNumber = TrackingNumber,
                PaymentId = PaymentId,
                WalletTransactionId = WalletTransactionId,
                IsHold = IsHold,
                IsHDS = IsHDS,
                IsBranch = IsBranch,
            };
        }
    }
}
