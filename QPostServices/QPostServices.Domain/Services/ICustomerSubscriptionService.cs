﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerSubscriptionService
    {
        Task<ResponseModel> CreateAsync(CustomerSubscriptionModel model);
        Task<ResponseModel> CreateAddOnAsync(CustomerSubscriptionModel model);
        Task MapExistingCustomer(CustomerSubscriptionModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<IdValueModel>> GetAddOnServicesAsync();
        Task<List<CustomerSubscriptionDetailModel>> GetAllActiveByCustomerIdAsync(int serviceId);
        Task<List<CustomerSubscriptionModel>> GetNearbyExpirySubscriptions();
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(CustomerSubscriptionModel model);
        Task<ResponseModel> RenewAsync(SubscriptionRenewModel model);
        Task<ResponseModel> GetCurrentSubscriptionDetailsForRenew(int serviceId, int customerId);
    }
}
