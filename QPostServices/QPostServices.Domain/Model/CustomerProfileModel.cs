﻿namespace QPostServices.Domain.Model
{
    public class CustomerProfileModel
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? MiddleName { get; set; } 
        public string? LastName { get; set; } 
        public string Name { get; set; } = string.Empty;
        public string EmailAddress { get; set; } = string.Empty;
        public string MobileNumber { get; set; } = default!;
        public OtpModel OTP { get; set; } = new OtpModel();
        public string? FirstNameAr { get; set; }
        public string? MiddleNameAr { get; set; }
        public string? LastNameAr { get; set; }
        public string? NameAr { get; set; } 
    }
}
