﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Services;

namespace QPostServices.Storage
{
    public class PaymentMethodRepository : IPaymentMethodRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;
        private readonly IFileService _fileServices;

        public PaymentMethodRepository(IConfiguration configuration,IFileService fileServices)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
            _fileServices = fileServices;
        }

        #region Create
        public async Task<int> CreateAsync(PaymentMethodModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentMethods_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@image", model.Image);
                parameters.Add("@redirectingURL", model.RedirectingURL);
                parameters.Add("@listingOrder", model.ListingOrder);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@recordStatus", RecordStatus.Active);
                parameters.Add("@nameAr", model.NameAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region Get By Id
        public async Task<PaymentMethodModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentMethods_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<PaymentMethodModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault()??new PaymentMethodModel();
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region Get
        public async Task<List<PaymentMethodModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentMethods_Get]";
                var result = await connection.QueryAsync<PaymentMethodModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(PaymentMethodModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentMethods_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@name", model.Name);
                parameters.Add("@image", model.Image);
                parameters.Add("@redirectionURL", model.RedirectingURL);
                parameters.Add("@listingOrder", model.ListingOrder);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@nameAr", model.NameAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_PaymentMethods_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
