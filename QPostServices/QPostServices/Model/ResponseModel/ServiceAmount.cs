﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class ServiceAmount
    {
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }

        public static ServiceAmount FromModel(ServiceAmountModel model)
        {
            return new ServiceAmount
            {
                SubscriptionAmount = model.SubscriptionAmount,
                RenewAmount = model.RenewAmount,
                CancelAmount = model.CancelAmount,
            };
        }
    }
}
