﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly ILocationService _locationService;

        public LocationController(ILocationService locationService)
        {
            _locationService = locationService;
        }

        #region Get Zones
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
        [HttpGet("GetZones/{zoneNumber}", Name = nameof(GetZones))]
        public async Task<IActionResult> GetZones(int zoneNumber)
        {
            try
            {
                var results = await _locationService.GetZonesAsync(zoneNumber);

                return Ok(results.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Streets
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
        [HttpGet("GetStreets/{zoneNumber}", Name = nameof(GetStreets))]
        public async Task<IActionResult> GetStreets(int zoneNumber)
        {
            try
            {
                var results = await _locationService.GetStreetsAsync(zoneNumber);

                return Ok(results.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Buildings
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
        [HttpGet("GetBuildings/{zoneNumber}/{streetNumber}", Name = nameof(GetBuildings))]
        public async Task<IActionResult> GetBuildings(int zoneNumber, int streetNumber)
        {
            try
            {
                var results = await _locationService.GetBuildingsAsync(zoneNumber, streetNumber);

                return Ok(results.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
        //test
        #region ValidateAddress
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseModel))]
        [HttpPost("ValidateAddress", Name = nameof(ValidateAddress))]
        public async Task<IActionResult> ValidateAddress([FromBody] ValidAddressRequest request)
        {
            try
            {
                var result = await _locationService.ValidateAddressAsync(request.ToModel());
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
