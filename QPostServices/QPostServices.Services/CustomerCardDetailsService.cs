﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class CustomerCardDetailsService : ICustomerCardDetailsService
    {
        private const string _cardDoesNotExist = "Card with id {0} does not exists";
        private const string _successCardCreation = "Card created successfully";
        private const string _failedCardCreation = "Card creation failed";
        private const string _successCardDeleted = "Card with id {0} deleted successfully";
        private const string _successCardUpdate = "Card with id {0} updated successfully";

        private readonly ICustomerCardDetailsRepository _customerCardDetailsRepository;
        public CustomerCardDetailsService(ICustomerCardDetailsRepository customerCardDetailsRepository)
        {
            _customerCardDetailsRepository = customerCardDetailsRepository;
        }
        #region Create Card Details
        public async Task<ResponseModel> CreateCardDetailsAsync(CustomerCardDetailsModel model)
        {
            try
            {
                var response = new ResponseModel();
                var result = await _customerCardDetailsRepository.CreateCardDetailsAsync(model);

                if (result > 0)
                {
                    response.Response = _successCardCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCardCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Card Details
        public async Task<ResponseModel> DeleteCardDetailsAsync(int id)
        {
            try
            {
                var response = new ResponseModel();
                var result = await _customerCardDetailsRepository.UpdateCardStatusAsync(id, false);

                if (result > 0)
                {
                    response.Response = string.Format(_successCardDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_cardDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Card Details By Customer Id
        public async Task<List<CustomerCardDetailsModel>> GetCardDetailsByCustomerId(int customerId)
        {
            return await _customerCardDetailsRepository.GetCardDetailsByCustomerIdAsync(customerId);
        }
        #endregion

        #region Get Card Details By Id
        public async Task<ResponseModel> GetCardDetailsByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _customerCardDetailsRepository.GetCardDetailsByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_cardDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update Card Details
        public async Task<ResponseModel> UpdateCardDetailsAsync(CustomerCardDetailsModel model)
        {
            try
            {
                var response = new ResponseModel();
                var result = await _customerCardDetailsRepository.UpdateCardDetailAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successCardUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_cardDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
