﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IShipmentCostRepository
    {
        Task<List<ShipmentCostDetailsModel>> GetListAsync();
    }
}
