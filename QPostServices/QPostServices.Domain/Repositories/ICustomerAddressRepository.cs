﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerAddressRepository
    {
        Task<int> CreateAsync(CustomerAddressModel model);
        Task<CustomerAddressModel?> GetByIdAsync(int id);
        Task<List<CustomerAddressModel>> GetAddressesListByCustomerId(int customerId);
        Task<CustomerAddressModel?> GetPrimaryAddressByCustomerId(int customerId,AddressType type);
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(CustomerAddressModel model);
    }
}
