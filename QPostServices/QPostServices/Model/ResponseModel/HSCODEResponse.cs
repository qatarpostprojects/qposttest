﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class HSCODEResponse
    {
        public int Id { get; set; }
        public string HSCODE { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string DisplayKeyWord { get; set; } = string.Empty;
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? DescriptionAr { get; set; }
        public string? DisplayKeyWordAr { get; set; }

        public static HSCODEResponse FromModel(HSCODEModel model)
        {
            return new HSCODEResponse()
            {
                Id = model.Id,
                HSCODE = model.HSCODE,
                Description = model.Description,
                Active = model.Active,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CreatedDate = model.CreatedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                DisplayKeyWord = model.DisplayKeyWord,
                DescriptionAr = model.DescriptionAr,
                DisplayKeyWordAr = model.DisplayKeyWordAr
            };
        }
    }
}
