﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ILockTypeServices
    {
        Task<List<LockTypeModel>> GetListAsync();
        Task<List<IdValueModel>> GetLookUpAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(LockTypeModel model);
        Task<ResponseModel> UpdateAsync(LockTypeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
