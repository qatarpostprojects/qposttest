﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class SubscriptionTransaction
    {
        public int Id { get; set; }
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public string TransactionReferenceNo { get; set; } = string.Empty;
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string? PaymentType { get; set; }
        public int? WalletTransID { get; set; }
        public int? CardPaymentID { get; set; }
        public string? Remarks { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int ServiceId { get; set; }
        public string? ServiceName { get; set; }

        public static SubscriptionTransaction FromModel(SubscriptionTransactionModel model)
        {
            return new SubscriptionTransaction
            {
                Id = model.Id,
                SubscriptionId = model.SubscriptionId,
                CardPaymentID = model.CardPaymentId,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CustomerId = model.CustomerId,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                PaymentType = model.PaymentType,
                Remarks = model.Remarks,
                RecordStatus = model.RecordStatus.ToString(),
                TransactionAmount = model.TransactionAmount,
                TransactionDate = model.TransactionDate,
                TransactionReferenceNo = model.TransactionReferenceNumber,
                WalletTransID = model.WalletTransactioId,
                ServiceId= model.ServiceId,
                ServiceName= model.ServiceName
            };
        }
    }
}
