﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerRequest
    {
        [Required]
        public string FirstName { get; set; } = default!;
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }

        [Required]
        public string DocumentType { get; set; } = default!;
        public string? DocumentReference { get; set; }

        [Required]
        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string MobileNumber { get; set; } = default!;

        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string? AlternativeMobileNumber { get; set; }

        [Required]
        public string Nationality { get; set; } = default!;

        [Required]
        public DateTime? DateOfBirth { get; set; } 

        [Required]
        public string PoBoxNumber { get; set; } = default!;

        [Required]
        public string ConnectedVpNumber { get; set; } = default!;
        public string? SmartBoxNumber { get; set; }
        public DateTime? PoBoxExpiry { get; set; }
        public DateTime? SmartBoxExpiry { get; set; }
        public int? WalletId { get; set; }
        public string? WalletReferenceNumber { get; set; }
        public decimal? WalletBalanceAmount { get; set; }
        public string? HDS { get; set; }
        public int? HDSPackageId { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? Reference3 { get; set; }
        [Required]
        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string EmailAddress { get; set; } = string.Empty;
        public string? FirstNameAr { get; set; }
        public string? MiddleNameAr { get; set; }
        public string? LastNameAr { get; set; }

        public CustomerModel ToCreateModel()
        {
            return new CustomerModel
            {
                FirstName = FirstName,
                MiddleName = MiddleName,
                LastName = LastName,
                DocumentType = DocumentType,
                DocumentReference = DocumentReference,
                MobileNumber = MobileNumber,
                AlternativeMobileNumber = AlternativeMobileNumber,
                Nationality = Nationality,
                DateOfBirth = DateOfBirth,
                PoBoxExpiry = PoBoxExpiry,
                PoBoxNumber = PoBoxNumber,
                ConnectedVpNumber = ConnectedVpNumber,
                SmartBoxNumber = SmartBoxNumber,
                SmartBoxExpiry = SmartBoxExpiry,
                WalletId = WalletId,
                WalletBalanceAmount = WalletBalanceAmount,
                HDS = HDS,
                HDSPackageId = HDSPackageId,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Reference3 = Reference3,
                WalletReferenceNumber= WalletReferenceNumber,
                EmailAddress = EmailAddress,
                FirstNameAr = FirstNameAr,
                MiddleNameAr = MiddleNameAr,
                LastNameAr = LastNameAr
            };
        }

        public CustomerModel ToUpdateModel(int id)
        {
            return new CustomerModel
            {
                Id = id,
                FirstName = FirstName,
                MiddleName = MiddleName,
                LastName = LastName,
                DocumentType = DocumentType,
                DocumentReference = DocumentReference,
                MobileNumber = MobileNumber,
                AlternativeMobileNumber = AlternativeMobileNumber,
                Nationality = Nationality,
                DateOfBirth = DateOfBirth,
                PoBoxExpiry = PoBoxExpiry,
                PoBoxNumber = PoBoxNumber,
                ConnectedVpNumber = ConnectedVpNumber,
                SmartBoxNumber = SmartBoxNumber,
                SmartBoxExpiry = SmartBoxExpiry,
                WalletBalanceAmount = WalletBalanceAmount,
                HDS = HDS,
                HDSPackageId = HDSPackageId,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Reference3 = Reference3,
                WalletReferenceNumber = WalletReferenceNumber,
                EmailAddress= EmailAddress,
                FirstNameAr = FirstNameAr,
                MiddleNameAr = MiddleNameAr,
                LastNameAr = LastNameAr
            };
        }
    }


    public class PBXCustomerRequest
    {
        public string CompanyActivity { get; set; } = default!;

        public PBXCompanySector Sector { get; set; } = default!;

        public DateTime DateOfBirth { get; set; }

        public PBXCustomerIdType CustomerIDType { get; set; } = default!;

        public string QatarID { get; set; } = default!;

        public string PassportNo { get; set; } = default!;

        public DateTime PassportExpiryDate { get; set; }

        public string PassportIssueCountry { get; set; } = default!;

        public int AdditionalNumberOfKeys { get; set; }

        public PBXNumberOfEmployees NumOfEmployees { get; set; } = default!;

        public CustomerLanguage CustomerLanguage { get; set; } = default!;

        public string CustomerName { get; set; } = default!;

        public string Address { get; set; } = default!;

        public string BuildingNumber { get; set; } = default!;

        public string ZoneNumber { get; set; } = default!;

        public string StreetNumber { get; set; } = default!;

        public string City { get; set; } = default!;

        public string MobileNumber { get; set; } = default!;

        public string OfficeTelephoneNo { get; set; } = default!;

        public string Email { get; set; } = default!;
        public List<string> SmartCardNumber { get; set; }
        public PBXCustomerModel ToModel()
        {
            return new PBXCustomerModel()
            {
                AdditionalNumberOfKeys = AdditionalNumberOfKeys,
                NumOfEmployees = NumOfEmployees,
                CustomerLanguage = CustomerLanguage,
                CustomerName = CustomerName,
                Address = Address,
                BuildingNumber = BuildingNumber,
                ZoneNumber = ZoneNumber,
                StreetNumber = StreetNumber,
                City = City,
                MobileNumber = MobileNumber,
                CompanyActivity = CompanyActivity,
                CustomerIDType = CustomerIDType,
                DateOfBirth = DateOfBirth,
                Email = Email,
                OfficeTelephoneNo = OfficeTelephoneNo,
                PassportExpiryDate = PassportExpiryDate,
                PassportIssueCountry = PassportIssueCountry,
                PassportNo = PassportNo,
                QatarID = QatarID,
                Sector = Sector,
                SmartCardNumber = SmartCardNumber
            };
        }
    }
}
