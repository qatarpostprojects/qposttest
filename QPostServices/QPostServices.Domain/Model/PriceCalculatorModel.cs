﻿using QPostServices.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Domain.Model
{
    public class PriceCalculatorModel
    {
        [Required]
        public decimal WeightInKg { get; set; }
        [Required]
        public string Destination { get; set; } = string.Empty;
        [Required]
        public string ShipmentType { get; set; } = string.Empty;
    }
}
