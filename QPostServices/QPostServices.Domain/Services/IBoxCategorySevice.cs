﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IBoxCategoryServices
    {
        Task<List<BoxCategoryModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(BoxCategoryModel model);
        Task<ResponseModel> UpdateAsync(BoxCategoryModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
