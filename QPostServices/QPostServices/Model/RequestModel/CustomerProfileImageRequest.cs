﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class CustomerProfileImageRequest
    {
        public int CustomerId { get; set; }
        public IFormFile? Image { get; set; }
        public CustomerProfileImageModel ToModel()
        {
            return new CustomerProfileImageModel { CustomerId = CustomerId, ImageFile = Image, FileType = Image.ContentType };
        }
    }
}
