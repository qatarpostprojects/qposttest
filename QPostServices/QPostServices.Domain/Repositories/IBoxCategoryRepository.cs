﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IBoxCategoryRepository
    {
        Task<List<BoxCategoryModel>> GetListAsync();
        Task<BoxCategoryModel> GetByIdAsync(int id);
        Task<int> CreateAsync(BoxCategoryModel model);
        Task<int> UpdateAsync(BoxCategoryModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
