﻿namespace QPostServices.Domain.Model
{
    public class BranchLocationModel
    {
        public string Latitude { get; set; } = string.Empty;
        public string Longitude { get; set; } = string.Empty;
    }
}
