﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class ProductWeight
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        public bool Active { get; set; }
        public int DeliveryProductId { get; set; }
        public decimal Weight { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public static ProductWeight FromModel(ProductWeightModel model)
        {
            return new ProductWeight()
            {
                Active = model.Active,
                DeliveryProductId = model.DeliveryProductId,
                Weight = model.Weight,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                CreatedDate = model.CreatedDate,
                Description = model.Description,
                Id = model.Id,
                Name = model.Name,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr
            };
        }
    }
}
