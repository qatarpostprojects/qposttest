﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class CustomerNotificationRepository : ICustomerNotificationRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerNotificationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateNotificationAsync(CustomerNotificationModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_CustomerNotifications_Create";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@isRead", false);
                parameters.Add("@message", model.Message);
                parameters.Add("@categoryId", model.CategoryId);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get by filter
        public async Task<List<CustomerNotificationModel>> GetAllNotificationsAsync(CustomerNotificationFetchModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_CustomerNotifications_GetByFilter";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@isRead", model.IsRead);
                parameters.Add("@categoryId", model.CategoryId);
                var result = await connection.QueryAsync<CustomerNotificationModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateReadStatusAsync(int id,bool isRead)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_CustomerNotifications_UpdateReadStatus";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@isRead", isRead);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
