﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/Customer")]
    [ApiController]
    public class CustomerAddressController : ControllerBase
    {
        private readonly ICustomerAddressService _customerAddressService;
        private readonly ICustomerService _customerService;
        public CustomerAddressController(ICustomerAddressService customerAddressService, ICustomerService customerService)
        {
            _customerAddressService = customerAddressService;
            _customerService = customerService;
        }

        #region Create Customer Address
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("Address", Name = nameof(CreateCustomerAddress))]
        public async Task<IActionResult> CreateCustomerAddress([FromBody] CustomerAddressRequest request)
        {
            try
            {
                var result = new ResponseModel();
                if (request.AddressType!=AddressType.Sender|| await _customerService.VerifyOTPAsync(request.OTPRequest.Value, request!.OTPRequest!.OTP))
                {
                    result = await _customerAddressService.CreateAsync(request.ToCreateModel());
                    if (result.Error)
                    {
                        if (result.StatusCode == HttpStatusCode.NotFound)
                        {
                            return NotFound(result.Response);
                        }
                        else
                        {
                            return BadRequest(result.Response);
                        }
                    }
                }
                else
                {
                    result.Response = "Invalid OTP";
                    return BadRequest(result.Response);
                }

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer Address by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerAddress))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("Address/{id}", Name = nameof(GetCustomerAddress))]
        public async Task<IActionResult> GetCustomerAddress(int id)
        {
            try
            {
                var result = await _customerAddressService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(CustomerAddress.FromModel((CustomerAddressModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer Addresses by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerAddress>))]
        [HttpGet("{customerId}/Address", Name = nameof(GetAddressList))]
        public async Task<IActionResult> GetAddressList(int customerId)
        {
            try
            {
                var results = await _customerAddressService.GetAddressesListByCustomerId(customerId);

                return Ok(results.Select(x => CustomerAddress.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Customer address
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("Address/{id}/Delete", Name = nameof(DeleteCustomerAddress))]
        public async Task<IActionResult> DeleteCustomerAddress(int id, [FromBody] OTPRequest request)
        {
            try
            {
                if (request.OTP != null && await _customerService.VerifyOTPAsync(request.Value, request.OTP))
                {
                    var result = await _customerAddressService.DeleteAsync(id);
                    if (result.Error)
                    {
                        return NotFound(result.Response);
                    }
                    return Ok(result.Response);
                }
                return BadRequest(new ResponseModel() { Response = "Invalid OTP" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Customer Address
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("Address/{id}", Name = nameof(UpdateCustomerAddress))]
        public async Task<IActionResult> UpdateCustomerAddress(int id, [FromBody] CustomerAddressRequest request)
        {
            try
            {
                var result = new ResponseModel();
                if (request.AddressType != AddressType.Sender || await _customerService.VerifyOTPAsync(request.OTPRequest.Value, request!.OTPRequest!.OTP))
                {
                    result = await _customerAddressService.UpdateAsync(request.ToUpdateModel(id));
                    if (result.Error)
                    {
                        if (result.StatusCode == HttpStatusCode.NotFound)
                        {
                            return NotFound(result.Response);
                        }
                        else
                        {
                            return BadRequest(result.Response);
                        }
                    }
                }
                else
                {
                    result.Response = "Invalid OTP";
                    return BadRequest(result.Response);
                }

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer default Addresses
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerAddress>))]
        [HttpGet("Type/{type}/Default", Name = nameof(GetPrimaryAddress))]
        public async Task<IActionResult> GetPrimaryAddress(AddressType type)
        {
            try
            {
                var results = await _customerAddressService.GetPrimaryAddressByType(type);

                return Ok(results != null ? CustomerAddress.FromModel(results) : null);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
