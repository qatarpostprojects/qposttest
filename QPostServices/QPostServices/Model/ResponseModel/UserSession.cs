﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class UserSession
    {
        public int Id { get; set; }
        public string UserSessionValue { get; set; } = string.Empty;
        public string RecordStatus { get; set; } = string.Empty;
        public string Prefix { get; set; } = string.Empty;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }

        public static UserSession FromModel(UserSessionModel model)
        {
            return new UserSession
            {
                Id = model.Id,
                RecordStatus = model.RecordStatus.ToString(),
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                Prefix = model.Prefix,
                UserSessionValue = model.UserSessionValue
            };
        }
    }
}
