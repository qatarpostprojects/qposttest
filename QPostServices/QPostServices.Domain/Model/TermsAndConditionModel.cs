﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class TermsAndConditionModel
    {
        public int Id { get; set; }
        public string? Value { get; set; }
        public string? ValueAr { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public string? CreatedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
