﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class DeliveryProduct
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        public string? Image { get; set; }
        public int ListingOrder { get; set; }
        public bool IsOverseas { get; set; }
        public int RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public static DeliveryProduct FromModel(DeliveryProductModel model)
        {
            return new DeliveryProduct()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Image = model.Image,
                ListingOrder = model.ListingOrder,
                IsOverseas = model.IsOverseas,
                RecordStatus = model.RecordStatus,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                CreatedDate = model.CreatedDate,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr
            };
        }
    }
}
