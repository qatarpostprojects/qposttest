﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerCardDetailsRequest
    {
        [Required]
        public string CardNumber { get; set; } = string.Empty;
        [Required]
        public string CardProvider { get; set; } = string.Empty;
        public string CardType { get; set; } = string.Empty;
        [Required]
        public int CustomerId { get; set; }
        public CustomerCardDetailsModel ToCreateModel()
        {
            return new CustomerCardDetailsModel()
            {
                CardNumber = CardNumber,
                CardType = CardType,
                CustomerId = CustomerId,
                CardProvider = CardProvider
            };
        }
        public CustomerCardDetailsModel ToUpdateModel(int id)
        {
            return new CustomerCardDetailsModel()
            {
                CardNumber = CardNumber,
                CardType = CardType,
                CustomerId = CustomerId,
                CardProvider = CardProvider,
                Id= id
            };
        }
    }
}
