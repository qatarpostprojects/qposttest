﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CreateOrderRequest
    {
        [Required]
        public string DeliveryProductCode { get; set; } = string.Empty;
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public string OrderAWBNumber { get; set; } = string.Empty;

        [Required]
        public string PickupSlot { get; set; } = string.Empty;
        [Required]
        public string DeliverySlot { get; set; } = string.Empty;
        [Required]
        public DateTime DeliveryDate { get; set; }
        [Required]
        public int DeliveryTypeId { get; set; }
        [Required]
        public string Source { get; set; } = string.Empty;
        [Required]
        public string OriginCountry { get; set; } = string.Empty;
        [Required]
        public string DestinationCountry { get; set; } = string.Empty;
        [Required]
        public string MailFlow { get; set; } = string.Empty;
        [Required]
        public string MerchantName { get; set; } = string.Empty;
        //[Required]
        //public string MerchantTrackingNo { get; set; } = string.Empty;
        [Required]
        public float TotalWeight { get; set; }
        [Required]
        public float ItemWeight { get; set; }
        [Required]
        public decimal VolumetricWeight { get; set; }
        [Required]
        public string ShipmentContent { get; set; } = string.Empty;
        [Required]
        public int BranchId { get; set; }
        [Required]
        public decimal TotalAmountToPaid { get; set; }
        //[Required]
        //public decimal TotalAmountToCollect { get; set; }
        public bool CODRequired { get; set; }
        [Required]
        public int Quantity { get; set; }
        public string? HSCode { get; set; }
        //[Required]
        //public int CurrentStatusCode { get; set; }

        //[Required]
        //public string CurrentStatusDescription { get; set; } = string.Empty;
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public DateTime PickupDate { get; set; }
        public string? ShipmentType { get; set; }
        public string? DeliveryMode { get; set; }
        public string? PickupMode { get; set; }
        public string? ProductServiceCategory { get; set; }
        public string? TransactionDate { get; set; }
        public string? PaymentType { get; set; }
        public string? PaymentCurrency { get; set; }
        public int PaymentId { get; set; }
        public int? WalletTransactionId { get; set; }
        public int? ItemValue { get; set; }
        public string? ItemDescription { get; set; }
        public CreateOrderAddressRequest SenderOrderAddress { get; set; } = new CreateOrderAddressRequest();
        public CreateOrderAddressRequest RecieverOrderAddress { get; set; } = new CreateOrderAddressRequest();
        public CreateOrderAddressRequest PickUpOrderAddress { get; set; } = new CreateOrderAddressRequest();



        public CreateOrderModel ToModel()
        {
            return new CreateOrderModel
            {
                DeliveryTypeId = DeliveryTypeId,
                DeliverySlot = DeliverySlot,
                PickupSlot = PickupSlot,
                DeliveryProductCode = DeliveryProductCode,
                DeliveryDate = DeliveryDate,
                CurrentStatusDescription = EventStatus.OrderPlaced.ToString(),
                CurrentStatusCode = (int)EventStatus.OrderPlaced,
                BranchId = BranchId,
                CODRequired = CODRequired,
                DestinationCountry = DestinationCountry,
                HSCode = HSCode,
                MailFlow = MailFlow,
                MerchantName = MerchantName,
                //MerchantTrackingNo = MerchantTrackingNo,
                OrderAWBNumber = OrderAWBNumber,
                OrderDate = OrderDate,
                OriginCountry = OriginCountry,
                Quantity = Quantity,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Remarks = Remarks,
                ShipmentContent = ShipmentContent,
                Source = Source,
                TotalAmountToCollect = CODRequired ? TotalAmountToPaid : 0,
                TotalAmountToPaid = TotalAmountToPaid,
                TotalWeight = TotalWeight,
                ItemWeight = ItemWeight,
                VolumetricWeight = VolumetricWeight,
                deliveryMode = DeliveryMode,
                paymentCurrency = PaymentCurrency,
                PaymentType = PaymentType,
                PickupDate = PickupDate,
                ProductServiceCategory = ProductServiceCategory,
                shipmentType = ShipmentType,
                PickupMode = PickupMode,
                transactionDate = TransactionDate,
                RecieverOrderAddress = RecieverOrderAddress.ToModel(),
                SenderOrderAddress = SenderOrderAddress.ToModel(),
                PickUpOrderAddress = PickUpOrderAddress.ToModel(),
                IsBranch = false,
                IsHDS = false,
                IsHold = false,
                PaymentId = PaymentId,
                WalletTransactionId = WalletTransactionId,
                ItemValue = ItemValue,
                ItemDescription = ItemDescription
            };
        }
    }

    public class CreateOrderAddressRequest
    {
        public string TrackingNumber { get; set; } = string.Empty;
        public string CustomerType { get; set; } = string.Empty;
        public string CustomerName { get; set; } = string.Empty;
        public string CustomerMobileNumber { get; set; } = string.Empty;
        //[ConditionalRequired(nameof(isoCountryCode), "QA", true, ErrorMessage = "Required if Qatar is selected")]
        public string? CustomerAddressZone { get; set; }
        //[ConditionalRequired(nameof(isoCountryCode), "QA", true, ErrorMessage = "Required if Qatar is selected")]
        public string? CustomerAddressStreet { get; set; }
        //[ConditionalRequired(nameof(isoCountryCode), "QA", true, ErrorMessage = "Required if Qatar is selected")]
        public string? CustomerAddressBuilding { get; set; }
        //[ConditionalRequired(nameof(isoCountryCode), "QA", true, ErrorMessage = "Required if Qatar is selected")]
        public string? CustomerAddressUnit { get; set; }
        public string? CustomerLocationDetails { get; set; }
        public string? CustomerPOBOXNumber { get; set; }
        public string CustomerPostCode { get; set; } = string.Empty;
        public string CustomerEmail { get; set; } = string.Empty;
        public string CustomerIdentificationNumber { get; set; } = string.Empty;
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? ArabicName { get; set; }
        public string? isoCountryCode { get; set; }
        public string? streetType { get; set; }
        public string? floor { get; set; }
        public string? City { get; set; }
        //[ConditionalRequired(nameof(isoCountryCode), "QA", false, ErrorMessage = "Required if Qatar is not selected")]
        public string? CustomerAddress1 { get; set; }
        //[ConditionalRequired(nameof(isoCountryCode), "QA", false, ErrorMessage = "Required if Qatar is not selected")]
        public string? CustomerAddress2 { get; set; }
        //[ConditionalRequired(nameof(isoCountryCode), "QA", false, ErrorMessage = "Required if Qatar is not selected")]
        public string? CustomerZipCode { get; set; }

        public CreateOrderAddressModel ToModel()
        {
            return new CreateOrderAddressModel
            {
                ArabicName = ArabicName,
                City = City,
                CustomerAddressBuilding = CustomerAddressBuilding,
                CustomerAddressStreet = CustomerAddressStreet,
                CustomerAddressUnit = CustomerAddressUnit,
                CustomerAddressZone = CustomerAddressZone,
                CustomerIdentificationNumber = CustomerIdentificationNumber,
                CustomerLocationDetails = CustomerLocationDetails,
                CustomerEmail = CustomerEmail,
                CustomerMobileNumber = CustomerMobileNumber,
                CustomerPOBOXNumber = CustomerPOBOXNumber,
                CustomerPostCode = CustomerPostCode,
                CustomerName = CustomerName,
                CustomerType = CustomerType,
                floor = floor,
                IsoCountryCode = isoCountryCode,
                streetType = streetType,
                Reference1 = Reference1,
                Reference2 = Reference2,
                TrackingNumber = TrackingNumber,
                CustomerAddress1 = CustomerAddress1,
                CustomerAddress2 = CustomerAddress2,
                CustomerZipCode = CustomerZipCode
            };
        }
    }
}
