﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IWalletRepository
    {
        Task<int> CreateAsync(WalletModel model);
        Task<WalletModel?> GetByIdAsync(int id);
        Task<List<WalletModel>> GetAllActiveAsync();
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(WalletModel model);
        Task<int> DepositAsync(WalletTransactionModel model);
        Task<int> PaymentAsync(WalletTransactionModel model);
    }
}
