﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class PickupType
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Logo { get; set; }
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static PickupType FromModel(PickupTypeModel model)
        {
            return new PickupType 
            { 
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Logo = model.Logo,
                Active = model.Active,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                CreatedDate = model.CreatedDate,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr,
            };
        }
    }
}
