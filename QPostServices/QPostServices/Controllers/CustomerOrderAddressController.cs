﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using QPostServices.Services;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerOrderAddressController:ControllerBase
    {
        private readonly ICustomerOrderAddressService _customerOrderAddressService;
        public CustomerOrderAddressController(ICustomerOrderAddressService customerOrderAddressService)
        {
            _customerOrderAddressService = customerOrderAddressService;
        }
        #region Create
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateOrderAddress))]
        public async Task<IActionResult> CreateOrderAddress([FromBody] CustomerOrderAddressRequest request)
        {
            try
            {
                var result = await _customerOrderAddressService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Order address by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Service))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetOrderAddress))]
        public async Task<IActionResult> GetOrderAddress(int id)
        {
            try
            {
                var result = await _customerOrderAddressService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(CustomerOrderAddress.FromModel((CustomerOrderAddressModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
        
        #region Get Order address by order id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Service))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("order/{orderId}", Name = nameof(GetOrderAddressByOrderId))]
        public async Task<IActionResult> GetOrderAddressByOrderId(int orderId)
        {
            try
            {
                var results = await _customerOrderAddressService.GetByOrderIdAsync(orderId);
                return Ok(results.Select(x => CustomerOrderAddress.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get All Order Addresses
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Service>))]
        [HttpGet(Name = nameof(GetOrderAddressList))]
        public async Task<IActionResult> GetOrderAddressList()
        {
            try
            {
                var results = await _customerOrderAddressService.GetAllActiveAsync();

                return Ok(results.Select(x => CustomerOrderAddress.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion    
       
        #region Delete Address
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteAddress))]
        public async Task<IActionResult> DeleteAddress(int id)
        {
            try
            {
                var result = await _customerOrderAddressService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Address
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateAddress))]
        public async Task<IActionResult> UpdateAddress(int id, [FromBody] CustomerOrderAddressRequest request)
        {
            try
            {
                var result = await _customerOrderAddressService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
