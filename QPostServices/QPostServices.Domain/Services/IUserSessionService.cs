﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IUserSessionService
    {
        Task<ResponseModel> CreateAsync(UserSessionModel model);
        Task<ResponseModel> UpdateStatusAsync(string sessionValue, UserSessionStatus status);
        Task<ResponseModel> GetBySessionValueAsync(string sessionValue);
    }
}
