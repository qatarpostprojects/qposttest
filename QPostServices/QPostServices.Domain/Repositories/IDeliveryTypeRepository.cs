﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IDeliveryTypeRepository
    {
        Task<List<DeliveryTypeModel>> GetListAsync();
        Task<List<DeliveryTypeModel>> GetByProductIdAsync(int productId);
        Task<DeliveryTypeModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(DeliveryTypeModel model);
        Task<int> UpdateAsync(DeliveryTypeModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
