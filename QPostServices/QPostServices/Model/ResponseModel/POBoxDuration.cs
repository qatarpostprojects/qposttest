﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class POBoxDuration
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Logo { get; set; }
        public bool Active { get; set; }
        public int MonthValue { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static POBoxDuration FromModel(POBoxDurationModel model)
        {
            return new POBoxDuration() 
            { 
                Active = model.Active,
                MonthValue = model.MonthValue,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                CreatedDate = model.CreatedDate,
                Description = model.Description,
                Id = model.Id,
                Logo = model.Logo,
                Name = model.Name,
                ServiceId = model.ServiceId,
                ServiceName = model.ServiceName,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr,
            };
        }
    }
}
