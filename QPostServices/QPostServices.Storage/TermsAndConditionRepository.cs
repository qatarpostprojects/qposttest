﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

namespace QPostServices.Storage
{
    public class TermsAndConditionRepository : ITermsAndConditionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public TermsAndConditionRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(TermsAndConditionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_TermsAndCondition_Create]";
                var parameters = new DynamicParameters();               
                parameters.Add("@value", model.Value);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedBy);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Value
        public async Task<string> GetAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_TermsAndCondition_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<string>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault()??"";
            }
            catch
            {
                throw;
            }
        }
        #endregion
        
        #region Get all details
        public async Task<TermsAndConditionModel> GetDetailsAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_TermsAndCondition_GetDetails]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<TermsAndConditionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault()??new TermsAndConditionModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(TermsAndConditionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_TermsAndCondition_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@value", model.Value);
                parameters.Add("@valueAr", model.ValueAr);
                parameters.Add("@modifiedUser", model.ModifiedBy);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update status
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_TermsAndCondition_UpdateStatus";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
