﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IBoxTypeServices
    {
        Task<List<BoxTypeModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(BoxTypeModel model);
        Task<ResponseModel> UpdateAsync(BoxTypeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
