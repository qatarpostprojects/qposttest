﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICountryRepository
    {
        Task<List<CountryModel>> GetListAsync(string? searchString);
        Task<CountryModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(CountryModel model);
        Task<int> UpdateAsync(CountryModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
