﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IProductWeightService
    {
        Task<List<ProductWeightModel>> GetListAsync();
        Task<List<ProductWeightModel>> GetByDeliveryProductId(int deliveryProductId);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(ProductWeightModel model);
        Task<ResponseModel> UpdateAsync(ProductWeightModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
