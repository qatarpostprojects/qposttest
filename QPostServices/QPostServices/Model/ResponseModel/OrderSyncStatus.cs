﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class OrderSyncStatus
    {
        public int Id { get; set; }
        public string TrackingNumber { get; set; } = string.Empty;
        public DateTime SyncDate { get; set; }
        public string SyncStatus { get; set; } = string.Empty;
        public string? ErrorDescription { get; set; }
        public string? Remarks { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;

        public static OrderSyncStatus FromModel(OrderSyncStatusModel model)
        {
            return new OrderSyncStatus
            {
                Id = model.Id,
                TrackingNumber = model.TrackingNumber,
                SyncDate = model.SyncDate,
                SyncStatus = model.SyncStatus,
                ErrorDescription = model.ErrorDescription,
                Remarks = model.Remarks,
                RecordStatus = model.RecordStatus.ToString(),
                CreatedDate = model.CreatedDate,
                CreatedUserId = model.CreatedUserId,
                CreatedUserName = model.CreatedUserName
            };
        }
    }
}
