﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class BannerService : IBannerService
    {
        private const string _successCreation = "Banner created successfully";
        private const string _successUpdate = "Banner updated successfully";
        private const string _failedCreation = "Banner creation failed";
        private const string _failedUpdation = "Banner updation failed";
        private const string _successDelete = "Banner deleted successfully";
        private const string _failedDelete = "Banner deletion failed";
        private const string _DoesNotExist = "Banner with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly IBannerRepository _bannerRepository;
        private readonly IFileService _fileService;
        public BannerService(IBannerRepository bannerRepository, ILoginService loginService, IFileService fileService)
        {
            _bannerRepository = bannerRepository;
            _loginService = loginService;
            _fileService = fileService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(BannerModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();
            if(model.BannerArFile != null)
            {
                model.BannerAr = await _fileService.ConvertImageToBase64(model.BannerArFile);
            }
            if(model.BannerEngFile != null)
            {
                model.BannerEng = await _fileService.ConvertImageToBase64(model.BannerEngFile);
            }
            var result = await _bannerRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _bannerRepository.UpdateStatusAsync(id, false, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Device
        public async Task<List<BannerModel>> GetByDevice(string device)
        {
            return await _bannerRepository.GetByDevice(device);
        } 
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _bannerRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All
        public async Task<List<BannerModel>> GetListAsync()
        {
            return await _bannerRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(BannerModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();
            if (model.BannerArFile != null)
            {
                model.BannerAr = await _fileService.ConvertImageToBase64(model.BannerArFile);
            }
            if (model.BannerEngFile != null)
            {
                model.BannerEng = await _fileService.ConvertImageToBase64(model.BannerEngFile);
            }
            var result = await _bannerRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
