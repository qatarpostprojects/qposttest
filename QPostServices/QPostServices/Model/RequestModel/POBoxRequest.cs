﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class POBoxRequest
    {
        public string POBoxNumber { get; set; } = string.Empty;
        public int POBoxTypeId { get; set; } 
        public string POBoxSubType { get; set; } = string.Empty;
        public int BranchId { get; set; }
        public int Rented { get; set; }
        public string? POBoxLocationId { get; set; }
        public string? POBoxLocationName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime PaidUntil { get; set; }
        public DateTime LastPaidUntil { get; set; }
        public string? GeoLocationX { get; set; }
        public string? GeoLocationY { get; set; }
        public string? SmartBoxNumber { get; set; }
        public int POBoxCategoryID { get; set; }
        public int IsSmartBox { get; set; }
        public int POBoxPositionID { get; set; }
        public int POBoxLockTypeID { get; set; }
        public string? SmartCardNumber { get; set; }
        public string? Location { get; set; }
        public string? Reserved { get; set; }
        public bool Blocked { get; set; }
        public string? Remarks { get; set; }
        public string? GroupId { get; set; }
        public POBoxModel ToModel()
        {
            return new POBoxModel()
            {
                Blocked = Blocked,
                Remarks = Remarks,
                GroupId = GroupId,
                BranchId = BranchId,
                GeoLocationX = GeoLocationX,
                GeoLocationY = GeoLocationY,
                SmartBoxNumber = SmartBoxNumber,
                POBoxCategoryID = POBoxCategoryID,
                IsSmartBox = IsSmartBox,
                POBoxPositionID = POBoxPositionID,
                POBoxLockTypeID = POBoxLockTypeID,
                LastPaidUntil = LastPaidUntil,
                Location = Location,
                Reserved = Reserved,
                PaidUntil = PaidUntil,
                POBoxLocationId = POBoxLocationId,
                POBoxLocationName = POBoxLocationName,
                POBoxNumber = POBoxNumber,
                POBoxSubType = POBoxSubType,
                POBoxTypeId = POBoxTypeId,
                Rented = Rented,
                SmartCardNumber = SmartCardNumber,
                StartDate = StartDate
            };
        }
        public POBoxModel ToUpdateModel(int id)
        {
            return new POBoxModel
            {
                Id = id,
                Blocked = Blocked,
                Remarks = Remarks,
                GroupId = GroupId,
                BranchId = BranchId,
                GeoLocationX = GeoLocationX,
                GeoLocationY = GeoLocationY,
                SmartBoxNumber = SmartBoxNumber,
                POBoxCategoryID = POBoxCategoryID,
                IsSmartBox = IsSmartBox,
                POBoxPositionID = POBoxPositionID,
                POBoxLockTypeID = POBoxLockTypeID,
                LastPaidUntil = LastPaidUntil,
                Location = Location,
                Reserved = Reserved,
                PaidUntil = PaidUntil,
                POBoxLocationId = POBoxLocationId,
                POBoxLocationName = POBoxLocationName,
                POBoxNumber = POBoxNumber,
                POBoxSubType = POBoxSubType,
                POBoxTypeId = POBoxTypeId,
                Rented = Rented,
                SmartCardNumber = SmartCardNumber,
                StartDate = StartDate
            };
        }
    }
}
