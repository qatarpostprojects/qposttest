﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerNotification
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string? TrackingNumber { get; set; }
        public string? MobileNumber { get; set; }
        public string? Message { get; set; }
        public bool IsRead { get; set; }
        public int CategoryId { get; set; }
        public string? CategoryName { get; set; }
        public int RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public static CustomerNotification FromModel(CustomerNotificationModel model)
        {
            return new CustomerNotification()
            {
                Id = model.Id,
                CustomerId = model.CustomerId,
                TrackingNumber = model.TrackingNumber,
                MobileNumber = model.MobileNumber,
                Message = model.Message,
                IsRead = model.IsRead,
                CategoryId = model.CategoryId,
                CategoryName = model.CategoryName,
                RecordStatus = model.RecordStatus,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                CreatedDate = model.CreatedDate,
                ModifiedDate = model.ModifiedDate,
            };
        }
    }
}
