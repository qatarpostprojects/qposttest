﻿using QPostPBXService;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Helper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Security;

namespace QPostServices.Services
{
    public class POBoxService : IPOBoxService
    {
        private const string _successCreation = "POBox created successfully";
        private const string _successUpdate = "POBox updated successfully";
        private const string _failedCreation = "POBox creation failed";
        private const string _successDelete = "POBox deleted successfully";
        private const string _nameAlreadyExist = "POBox with name : {0} already exist";
        private const string _POBoxDoesNotExist = "POBox with id : {0} Does not exist";

        private readonly IPOBoxRepository _poBoxRepository;
        private readonly ILoginService _loginService;
        private readonly PostBoxServiceSoapClient _serviceClient;
        private readonly AppSettings _appSettings;
        private readonly UserDetails _pbxUser;

        public POBoxService(IPOBoxRepository pOBoxRepository, ILoginService loginService, AppSettings appSettings)
        {
            _appSettings = appSettings;
            _serviceClient = GetServiceClient(GetServiceURL());
            _poBoxRepository = pOBoxRepository;
            _loginService = loginService;
            _pbxUser = new UserDetails()
            {
                AgentId = _appSettings.PBXAgentId,
                Password = _appSettings.PBXPassword,
                UserName = _appSettings.PBXUsername
            };
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(POBoxModel model)
        {
            model.CreatedUserId = _loginService.LoggedInUser();
            var response = new ResponseModel();

            var result = await _poBoxRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.POBoxNumber);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }


        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _poBoxRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get
        public async Task<List<POBoxModel>> GetListAsync()
        {
            return await _poBoxRepository.GetListAsync();
        }
        #endregion

        #region Get all landing page service
        public async Task<List<POBoxServiceModel>> GetAllLandingPageServicesAsync()
        {
            return await _poBoxRepository.GetAllLandingPageServicesAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(POBoxModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUserId = _loginService.LoggedInUser();

            var result = await _poBoxRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.POBoxNumber);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _poBoxRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }


        #endregion

        #region Get POBox Enqiry
        public async Task<PBXResponseModel> GetPOBoxEnqiryAsync(POBoxEnquiryModel model)
        {
            try
            {
                var result = await _serviceClient.POBoxEnquiryAsync(_pbxUser, model.POBoxNo, model.QatarID, model.PassportNo, model.CompanyRegistrationNo);

                return new PBXResponseModel
                {
                    Entry = result.POBoxEnquiryResult.Entry,
                    ErrorCode = result.POBoxEnquiryResult.ErrorCode,
                    Message = result.POBoxEnquiryResult.Message,
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Add OnServices
        public async Task<PBXResponseModel> GetAddOnServicesAsync(string pOBoxNo, PBXAddOnPeriod addOnPeriod)
        {
            try
            {
                var result = await _serviceClient.GetAddOnServicesAsync(_pbxUser, pOBoxNo, (AddOnPeriod)addOnPeriod);
                return new PBXResponseModel
                {
                    Entry = result.GetAddOnServicesResult.Entry,
                    ErrorCode = result.GetAddOnServicesResult.ErrorCode,
                    Message = result.GetAddOnServicesResult.Message,
                };
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Renew PostBox
        public async Task<PBXResponseModel> RenewPostBoxAsync(string POBoxNo, POBoxRenewalPeriod Period)
        {
            var result = await _serviceClient.RenewPostBoxAsync(_pbxUser, POBoxNo, (RenewPeriod)Period);
            return new PBXResponseModel
            {
                Entry = result.RenewPostBoxResult.Entry,
                ErrorCode = result.RenewPostBoxResult.ErrorCode,
                Message = result.RenewPostBoxResult.Message,
            };
        }
        #endregion

        #region Update Address
        public async Task<PBXResponseModel> UpdateAddressAsync(PBXAddressModel model, string poBoxNumber)
        {
            try
            {
                var result = await _serviceClient.UpdateAddressAsync(_pbxUser, poBoxNumber, model.ToModel());
                return new PBXResponseModel
                {
                    Entry = result.UpdateAddressResult.Entry,
                    ErrorCode = result.UpdateAddressResult.ErrorCode,
                    Message = result.UpdateAddressResult.Message,
                };
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Get Available POBoxes
        public async Task<PBXResponseModel> GetAvailablePOBoxesAsync(string pOBoxNo, string costCenter)
        {
            try
            {
                var result = await _serviceClient.GetAvailablePOBoxesAsync(_pbxUser, pOBoxNo, costCenter);
                return new PBXResponseModel
                {
                    Entry = result.GetAvailablePOBoxesResult.Entry,
                    ErrorCode = result.GetAvailablePOBoxesResult.ErrorCode,
                    Message = result.GetAvailablePOBoxesResult.Message,
                };
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Add AddOnServices
        public async Task<PBXResponseModel> AddAddOnServicesAsync(string pOBoxNo, PBXAddOnPeriod addOnPeriod, PBXAddOnServiceType addOnServiceType)
        {
            try
            {
                var result = await _serviceClient.AddAddonServiceAsync(_pbxUser, pOBoxNo, (AddOnPeriod)addOnPeriod, (AddOnServiceType)addOnServiceType);
                return new PBXResponseModel
                {
                    Entry = result.AddAddonServiceResult.Entry,
                    ErrorCode = result.AddAddonServiceResult.ErrorCode,
                    Message = result.AddAddonServiceResult.Message,
                };
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Subscribe POBox
        public async Task<PBXResponseModel> SubscribePOBoxAsync(PBXSubscribeModel subscribeModel)
        {
            try
            {
                var result = await _serviceClient.SubscribePOBoxAsync(_pbxUser, subscribeModel.ToModel());
                return new PBXResponseModel
                {
                    Entry = result.SubscribePOBoxResult.Entry,
                    ErrorCode = result.SubscribePOBoxResult.ErrorCode,
                    Message = result.SubscribePOBoxResult.Message,
                };
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Hold POBox
        public async Task<PBXResponseModel> HoldPOBoxAsync(string poBoxNo, string CostCenter)
        {
            try
            {
                var result = await _serviceClient.HoldPOBoxAsync(_pbxUser, poBoxNo, CostCenter);
                return new PBXResponseModel
                {
                    Entry = result.HoldPOBoxResult.Entry,
                    ErrorCode = result.HoldPOBoxResult.ErrorCode,
                    Message = result.HoldPOBoxResult.Message,
                };
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region privet method for get soap service connection
        private string GetServiceURL()
        {
            string value = "";
            try
            {
                value = _appSettings.SoapServiceUrl;// app.AppSetting["EPOSS_Settings:EPossURL"];
            }
            catch
            {



            }
            return value;
        }
        private static PostBoxServiceSoapClient GetServiceClient(string _endpointAddress)
        {
            string svcURL = _endpointAddress;// GetServiceURL();
            PostBoxServiceSoapClient? clt = null;
            if (!string.IsNullOrEmpty(svcURL) && svcURL.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                var binding = new BasicHttpsBinding();
                var endpoint = new EndpointAddress(new Uri(svcURL));
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                binding.SendTimeout = new TimeSpan(0, 3, 0);
                clt = new PostBoxServiceSoapClient(binding, endpoint);
                clt.ClientCredentials.ServiceCertificate.SslCertificateAuthentication =
                    new X509ServiceCertificateAuthentication()
                    {
                        CertificateValidationMode = X509CertificateValidationMode.None,
                        RevocationMode = X509RevocationMode.NoCheck
                    };
            }
            else if (!string.IsNullOrEmpty(svcURL) && svcURL.StartsWith("http://", StringComparison.OrdinalIgnoreCase))
            {
                var binding = new BasicHttpBinding();
                var endpoint = new EndpointAddress(new Uri(svcURL));
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                binding.SendTimeout = new TimeSpan(0, 3, 0);
                clt = new PostBoxServiceSoapClient(binding, endpoint);
            }
            return clt;
        }
        #endregion
    }
}
