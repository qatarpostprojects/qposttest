﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ILockTypeRepository
    {
        Task<List<LockTypeModel>> GetListAsync();
        Task<List<IdValueModel>> GetLookUpAsync();
        Task<LockTypeModel> GetByIdAsync(int id);
        Task<int> CreateAsync(LockTypeModel model);
        Task<int> UpdateAsync(LockTypeModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status,int userId);
    }
}
