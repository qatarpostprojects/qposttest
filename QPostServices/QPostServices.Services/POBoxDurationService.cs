﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class POBoxDurationService : IPOBoxDurationService
    {
        private const string _successCreation = "POBox Duration created successfully";
        private const string _successUpdate = "POBox Duration updated successfully";
        private const string _failedCreation = "POBox Duration creation failed";
        private const string _failedUpdation = "POBox Duration updation failed";
        private const string _successDelete = "POBox Duration deleted successfully";
        private const string _failedDelete = "POBox Duration deletion failed";
        private const string _POBoxDurationDoesNotExist = "POBox Duration with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly IPOBoxDurationRepository _poBoxDurationRepository;
        public POBoxDurationService(IPOBoxDurationRepository pOBoxDurationRepository, ILoginService loginService)
        {
            _poBoxDurationRepository = pOBoxDurationRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(POBoxDurationModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();

            var result = await _poBoxDurationRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _poBoxDurationRepository.UpdateStatusAsync(id, false, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxDurationDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _poBoxDurationRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxDurationDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All by service id
        public async Task<List<POBoxDurationModel>> GetListByServiceIdAsync(int serviceId)
        {
            return await _poBoxDurationRepository.GetListByServiceIdAsync(serviceId);
        }
        #endregion

        #region Get All
        public async Task<List<POBoxDurationModel>> GetListAsync()
        {
            return await _poBoxDurationRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(POBoxDurationModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();

            var result = await _poBoxDurationRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxDurationDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
