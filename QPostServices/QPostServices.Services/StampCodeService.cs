﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Services
{
    public class StampCodeService : IStampCodeService
    {
        private readonly ILoginService _loginService;
        private readonly IStampCodeRepository _stampCodeRepository;
        public StampCodeService(IStampCodeRepository stampCodeRepository, ILoginService loginService)
        {
            _stampCodeRepository = stampCodeRepository;
            _loginService = loginService;
        }
        public async Task<ResponseModel> CreateAsync(StampCodeModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _stampCodeRepository.CreateStampCodeAsync(model);

            if (!string.IsNullOrEmpty(result))
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
           
            else
            {
                response.Error = true;
                response.Response = "Failed create stamp code";
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }

        public async Task<ResponseModel> GetByOrderIdAsync(int orderId)
        {
            var response = new ResponseModel();

            var result = await _stampCodeRepository.GetByOrderId(orderId);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = "Stamp Code does not exist";
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
    }
}
