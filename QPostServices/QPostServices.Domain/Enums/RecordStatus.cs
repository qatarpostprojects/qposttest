﻿namespace QPostServices.Domain.Enums
{
    public enum RecordStatus
    {
        Inactive = 0,
        Active = 1
    }
}
