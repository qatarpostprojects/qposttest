﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IWalletTransactionDetailsService
    {
        Task<ResponseModel> CreateAsync(WalletTransactionDetailsModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<WalletTransactionDetailsModel>> GetListByCustomerId(int customerId);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(WalletTransactionDetailsModel model);
        Task<List<WalletTransactionDetailsModel>> GetRecentTransactionsAsync(int customerId);
    }
}
