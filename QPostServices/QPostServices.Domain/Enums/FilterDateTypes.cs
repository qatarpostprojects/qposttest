﻿namespace QPostServices.Domain.Enums
{
    public enum FilterDateTypes
    {
        Recent = 1,
        CurrentMonth = 2,
        LastMonth = 3,
        Last3Months = 4,
        Last6Months = 5
    }
}
