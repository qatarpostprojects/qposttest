﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class WalletTransactionSummaryRepository : IWalletTransactionSummaryRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public WalletTransactionSummaryRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create wallet transaction summary
        public async Task<int> CreateAsync(WalletTransactionSummaryModel model, TransactionType transactionType)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@transactionType", (int)transactionType);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@walletAmount", model.Amount);
                parameters.Add("@lastTransactionId", model.LastTransactionId);
                parameters.Add("@lastTransactionDate", DateTime.Now);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@walletReferenceNumber", $"{model.WalletId}{model.CustomerId}{GenerateRandomNo()}");
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get wallet transaction summary by Customer Id
        public async Task<List<WalletTransactionSummaryModel>> GetListByCustomerId(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<WalletTransactionSummaryModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Wallet Balance Amount
        public async Task<decimal> GetWalletBalanceAmountAsync(int customerId, int walletId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_GetWalletBalanceAmount]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                parameters.Add("@walletId", walletId);
                var result = await connection.QueryAsync<decimal>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get  wallet transaction summary
        public async Task<WalletTransactionSummaryModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<WalletTransactionSummaryModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get by wallet id
        public async Task<List<WalletTransactionSummaryModel>> GetByWalletIdAsync(int walletId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_GetByWalletId]";
                var parameters = new DynamicParameters();
                parameters.Add("@walletId", walletId);
                var result = await connection.QueryAsync<WalletTransactionSummaryModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
            #endregion
        }

        #region Update  wallet transaction summary
        public async Task<int> UpdateAsync(WalletTransactionSummaryModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@walletAmount", model.Amount);
                parameters.Add("@lastTransactionId", model.LastTransactionId);
                parameters.Add("@lastTransactionDate", model.LastTransactionDate);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update  wallet transaction summary status
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionSummary_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Generate 4 digit random number
        private static int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new();
            return _rdm.Next(_min, _max);
        }
        #endregion
    }
}
