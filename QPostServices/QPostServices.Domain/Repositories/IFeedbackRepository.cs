﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IFeedbackRepository
    {
        Task<int> AddAsync(FeedbackModel model);
        Task<FeedbackModel?> GetByCustomerIdAsync(int customerId);
        Task<List<FeedbackModel>> GetAsync();
    }
}
