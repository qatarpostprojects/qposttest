﻿using Microsoft.AspNetCore.Http;

namespace QPostServices.Domain.Model
{
    public class CustomerProfileImageModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public IFormFile? ImageFile { get; set; }
        public string? Image { get; set; }
        public string FileType { get; set; } = string.Empty;
    }
}
