﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CreateOrderModel
    {
        public int Id { get; set; }
        public string? SubOrderReference { get; set; }
        public int CustomerId { get; set; }
        public string DeliveryProductCode { get; set; } = string.Empty;
        public DateTime OrderDate { get; set; }
        public string OrderAWBNumber { get; set; } = string.Empty;
        public string TrackingNumber { get; set; } = string.Empty;
        public string DeliverySlot { get; set; } = string.Empty;
        public string PickupSlot { get; set; } = string.Empty;
        public DateTime DeliveryDate { get; set; }
        public int DeliveryTypeId { get; set; }
        public string Source { get; set; } = string.Empty;
        public string OriginCountry { get; set; } = string.Empty;
        public string DestinationCountry { get; set; } = string.Empty;
        public string MailFlow { get; set; } = string.Empty;
        public string MerchantName { get; set; } = string.Empty;
        public string MerchantTrackingNo { get; set; } = string.Empty;
        public float TotalWeight { get; set; }
        public float ItemWeight { get; set; }
        public decimal VolumetricWeight { get; set; }
        public string ShipmentContent { get; set; } = string.Empty;
        public int BranchId { get; set; }
        public decimal TotalAmountToPaid { get; set; }
        public decimal TotalAmountToCollect { get; set; }
        public bool CODRequired { get; set; }
        public int Quantity { get; set; }
        public string? HSCode { get; set; }
        public int CurrentStatusCode { get; set; } 
        public string CurrentStatusDescription { get; set; } = string.Empty;
        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime PickupDate { get; set; }
        public string? shipmentType { get; set; }
        public string? deliveryMode { get; set; }
        public string? ProductServiceCategory { get; set; }
        public string? PickupMode { get; set; }
        public string? transactionDate { get; set; }
        public string? PaymentType { get; set; }
        public string? paymentCurrency { get; set; }
        public bool? IsHDS { get; set; }
        public bool? IsHold { get; set; }
        public bool? IsBranch { get; set; }
        public int PaymentId { get; set; }
        public int? WalletTransactionId { get; set; }
        public int? ItemValue { get; set; }
        public string? ItemDescription { get; set; }
        public CreateOrderAddressModel SenderOrderAddress { get; set; } = new CreateOrderAddressModel();
        public CreateOrderAddressModel RecieverOrderAddress { get; set; } = new CreateOrderAddressModel();
        public CreateOrderAddressModel PickUpOrderAddress { get; set; } = new CreateOrderAddressModel();

        public ExternalCreateOrderModel ToExternalModel()
        {
            return new ExternalCreateOrderModel
            {
                TotalWeight = TotalWeight,
                BranchID = BranchId.ToString(),
                PostingDate = OrderDate.ToString(),
                shipmentType = shipmentType,
                Source = Source,
                airWayBillNumber = OrderAWBNumber,
                item = new OrderItemModel
                {
                    financeDetails = new FinancedetailsModel(),
                    recipient = new RecipientModel
                    {
                        firstName = RecieverOrderAddress.CustomerName,
                        buildingNo = RecieverOrderAddress.CustomerAddressBuilding,
                        City = RecieverOrderAddress.City,
                        streetNumber = RecieverOrderAddress.CustomerAddressStreet,
                        streetType = RecieverOrderAddress.streetType,
                        floor = RecieverOrderAddress.floor,
                        zone = RecieverOrderAddress.CustomerAddressZone,
                        poBoxNumber = RecieverOrderAddress.CustomerPOBOXNumber,
                        isoCountryCode = RecieverOrderAddress.IsoCountryCode,
                        email = RecieverOrderAddress.CustomerEmail,
                        MobileNumber = RecieverOrderAddress.CustomerMobileNumber,
                        ArabicName = RecieverOrderAddress.ArabicName,
                        UnitNumber = RecieverOrderAddress.CustomerAddressUnit,
                        PhoneNumber = RecieverOrderAddress.CustomerMobileNumber,
                        PostalCode = RecieverOrderAddress.CustomerPostCode
                    },
                    pickupaddress = new PickupaddressModel
                    {
                        firstName = PickUpOrderAddress.CustomerName,
                        buildingNo = PickUpOrderAddress.CustomerAddressBuilding,
                        City = PickUpOrderAddress.City,
                        streetNumber = PickUpOrderAddress.CustomerAddressStreet,
                        streetType = PickUpOrderAddress.streetType,
                        floor = PickUpOrderAddress.floor,
                        zone = PickUpOrderAddress.CustomerAddressZone,
                        poBoxNumber = PickUpOrderAddress.CustomerPOBOXNumber,
                        isoCountryCode = PickUpOrderAddress.IsoCountryCode,
                        email = PickUpOrderAddress.CustomerEmail,
                        MobileNumber = PickUpOrderAddress.CustomerMobileNumber,
                        ArabicName = PickUpOrderAddress.ArabicName,
                        UnitNumber = PickUpOrderAddress.CustomerAddressUnit,
                        PhoneNumber = PickUpOrderAddress.CustomerMobileNumber
                    },
                    ProductServiceCategory = ProductServiceCategory,
                    transactionDate = transactionDate,
                    PostingDate = OrderDate.ToString(),
                    PickupDate = PickupDate.ToString(),
                    PickupMode = PickupMode,
                    deliveryMode = deliveryMode,
                    itemWeight = ItemWeight,
                    quantity = Quantity,
                    TotalAmount = TotalAmountToPaid.ToString(),
                    codRequired = CODRequired,
                    podRequired = !CODRequired,
                    volumetricWeight = (int)VolumetricWeight,
                    PaymentType = PaymentType,
                    paymentCurrency = paymentCurrency,
                },
                sender = new SenderModel
                {
                    firstName = SenderOrderAddress.CustomerName,
                    buildingNo = SenderOrderAddress.CustomerAddressBuilding,
                    City = SenderOrderAddress.City,
                    streetNumber = SenderOrderAddress.CustomerAddressStreet,
                    floor = SenderOrderAddress.floor,
                    zone = SenderOrderAddress.CustomerAddressZone,
                    poBoxNumber = SenderOrderAddress.CustomerPOBOXNumber,
                    postalCode = SenderOrderAddress.CustomerPostCode,
                    isoCountryCode = SenderOrderAddress.IsoCountryCode,
                    IdentifierNumber = SenderOrderAddress.CustomerIdentificationNumber,
                    PhoneNumber = SenderOrderAddress.CustomerMobileNumber,
                    MobileNumber = SenderOrderAddress.CustomerMobileNumber,
                    Email = SenderOrderAddress.CustomerEmail,
                    UnitNo = SenderOrderAddress.CustomerAddressUnit
                },
                LabelData = new LabelDataModel
                {
                    BarCodeDimention = "1D",
                    CopyCount = "1",
                    LabelType = "DGADLBL01",
                    OutputType = "PDF",
                    Quantity = "1",
                    Suffix = "Country",
                    SuffixCode = "QA"
                },
                remarks = Remarks
            };
        }
    }

    public class CreateOrderAddressModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string TrackingNumber { get; set; } = string.Empty;
        public string CustomerType { get; set; } = string.Empty;
        public string CustomerName { get; set; } = string.Empty;
        public string CustomerMobileNumber { get; set; } = string.Empty;
        public string? CustomerAddressZone { get; set; } 
        public string? CustomerAddressStreet { get; set; } 
        public string? CustomerAddressBuilding { get; set; }
        public string? CustomerAddressUnit { get; set; }
        public string? CustomerLocationDetails { get; set; }
        public string? CustomerPOBOXNumber { get; set; }
        public string CustomerPostCode { get; set; } = string.Empty;
        public string CustomerEmail { get; set; } = string.Empty;
        public string CustomerIdentificationNumber { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ArabicName { get; set; }
        public string? IsoCountryCode { get; set; }
        public string? streetType { get; set; }
        public string? floor { get; set; }
        public string? City { get; set; }
        public string? CustomerAddress1 { get; set; }
        public string? CustomerAddress2 { get; set; }
        public string? CustomerZipCode { get; set; }
    }
}
