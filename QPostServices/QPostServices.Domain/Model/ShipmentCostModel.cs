﻿namespace QPostServices.Domain.Model
{
    public class ShipmentCostModel
    {
        public string SLA { get; set; } = string.Empty;
        public string Price { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        public string? TypeAr { get; set; } 
    }
}
