﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class BranchService : IBranchService
    {
        private const string _successCreation = "Branch created successfully";
        private const string _successUpdate = "Branch updated successfully";
        private const string _failedCreation = "Branch creation failed";
        private const string _failedUpdation = "Branch updation failed";
        private const string _successDelete = "Branch deleted successfully";
        private const string _failedDelete = "Branch deletion failed";
        private const string _nameAlreadyExist = "Branch with name : {0} already exist";
        private const string _branchDoesNotExist = "Branch with id : {0} Does not exist";

        private readonly IBranchRepository _branchRepository;
        private readonly ILoginService _loginService;

        public BranchService(IBranchRepository branchRepository, ILoginService loginService)
        {
            _branchRepository = branchRepository;
            _loginService = loginService;
        }

        #region GetAllBranches
        public Task<List<BranchModel>> GetListAsync()
        {
            return _branchRepository.GetListAsync();
        }
        #endregion

        #region Get Nearest Branches
        public async Task<List<BranchModel>> GetNearestBranchesAsync(BranchLocationModel branchLocation)
        {
            try
            {
                var branches = await _branchRepository.GetListAsync();

                double dLat = Convert.ToDouble(branchLocation.Latitude);
                double dLon = Convert.ToDouble(branchLocation.Longitude);

                List<BranchCoordinatesModel> bCordList = new List<BranchCoordinatesModel>();

                foreach (var branch in branches)
                {
                    if (branch.GoogleCordinates != null)
                    {
                        BranchCoordinatesModel bCord = new BranchCoordinatesModel(branch, dLat, dLon);
                        bCordList.Add(bCord);
                    }
                }

                var selectedCoords = bCordList.OrderBy(a => a.CalculatedDistance).Select(x => x.BranchId).Take(3).ToList();

                var nearByBranches = await _branchRepository.GetNearestBranchesAsync(selectedCoords);

                return nearByBranches;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Create Branch
        public async Task<ResponseModel> CreateAsync(BranchModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _branchRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get Branch by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _branchRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_branchDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete Branch
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _branchRepository.UpdateStatusAsync(id, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_branchDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Update Branch
        public async Task<ResponseModel> UpdateAsync(BranchModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            var result = await _branchRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_branchDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
