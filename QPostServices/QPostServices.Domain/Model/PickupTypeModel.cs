﻿using Microsoft.AspNetCore.Http;

namespace QPostServices.Domain.Model
{
    public class PickupTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Logo { get; set; }
        public IFormFile? LogoImageFile { get; set; }
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
