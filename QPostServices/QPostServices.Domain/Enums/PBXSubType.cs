﻿namespace QPostServices.Domain.Enums
{
    public enum PBXSubType
    {
        Normal = 0,
        Free = 1,
        None = 2
    }
}
