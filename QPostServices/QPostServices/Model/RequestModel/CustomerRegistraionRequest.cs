﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerRegistraionRequest
    {
        [Required]
        public string FirstName { get; set; } = default!;
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }


        [Required]
        public string CustomerUserName { get; set; } = string.Empty;

        [Required]
        public string CustomerPassword { get; set; } = string.Empty;

        [Required]
        public string SecurityQuestion { get; set; } = string.Empty;

        [Required]
        public string SecurityAnswer { get; set; } = string.Empty;

        [Required]
        public string DocumentType { get; set; } = default!;
        public string? DocumentReference { get; set; }

        [Required]
        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string MobileNumber { get; set; } = default!;

        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string? AlternativeMobileNumber { get; set; }

        [Required]
        public string Nationality { get; set; } = default!;

        [Required]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        public string PoBoxNumber { get; set; } = default!;

        [Required]
        public string ConnectedVpNumber { get; set; } = default!;
        public string? SmartBoxNumber { get; set; }
        public DateTime? PoBoxExpiry { get; set; }
        public DateTime? SmartBoxExpiry { get; set; }
        public int? WalletId { get; set; }
        public string? WalletReferenceNumber { get; set; }
        public decimal? WalletBalanceAmount { get; set; }
        public string? HDS { get; set; }
        public int? HDSPackageId { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? Reference3 { get; set; }
        public int RoleId { get; set; }
        public string? PreferedLanguage { get; set; }
        [Required]
        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string EmailAddress { get; set; } = string.Empty;
        public string? FirstNameAr { get; set; }
        public string? MiddleNameAr { get; set; }
        public string? LastNameAr { get; set; }
        public OTPRequest OTPRequest { get; set; } = new OTPRequest();
        public CustomerRegistraionModel ToModel()
        {
            return new CustomerRegistraionModel
            {
                FirstName = FirstName,
                MiddleName = MiddleName,
                LastName = LastName,
                DocumentType = DocumentType,
                DocumentReference = DocumentReference,
                MobileNumber = MobileNumber,
                AlternativeMobileNumber = AlternativeMobileNumber,
                Nationality = Nationality,
                DateOfBirth = DateOfBirth,
                PoBoxExpiry = PoBoxExpiry,
                PoBoxNumber = PoBoxNumber,
                ConnectedVpNumber = ConnectedVpNumber,
                SmartBoxNumber = SmartBoxNumber,
                SmartBoxExpiry = SmartBoxExpiry,
                WalletId = WalletId,
                WalletBalanceAmount = WalletBalanceAmount,
                HDS = HDS,
                HDSPackageId = HDSPackageId,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Reference3 = Reference3,
                WalletReferenceNumber = WalletReferenceNumber,
                CustomerUserName = CustomerUserName,
                CustomerPassword = CustomerPassword,
                SecurityAnswer = SecurityAnswer,
                SecurityQuestion = SecurityQuestion,
                RoleId = RoleId,
                EmailAddress = EmailAddress,
                PreferedLanguage = PreferedLanguage,
                FirstNameAr = FirstNameAr,
                MiddleNameAr = MiddleNameAr,
                LastNameAr = LastNameAr
            };
        }
    }
}
