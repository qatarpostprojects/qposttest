﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/Wallet/TransactionDetails")]
    [ApiController]
    public class WalletTransactionDetailsController : ControllerBase
    {
        private readonly IWalletTransactionDetailsService _walletTransactionDetailsService;
        public WalletTransactionDetailsController(IWalletTransactionDetailsService walletTransactionDetailsService)
        {
            _walletTransactionDetailsService = walletTransactionDetailsService;
        }

        #region Create Wallet Transaction Details
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateWalletTransactionDetails))]
        public async Task<IActionResult> CreateWalletTransactionDetails([FromBody] WalletTransactionDetailsRequest request)
        {
            try
            {
                var result = await _walletTransactionDetailsService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Wallet Transaction Details by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(WalletTransactionDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetWalletTransactionDetails))]
        public async Task<IActionResult> GetWalletTransactionDetails(int id)
        {
            try
            {
                var result = await _walletTransactionDetailsService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(WalletTransactionDetails.FromModel((WalletTransactionDetailsModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Wallet Transaction Details by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<WalletTransactionDetails>))]
        [HttpGet("Customer/{customerId}", Name = nameof(GetWalletTransactionDetailsList))]
        public async Task<IActionResult> GetWalletTransactionDetailsList(int customerId)
        {
            try
            {
                var results = await _walletTransactionDetailsService.GetListByCustomerId(customerId);

                return Ok(results.Select(x => WalletTransactionDetails.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Wallet Transaction Details
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteWalletTransactionDetails))]
        public async Task<IActionResult> DeleteWalletTransactionDetails(int id)
        {
            try
            {
                var result = await _walletTransactionDetailsService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Wallet Transaction Details
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateWalletTransactionDetails))]
        public async Task<IActionResult> UpdateWalletTransactionDetails(int id, [FromBody] WalletTransactionDetailsRequest request)
        {
            try
            {
                var result = await _walletTransactionDetailsService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Recent Transaction Details by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<WalletTransactionDetails>))]
        [HttpGet("GetRecentTransactions/{customerId}", Name = nameof(GetRecentTransactionDetailsList))]
        public async Task<IActionResult> GetRecentTransactionDetailsList(int customerId)
        {
            try
            {
                var results = await _walletTransactionDetailsService.GetRecentTransactionsAsync(customerId);

                return Ok(results.Select(x => WalletTransactionDetails.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
