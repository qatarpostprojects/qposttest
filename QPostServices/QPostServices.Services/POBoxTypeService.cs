﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class POBoxTypeService : IPOBoxTypeService
    {
        private const string _successCreation = "POBox Type created successfully";
        private const string _successUpdate = "POBox Type updated successfully";
        private const string _failedCreation = "POBox Type creation failed";
        private const string _failedUpdation = "POBox Type updation failed";
        private const string _successDelete = "POBox Type deleted successfully";
        private const string _failedDelete = "POBox Type deletion failed";
        private const string _POBoxTypeDoesNotExist = "POBox Type with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly IPOBoxTypeRepository _pOBoxTypeRepository;
        public POBoxTypeService(IPOBoxTypeRepository pOBoxTypeRepository, ILoginService loginService)
        {
            _pOBoxTypeRepository = pOBoxTypeRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(POBoxTypeModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();

            var result = await _pOBoxTypeRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _pOBoxTypeRepository.UpdateStatusAsync(id, false, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _pOBoxTypeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All
        public async Task<List<POBoxTypeModel>> GetListAsync()
        {
            return await _pOBoxTypeRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(POBoxTypeModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();

            var result = await _pOBoxTypeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_POBoxTypeDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
