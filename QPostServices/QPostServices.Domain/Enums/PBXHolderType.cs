﻿namespace QPostServices.Domain.Enums
{
    public enum PBXHolderType
    {
        None = 0,
        IndividualUser = 1,
        Company = 2,
        Company2to5 = 3,
        Company6to10 = 4,
        Company11to20 = 5,
        Company21to50 = 6,
        Company51to100 = 7,
        Company101to300 = 8
    }
}
