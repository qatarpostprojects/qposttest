﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{

    public class WalletModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string Description { get; set; } = string.Empty;
        public string? DescriptionAr { get; set; }
        public string? Reference1 { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
