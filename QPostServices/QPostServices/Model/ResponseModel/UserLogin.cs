﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class UserLogin
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string SecurityQuestion { get; set; } = string.Empty;
        public string SecurityAnswer { get; set; } = string.Empty;
        public int RoleId { get; set; }
        public string RoleName { get; set; } = string.Empty;
        public string RecordStatus { get; set; } = string.Empty;
        public DateTime? CreatedDate { get; set; }
        public int? CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? CreatedUserName { get; set; } = string.Empty;
        public string? Token { get; set; }
        public string? PreferedLanguage { get; set; }

        public static UserLogin FromModel(UserLoginModel model)
        {
            return new UserLogin()
            {
                Id = model.Id,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CustomerId = model.CustomerId,
                Password = model.Password,
                UserName = model.UserName,
                Name = model.Name,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                SecurityAnswer = model.SecurityAnswer,
                SecurityQuestion = model.SecurityQuestion,
                RecordStatus = model.RecordStatus.ToString(),
                RoleId = model.RoleId,
                RoleName = model.RoleName,
                Token = model.Token,
                PreferedLanguage = model.PreferedLanguage
            };
        }
    }
}
