﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Payment
    {
        public int Id { get; set; }
        public string PaymentSessionId { get; set; } = string.Empty;
        public int PaymentProviderId { get; set; }
        public string PaymentProviderName { get; set; } = string.Empty;
        public int PaymentServiceId { get; set; }
        public string PaymentServiceName { get; set; } = string.Empty;
        public decimal? PaymentRequestAmount { get; set; }
        public decimal? PaymentReceiveAmount { get; set; }
        public int? PaymentTransactionId { get; set; }
        public string? PaymentBankReferenceNumber { get; set; }
        public int PaymentCustomerId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string? PaymentReference1 { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static Payment FromModel(PaymentModel model)
        {
            return new Payment { 
                PaymentReceiveAmount = model.PaymentReceiveAmount,
                PaymentTransactionId = model.PaymentTransactionId,
                PaymentRequestAmount = model.PaymentRequestAmount,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                Id = model.Id,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                PaymentBankReferenceNumber = model.PaymentBankReferenceNumber,
                PaymentCustomerId = model.PaymentCustomerId,
                PaymentDate = model.PaymentDate,
                PaymentProviderId = model.PaymentProviderId,
                PaymentProviderName = model.PaymentProviderName,
                PaymentReference1 = model.PaymentReference1,
                PaymentServiceId = model.PaymentServiceId,
                PaymentServiceName = model.PaymentServiceName,
                PaymentSessionId = model.PaymentSessionId,
                RecordStatus = model.RecordStatus
            };
        }
    }
}
