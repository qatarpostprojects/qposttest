﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;
using System.Xml.Linq;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceTypeController : ControllerBase
    {
        private readonly IServiceTypeService _serviceTypeServices;
        public ServiceTypeController(IServiceTypeService serviceTypeServices)
        {
            _serviceTypeServices = serviceTypeServices;
        }

        #region Create ServiceType
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateServiceType))]
        public async Task<IActionResult> CreateServiceType([FromBody] ServiceTypeRequest request)
        {
            try
            {
                var result = await _serviceTypeServices.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get ServiceType by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceTypeResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetServiceType))]
        public async Task<IActionResult> GetServiceType(int id)
        {
            try
            {
                var result = await _serviceTypeServices.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(ServiceTypeResponse.FromModel((ServiceTypeModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get ServiceType
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ServiceTypeResponse>))]
        [HttpGet(Name = nameof(GetServiceTypeList))]
        public async Task<IActionResult> GetServiceTypeList()
        {
            try
            {
                var results = await _serviceTypeServices.GetListAsync();

                return Ok(results.Select(x => ServiceTypeResponse.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
        
        #region Get IdValue
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<IdValue>))]
        [HttpGet("Lookup",Name = nameof(GetServiceTypeIdValueList))]
        public async Task<IActionResult> GetServiceTypeIdValueList()
        {
            try
            {
                var results = await _serviceTypeServices.GetIdValueListAsync();

                return Ok(results.Select(x => IdValue.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete ServiceType
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteServiceType))]
        public async Task<IActionResult> DeleteServiceType(int id)
        {
            try
            {
                var result = await _serviceTypeServices.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update ServiceType
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateServiceType))]
        public async Task<IActionResult> UpdateServiceType(int id, [FromBody] ServiceTypeRequest request)
        {
            try
            {
                var result = await _serviceTypeServices.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
