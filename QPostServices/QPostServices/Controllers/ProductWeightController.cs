﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductWeightController : ControllerBase
    {
        private readonly IProductWeightService _productWeightService;
        public ProductWeightController(IProductWeightService productWeightService)
        {
            _productWeightService = productWeightService;
        }

        #region Get Product weight List
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ProductWeight>))]
        [HttpGet(Name = nameof(GetProductWeightList))]
        public async Task<IActionResult> GetProductWeightList()
        {
            try
            {
                var results = await _productWeightService.GetListAsync();
                return Ok(results.Select(x => ProductWeight.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Product Weight By Id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductWeight))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetProductWeight))]
        public async Task<IActionResult> GetProductWeight(int id)
        {
            try
            {
                var result = await _productWeightService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(ProductWeight.FromModel((ProductWeightModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Product Weight by delivery product 
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductWeight))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("deliveryproduct/{id}", Name = nameof(GetProductWeightByDeliveryProductId))]
        public async Task<IActionResult> GetProductWeightByDeliveryProductId(int id)
        {
            try
            {
                var results = await _productWeightService.GetByDeliveryProductId(id);
                return Ok(results.Select(x => ProductWeight.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateProductWeight))]
        public async Task<IActionResult> CreateProductWeight([FromBody] ProductWeightRequest request)
        {
            try
            {
                var result = await _productWeightService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateProductWeight))]
        public async Task<IActionResult> UpdateProductWeight(int id, [FromBody] ProductWeightRequest request)
        {
            try
            {
                var result = await _productWeightService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteProductWeight))]
        public async Task<IActionResult> DeleteProductWeight(int id)
        {
            try
            {
                var result = await _productWeightService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
