﻿using QPostPBXService;

namespace QPostServices.Domain.Model
{
    public class PBXAddressModel
    {
        public string Address { get; set; }
        public string BuildingNumber { get; set; }
        public string ZoneNumber { get; set; }
        public string StreetNumber { get; set; }
        public string City { get; set; }
        public string MobileNumber { get; set; } = default!;
        public string OfficeTelephoneNo { get; set; }
        public string Email { get; set; } = default!;
        public CustomerAddress ToModel()
        {
            return new CustomerAddress()
            {
                Address = Address,
                BuildingNumber = BuildingNumber,
                ZoneNumber = ZoneNumber,
                StreetNumber = StreetNumber,
                City = City,
                MobileNumber = MobileNumber,
                Email = Email,
                OfficeTelephoneNo = OfficeTelephoneNo
            };
        }
    }
}
