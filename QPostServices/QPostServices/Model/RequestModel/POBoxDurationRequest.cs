﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class POBoxDurationRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Logo { get; set; }
        [Required]
        public int MonthValue { get; set; }
        [Required]
        public int ServiceId { get; set; }
        public POBoxDurationModel ToCreateModel()
        {
            return new POBoxDurationModel()
            {
                Name = Name,
                Description = Description,
                Logo = Logo,
                MonthValue = MonthValue,
                ServiceId = ServiceId,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
        public POBoxDurationModel ToUpdateModel(int id)
        {
            return new POBoxDurationModel()
            {
                Name = Name,
                Description = Description,
                Logo = Logo,
                MonthValue = MonthValue,
                Id = id,
                ServiceId= ServiceId,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
