﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IProfileNotificationSettingsRepository
    {
        Task<int> CreateAsync(ProfileNotificationSettingsModel model);
        Task<int> UpdateAsync(ProfileNotificationSettingsModel model);
        Task<ProfileNotificationSettingsModel?> GetByUserId(int userId);
    }
}
