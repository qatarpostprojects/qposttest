﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerCardDetailsRepository
    {
        Task<int> CreateCardDetailsAsync(CustomerCardDetailsModel model);
        Task<CustomerCardDetailsModel?> GetCardDetailsByIdAsync(int id);
        Task<List<CustomerCardDetailsModel>> GetCardDetailsByCustomerIdAsync(int customerId);
        Task<int> UpdateCardStatusAsync(int id, bool status);
        Task<int> UpdateCardDetailAsync(CustomerCardDetailsModel model);
    }
}
