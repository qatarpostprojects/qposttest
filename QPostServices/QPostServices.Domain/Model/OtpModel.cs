﻿namespace QPostServices.Domain.Model
{
    public class OtpModel
    {
        public string Type { get; set; } = string.Empty;

        public string Value { get; set; } = string.Empty;

        public string Note { get; set; } = string.Empty;

        public string? OTP { get; set; }
    }
}
