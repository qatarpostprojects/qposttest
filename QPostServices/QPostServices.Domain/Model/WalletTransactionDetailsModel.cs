﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class WalletTransactionDetailsModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string BillNo { get; set; } = default!;
        public decimal Amount { get; set; }
        public TransactionType Type { get; set; }
        public string ReferenceId { get; set; } = default!;
        public string BankTransactionId { get; set; } = default!;
        public DateTime Date { get; set; }
        public int PaymentId { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
