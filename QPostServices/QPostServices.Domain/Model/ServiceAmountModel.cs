﻿namespace QPostServices.Domain.Model
{
    public class ServiceAmountModel
    {
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }
    }
}
