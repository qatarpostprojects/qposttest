﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class TimeSlotRequest
    {
        public string TimeSlot { get; set; } = string.Empty;
        public string? TimeSlotAr { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public TimeSlotModel ToCreateModel()
        {
            return new TimeSlotModel()
            {
                TimeSlot = TimeSlot,
                TimeSlotAr = TimeSlotAr,
                StartTime = StartTime.ToString("HH:mm"),
                EndTime = EndTime.ToString("HH:mm"),
            };
        }
        public TimeSlotModel ToUpdateModel(int id)
        {
            return new TimeSlotModel()
            {
                TimeSlot = TimeSlot,
                TimeSlotAr = TimeSlotAr,
                StartTime = StartTime.ToString("HH:mm"),
                EndTime = EndTime.ToString("HH:mm"),
                Id = id
            };
        }
    }
}
