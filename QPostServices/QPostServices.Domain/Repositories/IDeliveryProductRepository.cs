﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IDeliveryProductRepository
    {
        Task<List<DeliveryProductModel>> GetListAsync();
        Task<List<DeliveryProductModel>> GetByTypeAsync(bool isOverseas);
        Task<int> CreateAsync(DeliveryProductModel model);
        Task<int> UpdateAsync(DeliveryProductModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
        Task<DeliveryProductModel> GetByIdAsync(int id);
    }
}
