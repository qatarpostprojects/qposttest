﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class POBoxType
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        public string? Logo { get; set; }
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public static POBoxType FromModel(POBoxTypeModel model)
        {
            return new POBoxType()
            { 
                Id = model.Id, 
                Active = model.Active,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CreatedDate = model.CreatedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                Description = model.Description,
                Logo = model.Logo,
                Name = model.Name,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr
            };
        }
    }
}
