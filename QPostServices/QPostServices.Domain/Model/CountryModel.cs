﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CountryModel
    {
        public int Id { get; set; }
        public string CountryName { get; set; } = string.Empty;
        public string? CountryCode { get; set; }
        public string? Flag { get; set; }
        public IFormFile? FlagFile { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? CountryNameAr { get; set; }
    }
}