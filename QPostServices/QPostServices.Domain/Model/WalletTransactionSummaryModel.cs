﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class WalletTransactionSummaryModel
    {
        public int Id { get; set; }
        public int WalletId { get; set; }
        public int CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public decimal Amount { get; set; }
        public int? LastTransactionId { get; set; }
        public DateTime? LastTransactionDate { get; set; }
        public string? Remarks { get; set; }
        public string ReferenceNumber { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
