﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class SubscriptionRenewRequest
    {
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public int SubscriptionTypeValue { get; set; }
        public int? CardPaymentId { get; set; }
        public int? WalletTransactioId { get; set; }
        public string PaymentType { get; set; } = string.Empty;
        public string AWBNumber { get; set; } = string.Empty;
        public PBXRenewPostBoxRequest ExternalRenewRequest { get; set; } = new PBXRenewPostBoxRequest();
        public SubscriptionRenewModel ToModel()
        {
            return new SubscriptionRenewModel()
            {
                CardPaymentId= CardPaymentId,
                CustomerId= CustomerId,
                SubscriptionTypeValue= SubscriptionTypeValue,
                PaymentType= PaymentType,
                SubscriptionId= SubscriptionId,
                WalletTransactioId = WalletTransactioId,
                AWBNumber = AWBNumber,
                ExternalRenewModel = ExternalRenewRequest.ToModel()
            };
        }
    }
}
