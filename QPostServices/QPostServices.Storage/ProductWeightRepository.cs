﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class ProductWeightRepository : IProductWeightRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public ProductWeightRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(ProductWeightModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProductWeight_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@deliveryProductId", model.DeliveryProductId);
                parameters.Add("@weight", model.Weight);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@active", true);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Delivery Product Id
        public async Task<List<ProductWeightModel>> GetByDeliveryProductId(int deliveryProductId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProductWeight_GetByDeliveryProduct]";
                var parameters = new DynamicParameters();
                parameters.Add("@deliveryProductId", deliveryProductId);
                var result = await connection.QueryAsync<ProductWeightModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region Get By Id
        public async Task<ProductWeightModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProductWeight_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<ProductWeightModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault() ?? new ProductWeightModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get
        public async Task<List<ProductWeightModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProductWeight_Get]";
                var result = await connection.QueryAsync<ProductWeightModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(ProductWeightModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProductWeight_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@weight", model.Weight);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, bool status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProductWeight_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
