﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class PaymentModel
    {
        public int Id { get; set; }
        public string PaymentSessionId { get; set; } = string.Empty;
        public int PaymentProviderId { get; set; }
        public string PaymentProviderName { get; set; } = string.Empty;
        public int PaymentServiceId { get; set; }
        public string PaymentServiceName { get; set; } = string.Empty;
        public decimal? PaymentRequestAmount { get; set; }
        public decimal? PaymentReceiveAmount { get; set; }
        public int? PaymentTransactionId { get; set; }
        public string? PaymentBankReferenceNumber { get; set; }
        public int PaymentCustomerId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string? PaymentReference1 { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
