﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Net;

namespace QPostServices.Services
{
    public class CustomerAddressService : ICustomerAddressService
    {
        private const string _addressDoesNotExist = "Address with id {0} does not exists";
        private const string _successAddressCreation = "Address created successfully";
        private const string _failedAddressCreation = "Address creation failed";
        private const string _successAddressDeleted = "Address with id {0} deleted successfully";
        private const string _successAddressUpdate = "Address with id {0} updated successfully";
        private const string _addressLimitExceed = "Only 5 address per type allowed";

        private readonly ICustomerAddressRepository _customerAddressRepository;
        private readonly ILoginService _loginService;
        public CustomerAddressService(ICustomerAddressRepository customerAddressRepository, ILoginService loginService)
        {
            _customerAddressRepository = customerAddressRepository;
            _loginService = loginService;
        }

        #region Create Customer Address
        public async Task<ResponseModel> CreateAsync(CustomerAddressModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _customerAddressRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successAddressCreation;
                    response.Error = false;
                }
                else if (result == -1)
                {
                    response.Error = true;
                    response.Response = _addressLimitExceed;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedAddressCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Customer Addresses list by customer Id
        public Task<List<CustomerAddressModel>> GetAddressesListByCustomerId(int customerId)
        {
            return _customerAddressRepository.GetAddressesListByCustomerId(customerId);
        }
        #endregion

        #region Get Customer Address by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _customerAddressRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_addressDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update Customer Address
        public async Task<ResponseModel> UpdateAsync(CustomerAddressModel model)
        {
            try
            {
                var response = new ResponseModel();

                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _customerAddressRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successAddressUpdate, model.Id);
                    response.Error = false;
                }
                else if (result == -1)
                {
                    response.Error = true;
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Response = _addressLimitExceed;
                }
                else
                {
                    response.Error = true;
                    response.StatusCode = HttpStatusCode.NotFound;
                    response.Response = string.Format(_addressDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Customer Address
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();
                var result = await _customerAddressRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successAddressDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_addressDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<CustomerAddressModel?> GetPrimaryAddressByType(AddressType type)
        {
            var customerId = _loginService.LoggedInCustomer();
            return _customerAddressRepository.GetPrimaryAddressByCustomerId(customerId, type);
        }
        #endregion
    }
}
