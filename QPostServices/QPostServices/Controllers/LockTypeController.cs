﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LockTypeController : ControllerBase
    {
        private readonly ILockTypeServices _lockTypeServices;
        public LockTypeController(ILockTypeServices lockTypeServices)
        {
            _lockTypeServices = lockTypeServices;
        }

        #region Create lock type
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateLockType))]
        public async Task<IActionResult> CreateLockType([FromBody] LockTypeRequest request)
        {
            try
            {
                var result = await _lockTypeServices.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Lock type by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LockType))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetLockType))]
        public async Task<IActionResult> GetLockType(int id)
        {
            try
            {
                var result = await _lockTypeServices.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(LockType.FromModel((LockTypeModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Lock types
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<LockType>))]
        [HttpGet(Name = nameof(GetLockTypeList))]
        public async Task<IActionResult> GetLockTypeList()
        {
            try
            {
                var results = await _lockTypeServices.GetListAsync();

                return Ok(results.Select(x => LockType.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Lock type
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteLockType))]
        public async Task<IActionResult> DeleteLockType(int id)
        {
            try
            {
                var result = await _lockTypeServices.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Lock type
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateLockType))]
        public async Task<IActionResult> UpdateLockType(int id, [FromBody] LockTypeRequest request)
        {
            try
            {
                var result = await _lockTypeServices.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
