﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerOrderAddress
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string TrackingNumber { get; set; } = string.Empty;
        public string CustomerType { get; set; } = string.Empty;
        public string CustomerName { get; set; } = string.Empty;
        public string CustomerMobileNumber { get; set; } = string.Empty;
        public string? CustomerAddressZone { get; set; }
        public string? CustomerAddressStreet { get; set; }
        public string? CustomerAddressBuilding { get; set; }
        public string? CustomerAddressUnit { get; set; }
        public string? CustomerLocationDetails { get; set; }
        public string CustomerPOBOXNumber { get; set; } = string.Empty;
        public string CustomerPostCode { get; set; } = string.Empty;
        public string CustomerEmail { get; set; } = string.Empty;
        public string CustomerIdentificationNumber { get; set; } = string.Empty;
        public string RecordStatus { get; set; } = string.Empty;
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? CustomerAddress1 { get; set; }
        public string? CustomerAddress2 { get; set; }
        public string? CustomerZipCode { get; set; }

        public static CustomerOrderAddress FromModel(CustomerOrderAddressModel model)
        {
            return new CustomerOrderAddress()
            {
                CustomerAddressBuilding = model.CustomerAddressBuilding,
                CreatedDate = model.CreatedDate,
                ModifiedDate = model.ModifiedDate,
                CustomerAddressZone = model.CustomerAddressZone,
                CustomerAddressStreet = model.CustomerAddressStreet,
                CustomerAddressUnit = model.CustomerAddressUnit,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CustomerEmail = model.CustomerEmail,
                CustomerIdentificationNumber = model.CustomerIdentificationNumber,
                CustomerLocationDetails = model.CustomerLocationDetails,
                CustomerMobileNumber = model.CustomerMobileNumber,
                CustomerName = model.CustomerName,
                CustomerPOBOXNumber = model.CustomerPOBOXNumber,
                CustomerPostCode = model.CustomerPostCode,
                CustomerType = model.CustomerType,
                Id = model.Id,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                OrderId = model.OrderId,
                RecordStatus = model.RecordStatus.ToString(),
                Reference1 = model.Reference1,
                Reference2 = model.Reference2,
                TrackingNumber = model.TrackingNumber,
                CustomerAddress1 = model.CustomerAddress1,
                CustomerAddress2 = model.CustomerAddress2,
                CustomerZipCode = model.CustomerZipCode
            };
        }
    }
}
