﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class WalletRepository : IWalletRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public WalletRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region CreateAsync
        public async Task<int> CreateAsync(WalletModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@walletName", model.Name);
                parameters.Add("@walletNameAr", model.NameAr);
                parameters.Add("@walletDescription", model.Description);
                parameters.Add("@walletDescriptionAr", model.DescriptionAr);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region DepositAsync
        public async Task<int> DepositAsync(WalletTransactionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_Deposit]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@walletId", model.WalletId);
                parameters.Add("@walletTransactionBillNo", model.BillNo);
                parameters.Add("@walletTransactionAmount", model.Amount);
                parameters.Add("@walletTransactionType", (int)TransactionType.Credit);
                parameters.Add("@transactionReferenceId", model.ReferenceId);
                parameters.Add("@bankTransactionId", model.BankTransactionId);
                parameters.Add("@walletTransactionPaymentId", model.PaymentId);
                parameters.Add("@walletRemarks", model.Remarks);
                parameters.Add("@walletTransactionDate", model.TransactionDate);
                parameters.Add("@walletReference1", model.Reference1);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@userId", model.UserId);
                parameters.Add("@date", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAllActiveAsync
        public async Task<List<WalletModel>> GetAllActiveAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<WalletModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetByIdAsync
        public async Task<WalletModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<WalletModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region PaymentAsync
        public async Task<int> PaymentAsync(WalletTransactionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_Payment]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@walletId", model.WalletId);
                parameters.Add("@walletTransactionBillNo", model.BillNo);
                parameters.Add("@walletTransactionAmount", model.Amount);
                parameters.Add("@walletTransactionType", (int)TransactionType.Debit);
                parameters.Add("@transactionReferenceId", model.ReferenceId);
                parameters.Add("@bankTransactionId", model.BankTransactionId);
                parameters.Add("@walletTransactionPaymentId", model.PaymentId);
                parameters.Add("@walletRemarks", model.Remarks);
                parameters.Add("@walletTransactionDate", model.TransactionDate);
                parameters.Add("@walletReference1", model.Reference1);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@userId", model.UserId);
                parameters.Add("@date", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateAsync
        public async Task<int> UpdateAsync(WalletModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@walletName", model.Name);
                parameters.Add("@walletNameAr", model.NameAr);
                parameters.Add("@walletDescription", model.Description);
                parameters.Add("@walletDescriptionAr", model.DescriptionAr);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch { throw; }
        }
        #endregion

        #region UpdateStatusAsync
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Wallet_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
