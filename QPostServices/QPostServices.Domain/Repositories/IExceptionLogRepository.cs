﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IExceptionLogRepository
    {
        Task CreateAsync(LogModel model);
    }
}
