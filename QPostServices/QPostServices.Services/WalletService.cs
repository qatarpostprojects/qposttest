﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class WalletService : IWalletService
    {
        private const string _walletDoesNotExist = "Wallet with id {0} does not exists";
        private const string _successWalletCreation = "Wallet created successfully";
        private const string _successPaymentCompleted = "Payment completed successfully";
        private const string _failPaymentCompleted = "Payment failed";
        private const string _failedWalletCreation = "Wallet creation failed";
        private const string _successWalletDeleted = "Wallet with id {0} deleted successfully";
        private const string _successWalletUpdate = "Wallet with id {0} updated successfully";


        private readonly IWalletRepository _walletRepository;
        private readonly ILoginService _loginService;
        public WalletService(IWalletRepository walletRepository, ILoginService loginService)
        {
            _walletRepository = walletRepository;
            _loginService = loginService;
        }


        #region Create wallet
        public async Task<ResponseModel> CreateAsync(WalletModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _walletRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successWalletCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedWalletCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Wallet
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _walletRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successWalletDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_walletDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region Get all active wallet
        public async Task<List<WalletModel>> GetAllActiveAsync()
        {
            return await _walletRepository.GetAllActiveAsync();
        }

        #endregion

        #region Get wallet by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _walletRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_walletDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Update wallet by id
        public async Task<ResponseModel> UpdateAsync(WalletModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _walletRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successWalletUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_walletDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Payment
        public async Task<ResponseModel> PaymentAsync(WalletTransactionModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.UserId = _loginService.LoggedInUser();
                var result = await _walletRepository.PaymentAsync(model);

                if (result > 0)
                {
                    response.Response = _successPaymentCompleted;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failPaymentCompleted;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Deposit
        public async Task<ResponseModel> DepositAsync(WalletTransactionModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.UserId = _loginService.LoggedInUser();
                var result = await _walletRepository.DepositAsync(model);

                if (result > 0)
                {
                    response.Response = _successWalletCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedWalletCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
