﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class POBoxDurationController : ControllerBase
    {
        private readonly IPOBoxDurationService _pOBoxDurationService;
        public POBoxDurationController(IPOBoxDurationService pOBoxDurationService)
        {
            _pOBoxDurationService = pOBoxDurationService;
        }

        #region Get PO Box Duration List By service
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<POBoxDuration>))]
        [HttpGet("Service/{serviceId}",Name = nameof(GetPOBoxDurationByServiceIdList))]
        public async Task<IActionResult> GetPOBoxDurationByServiceIdList(int serviceId)
        {
            try
            {
                var results = await _pOBoxDurationService.GetListByServiceIdAsync(serviceId);
                return Ok(results.Select(x => POBoxDuration.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get PO Box Duration List 
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<POBoxDuration>))]
        [HttpGet(Name = nameof(GetPOBoxDurationList))]
        public async Task<IActionResult> GetPOBoxDurationList()
        {
            try
            {
                var results = await _pOBoxDurationService.GetListAsync();
                return Ok(results.Select(x => POBoxDuration.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get PO Box Duration By Id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(POBoxDuration))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetPOBoxDuration))]
        public async Task<IActionResult> GetPOBoxDuration(int id)
        {
            try
            {
                var result = await _pOBoxDurationService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(POBoxDuration.FromModel((POBoxDurationModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreatePOBoxDuration))]
        public async Task<IActionResult> CreatePOBoxDuration([FromBody] POBoxDurationRequest request)
        {
            try
            {
                var result = await _pOBoxDurationService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdatePOBoxDuration))]
        public async Task<IActionResult> UpdatePOBoxDuration(int id, [FromBody] POBoxDurationRequest request)
        {
            try
            {
                var result = await _pOBoxDurationService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeletePOBoxDuration))]
        public async Task<IActionResult> DeletePOBoxDuration(int id)
        {
            try
            {
                var result = await _pOBoxDurationService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
