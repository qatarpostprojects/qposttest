﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Enums
{
    public enum ServiceType
    {
        All = 1,
        Parent = 2,
        AdOnService = 3
    }
}
