﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class CustomerNotificationRequest
    {
        public int CustomerId { get; set; }
        public string? TrackingNumber { get; set; }
        public string? MobileNumber { get; set; }
        public string? Message { get; set; }
        public int CategoryId { get; set; }
        public CustomerNotificationModel ToCreateModel()
        {
            return new CustomerNotificationModel()
            {
                CustomerId = CustomerId,
                TrackingNumber = TrackingNumber,
                MobileNumber = MobileNumber,
                Message = Message,
                CategoryId = CategoryId
            };
        }
    }
    public class CustomerNotificationFetchRequest
    {
        public int? CustomerId { get; set;}
        public string? TrackingNumber { get; set; }
        public string? MobileNumber { get; set; }
        public bool? IsRead { get; set; }
        public int? CategoryId { get; set; }
        public CustomerNotificationFetchModel ToModel()
        {
            return new CustomerNotificationFetchModel()
            {
                CustomerId = CustomerId,
                TrackingNumber = TrackingNumber,
                MobileNumber = MobileNumber,
                IsRead = IsRead,
                CategoryId = CategoryId
            };
        }
    }
}
