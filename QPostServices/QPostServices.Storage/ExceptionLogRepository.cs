﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class ExceptionLogRepository : IExceptionLogRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public ExceptionLogRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        public async Task CreateAsync(LogModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ExceptionLog_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@method", model.Method);
                parameters.Add("@requestName", model.RequestName);
                parameters.Add("@requestUrl", model.RequestUrl);
                parameters.Add("@queryString", model.QueryString);
                parameters.Add("@body", model.Body);
                parameters.Add("@response", model.Response);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
    }
}
