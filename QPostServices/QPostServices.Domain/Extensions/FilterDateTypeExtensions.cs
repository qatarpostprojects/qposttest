﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Extensions
{
    public static class FilterDateTypeExtensions
    {
        public static (DateTime StartDate, DateTime EndDate) GetDateRange(this FilterDateTypes filterType)
        {
            DateTime endDate = DateTime.Now;
            DateTime startDate = DateTime.Now;

            switch (filterType)
            {
                case FilterDateTypes.Recent:
                    // Get data for the last 14 days
                    startDate = endDate.AddDays(-14);
                    break;
                case FilterDateTypes.CurrentMonth:
                    // Get data for the current month
                    startDate = new DateTime(endDate.Year, endDate.Month, 1);
                    break;
                case FilterDateTypes.LastMonth:
                    // Get data for the last month
                    startDate = new DateTime(endDate.Year, endDate.Month, 1).AddMonths(-1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                    break;
                case FilterDateTypes.Last3Months:
                    // Get data for the last 3 months
                    startDate = endDate.AddMonths(-3);
                    break;
                case FilterDateTypes.Last6Months:
                    // Get data for the last 6 months
                    startDate = endDate.AddMonths(-6);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(filterType), filterType, null);
            }

            // Set the end date to the end of the selected day
            endDate = endDate.Date.AddDays(1).AddTicks(-1);

            return (startDate, endDate);
        }
    }
}
