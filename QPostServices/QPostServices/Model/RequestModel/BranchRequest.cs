﻿using Newtonsoft.Json;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class BranchRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        [Required]
        public string CostCenter { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;
        public string? AddressAr { get; set; }

        public string? GoogleCordinates { get; set; }

        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string? MobileNumber { get; set; }

        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string? Email { get; set; }
        public int? BranchTypeId { get; set; }
        public List<string>? WorkingHours { get; set; }
        public List<string>? WorkingHoursAr { get; set; }


        public BranchModel ToCreateModel()
        {
            return new BranchModel
            {
                Name = Name,
                CostCenter = CostCenter,
                Address = Address,
                GoogleCordinates = GoogleCordinates,
                NameAr = NameAr,
                AddressAr = AddressAr,
                MobileNumber = MobileNumber,
                Email = Email,
                BranchTypeId = BranchTypeId,
                WorkingHours = WorkingHours != null ? string.Join(",", WorkingHours) : null,
                WorkingHoursAr = WorkingHoursAr != null ? string.Join(",", WorkingHoursAr) : null
            };
        }

        public BranchModel ToUpdateModel(int id)
        {
            return new BranchModel
            {
                Id = id,
                Name = Name,
                CostCenter = CostCenter,
                Address = Address,
                GoogleCordinates = GoogleCordinates,
                NameAr = NameAr,
                AddressAr = AddressAr,
                MobileNumber = MobileNumber,
                Email = Email,
                BranchTypeId = BranchTypeId,
                WorkingHours = WorkingHours != null ? string.Join(",", WorkingHours) : null,
                WorkingHoursAr = WorkingHoursAr != null ? string.Join(",", WorkingHoursAr) : null
            };
        }
    }
}
