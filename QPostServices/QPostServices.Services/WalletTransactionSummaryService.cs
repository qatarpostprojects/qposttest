﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class WalletTransactionSummaryService : IWalletTransactionSummaryService
    {
        private const string _summaryDoesNotExist = "Wallet transaction summary with id {0} does not exists";
        private const string _successSummaryCreation = "Wallet transaction summary created successfully";
        private const string _failedSummaryCreation = "Wallet transaction summary creation failed";
        private const string _successSummaryDeleted = "Wallet transaction summary with id {0} deleted successfully";
        private const string _successSummaryUpdate = "Wallet transaction summary with id {0} updated successfully";

        private readonly IWalletTransactionSummaryRepository _walletTransactionSummaryRepository;
        private readonly ILoginService _loginService;
        public WalletTransactionSummaryService(IWalletTransactionSummaryRepository walletTransactionSummaryRepository, ILoginService loginService)
        {
            _walletTransactionSummaryRepository = walletTransactionSummaryRepository;
            _loginService = loginService;
        }

        #region Create wallet transaction summary
        public async Task<ResponseModel> CreateAsync(WalletTransactionSummaryModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _walletTransactionSummaryRepository.CreateAsync(model, TransactionType.Credit);

                if (result > 0)
                {
                    response.Response = _successSummaryCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedSummaryCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Create wallet transaction summary
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _walletTransactionSummaryRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successSummaryDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_summaryDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get wallet transaction summary by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _walletTransactionSummaryRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_summaryDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get by wallet id
        public async Task<List<WalletTransactionSummaryModel>> GetByWalletIdAsync(int walletId)
        {
            return await _walletTransactionSummaryRepository.GetByWalletIdAsync(walletId);
        }
        #endregion

        #region Get list of wallet transaction summary by customer id
        public Task<List<WalletTransactionSummaryModel>> GetListByCustomerId(int customerId)
        {
            return _walletTransactionSummaryRepository.GetListByCustomerId(customerId);
        }
        #endregion

        #region Get Wallet Balance Amount
        public Task<decimal> GetWalletBalanceAmountAsync(int customerId, int walletId)
        {
            return _walletTransactionSummaryRepository.GetWalletBalanceAmountAsync(customerId, walletId);
        }
        #endregion

        #region Update wallet transaction summary
        public async Task<ResponseModel> UpdateAsync(WalletTransactionSummaryModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _walletTransactionSummaryRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successSummaryUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_summaryDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
