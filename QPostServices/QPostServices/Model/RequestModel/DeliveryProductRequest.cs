﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class DeliveryProductRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        public string? Image { get; set; }
        public int ListingOrder { get; set; }
        public bool IsOverseas { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public DeliveryProductModel ToCreateModel()
        {
            return new DeliveryProductModel()
            {
                Name = Name,
                Description = Description,
                Image = Image,
                ListingOrder = ListingOrder,
                IsOverseas = IsOverseas,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
        public DeliveryProductModel ToUpdateModel(int id)
        {
            return new DeliveryProductModel()
            {
                Name = Name,
                Description = Description,
                Image = Image,
                ListingOrder = ListingOrder,
                IsOverseas = IsOverseas,
                Id = id,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
