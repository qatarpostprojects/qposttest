﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IServiceTypeRepository
    {
        Task<List<ServiceTypeModel>> GetListAsync();
        Task<List<IdValueModel>> GetIdValueListAsync();
        Task<ServiceTypeModel> GetByIdAsync(int id);
        Task<int> CreateAsync(ServiceTypeModel model);
        Task<int> UpdateAsync(ServiceTypeModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
