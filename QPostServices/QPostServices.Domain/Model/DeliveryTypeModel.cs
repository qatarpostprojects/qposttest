﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class DeliveryTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string PriceType { get; set; } = string.Empty;
        public string? Image { get; set; }
        public IFormFile? ImageFile { get; set; } 
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public int ListingOrder { get; set; }
        public int DeliveryProductId { get; set; }
        public string DeliveryProductName { get; set; } = string.Empty;
        public string? ProductServiceCategory { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }
    }
}
