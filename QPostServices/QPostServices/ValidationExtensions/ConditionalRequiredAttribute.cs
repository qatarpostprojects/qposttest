﻿using System.ComponentModel.DataAnnotations;

namespace QPostServices.ValidationExtensions
{
    public class ConditionalRequiredAttribute : ValidationAttribute
    {
        private readonly string _dependentPropertyName;
        private readonly string _dependentPropertyValue;
        private readonly bool _dependentPropertyCondition;

        public ConditionalRequiredAttribute(string dependentPropertyName, string dependentPropertyValue)
        {
            _dependentPropertyName = dependentPropertyName;
            _dependentPropertyValue = dependentPropertyValue;
            _dependentPropertyCondition = true;
        }

        public ConditionalRequiredAttribute(string dependentPropertyName, string dependentPropertyValue, bool isEqual)
        {
            _dependentPropertyName = dependentPropertyName;
            _dependentPropertyValue = dependentPropertyValue;
            _dependentPropertyCondition = isEqual;
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            var dependentProperty = validationContext.ObjectType.GetProperty(_dependentPropertyName);

            if (dependentProperty == null)
            {
                return new ValidationResult($"Property '{_dependentPropertyName}' not found.");
            }

            var dependentPropertyValueObj = dependentProperty.GetValue(validationContext.ObjectInstance);
            var isValid = dependentPropertyValueObj == null || !(dependentPropertyValueObj.ToString().ToLower() == _dependentPropertyValue.ToLower());
            if (!_dependentPropertyCondition) 
            {
                isValid = !isValid;
            }
            if (isValid)
            {
                return ValidationResult.Success;
            }

            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}



