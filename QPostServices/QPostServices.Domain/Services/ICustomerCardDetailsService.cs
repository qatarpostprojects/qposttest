﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerCardDetailsService
    {
        Task<List<CustomerCardDetailsModel>> GetCardDetailsByCustomerId(int customerId);
        Task<ResponseModel> DeleteCardDetailsAsync(int id);
        Task<ResponseModel> UpdateCardDetailsAsync(CustomerCardDetailsModel model);
        Task<ResponseModel> CreateCardDetailsAsync(CustomerCardDetailsModel model);
        Task<ResponseModel> GetCardDetailsByIdAsync(int id);
    }
}
