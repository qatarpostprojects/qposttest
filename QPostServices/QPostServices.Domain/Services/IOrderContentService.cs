﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IOrderContentService
    {
        Task<List<OrderContentModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(OrderContentModel model);
        Task<ResponseModel> UpdateAsync(OrderContentModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
