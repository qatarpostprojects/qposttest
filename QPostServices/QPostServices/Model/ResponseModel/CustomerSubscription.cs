﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerSubscription
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? ServiceProductId { get; set; }
        public string PaymentChannel { get; set; } = string.Empty;
        public bool IsEligibleForRenew { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public int? LastPaymentTransactionId { get; set; }
        public bool ActiveStatus { get; set; }
        public string? Remarks { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? MaximumNumberOfTransaction { get; set; }
        public int? NumberOfTransactionPending { get; set; }
        public int? NumberOfTransactionCompleted { get; set; }
        public decimal? ServiceAmount { get; set; }
        public decimal? ServiceAmountPending { get; set; }
        public decimal? ServiceAmountConsumed { get; set; }
        public static CustomerSubscription FromModel(CustomerSubscriptionModel model)
        {
            return new CustomerSubscription
            {
                ActiveStatus = model.ActiveStatus,
                LastPaymentDate = model.LastPaymentDate,
                LastPaymentTransactionId = model.LastPaymentTransactionId,
                CreatedDate = model.CreatedDate,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CustomerId = model.CustomerId,
                EndDate = model.EndDate,
                Id = model.Id,
                IsEligibleForRenew = model.IsEligibleForRenew,
                ModifiedUser = model.ModifiedUser,
                PaymentChannel = model.PaymentChannel,
                ServiceProductId = model.ServiceProductId,
                RecordStatus = model.RecordStatus.ToString(),
                Reference1 = model.Reference1,
                Reference2 = model.Reference2,
                Remarks = model.Remarks,
                ServiceId = model.ServiceId,
                StartDate = model.StartDate,
                NumberOfTransactionCompleted = model.NumberOfTransactionCompleted,
                NumberOfTransactionPending = model.NumberOfTransactionPending,
                MaximumNumberOfTransaction= model.MaximumNumberOfTransaction,
                ServiceAmount = model.ServiceAmount,
                ServiceAmountConsumed = model.ServiceAmountConsumed,
                ServiceAmountPending = model.ServiceAmountPending
            };
        }
    }
}
