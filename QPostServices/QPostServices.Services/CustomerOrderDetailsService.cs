﻿using Newtonsoft.Json;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Extensions;
using QPostServices.Domain.Helper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Net;
using System.Text;

namespace QPostServices.Services
{
    public class CustomerOrderDetailsService : ICustomerOrderDetailsService
    {
        private const string _successCreation = "Order created successfully";
        private const string _successUpdate = "Order updated successfully";
        private const string _failedCreation = "Order creation failed";
        private const string _failedUpdation = "Order updation failed";
        private const string _successDelete = "Order deleted successfully";
        private const string _failedDelete = "Order deletion failed";
        private const string _nameAlreadyExist = "Order with id : {0} already exist";
        private const string _orderDoesNotExist = "Order with id : {0} Does not exist";
        private const string _orderwithTrackDoesNotExist = "Order with tracking number : {0} Does not exist";

        private readonly ICustomerOrderDetailsRepository _customerOrderDetailsRepository;
        private readonly IInboundRequestRepository _inboundRequestRepository;
        private readonly ITrackService _trackService;
        private readonly ILoginService _loginService;
        private readonly ICustomerService _customerService;
        private readonly AppSettings _appSettings;
        public CustomerOrderDetailsService(ICustomerOrderDetailsRepository customerOrderDetailsRepository,
            ILoginService loginService, AppSettings appSettings, ITrackService trackService,
            IInboundRequestRepository inboundRequestRepository, ICustomerService customerService)
        {
            _customerOrderDetailsRepository = customerOrderDetailsRepository;
            _loginService = loginService;
            _appSettings = appSettings;
            _trackService = trackService;
            _inboundRequestRepository = inboundRequestRepository;
            _customerService = customerService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(CustomerOrdersDetailsModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _customerOrderDetailsRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _customerOrderDetailsRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_orderDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All
        public async Task<List<CustomerOrdersDetailsModel>> GetAllActiveAsync()
        {
            return await _customerOrderDetailsRepository.GetListAsync();
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _customerOrderDetailsRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_orderDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(CustomerOrdersDetailsModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUser = _loginService.LoggedInUser();
            var result = await _customerOrderDetailsRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_orderDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion


        #region CreateOrderDetails
        public async Task<ResponseModel> CreateOrderAsync(CreateOrderModel model)
        {
            try
            {
                var response = await ExternalCreateOrder(model.ToExternalModel());
                if (response != null && !string.IsNullOrEmpty(response.Order_ID))
                {
                    model.CustomerId = _loginService.LoggedInCustomer();
                    model.CreatedUser = _loginService.LoggedInUser();
                    model.MerchantTrackingNo = response.Order_ID;
                    model.TrackingNumber = response.Order_ID;
                    var orderId = await _customerOrderDetailsRepository.CreateOrderAsync(model);
                    if (orderId > 0)
                    {
                        return new ResponseModel
                        {
                            Error = false,
                            Response = new CreateOrderResponse
                            {
                                OrderId = orderId,
                                MerchantTrackingNo = model.MerchantTrackingNo,
                                TrackingNumber = model.TrackingNumber
                            },
                            StatusCode = HttpStatusCode.OK
                        };
                    }
                    return new ResponseModel
                    {
                        Error = true,
                        StatusCode = HttpStatusCode.BadRequest
                    };
                }
                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest
                };
            }
            catch (Exception)
            {

                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest
                };
            }
        }
        #endregion

        #region Get Order By Id
        public async Task<ResponseModel> GetOrderByIdOrTrackingNumberAsync(string trackingNumber)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _customerOrderDetailsRepository.GetOrderByIdOrTrackingNumberAsync(trackingNumber);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    //response.Response = string.Format(_addressDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region UpdateOrderDetails
        public async Task<ResponseModel> UpdateOrderAsync(int id, CreateOrderModel model)
        {
            try
            {
                var response = await ExternalUpdateOrder(id, model.ToExternalModel());
                if (response != null && !string.IsNullOrEmpty(response.Order_ID))
                {
                    model.CustomerId = _loginService.LoggedInCustomer();
                    model.CreatedUser = _loginService.LoggedInUser();
                    model.OrderAWBNumber = response.Order_ID;
                    model.MerchantTrackingNo = response.Order_ID;
                    var orderId = await _customerOrderDetailsRepository.UpdateOrderAsync(id, model);
                    if (orderId > 0)
                    {
                        return new ResponseModel
                        {
                            Error = false,
                            Response = _successCreation,
                            StatusCode = HttpStatusCode.OK
                        };
                    }
                    return new ResponseModel
                    {
                        Error = true,
                        StatusCode = HttpStatusCode.BadRequest
                    };
                }
                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest
                };
            }
            catch (Exception)
            {

                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest
                };
            }
        }
        #endregion

        public async Task<List<GetOrderDetails>> GetOrdersByFilterAsync(string? phoneNumber, string? poBoxNo, string? type, FilterDateTypes? filterDate)
        {
            var orderdetails = new List<GetOrderDetails>();

            DateTime? startDate = null;
            DateTime? endDate = null;
            if (filterDate != null)
            {
                var dateRange = ((FilterDateTypes)filterDate).GetDateRange();

                startDate = dateRange.StartDate;
                endDate = dateRange.EndDate;
            }


            var orders = await _customerOrderDetailsRepository.GetOrdersByFilterAsync(phoneNumber, poBoxNo, type, startDate, endDate);
            foreach (var order in orders)
            {
                var track = await _trackService.TrackAndTrace(new TrackAndTraceModel { req_Input = order.MerchantTrackingNo });
                orderdetails.Add(new GetOrderDetails
                {
                    OrderId = order.Id,
                    MailFlow = order.MailFlow,
                    CODRequired = order.CODRequired,

                    CurrentStatusCode = order.CurrentStatusCode,
                    CurrentStatusDescription = order.CurrentStatusDescription,
                    CurrentStatusCategory = order.CurrentStatusCategory,
                    CurrentStatusCategoryAr = order.CurrentStatusCategoryAr,
                    CurrentStatusDescriptionAr = order.CurrentStatusDescriptionAr,
                    CurrentStatusLevel = order.CurrentStatusLevel,

                    TotalAmountToCollect = order.TotalAmountToCollect,
                    TotalAmountToPaid = order.TotalAmountToPaid,
                    ReceiverAddressId = order.ReceiverAddressId,
                    ReceiverBoxNumber = order.ReceiverBoxNumber,
                    DeliveryDate = order.DeliveryDate,
                    MerchantTrackingNo = order.MerchantTrackingNo,
                    OrderDate = order.OrderDate,
                    TrackingNumber = order.TrackingNumber,
                    OrderImage = order.OrderImage,
                    BranchName = order.BranchName,
                    ItemValue = order.ItemValue,
                    ItemDescription = order.ItemDescription,
                    OtherDetails = track
                });
            }

            return orderdetails;
        }
        #region Get My orders
        public async Task<List<GetOrderDetails>> GetMyOrdersAsync(int customerId, string? type)
        {
            var orderdetails = new List<GetOrderDetails>();
            var orders = await _customerOrderDetailsRepository.GetOrdersByCustomerId(customerId, type);
            foreach (var order in orders)
            {
                var track = await _trackService.TrackAndTrace(new TrackAndTraceModel { req_Input = order.MerchantTrackingNo });
                orderdetails.Add(new GetOrderDetails
                {
                    OrderId = order.Id,
                    MailFlow = order.MailFlow,
                    CODRequired = order.CODRequired,
                    CurrentStatusCode = order.CurrentStatusCode,
                    CurrentStatusDescription = order.CurrentStatusDescription,
                    CurrentStatusCategory= order.CurrentStatusCategory,
                    CurrentStatusCategoryAr = order.CurrentStatusCategoryAr,    
                    CurrentStatusDescriptionAr= order.CurrentStatusDescriptionAr,
                    CurrentStatusLevel = order.CurrentStatusLevel,
                    TotalAmountToCollect = order.TotalAmountToCollect,
                    TotalAmountToPaid = order.TotalAmountToPaid,
                    ReceiverAddressId = order.ReceiverAddressId,
                    ReceiverBoxNumber = order.ReceiverBoxNumber,
                    DeliveryDate = order.DeliveryDate,
                    MerchantTrackingNo = order.MerchantTrackingNo,
                    OrderDate = order.OrderDate,
                    TrackingNumber = order.TrackingNumber,
                    BranchName = order.BranchName,
                    OrderImage = order.OrderImage,
                    ItemDescription = order.ItemDescription,
                    ItemValue = order.ItemValue,
                    OtherDetails = track
                });
            }

            return orderdetails;
        }
        #endregion

        #region Get Latest Tracking Details
        public async Task<ResponseModel> GetLatestTrackingDetails(string? type)
        {
            var response = new ResponseModel();
            var customerId = _loginService.LoggedInCustomer();

            var orderdetails = new GetOrderDetails();
            var latestOrder = await _customerOrderDetailsRepository.GetLatestOrderByCustomerId(customerId, type);

            if (latestOrder != null)
            {
                var track = await _trackService.TrackAndTrace(new TrackAndTraceModel { req_Input = latestOrder.MerchantTrackingNo });
                orderdetails.OtherDetails = track;

                orderdetails.OrderId = latestOrder.Id;
                orderdetails.MailFlow = latestOrder.MailFlow;
                orderdetails.CODRequired = latestOrder.CODRequired;
                
                orderdetails.CurrentStatusCode = latestOrder.CurrentStatusCode;
                orderdetails.CurrentStatusDescription = latestOrder.CurrentStatusDescription;
                orderdetails.CurrentStatusDescriptionAr = latestOrder.CurrentStatusDescriptionAr;
                orderdetails.CurrentStatusCategory= latestOrder.CurrentStatusCategory;
                orderdetails.CurrentStatusCategoryAr = latestOrder.CurrentStatusCategoryAr;
                orderdetails.CurrentStatusLevel= latestOrder.CurrentStatusLevel;
                
                orderdetails.TotalAmountToCollect = latestOrder.TotalAmountToCollect;
                orderdetails.TotalAmountToPaid = latestOrder.TotalAmountToPaid;
                orderdetails.ReceiverAddressId = latestOrder.ReceiverAddressId;
                orderdetails.ReceiverBoxNumber = latestOrder.ReceiverBoxNumber;
                orderdetails.OrderDate = latestOrder.OrderDate;
                orderdetails.BranchName = latestOrder.BranchName;
                orderdetails.OrderImage = latestOrder.OrderImage;
                orderdetails.DeliveryDate = latestOrder.DeliveryDate;
                orderdetails.MerchantTrackingNo = latestOrder.MerchantTrackingNo;
                orderdetails.TrackingNumber = latestOrder.TrackingNumber;
                orderdetails.ItemDescription = latestOrder.ItemDescription;
                orderdetails.ItemValue = latestOrder.ItemValue;

                response.Response = orderdetails;
                response.Error = false;
                response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.StatusCode = HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Create inbound request
        public async Task<ResponseModel> CreateInboundRequestAsync(InboundModel model)
        {
            var userId = _loginService.LoggedInUser();
            var order = await _customerOrderDetailsRepository.GetByIdAsync(model.OrderId);
            if (order != null)
            {
                var types = await _inboundRequestRepository.GetMasterLookUpAsync();
                //var typeVal = types.FirstOrDefault(x => x.Id == model.TypeId)?.Value ?? string.Empty;
                if (IsAvailbleForAction(order.CurrentStatusCode, model.TypeId, order))
                {
                    var inboundRequest = new InboundRequestModel
                    {
                        AddressId = order.ReceiverAddressId,
                        BoxNumber = order.ReceiverBoxNumber ?? string.Empty,
                        CODAmount = order.TotalAmountToCollect,
                        CodRequired = order.CODRequired,
                        CreatedUser = userId,
                        CurrentStatus = order.CurrentStatusCode,
                        CustomerId = order.CustomerId,
                        HoldDays = model.Holddays,
                        PaidAmount = order.TotalAmountToPaid,
                        PaymentType = model.PaymentType,
                        Remarks = model.Remarks,
                        RequestTypeId = model.TypeId,
                        SpecialInstructions = model.SpecialInstructions
                    };
                    var result = await _inboundRequestRepository.CreateAsync(inboundRequest);
                    return new ResponseModel
                    {
                        Error = false,
                        StatusCode = HttpStatusCode.OK,
                        Response = "Inbound request created successfully"
                    };
                }
                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest,
                    Response = "You are not eligble for this service"
                };
            }
            return new ResponseModel
            {
                Error = true,
                StatusCode = HttpStatusCode.NotFound,
                Response = "Order not found"
            };
        }
        #endregion



        #region IsAvailbleForAction
        private bool IsAvailbleForAction(int statusCode, int typeId, CustomerOrdersDetailsModel order)
        {
            switch (typeId)
            {
                case 100:
                    return order.IsHDS ?? false;
                case 101:
                    return order.IsHold ?? false;
                case 102:
                    return true;
                case 103:
                    return true;
                case 105:
                    return order.IsBranch ?? false;
                default:
                    return false;
            }
        }
        #endregion

        #region ExternalCreateOrder
        private async Task<CreateOrderResponseModel?> ExternalCreateOrder(ExternalCreateOrderModel model)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, $"{_appSettings.UATApiUrl}ReceiveOrderREST/CreateOrder");
            request.Headers.Add("Accept", "application/json");
            var requestModel = JsonConvert.SerializeObject(model);
            request.Content = new StringContent(requestModel, Encoding.UTF8, "application/json");
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var orderModel = JsonConvert.DeserializeObject<CreateOrderResponseModel>(await response.Content.ReadAsStringAsync());
                    return orderModel;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            else
            {
                return null;
            }
        }
        #endregion

        #region  Get By Tracking Number
        public async Task<ResponseModel> GetByTrackingNumberAsync(string trackingNumber)
        {
            var response = new ResponseModel();

            var result = await _customerOrderDetailsRepository.GetByTrackingNumberAsync(trackingNumber);
            if (result != null)
            {
                var track = await _trackService.TrackAndTrace(new TrackAndTraceModel { req_Input = result.MerchantTrackingNo });
                result.OtherDetails = track;
            }

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_orderwithTrackDoesNotExist, trackingNumber);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region ExternalUpdateOrder
        private async Task<CreateOrderResponseModel?> ExternalUpdateOrder(int id, ExternalCreateOrderModel model)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, $"{_appSettings.UATApiUrl}ReceiveOrderREST/CreateOrder");
            request.Headers.Add("Accept", "application/json");
            var requestModel = JsonConvert.SerializeObject(model);
            request.Content = new StringContent(requestModel, Encoding.UTF8, "application/json");
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var orderModel = JsonConvert.DeserializeObject<CreateOrderResponseModel>(await response.Content.ReadAsStringAsync());
                    return orderModel;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            else
            {
                return null;
            }
        }




        #endregion
        public async Task<List<CustomerOrdersDetailsModel>> GetByCustomerIdAsync(int customerId)
        {
            return await _customerOrderDetailsRepository.GetOrdersByCustomerId(customerId, null);
        }

        public async Task<ResponseModel> CheckOrderDetailAsync(string trackingNumber, string? phoneNumber, string? POBoxNumber)
        {
            var checkedData = await _customerOrderDetailsRepository.CheckOrderDetailAsync(trackingNumber, phoneNumber, POBoxNumber);
            if (checkedData == null)
            {
                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.NotFound
                };
            }
            if (checkedData.IsOwnOrder)
            {
                return new ResponseModel
                {
                    Error = false,
                    StatusCode = HttpStatusCode.OK,
                    Response = (await GetByTrackingNumberAsync(trackingNumber)).Response
                };
            }
            //SendOTP


            if (!string.IsNullOrEmpty(checkedData.ActualPhoneNo))
            {
                var otpModel = new OtpModel
                {
                    Note = OtpNote.TrackAndTrace.ToString(),
                    Type = OtpType.SMS.ToString(),
                    Value = checkedData.ActualPhoneNo
                };

                await _customerService.SendOtpAsync(otpModel);
                return new ResponseModel
                {
                    Error = false,
                    Response = checkedData.ActualPhoneNo
                };
            }
            return new ResponseModel
            {
                Error = true,
                StatusCode = HttpStatusCode.NotFound,
            };
        }
    }
}
