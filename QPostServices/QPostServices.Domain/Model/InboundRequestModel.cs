﻿using QPostServices.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Domain.Model
{
    public class InboundRequestModel
    {
        public string BoxNumber { get; set; } = string.Empty;
        public int CustomerId { get; set; } 
        public DateTime RequestedDate { get; set; } 
        public int? AddressId { get; set; } 
        public int RequestTypeId { get; set; } 
        public int? HoldDays { get; set; } 
        public string? SpecialInstructions { get; set; } 
        public string? Remarks { get; set; } 
        public int CurrentStatus { get; set; } 
        public RecordStatus RecordStatus { get; set; }
        public bool CodRequired { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal? CODAmount { get; set; }
        public string PaymentType { get; set; } = string.Empty;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
    public class InboundModel
    {
        public int OrderId { get; set; }
        public int TypeId { get; set; }
        public int? Holddays { get; set; }
        public string? SpecialInstructions { get; set; }
        public string? Remarks { get; set; }

        public string PaymentType { get; set; } = string.Empty;
    }
}
