﻿using Dapper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace QPostServices.Storage
{
    public class SubscriptionTransactionNumber : ISubscriptionTransactionNumber
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public SubscriptionTransactionNumber(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }
        #region GetLatestAsync
        public async Task<TransactionNumberModel> GetLatestAsync(int serviceId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransactionNumber_GetLatest]";
                var parameters = new DynamicParameters();
                parameters.Add("@serviceId", serviceId);
                var result = await connection.QueryAsync<TransactionNumberModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
