﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class LoginLogsModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string IPAddress { get; set; } = string.Empty;
        public string DeviceName { get; set; } = string.Empty;
        public DateTime LoginDate { get; set; }
        public string LoginStatus { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
    }
}
