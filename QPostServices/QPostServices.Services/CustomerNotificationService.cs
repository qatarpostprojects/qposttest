﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Reflection;

namespace QPostServices.Services
{
    public class CustomerNotificationService : ICustomerNotificationService
    {
        private const string _cardDoesNotExist = "Notification with id {0} does not exists";
        private const string _successCardCreation = "Notification created successfully";
        private const string _failedCardCreation = "Notification creation failed";
        private const string _successCardUpdate = "Notification with id {0} updated successfully";

        private readonly ICustomerNotificationRepository _customerNotificationRepository;
        private readonly ILoginService _loginService;
        public CustomerNotificationService(ICustomerNotificationRepository customerNotificationRepository, ILoginService loginService)
        {
            _customerNotificationRepository = customerNotificationRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateNotificationAsync(CustomerNotificationModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInCustomer();
                var result = await _customerNotificationRepository.CreateNotificationAsync(model);

                if (result > 0)
                {
                    response.Response = _successCardCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCardCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get all by filter 
        public async Task<List<CustomerNotificationModel>> GetAllNotificationsAsync(CustomerNotificationFetchModel model)
        {
            return await _customerNotificationRepository.GetAllNotificationsAsync(model);
        }
        #endregion

        #region Update status
        public async Task<ResponseModel> UpdateReadStatusAsync(int id, bool isRead)
        {
            try
            {
                var response = new ResponseModel();
                var result = await _customerNotificationRepository.UpdateReadStatusAsync(id, isRead);

                if (result > 0)
                {
                    response.Response = string.Format(_successCardUpdate, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_cardDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        } 
        #endregion
    }
}
