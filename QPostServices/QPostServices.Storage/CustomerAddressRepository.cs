﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class CustomerAddressRepository : ICustomerAddressRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerAddressRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create Customer Address
        public async Task<int> CreateAsync(CustomerAddressModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerAddress_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@name", model.Name);
                parameters.Add("@phone", model.Phone);
                parameters.Add("@email", model.Email);
                parameters.Add("@isPrimary", model.IsPrimary);
                parameters.Add("@addressType", model.AddressType);
                parameters.Add("@city", model.City);
                parameters.Add("@pobox", model.PoBox);
                parameters.Add("@smartBox", model.SmartBox);
                parameters.Add("@zoneNumber", model.ZoneNumber);
                parameters.Add("@streetNumber", model.StreetNumber);
                parameters.Add("@buildingNumber", model.BuildingNumber);
                parameters.Add("@floorNumber", model.FloorNumber);
                parameters.Add("@unitNumber", model.UnitNumber);
                parameters.Add("@geoLocationX", model.GeoLocationX);
                parameters.Add("@geoLatitude", model.GeoLatitude);
                parameters.Add("@geoLongitude", model.GeoLongitude);
                parameters.Add("@addressDescription", model.AddressDescription);
                parameters.Add("@country", model.Country);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@address1", model.Address1);
                parameters.Add("@address2", model.Address2);
                parameters.Add("@zipCode", model.ZipCode);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAddressesListByCustomerId
        public async Task<List<CustomerAddressModel>> GetAddressesListByCustomerId(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerAddress_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<CustomerAddressModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetByIdAsync
        public async Task<CustomerAddressModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerAddress_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<CustomerAddressModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public async Task<CustomerAddressModel?> GetPrimaryAddressByCustomerId(int customerId, AddressType type)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerAddress_GetPrimaryByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                parameters.Add("@addressType", (int)type);
                var result = await connection.QueryAsync<CustomerAddressModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateAsync
        public async Task<int> UpdateAsync(CustomerAddressModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerAddress_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@phone", model.Phone);
                parameters.Add("@email", model.Email);
                parameters.Add("@name", model.Name);
                parameters.Add("@isPrimary", model.IsPrimary);
                parameters.Add("@addressType", model.AddressType);
                parameters.Add("@city", model.City);
                parameters.Add("@pobox", model.PoBox);
                parameters.Add("@smartBox", model.SmartBox);
                parameters.Add("@zoneNumber", model.ZoneNumber);
                parameters.Add("@streetNumber", model.StreetNumber);
                parameters.Add("@buildingNumber", model.BuildingNumber);
                parameters.Add("@floorNumber", model.FloorNumber);
                parameters.Add("@unitNumber", model.UnitNumber);
                parameters.Add("@geoLocationX", model.GeoLocationX);
                parameters.Add("@geoLatitude", model.GeoLatitude);
                parameters.Add("@geoLongitude", model.GeoLongitude);
                parameters.Add("@addressDescription", model.AddressDescription);
                parameters.Add("@country", model.Country);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@address1", model.Address1);
                parameters.Add("@address2", model.Address2);
                parameters.Add("@zipCode", model.ZipCode);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region UpdateStatusAsync
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerAddress_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
