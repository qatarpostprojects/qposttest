﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerProfileImage
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string? Image { get; set; }
        public string FileType { get; set; } = string.Empty;
        public static CustomerProfileImage FromModel(CustomerProfileImageModel model)
        {
            return new CustomerProfileImage { Id = model.Id, CustomerId = model.CustomerId, Image = model.Image,FileType=model.FileType };
        }
    }
}
