﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class OrderSyncStatusRequest
    {
        [Required]
        public string TrackingNumber { get; set; } = string.Empty;
        [Required]
        public DateTime SyncDate { get; set; }
        [Required]
        public string SyncStatus { get; set; } = string.Empty;
        public string? ErrorDescription { get; set; }
        public string? Remarks { get; set; }

        public OrderSyncStatusModel ToModel()
        {
            return new OrderSyncStatusModel
            {
                TrackingNumber = TrackingNumber,
                SyncDate = SyncDate,
                SyncStatus = SyncStatus,
                ErrorDescription = ErrorDescription,
                Remarks = Remarks
            };
        }
    }
}
