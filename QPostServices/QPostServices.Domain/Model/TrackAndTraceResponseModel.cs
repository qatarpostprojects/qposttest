﻿namespace QPostServices.Domain.Model
{
    public class TrackAndTraceResponseModel
    {
        public List<TrackAndTraceResponse> response { get; set; }
    }

    public class TrackAndTraceResponse
    {
        public LineItemDtls line_item_dtls { get; set; }
        public int total_count { get; set; }
        public List<TrackTraceDetails> track_trace_details { get; set; }
    }

    public class LineItemDtls
    {
        public string country_of_origin { get; set; }
        public string customer_id { get; set; }
        public string dispatch_number { get; set; }
        public string exp_delivery_time { get; set; }
        public string liveTrackingEligible { get; set; }
        public string order_id { get; set; }
        public string order_placed { get; set; }
        public string product_service_code { get; set; }
        public RecepientDtls recepient_dtls { get; set; }
        public string receptacle_number { get; set; }
        public string signatory_id { get; set; }
        public string signatory_mobile { get; set; }
        public string signatory_name { get; set; }
        public string sla_in_hours { get; set; }
        public string track_facility { get; set; }
        public string tracking_number { get; set; }
    }

    public class RecepientDtls
    {
        public string building_no { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string floor { get; set; }
        public string last_name { get; set; }
        public string middle_name { get; set; }
        public string mobileNo { get; set; }
        public string po_box_no { get; set; }
        public string postal_code { get; set; }
        public string qatar_id { get; set; }
        public string region { get; set; }
        public string street_name { get; set; }
        public string street_no { get; set; }
        public string town { get; set; }
        public string zone { get; set; }
    }

    public class TrackTraceDetails
    {
        public string branch { get; set; }
        public string date_time { get; set; }
        public string event_code { get; set; }
        public string event_name_ar { get; set; }
        public string event_name_en { get; set; }
        public string expected_time { get; set; }
        public string product_service_code { get; set; }
        public string remarks { get; set; }
        public string source { get; set; }
        public string visible_digital_channel { get; set; }
    }
}
