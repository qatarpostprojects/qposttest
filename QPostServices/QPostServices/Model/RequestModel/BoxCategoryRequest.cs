﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class BoxCategoryRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string Description { get; set; } = string.Empty;

        public BoxCategoryModel ToCreateModel()
        {
            return new BoxCategoryModel
            {
                Name = Name,
                Description = Description
            };
        }

        public BoxCategoryModel ToUpdateModel(int id)
        {
            return new BoxCategoryModel
            {
                Id = id,
                Name = Name,
                Description = Description
            };
        }
    }
}
