﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = default!;
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
        public string? Name { get; set; }
        public string DocumentType { get; set; } = default!;
        public string? DocumentReference { get; set; }
        public string MobileNumber { get; set; } = default!;
        public string? AlternativeMobileNumber { get; set; }
        public string Nationality { get; set; } = default!;
        public DateTime? DateOfBirth { get; set; }
        public string PoBoxNumber { get; set; } = default!;
        public string ConnectedVpNumber { get; set; } = default!;
        public string? SmartBoxNumber { get; set; }
        public DateTime? PoBoxExpiry { get; set; }
        public DateTime? SmartBoxExpiry { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int? WalletId { get; set; }
        public decimal? WalletBalanceAmount { get; set; }
        public string? WalletReferenceNumber { get; set; }
        public string? HDS { get; set; }
        public int? HDSPackageId { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? Reference3 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string EmailAddress { get; set; } = string.Empty;
        public string? ProfileImage { get; set; }
        public string? FirstNameAr { get; set; }
        public string? MiddleNameAr { get; set; }
        public string? LastNameAr { get; set; }
        public string? NameAr { get; set; }
    }
}
