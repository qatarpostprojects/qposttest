﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class UserSessionModel
    {
        public int Id { get; set; }
        public string UserSessionValue { get; set; } = string.Empty;
        public UserSessionStatus RecordStatus { get; set; }
        public string Prefix { get; set; } = string.Empty;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
