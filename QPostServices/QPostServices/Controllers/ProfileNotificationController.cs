﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileNotificationController : ControllerBase
    {
        private readonly IProfileNotificationSettingsService _profileNotificationSettingsService;
        public ProfileNotificationController(IProfileNotificationSettingsService profileNotificationSettingsService)
        {
            _profileNotificationSettingsService = profileNotificationSettingsService;
        }

        #region Create Settings
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateSettings))]
        public async Task<IActionResult> CreateSettings([FromBody] ProfileNotificationSettingsRequest request)
        {
            try
            {
                var result = await _profileNotificationSettingsService.CreateAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get settings by userid
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Wallet))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("User/{userId}", Name = nameof(GetSettings))]
        public async Task<IActionResult> GetSettings(int userId)
        {
            try
            {
                var result = await _profileNotificationSettingsService.GetByUserId(userId);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(ProfileNotificationSettings.FromModel((ProfileNotificationSettingsModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Settings
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut(Name = nameof(UpdateSettings))]
        public async Task<IActionResult> UpdateSettings([FromBody] ProfileNotificationSettingsRequest request)
        {
            try
            {
                var result = await _profileNotificationSettingsService.UpdateAsync(request.ToModel());

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
