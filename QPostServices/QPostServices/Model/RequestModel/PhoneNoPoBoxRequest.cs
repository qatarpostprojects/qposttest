﻿using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PhoneNoPoBoxRequest
    {
        [AnyRequired(nameof(PhoneNumber), ErrorMessage = "POBoxNumber is required if PhoneNumber is not provided.")]
        public string? POBoxNumber { get; set; }

        [AnyRequired(nameof(POBoxNumber), ErrorMessage = "PhoneNumber is required if POBoxNumber is not provided.")]
        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string? PhoneNumber { get; set; }
    }
}
