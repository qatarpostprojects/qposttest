﻿using QPostServices.Domain.Extensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.ValidationExtensions
{
    public class EnumRequiredAttribute : ValidationAttribute
    {
        private const string _invalidTypeErrorMessage = "{0} requires a valid {1} value";
        private const string _requiredErrorMessage = "Invalid {0}";
        private const string _invalidValueErrorMessage = "Only value(s) {0} are valid for {1}";
        private readonly Type _enumType;

        private readonly object[] _validOptions;

        public EnumRequiredAttribute(Type enumType)
            : base("Enumeration")
        {
            _enumType = enumType;
        }

        public EnumRequiredAttribute(Type enumType, params object[] validOptions)
            : base("Enumeration")
        {
            _enumType = enumType;
            _validOptions = validOptions;
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value != null)
            {
                if (!_enumType.IsEnum)
                {
                    throw new InvalidOperationException(string.Format(_invalidTypeErrorMessage, validationContext.DisplayName, _enumType.Name));
                }

                if (!Enum.IsDefined(_enumType, value))
                {
                    return new ValidationResult(string.Format(_requiredErrorMessage, validationContext.DisplayName));
                }
                else if (_validOptions != null)
                {
                    if (!_validOptions.Contains(Enum.Parse(_enumType, value.ToString())))
                    {
                        string validOptionNames = string.Join(", ", _validOptions.Select(x => (x as Enum).GetDescription()));
                        return new ValidationResult(string.Format(_invalidValueErrorMessage, validOptionNames, validationContext.DisplayName));
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
