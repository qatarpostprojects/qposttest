﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;
        private readonly ILoginService _loginService;
        private readonly IFileService _fileService;

        private const string _successCreation = "Country created successfully";
        private const string _successUpdate = "Country updated successfully";
        private const string _failedCreation = "Country creation failed";
        private const string _failedUpdation = "Country updation failed";
        private const string _successDelete = "Country deleted successfully";
        private const string _failedDelete = "Country deletion failed";
        private const string _nameAlreadyExist = "Country with name : {0} already exist";
        private const string _doesNotExist = "Country with id : {0} Does not exist";


        public CountryService(ICountryRepository countryRepository, ILoginService loginService, IFileService fileService)
        {
            _countryRepository = countryRepository;
            _loginService = loginService;
            _fileService = fileService;
        }
        public async Task<ResponseModel> CreateAsync(CountryModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();

            if (model.FlagFile != null)
            {
                model.Flag = await _fileService.ConvertImageToBase64(model.FlagFile);
            }

            var result = await _countryRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.CountryName);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }

        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _countryRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_doesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }

        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _countryRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_doesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }

        public Task<List<CountryModel>> GetListAsync(string? searchString)
        {
            return _countryRepository.GetListAsync(searchString);
        }

        public async Task<ResponseModel> UpdateAsync(CountryModel model)
        {
            var response = new ResponseModel();
            if (model.FlagFile != null)
            {
                model.Flag = await _fileService.ConvertImageToBase64(model.FlagFile);
            }
            model.ModifiedUser = _loginService.LoggedInUser();
            var result = await _countryRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.CountryName);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_doesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
    }
}