﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IDeliveryTypeService
    {
        Task<List<DeliveryTypeModel>> GetListAsync();
        Task<List<DeliveryTypeModel>> GetByProductIdAsync(int productId);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(DeliveryTypeModel model);
        Task<ResponseModel> UpdateAsync(DeliveryTypeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
