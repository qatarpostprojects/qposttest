﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class EventMaster
    {
        public int Id { get; set; }
        public int EventCode { get; set; }
        public string EventDescription { get; set; } = string.Empty;
        public string Remarks { get; set; } = string.Empty;
        public string RecordStatus { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }

        public static EventMaster FromModel(EventMasterModel model)
        {
            return new EventMaster
            {
                Id = model.Id,
                EventCode = model.EventCode,
                EventDescription = model.EventDescription,
                Remarks = model.Remarks,
                RecordStatus = model.RecordStatus.ToString(),
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                CreatedUserName = model.CreatedUserName
            };
        }
    }
}
