﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class BannerRepository : IBannerRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public BannerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(BannerModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Banners_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@bannerAr", model.BannerAr);
                parameters.Add("@bannerEng", model.BannerEng);
                parameters.Add("@device", model.Device);
                parameters.Add("@listingOrder", model.ListingOrder);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@active", true);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Device
        public async Task<List<BannerModel>> GetByDevice(string device)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Banners_GetByDevice]";
                var parameters = new DynamicParameters();
                parameters.Add("@device", device);
                var result = await connection.QueryAsync<BannerModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region Get by Id
        public async Task<BannerModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Banners_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<BannerModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault() ?? new BannerModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get
        public async Task<List<BannerModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Banners_Get]";
                var result = await connection.QueryAsync<BannerModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(BannerModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Banners_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@bannerAr", model.BannerAr);
                parameters.Add("@bannerEng", model.BannerEng);
                parameters.Add("@device", model.Device);
                parameters.Add("@listingOrder", model.ListingOrder);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, bool status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Banners_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
