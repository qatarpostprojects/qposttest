﻿using Microsoft.AspNetCore.Http;

namespace QPostServices.Domain.Model
{
    public class BannerModel
    {
        public int Id { get; set; }
        public string? BannerAr { get; set; }
        public IFormFile? BannerArFile { get; set; }
        public string? BannerEng { get; set; }
        public IFormFile? BannerEngFile { get; set; }
        public string Device { get; set; } = string.Empty;
        public int ListingOrder { get; set; }
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
