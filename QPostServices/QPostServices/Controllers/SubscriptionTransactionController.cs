﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubscriptionTransactionController : ControllerBase
    {
        private readonly ISubscriptionTransactionService _subscriptionTransactionService;
        public SubscriptionTransactionController(ISubscriptionTransactionService subscriptionTransactionService)
        {
            _subscriptionTransactionService = subscriptionTransactionService;
        }

        #region Create Subscription Transaction
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateSubscriptionTransaction))]
        public async Task<IActionResult> CreateSubscriptionTransaction([FromBody] SubscriptionTransactionRequest request)
        {
            try
            {
                var result = await _subscriptionTransactionService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Subscription Transaction by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SubscriptionTransaction))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetSubscriptionTransaction))]
        public async Task<IActionResult> GetSubscriptionTransaction(int id)
        {
            try
            {
                var result = await _subscriptionTransactionService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(SubscriptionTransaction.FromModel((SubscriptionTransactionModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Subscription Transactions
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SubscriptionTransaction>))]
        [HttpGet(Name = nameof(GetSubscriptionTransactionList))]
        public async Task<IActionResult> GetSubscriptionTransactionList()
        {
            try
            {
                var results = await _subscriptionTransactionService.GetAllActiveAsync();

                return Ok(results.Select(x => SubscriptionTransaction.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Subscription Transactions by Filter
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SubscriptionTransaction>))]
        [HttpGet("filter", Name = nameof(GetSubscriptionTransactionByFilter))]
        public async Task<IActionResult> GetSubscriptionTransactionByFilter([FromQuery]CustomerSubscriptionFilterRequest request)
        {
            try
            {
                var results = await _subscriptionTransactionService.GetByFilterAsync(request.CustomerId, request.ServiceId);

                return Ok(results.Select(x => SubscriptionTransaction.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Subscription Transaction
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteSubscriptionTransaction))]
        public async Task<IActionResult> DeleteSubscriptionTransaction(int id)
        {
            try
            {
                var result = await _subscriptionTransactionService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Subscription Transaction
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateSubscriptionTransaction))]
        public async Task<IActionResult> UpdateSubscriptionTransaction(int id, [FromBody] SubscriptionTransactionRequest request)
        {
            try
            {
                var result = await _subscriptionTransactionService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
