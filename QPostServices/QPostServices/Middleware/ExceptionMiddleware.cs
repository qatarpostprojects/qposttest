﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace QPostServices.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IExceptionLogService _exceptionLogService;

        public ExceptionMiddleware(RequestDelegate next,
            IExceptionLogService exceptionLogService)
        {
            _next = next;
            _exceptionLogService = exceptionLogService;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }

            catch (Exception ex)
            {
                var data = context.User.Claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier)?.Value;
                int? userId = string.IsNullOrEmpty(data) ? null : int.Parse(data);

                string requestBody = await FormatRequest(context.Request);

                var requestLog = new LogModel
                {
                    Method = context.Request.Method,
                    RequestName = context.Request.Path,
                    RequestUrl = $"{context.Request.Scheme}://{context.Request.Host}{context.Request.Path}",
                    QueryString = context.Request.QueryString.ToString(),
                    Body = requestBody,
                    CreatedDate = DateTime.Now,
                    Response = ex.Message + "-" + ex.InnerException?.Message,
                    CreatedUser = userId
                };

                await _exceptionLogService.CreateAsync(requestLog);

                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";

                await context.Response.WriteAsync(ex.Message + (ex.InnerException != null ? " - " + ex.InnerException.Message : ""));

            }
        }

        private async Task<string> FormatRequest(HttpRequest request)
        {
            request.EnableBuffering();

            using var reader = new StreamReader(request.Body, encoding: Encoding.UTF8, detectEncodingFromByteOrderMarks: false, leaveOpen: true);
            var body = await reader.ReadToEndAsync();
            request.Body.Seek(0, SeekOrigin.Begin);

            return body;
        }
    }
}
