﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class LockType
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; } 

        public static LockType FromModel(LockTypeModel model)
        {
            return new LockType
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                RecordStatus = model.RecordStatus,
                CreatedDate = model.CreatedDate,
                CreatedUserId = model.CreatedUserId,
                CreatedUserName= model.CreatedUserName,
                ModifiedDate= model.ModifiedDate,
                ModifiedUserId= model.ModifiedUserId,
                ModifiedUserName = model.ModifiedUserName
            };
        }
    }
}
