﻿namespace QPostServices.Domain.Enums
{
    public enum AddressType
    {
        PickUp = 1,
        Delivery = 2,
        Sender =3,
        Receiver = 4
    }
}
