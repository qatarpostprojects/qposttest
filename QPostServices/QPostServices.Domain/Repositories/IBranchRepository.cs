﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IBranchRepository
    {
        Task<List<BranchModel>> GetListAsync();
        Task<List<BranchModel>> GetNearestBranchesAsync(List<int?> branchIds);
        Task<int> CreateAsync(BranchModel model);
        Task<BranchModel?> GetByIdAsync(int id);
        Task<int> UpdateAsync(BranchModel model);
        Task<int> UpdateStatusAsync(int id, int userId);
    }
}
