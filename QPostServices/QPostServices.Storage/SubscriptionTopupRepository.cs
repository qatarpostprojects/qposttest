﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class SubscriptionTopupRepository : ISubscriptionTopupRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public SubscriptionTopupRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create SubscriptionTopup
        public async Task<int> CreateAsync(SubscriptionTopupModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTopup_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@amount", model.Amount);
                parameters.Add("@serviceId", model.ServiceId);
                parameters.Add("@currency", model.Currency);
                parameters.Add("@description", model.Description);
                parameters.Add("@validityDescription", model.ValidityDescription);
                parameters.Add("@validity", model.Validity);
                parameters.Add("@type", model.Type);
                parameters.Add("@active", true);
                parameters.Add("@createdUser", model.CreatedUserId);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@colorCode", model.ColorCode);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                parameters.Add("@validityDescriptionAr", model.ValidityDescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get SubscriptionTopup by Id
        public async Task<SubscriptionTopupModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTopup_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<SubscriptionTopupModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get SubscriptionTopup List By Service Id
        public async Task<List<SubscriptionTopupModel>> GetListByServiceIdAsync(int serviceId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTopup_GetByServiceId]";
                var parameters = new DynamicParameters();
                parameters.Add("@serviceId", serviceId);
                var result = await connection.QueryAsync<SubscriptionTopupModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get SubscriptionTopup List 
        public async Task<List<SubscriptionTopupModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTopup_Get]";
                var result = await connection.QueryAsync<SubscriptionTopupModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update SubscriptionTopup
        public async Task<int> UpdateAsync(SubscriptionTopupModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTopup_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@amount", model.Amount);
                parameters.Add("@currency", model.Currency);
                parameters.Add("@description", model.Description);
                parameters.Add("@validityDescription", model.ValidityDescription);
                parameters.Add("@validity", model.Validity);
                parameters.Add("@type", model.Type);
                parameters.Add("@active", true);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@modifiedUser", model.ModifiedUserId);
                parameters.Add("@id", model.Id);
                parameters.Add("@colorCode", model.ColorCode);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                parameters.Add("@validityDescriptionAr", model.ValidityDescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update SubscriptionTopup Active Status
        public async Task<int> UpdateStatusAsync(int id, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTopup_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", false);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
