﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class OrderContentRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; } 
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Icon { get; set; }
        public IFormFile? IconFile { get; set; }
        public int ListingOrder { get; set; }
        public OrderContentModel ToCreateModel()
        {
            return new OrderContentModel()
            {
                Name = Name,
                Description = Description,
                Icon = Icon,
                IconFile = IconFile,
                ListingOrder = ListingOrder,
                DescriptionAr = DescriptionAr,
                NameAr = NameAr,
            };
        }
        public OrderContentModel ToUpdateModel(int id)
        {
            return new OrderContentModel()
            {
                Id = id,
                Name = Name,
                Description = Description,
                Icon = Icon,
                IconFile = IconFile,
                ListingOrder = ListingOrder,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr,
            };
        }
    }
}
