﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface INotificationCategoryServices
    {
        Task<ResponseModel> CreateAsync(NotificationCategoryModel model);
        Task<List<NotificationCategoryModel>> GetListAsync();
    }
}
