﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class POBOxService
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Logo { get; set; }
        public int ServiceId { get; set; }
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static POBOxService FromModel(POBoxServiceModel model)
        {
            return new POBOxService()
            {
                Name = model.Name,
                Description = model.Description,
                Logo = model.Logo,
                Active = model.Active,
                CreatedUser = model.CreatedUser,
                CreatedDate = model.CreatedDate,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                Id = model.Id,
                DescriptionAr = model.DescriptionAr,
                NameAr = model.NameAr,
                ServiceId = model.ServiceId,
            };
        }
    }
}
