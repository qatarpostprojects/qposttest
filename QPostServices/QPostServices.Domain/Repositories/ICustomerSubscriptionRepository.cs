﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerSubscriptionRepository
    {
        Task<int> CreateAsync(CustomerSubscriptionModel model);
        Task<CustomerSubscriptionModel?> GetByIdAsync(int id);
        Task<List<int>> GetServiceIdsByCustomerId(int customerId);
        Task<List<CustomerSubscriptionDetailModel>> GetAllActiveByCustomerIdAsync(int customerId);
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> RenewAsync(CustomerSubscriptionRenewModel model);
        Task<int> UpdateAsync(CustomerSubscriptionModel model);
        Task<List<CustomerSubscriptionModel>> GetNearbyExpirySubscriptions();
        Task<CurrentServiceDetailsModel> GetByServiceAndCustomerAsync(int serviceId,int customerId);
    }
}
