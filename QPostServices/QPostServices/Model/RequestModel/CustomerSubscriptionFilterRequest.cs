﻿namespace QPostServices.Model.RequestModel
{
    public class CustomerSubscriptionFilterRequest
    {
        public int? CustomerId { get; set; }
        public int? ServiceId { get; set; }
    }
}
