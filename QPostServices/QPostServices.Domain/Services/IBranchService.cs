﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IBranchService
    {
        Task<List<BranchModel>> GetListAsync();
        Task<List<BranchModel>> GetNearestBranchesAsync(BranchLocationModel branchLocation);
        Task<ResponseModel> CreateAsync(BranchModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> UpdateAsync(BranchModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
