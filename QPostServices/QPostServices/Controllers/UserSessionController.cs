﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using QPostServices.Services;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserSessionController : ControllerBase
    {
        private readonly IUserSessionService _userSessionService;
        public UserSessionController(IUserSessionService userSessionService)
        {
            _userSessionService = userSessionService;
        }

        #region Create UserSession
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateSessionValue))]
        public async Task<IActionResult> CreateSessionValue([FromBody] UserSessionRequest request)
        {
            try
            {
                var result = await _userSessionService.CreateAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update status
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{sessionValue}/Status/{status}", Name = nameof(UpdateSessionValue))]
        public async Task<IActionResult> UpdateSessionValue(string sessionValue, UserSessionStatus status)
        {
            try
            {
                var result = await _userSessionService.UpdateStatusAsync(sessionValue, status);

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get userSession
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserSession))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{sessionValue}", Name = nameof(GetSession))]
        public async Task<IActionResult> GetSession(string sessionValue)
        {
            try
            {
                var result = await _userSessionService.GetBySessionValueAsync(sessionValue);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(UserSession.FromModel((UserSessionModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
