﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class POBox
    {
        public int Id { get; set; }
        public string POBoxNumber { get; set; } = string.Empty;
        public int POBoxTypeId { get; set; } 
        public string POBoxTypeName { get; set; } = string.Empty;
        public string POBoxSubType { get; set; } = string.Empty;
        public int BranchId { get; set; }
        public int Rented { get; set; }
        public string? POBoxLocationId { get; set; }
        public string? POBoxLocationName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime PaidUntil { get; set; }
        public DateTime LastPaidUntil { get; set; }
        public string? GeoLocationX { get; set; }
        public string? GeoLocationY { get; set; }
        public string? SmartBoxNumber { get; set; }
        public int POBoxCategoryID { get; set; }
        public string POBoxCategoryName { get; set; } = string.Empty;
        public int IsSmartBox { get; set; }
        public int POBoxPositionID { get; set; }
        public string POBoxPositionName { get; set; } = string.Empty;
        public int POBoxLockTypeID { get; set; }
        public string POBoxLockTypeName { get; set; } = string.Empty;
        public string? SmartCardNumber { get; set; }
        public string? Location { get; set; }
        public string? Reserved { get; set; }
        public bool Blocked { get; set; }
        public string? Remarks { get; set; }
        public string? GroupId { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public static POBox FromModel(POBoxModel model)
        {
            return new POBox
            {
                Id = model.Id,
                RecordStatus = model.RecordStatus,
                CreatedDate = model.CreatedDate,
                CreatedUserId = model.CreatedUserId,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserId = model.ModifiedUserId,
                ModifiedUserName = model.ModifiedUserName,
                Blocked = model.Blocked,
                Remarks = model.Remarks,
                BranchId = model.BranchId,
                GeoLocationX = model.GeoLocationX,
                GeoLocationY = model.GeoLocationY,
                GroupId = model.GroupId,
                IsSmartBox = model.IsSmartBox,
                LastPaidUntil = model.LastPaidUntil,
                Location = model.Location,
                PaidUntil = model.PaidUntil,
                POBoxCategoryID = model.POBoxCategoryID,
                POBoxLocationId = model.POBoxLocationId,
                POBoxLocationName = model.POBoxLocationName,
                POBoxLockTypeID = model.POBoxLockTypeID,
                POBoxNumber = model.POBoxNumber,
                POBoxPositionID = model.POBoxPositionID,
                POBoxSubType = model.POBoxSubType,
                POBoxTypeId = model.POBoxTypeId,
                POBoxTypeName = model.POBoxTypeName,
                Rented = model.Rented,
                Reserved = model.Reserved,
                SmartBoxNumber = model.SmartBoxNumber,
                SmartCardNumber = model.SmartCardNumber,
                StartDate = model.StartDate,
                POBoxCategoryName = model.POBoxCategoryName,
                POBoxLockTypeName = model.POBoxLockTypeName,
                POBoxPositionName = model.POBoxPositionName
            };
        }
    }
}
