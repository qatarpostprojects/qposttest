﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPositionServices
    {
        Task<List<PositionModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(PositionModel model);
        Task<ResponseModel> UpdateAsync(PositionModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
