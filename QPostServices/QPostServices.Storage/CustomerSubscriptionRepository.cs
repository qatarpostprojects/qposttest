﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class CustomerSubscriptionRepository : ICustomerSubscriptionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerSubscriptionRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create Customer Subscription
        public async Task<int> CreateAsync(CustomerSubscriptionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@serviceId", model.ServiceId);
                parameters.Add("@subscriptionStartDate", model.StartDate);
                parameters.Add("@subscriptionEndDate", model.EndDate);
                parameters.Add("@serviceProductid", model.ServiceProductId);
                parameters.Add("@paymentChannel", model.PaymentChannel);
                parameters.Add("@eligibleForRenew", model.IsEligibleForRenew);
                parameters.Add("@lastPaymentDate", model.LastPaymentDate);
                parameters.Add("@lastPaymentTransactionId", model.LastPaymentTransactionId);
                parameters.Add("@activeStatus", model.ActiveStatus);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@maximumNumberOfTransaction", model.MaximumNumberOfTransaction);
                parameters.Add("@numberOfTransactionPending", model.NumberOfTransactionPending);
                parameters.Add("@numberOfTransactionCompleted", model.NumberOfTransactionCompleted);
                parameters.Add("@serviceAmount", model.ServiceAmount);
                parameters.Add("@serviceAmountPending", model.ServiceAmountPending);
                parameters.Add("@serviceAmountConsumed", model.ServiceAmountConsumed);

                parameters.Add("@qatarIdBackSide", model.QatarIdBackSide);
                parameters.Add("@qatarIdFrontSide", model.QatarIdFrontSide);
                parameters.Add("@companyDocBackSide", model.CompanyDocBackSide);
                parameters.Add("@companyDocFrontSide", model.CompanyDocFrontSide);

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAllActiveAsync
        public async Task<List<CustomerSubscriptionDetailModel>> GetAllActiveByCustomerIdAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<CustomerSubscriptionDetailModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetByIdAsync
        public async Task<CustomerSubscriptionModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<CustomerSubscriptionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Service Ids By CustomerId
        public async Task<List<int>> GetServiceIdsByCustomerId(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_GetServiceByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetNearbyExpirySubscriptions
        public async Task<List<CustomerSubscriptionModel>> GetNearbyExpirySubscriptions()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_GetNearbyExpiry]";
                var parameters = new DynamicParameters();
                parameters.Add("@startDate", DateTime.Now);
                parameters.Add("@endDate", DateTime.Now.AddMonths(1));

                var result = await connection.QueryAsync<CustomerSubscriptionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region RenewAsync
        public async Task<int> RenewAsync(CustomerSubscriptionRenewModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_Renew]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.SubscriptionId);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@subscriptionEndDate", model.EndDate);
                parameters.Add("@maximumNumberOfTransaction", model.MaximumNumberOfTransaction);
                parameters.Add("@numberOfTransactionPending", model.NumberOfTransactionPending);
                parameters.Add("@serviceAmount", model.ServiceAmount);
                parameters.Add("@serviceAmountPending", model.ServiceAmountPending);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region UpdateAsync
        public async Task<int> UpdateAsync(CustomerSubscriptionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@serviceProductid", model.ServiceProductId);
                parameters.Add("@paymentChannel", model.PaymentChannel);
                parameters.Add("@eligibleForRenew", model.IsEligibleForRenew);
                parameters.Add("@activeStatus", model.ActiveStatus);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region UpdateStatusAsync
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerSubscription_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Service And Customer
        public async Task<CurrentServiceDetailsModel> GetByServiceAndCustomerAsync(int serviceId, int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_CustomerSubscription_GetRenewData";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                parameters.Add("@serviceId", serviceId);
                var result = await connection.QueryAsync<CurrentServiceDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault()??new CurrentServiceDetailsModel();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
