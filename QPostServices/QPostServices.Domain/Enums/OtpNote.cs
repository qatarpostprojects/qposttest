﻿namespace QPostServices.Domain.Enums
{
    public enum OtpNote
    {
        ChangePassword = 1,
        EditProfile = 2,
        Address = 3,
        TrackAndTrace = 4,
        MapExisting = 5,
        CustomerRegistration = 6
    }
}
