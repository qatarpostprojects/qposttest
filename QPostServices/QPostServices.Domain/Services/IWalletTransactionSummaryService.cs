﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IWalletTransactionSummaryService
    {
        Task<ResponseModel> CreateAsync(WalletTransactionSummaryModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<WalletTransactionSummaryModel>> GetByWalletIdAsync(int walletId);
        Task<List<WalletTransactionSummaryModel>> GetListByCustomerId(int customerId);
        Task<decimal> GetWalletBalanceAmountAsync(int customerId, int walletId);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(WalletTransactionSummaryModel model);
    }
}
