﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPickupTypeRepository
    {
        Task<List<PickupTypeModel>> GetListAsync();
        Task<int> CreateAsync(PickupTypeModel model);
        Task<int> UpdateAsync(PickupTypeModel model);
        Task<int> UpdateStatusAsync(int id, bool status, int userId);
        Task<PickupTypeModel> GetByIdAsync(int id);
    }
}
