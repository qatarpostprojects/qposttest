﻿using Newtonsoft.Json;
using QPostServices.Domain.Helper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Net;
using System.Text;

namespace QPostServices.Services
{
    public class OrderSyncStatusService : IOrderSyncStatusService
    {
        private readonly IOrderSyncStatusRepository _orderSyncStatusRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Order Sync Status created successfully";
        private const string _failedCreation = "Order Sync Status creation failed";
        private const string _OrderSyncStatusDoesNotExist = "Order Sync Status with id : {0} Does not exist";

        public OrderSyncStatusService(IOrderSyncStatusRepository orderSyncStatusRepository, ILoginService loginService)
        {
            _orderSyncStatusRepository = orderSyncStatusRepository;
            _loginService = loginService;
        }

       

        #region Create OrderSyncStatus
        public async Task<ResponseModel> CreateAsync(OrderSyncStatusModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _orderSyncStatusRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get OrderSyncStatus by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _orderSyncStatusRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_OrderSyncStatusDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of OrderSyncStatus
        public async Task<List<OrderSyncStatusModel>> GetListAsync()
        {
            return await _orderSyncStatusRepository.GetListAsync();
        }
        #endregion
    }
}
