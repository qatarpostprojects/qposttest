﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region CreateAsync
        public async Task<int> CreateAsync(CustomerModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@firstName", model.FirstName);
                parameters.Add("@middleName", model.MiddleName);
                parameters.Add("@lastName", model.LastName);
                parameters.Add("@documentType", model.DocumentType);
                parameters.Add("@documentReference", model.DocumentReference);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@alternativeMobileNumber", model.AlternativeMobileNumber);
                parameters.Add("@nationality", model.Nationality);
                parameters.Add("@dob", model.DateOfBirth);
                parameters.Add("@poboxNumber", model.PoBoxNumber);
                parameters.Add("@connectedVPNumber", model.ConnectedVpNumber);
                parameters.Add("@smartBoxNumber", model.SmartBoxNumber);
                parameters.Add("@poboxExpiry", model.PoBoxExpiry);
                parameters.Add("@smartBoxExpiry", model.SmartBoxExpiry);
                parameters.Add("@walletId", model.WalletId);
                parameters.Add("@walletBalanceAmount", model.WalletBalanceAmount);
                parameters.Add("@hds", model.HDS);
                parameters.Add("@hdsPackageId", model.HDSPackageId);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference1);
                parameters.Add("@reference3", model.Reference3);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@walletReferenceNumber", model.WalletReferenceNumber);
                parameters.Add("@emailAddress", model.EmailAddress);
                parameters.Add("@firstNameAr", model.FirstNameAr);
                parameters.Add("@middleNameAr", model.MiddleNameAr);
                parameters.Add("@lastNameAr", model.LastNameAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetAllActiveAsync
        public async Task<List<CustomerModel>> GetAllActiveAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<CustomerModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetByIdAsync
        public async Task<CustomerModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", id);
                var result = await connection.QueryAsync<CustomerModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region RegistrationAsync
        public async Task<int> RegistrationAsync(CustomerRegistraionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_Registration]";
                var parameters = new DynamicParameters();
                parameters.Add("@firstName", model.FirstName);
                parameters.Add("@middleName", model.MiddleName);
                parameters.Add("@lastName", model.LastName);
                parameters.Add("@documentType", model.DocumentType);
                parameters.Add("@documentReference", model.DocumentReference);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@alternativeMobileNumber", model.AlternativeMobileNumber);
                parameters.Add("@nationality", model.Nationality);
                parameters.Add("@dob", model.DateOfBirth);
                parameters.Add("@poboxNumber", model.PoBoxNumber);
                parameters.Add("@connectedVPNumber", model.ConnectedVpNumber);
                parameters.Add("@smartBoxNumber", model.SmartBoxNumber);
                parameters.Add("@poboxExpiry", model.PoBoxExpiry);
                parameters.Add("@smartBoxExpiry", model.SmartBoxExpiry);
                parameters.Add("@walletId", model.WalletId);
                parameters.Add("@walletBalanceAmount", model.WalletBalanceAmount);
                parameters.Add("@hds", model.HDS);
                parameters.Add("@hdsPackageId", model.HDSPackageId);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference1);
                parameters.Add("@reference3", model.Reference3);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@recordStatus", RecordStatus.Active);
                parameters.Add("@walletReferenceNumber", model.WalletReferenceNumber);
                parameters.Add("@customerUserName", model.CustomerUserName);
                parameters.Add("@customerPassword", model.CustomerPassword);
                parameters.Add("@securityQuestion", model.SecurityQuestion);
                parameters.Add("@securityAnswer", model.SecurityAnswer);
                parameters.Add("@roleId", model.RoleId);
                parameters.Add("@emailAddress", model.EmailAddress);
                parameters.Add("@preferedLanguage", model.PreferedLanguage);
                parameters.Add("@firstNameAr", model.FirstNameAr);
                parameters.Add("@middleNameAr", model.MiddleNameAr);
                parameters.Add("@lastNameAr", model.LastNameAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateAsync
        public async Task<int> UpdateAsync(CustomerModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@firstName", model.FirstName);
                parameters.Add("@middleName", model.MiddleName);
                parameters.Add("@lastName", model.LastName);
                parameters.Add("@documentType", model.DocumentType);
                parameters.Add("@documentReference", model.DocumentReference);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@alternativeMobileNumber", model.AlternativeMobileNumber);
                parameters.Add("@nationality", model.Nationality);
                parameters.Add("@dob", model.DateOfBirth);
                parameters.Add("@poboxNumber", model.PoBoxNumber);
                parameters.Add("@connectedVPNumber", model.ConnectedVpNumber);
                parameters.Add("@smartBoxNumber", model.SmartBoxNumber);
                parameters.Add("@poboxExpiry", model.PoBoxExpiry);
                parameters.Add("@smartBoxExpiry", model.SmartBoxExpiry);
                parameters.Add("@walletBalanceAmount", model.WalletBalanceAmount);
                parameters.Add("@hds", model.HDS);
                parameters.Add("@hdsPackageId", model.HDSPackageId);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference1);
                parameters.Add("@reference3", model.Reference3);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@walletReferenceNumber", model.WalletReferenceNumber);
                parameters.Add("@emailAddress", model.EmailAddress);
                parameters.Add("@firstNameAr", model.FirstNameAr);
                parameters.Add("@middleNameAr", model.MiddleNameAr);
                parameters.Add("@lastNameAr", model.LastNameAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region UpdateStatusAsync
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region SaveCustomerVerificationOTP
        public async Task SaveCustomerVerificationOTP(OtpModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerVerification_CreateOrUpdate]";
                var parameters = new DynamicParameters();
                parameters.Add("@type", model.Type);
                parameters.Add("@value", model.Value);
                parameters.Add("@note", model.Note);
                parameters.Add("@otp", model.OTP);
                parameters.Add("@ExpiryTime", DateTime.Now.AddMinutes(5));
                await connection.QueryAsync(procedure, parameters, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region VerifyOTP
        public async Task<bool> VerifyOTPAsync(string value, string otp)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerVerification_VerifyOTP]";
                var parameters = new DynamicParameters();
                parameters.Add("@currentTime", DateTime.Now);
                parameters.Add("@value", value);
                parameters.Add("@otp ", otp);
                var result = await connection.QueryAsync<bool>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateProfileAsync
        public async Task<int> UpdateProfileAsync(CustomerProfileModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_UpdateProfile]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@emailAddress", model.EmailAddress);
                parameters.Add("@firstName", model.FirstName);
                parameters.Add("@middleName", model.MiddleName);
                parameters.Add("@lastName", model.LastName);
                parameters.Add("@name", model.Name);
                parameters.Add("@mobile", model.MobileNumber);
                parameters.Add("@firstNameAr", model.FirstNameAr);
                parameters.Add("@middleNameAr", model.MiddleNameAr);
                parameters.Add("@lastNameAr", model.LastNameAr);
                parameters.Add("@nameAr", model.NameAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Check Current Password Valid
        public async Task<bool> CheckPasswordValidAsync(int customerId, string password)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_CheckValidPassword]";
                var parameters = new DynamicParameters();
                parameters.Add("@password", password);
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<bool>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Password
        public async Task<int> ChangePasswordAsync(int id, string password)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Customer_UpdatePassword]";
                var parameters = new DynamicParameters();
                parameters.Add("@password", password);
                parameters.Add("@customerId", id);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update language preference
        public async Task<int> UpdateLanguagePreference(int customerId, string language, int modifiedUser)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_Users_UpdateLanguagePreference";
                var parameters = new DynamicParameters();
                parameters.Add("@preferedLanguage", language);
                parameters.Add("@customerId", customerId);
                parameters.Add("@modifiedUser", modifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Map Customer Order
        public async Task<int> MapOrderCustomerAsync(MapOrderCustomerModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "POBox_CustomerOrderDetails_MapOrderCustomer";
                var parameters = new DynamicParameters();
                parameters.Add("@searchParameter", model.SearchParameter);
                parameters.Add("@customerId", model.CustomerId);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
