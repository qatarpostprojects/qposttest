﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class ShipmentCostRepository : IShipmentCostRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public ShipmentCostRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region GetListAsync
        public async Task<List<ShipmentCostDetailsModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ShipmentCostDetails_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<ShipmentCostDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
