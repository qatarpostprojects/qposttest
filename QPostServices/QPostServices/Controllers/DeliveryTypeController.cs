﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryTypeController : ControllerBase
    {
        private readonly IDeliveryTypeService _deliveryTypeService;

        public DeliveryTypeController(IDeliveryTypeService deliveryTypeService)
        {
            _deliveryTypeService = deliveryTypeService;
        }

        #region Create DeliveryType
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateDeliveryType))]
        public async Task<IActionResult> CreateDeliveryType([FromForm] DeliveryTypeRequest request)
        {
            try
            {
                var result = await _deliveryTypeService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get DeliveryType by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DeliveryType))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetDeliveryType))]
        public async Task<IActionResult> GetDeliveryType(int id)
        {
            try
            {
                var result = await _deliveryTypeService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(DeliveryType.FromModel((DeliveryTypeModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get DeliveryTypes
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<DeliveryType>))]
        [HttpGet(Name = nameof(GetDeliveryTypeList))]
        public async Task<IActionResult> GetDeliveryTypeList()
        {
            try
            {
                var results = await _deliveryTypeService.GetListAsync();

                return Ok(results.Select(x => DeliveryType.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get DeliveryTypes By Delivery Product Id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<DeliveryType>))]
        [HttpGet("GetDeliveryTypeByProductId/{productId}", Name = nameof(GetDeliveryTypeByProductId))]
        public async Task<IActionResult> GetDeliveryTypeByProductId(int productId)
        {
            try
            {
                var results = await _deliveryTypeService.GetByProductIdAsync(productId);

                return Ok(results.Select(x => DeliveryType.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete DeliveryType
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteDeliveryType))]
        public async Task<IActionResult> DeleteDeliveryType(int id)
        {
            try
            {
                var result = await _deliveryTypeService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update DeliveryType
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateDeliveryType))]
        public async Task<IActionResult> UpdateDeliveryType(int id, [FromForm] DeliveryTypeRequest request)
        {
            try
            {
                var result = await _deliveryTypeService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
