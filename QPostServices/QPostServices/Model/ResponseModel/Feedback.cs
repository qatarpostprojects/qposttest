﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Feedback
    {
        public int Id { get; set; }
        public string Review { get; set; } = string.Empty;
        public string? Comment { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }

        public static Feedback FromModel(FeedbackModel model)
        {
            return new Feedback
            {
                Id = model.Id,
                Review = model.Review,
                Comment = model.Comment,
                RecordStatus = model.RecordStatus,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName
            };
        }
    }
}
