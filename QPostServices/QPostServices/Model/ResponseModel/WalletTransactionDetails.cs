﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class WalletTransactionDetails
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string BillNo { get; set; } = default!;
        public decimal Amount { get; set; }
        public string Type { get; set; } = string.Empty;
        public string ReferenceId { get; set; } = default!;
        public string BankTransactionId { get; set; } = default!;
        public DateTime Date { get; set; }
        public int PaymentId { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public static WalletTransactionDetails FromModel(WalletTransactionDetailsModel model)
        {
            return new WalletTransactionDetails
            {
                Id = model.Id,
                CustomerId = model.CustomerId,
                BillNo = model.BillNo,
                Amount = model.Amount,
                Type = model.Type.ToString(),
                ReferenceId = model.ReferenceId,
                BankTransactionId = model.BankTransactionId,
                Date = model.Date,
                PaymentId = model.PaymentId,
                Remarks = model.Remarks,
                Reference1 = model.Reference1,
                RecordStatus = model.RecordStatus.ToString(),
                CreatedUser = model.CreatedUser,
                CreatedDate = model.CreatedDate,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName
            };
        }
    }
}
