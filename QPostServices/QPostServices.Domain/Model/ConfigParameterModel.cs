﻿namespace QPostServices.Domain.Model
{
    public class ConfigParameterModel
    {
        public int Id { get; set; }
        public string? Module { get; set; }
        public string? SubModule { get; set; }
        public string? ConfigKey { get; set; }
        public string? ConfigVal { get; set; }
        public bool IsActive { get; set; }
        public string? Remarks { get; set; }
    }
}
