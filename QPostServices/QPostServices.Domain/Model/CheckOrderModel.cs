﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class CheckOrderModel
    {
        public string? TrackingNumber{ get; set; }
        public string? ActualPhoneNo{ get; set; }
        public bool IsOwnOrder { get; set; }
    }

    public class CheckOrderResponseModel
    {
        public CustomerOrderWithAddressModel? OrderDetails { get; set; }
        public string? ActualPhoneNo { get; set; }
        public bool IsOwnOrder { get; set; }
    }
}
