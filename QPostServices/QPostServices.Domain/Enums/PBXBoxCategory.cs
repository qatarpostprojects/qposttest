﻿namespace QPostServices.Domain.Enums
{
    public enum PBXBoxCategory
    {
        Electric = 0,
        Mechanical = 1
    }
}
