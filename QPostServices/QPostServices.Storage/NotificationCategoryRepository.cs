﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class NotificationCategoryRepository : INotificationCategoryRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public NotificationCategoryRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create NotificationCategory
        public async Task<int> CreateAsync(NotificationCategoryModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_NotificationCategory_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUserId);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get list of NotificationCategory
        public async Task<List<NotificationCategoryModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_NotificationCategory_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<NotificationCategoryModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
