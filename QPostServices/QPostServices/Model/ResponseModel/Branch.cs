﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Branch
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? AddressAr { get; set; }
        public string CostCenter { get; set; } = string.Empty;
        public string? GoogleCordinates { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? MobileNumber { get; set; }
        public string? Email { get; set; }
        public int? BranchTypeId { get; set; }
        public string? BranchTypeName { get; set; }
        public string? BranchTypeNameAr { get; set; }
        public List<string>? WorkingHours { get; set; }
        public List<string>? WorkingHoursAr { get; set; }
        public static Branch FromModel(BranchModel model)
        {
            return new Branch
            {
                Id = model.Id,
                Name = model.Name,
                Address = model.Address,
                CostCenter = model.CostCenter,
                CreatedDate = model.CreatedDate,
                CreatedUserId = model.CreatedUserId,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserId = model.ModifiedUserId,
                ModifiedUserName = model.ModifiedUserName,
                Active = model.Active,
                GoogleCordinates = model.GoogleCordinates,
                NameAr = model.NameAr,
                AddressAr = model.AddressAr,
                Email = model.Email,
                BranchTypeId = model.BranchTypeId,
                BranchTypeName = model.BranchTypeName,
                BranchTypeNameAr = model.BranchTypeNameAr,
                MobileNumber = model.MobileNumber,
                WorkingHours = model.WorkingHours != null ? model.WorkingHours.Split(",").ToList() : null,
                WorkingHoursAr = model.WorkingHoursAr != null ? model.WorkingHoursAr.Split(",").ToList() : null
            };
        }
    }
}
