﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IUserSessionRepository
    {
        Task<string?> CreateAsync(UserSessionModel model);
        Task<int> UpdateStatusAsync(string sessionValue, UserSessionStatus status,int userId);
        Task<UserSessionModel?> GetBySessionValueAsync(string sessionValue);
    }
}
