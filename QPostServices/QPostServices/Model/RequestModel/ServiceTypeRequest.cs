﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class ServiceTypeRequest
    {
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public ServiceTypeModel ToCreateModel()
        {
            return new ServiceTypeModel()
            {
                Name = Name,
                Description = Description,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
        public ServiceTypeModel ToUpdateModel(int id)
        {
            return new ServiceTypeModel()
            {
                Name = Name,
                Description = Description,
                Id = id,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
