﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string SubscriptionType { get; set; } = string.Empty;
        public string? ApplicableFor { get; set; }
        public string SubscriptionPaymentType { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }
        public string? SubscriptionTypeAr { get; set; }
        public string? ApplicableForAr { get; set; }
        public string? SubscriptionPaymentTypeAr { get; set; }
        public bool IsUpgradeEligible { get; set; }
        public int? UpgradeID { get; set; }
        public bool IsAddonServiceAvailable { get; set; }
        public int? LoyaltyID { get; set; }
        public string? Remarks { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int ServiceBaseValue { get; set; }
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }
        public bool IsAddonService { get; set; }
        public int? ParentServiceId { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? ServiceBaseOn { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public int? ServiceTypeId { get; set; }

        public List<ServiceModel>? AddOnServices { get; set; }

        public static Service FromModel(ServiceModel model)
        {
            return new Service
            {
                Id = model.Id,
                Name = model.Name,
                LoyaltyID = model.LoyaltyID,
                IsAddonServiceAvailable = model.IsAddonServiceAvailable,
                UpgradeID = model.UpgradeID,
                IsUpgradeEligible = model.IsUpgradeEligible,
                ApplicableFor = model.ApplicableFor,
                SubscriptionType = model.SubscriptionType,
                Description = model.Description,
                RecordStatus = model.RecordStatus.ToString(),
                Reference1 = model.Reference1,
                Reference2 = model.Reference2,
                Remarks = model.Remarks,
                SubscriptionPaymentType = model.SubscriptionPaymentType,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                CreatedUserName = model.CreatedUserName,
                ServiceBaseOn = model.ServiceBaseOn,
                ServiceBaseValue = model.ServiceBaseValue,
                SubscriptionAmount = model.SubscriptionAmount,
                RenewAmount = model.RenewAmount,
                CancelAmount = model.CancelAmount,
                IsAddonService = model.IsAddonService,
                ParentServiceId = model.ParentServiceId,
                AddOnServices = model.AddOnServices,
                ServiceTypeId = model.ServiceTypeId,
                NameAr = model.NameAr,
                SubscriptionPaymentTypeAr = model.SubscriptionPaymentTypeAr,
                SubscriptionTypeAr = model.SubscriptionTypeAr,
                ApplicableForAr = model.ApplicableForAr,
                DescriptionAr = model.DescriptionAr,
            };
        }
    }
}
