﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class UserSessionRepository : IUserSessionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public UserSessionRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<string?> CreateAsync(UserSessionModel model)
        {
            try
            {

                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_UserSessionReference_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@userSessionValue", $"{model.Prefix}{GenerateRandomNo()}{await GetLatestId()}");
                parameters.Add("@recordStatus", model.RecordStatus.ToString());
                parameters.Add("@prefix", model.Prefix);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<string>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        public async Task<int> UpdateStatusAsync(string sessionValue, UserSessionStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_UserSessionReference_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@userSessionValue", sessionValue);
                parameters.Add("@recordStatus", status.ToString());
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        #region get latestid
        private async Task<int> GetLatestId()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_UserSessionReference_GetLatestId]";
                var result = (await connection.QueryAsync<int>(procedure, commandType: CommandType.StoredProcedure)).FirstOrDefault();
                return result + 1;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Generate 4 digit random number
        private int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public async Task<UserSessionModel?> GetBySessionValueAsync(string sessionValue)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_UserSessionReference_GetByUserSessionValue]";
                var parameters = new DynamicParameters();
                parameters.Add("@userSessionValue", sessionValue);
                var result = await connection.QueryAsync<UserSessionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
