﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IInboundRequestRepository
    {
        Task<int> CreateAsync(InboundRequestModel model);
        Task<List<IdValueModel>> GetMasterLookUpAsync();
    }
}
