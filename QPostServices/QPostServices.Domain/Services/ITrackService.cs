﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ITrackService
    {
        Task<object?> TrackAndTrace(TrackAndTraceModel model);
        Task<object?> TrackAndTraceByMobile(TrackMobileModel model);
        Task AddLogAsync(LogModel log);
        Task<string?> GetMobileNumberAsync(string searchString);
    }
}
