﻿namespace QPostServices.Domain.Enums
{
    public enum UserSessionStatus
    {
        Initiated = 1,
        Pending = 2,
        Failed = 3,
        Success = 4
    }
}
