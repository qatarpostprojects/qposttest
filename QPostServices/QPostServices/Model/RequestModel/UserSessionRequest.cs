﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class UserSessionRequest
    {
        [Required]
        public UserSessionStatus RecordStatus { get; set; }

        [Required]
        public string Prefix { get; set; } = string.Empty;
        public UserSessionModel ToModel()
        {
            return new UserSessionModel
            {
                RecordStatus = RecordStatus,
                Prefix = Prefix
            };
        }
    }
}
