﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;

namespace QPostServices.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        #region Add Feedback
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(AddFeedback))]
        public async Task<IActionResult> AddFeedback([FromBody] FeedbackRequest request)
        {
            try
            {
                var result = await _feedbackService.AddAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Feedback by CustomerId
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Feedback))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("Customer",Name = nameof(GetFeedbackByCustomerId))]
        public async Task<IActionResult> GetFeedbackByCustomerId()
        {
            try
            {
                var result = await _feedbackService.GetByCustomerIdAsync();

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(Feedback.FromModel((FeedbackModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get all
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Feedback))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet(Name = nameof(GetFeedback))]
        public async Task<IActionResult> GetFeedback()
        {
            try
            {
                var result = await _feedbackService.GetAsync();

             

                return Ok(result.Select(f=> Feedback.FromModel(f)));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
