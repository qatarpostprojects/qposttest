﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class WalletTransactionSummaryRequest
    {
        //[Required]
        public int WalletId { get; set; }
        
        [Required]
        public int CustomerId { get; set; }
        
        [Required]
        public decimal Amount { get; set; }
        public int? LastTransactionId { get; set; }
        public DateTime? LastTransactionDate { get; set; }
        public string? Remarks { get; set; }

        public WalletTransactionSummaryModel ToCreateModel()
        {
            return new WalletTransactionSummaryModel
            {
                WalletId = WalletId,
                CustomerId = CustomerId,
                Amount = Amount,
                LastTransactionId = LastTransactionId,
                LastTransactionDate = LastTransactionDate,
                Remarks = Remarks
            };
        }

        public WalletTransactionSummaryModel ToUpdateModel(int id)
        {
            return new WalletTransactionSummaryModel
            {
                Id = id,
                WalletId = WalletId,
                CustomerId = CustomerId,
                Amount = Amount,
                LastTransactionId = LastTransactionId,
                LastTransactionDate = LastTransactionDate,
                Remarks = Remarks
            };
        }
    }
}
