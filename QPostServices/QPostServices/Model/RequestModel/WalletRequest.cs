﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class WalletRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; } 
        [Required]
        public string Description { get; set; } = string.Empty;
        public string? DescriptionAr { get; set; } 
        public string? Reference1 { get; set; }

        public WalletModel ToCreateModel()
        {
            return new WalletModel
            {
                Name = Name,
                Description = Description,
                Reference1 = Reference1,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr,
            };
        }

        public WalletModel ToUpdateModel(int id)
        {
            return new WalletModel
            {
                Id = id,
                Name = Name,
                Description = Description,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr,
            };
        }
    }
}
