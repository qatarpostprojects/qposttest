﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class WalletTransactionSummary
    {
        public int Id { get; set; }
        public string ReferenceNumber { get; set; } = string.Empty;
        public int WalletId { get; set; }
        public int CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public decimal Amount { get; set; }
        public int? LastTransactionId { get; set; }
        public DateTime? LastTransactionDate { get; set; }
        public string? Remarks { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public static WalletTransactionSummary FromModel(WalletTransactionSummaryModel model)
        {
            return new WalletTransactionSummary
            {
                Id = model.Id,
                WalletId = model.WalletId,
                Amount = model.Amount,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CustomerId = model.CustomerId,
                LastTransactionDate = model.LastTransactionDate,
                LastTransactionId = model.LastTransactionId,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                RecordStatus = model.RecordStatus.ToString(),
                Remarks = model.Remarks,
                ReferenceNumber = model.ReferenceNumber,
                CustomerName = model.CustomerName,
            };
        }
    }
}
