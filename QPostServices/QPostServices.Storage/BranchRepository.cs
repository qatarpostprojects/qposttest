﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class BranchRepository : IBranchRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public BranchRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region GetAllBranches
        public async Task<List<BranchModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Branch_GetAll]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<BranchModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Nearest Branches
        public async Task<List<BranchModel>> GetNearestBranchesAsync(List<int?> branchIds)
        {
            try
            {
                DataTable table = new DataTable();
                table.Columns.Add("Value", typeof(int));
                foreach (int? id in branchIds)
                {
                    if (id.HasValue)
                    {
                        table.Rows.Add(id.Value);
                    }
                }

                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Branch_GetBranchListByIds]";
                var parameters = new DynamicParameters();

                parameters.Add("@BranchIds", table.AsTableValuedParameter("IntList"));

                var result = await connection.QueryAsync<BranchModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Create Branch
        public async Task<int> CreateAsync(BranchModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Branch_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@address", model.Address);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@addressAr", model.AddressAr);
                parameters.Add("@costCenter", model.CostCenter);
                parameters.Add("@googleCordinates", model.GoogleCordinates);
                parameters.Add("@createdUser", model.CreatedUserId);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@active", true);
                parameters.Add("@branchTypeId", model.BranchTypeId);
                parameters.Add("@email", model.Email);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@workingHoursAr", model.WorkingHoursAr);
                parameters.Add("@workingHours", model.WorkingHours);

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Branch by Id
        public async Task<BranchModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Branch_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<BranchModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Branch
        public async Task<int> UpdateAsync(BranchModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Branch_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@address", model.Address);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@addressAr", model.AddressAr);
                parameters.Add("@costCenter", model.CostCenter);
                parameters.Add("@googleCordinates", model.GoogleCordinates);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@modifiedUser", model.ModifiedUserId);
                parameters.Add("@id", model.Id);
                parameters.Add("@branchTypeId", model.BranchTypeId);
                parameters.Add("@email", model.Email);
                parameters.Add("@mobileNumber", model.MobileNumber);
                parameters.Add("@workingHoursAr", model.WorkingHoursAr);
                parameters.Add("@workingHours", model.WorkingHours);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Branch ActiveStatus
        public async Task<int> UpdateStatusAsync(int id, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Branch_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", false);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
