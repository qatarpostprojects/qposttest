﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class SubscriptionTransactionService : ISubscriptionTransactionService
    {
        private const string _subscriptionTransactionDoesNotExist = "Subscription transaction with id {0} does not exists";
        private const string _successSubscriptionTransactionCreation = "Subscription transaction created successfully";
        private const string _failedSubscriptionTransactionCreation = "Subscription transaction creation failed";
        private const string _successSubscriptionTransactionDeleted = "Subscription transaction with id {0} deleted successfully";
        private const string _successSubscriptionTransactionUpdate = "Subscription transaction with id {0} updated successfully";


        private readonly ISubscriptionTransactionRepository _subscriptionTransactionRepository;
        private readonly ILoginService _loginService;
        public SubscriptionTransactionService(ISubscriptionTransactionRepository subscriptionTransactionRepository, ILoginService loginService)
        {
            _subscriptionTransactionRepository = subscriptionTransactionRepository;
            _loginService = loginService;
        }


        #region Create subscription transaction
        public async Task<ResponseModel> CreateAsync(SubscriptionTransactionModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _subscriptionTransactionRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successSubscriptionTransactionCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedSubscriptionTransactionCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get All Active subscription transactions
        public Task<List<SubscriptionTransactionModel>> GetAllActiveAsync()
        {
            return _subscriptionTransactionRepository.GetAllActiveAsync();
        }
        #endregion

        #region Get subscription transactions by Filter
        public Task<List<SubscriptionTransactionModel>> GetByFilterAsync(int? customerId, int? serviceId)
        {
            return _subscriptionTransactionRepository.GetByFilterAsync(customerId, serviceId);
        }
        #endregion

        #region Get SubscriptionTransaction By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _subscriptionTransactionRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_subscriptionTransactionDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update subscription transaction
        public async Task<ResponseModel> UpdateAsync(SubscriptionTransactionModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _subscriptionTransactionRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successSubscriptionTransactionUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_subscriptionTransactionDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete subscription transaction
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _subscriptionTransactionRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successSubscriptionTransactionDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_subscriptionTransactionDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
