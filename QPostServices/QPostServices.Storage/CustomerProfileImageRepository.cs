﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class CustomerProfileImageRepository : ICustomerProfileImageRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerProfileImageRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create Image
        public async Task<int> CreateImageAsync(CustomerProfileImageModel model)
        {
            try
            {

                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerProfileImage_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@image", model.Image);
                parameters.Add("@fileType", model.FileType);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Image
        public async Task<CustomerProfileImageModel> GetCustomerProfileImageAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerProfileImage_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = (await connection.QueryAsync<CustomerProfileImageModel>(procedure, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();

                return result ?? new CustomerProfileImageModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Image
        public async Task<int> UpdateImageAsync(CustomerProfileImageModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerProfileImage_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@image", model.Image);
                parameters.Add("@fileType", model.FileType);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion       
    }
}
