﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using QPostServices.Services;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController:ControllerBase
    {
        private readonly ICustomerCardDetailsService _customerCardDetailsService;
        private readonly IPaymentService _paymentService;
        public PaymentController(IPaymentService paymentService,ICustomerCardDetailsService customerCardDetailsService)
        {
            _paymentService = paymentService;
            _customerCardDetailsService = customerCardDetailsService;
        }

        #region Create Payment
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreatePayment))]
        public async Task<IActionResult> CreatePayment([FromBody] PaymentRequest request)
        {
            try
            {
                var result = await _paymentService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get payment details by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Payment>))]
        [HttpGet("Customer/{customerId}", Name = nameof(GetPaymentDetailsList))]
        public async Task<IActionResult> GetPaymentDetailsList(int customerId)
        {
            try
            {
                var results = await _paymentService.GetPaymentListByCustomerId(customerId);

                return Ok(results.Select(x => Payment.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Payment by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Payment))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetPayment))]
        public async Task<IActionResult> GetPayment(int id)
        {
            try
            {
                var result = await _paymentService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(Payment.FromModel((PaymentModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Payment
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeletePayment))]
        public async Task<IActionResult> DeletePayment(int id)
        {
            try
            {
                var result = await _paymentService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Payment
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdatePayment))]
        public async Task<IActionResult> UpdatePayment(int id, [FromBody] PaymentRequest request)
        {
            try
            {
                var result = await _paymentService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create Card
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("Card",Name = nameof(CreatCard))]
        public async Task<IActionResult> CreatCard([FromBody] CustomerCardDetailsRequest request)
        {
            try
            {
                var result = await _customerCardDetailsService.CreateCardDetailsAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get card details by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerCardDetails>))]
        [HttpGet("card/customer/{customerId}", Name = nameof(GetCardDetailsList))]
        public async Task<IActionResult> GetCardDetailsList(int customerId)
        {
            try
            {
                var results = await _customerCardDetailsService.GetCardDetailsByCustomerId(customerId);

                return Ok(results.Select(x => CustomerCardDetails.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Card details by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerCardDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("card/{id}", Name = nameof(GetCard))]
        public async Task<IActionResult> GetCard(int id)
        {
            try
            {
                var result = await _customerCardDetailsService.GetCardDetailsByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(CustomerCardDetails.FromModel((CustomerCardDetailsModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete card
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("card/{id}", Name = nameof(DeleteCard))]
        public async Task<IActionResult> DeleteCard(int id)
        {
            try
            {
                var result = await _customerCardDetailsService.DeleteCardDetailsAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Card
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("card/{id}", Name = nameof(UpdateCard))]
        public async Task<IActionResult> UpdateCard(int id, [FromBody] CustomerCardDetailsRequest request)
        {
            try
            {
                var result = await _customerCardDetailsService.UpdateCardDetailsAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get recent transactions by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<RecentTransaction>))]
        [HttpGet("Customer/{customerId}/RecentTransactions", Name = nameof(GetRecentTransactionsList))]
        public async Task<IActionResult> GetRecentTransactionsList(int customerId)
        {
            try
            {
                var results = await _paymentService.GetRecentTransactionsByCustomerIdAsync(customerId);

                return Ok(results.Select(x => RecentTransaction.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
