﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly ICustomerProfileImageService _customerProfileImageService;
        public CustomerController(ICustomerService customerService, ICustomerProfileImageService customerProfileImageService)
        {
            _customerService = customerService;
            _customerProfileImageService = customerProfileImageService;
        }

        #region Create Customer
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateCustomer))]
        public async Task<IActionResult> CreateCustomer([FromBody] CustomerRequest request)
        {
            try
            {
                var result = await _customerService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Customer Registration
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("Registration", Name = nameof(CustomerRegistration))]
        public async Task<IActionResult> CustomerRegistration([FromBody] CustomerRegistraionRequest request)
        {
            try
            {
                var result = new ResponseModel();
                if(await _customerService.VerifyOTPAsync(request.OTPRequest.Value, request!.OTPRequest!.OTP))
                {
                    result = await _customerService.RegistrationAsync(request.ToModel());

                    if (!result.Error)
                    {
                        return Ok(result.Response);
                    }

                    return BadRequest(result.Response);
                }
                else
                {
                    result.Response = "Invalid OTP";
                    return BadRequest(result.Response);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Customer))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetCustomer))]
        public async Task<IActionResult> GetCustomer(int id)
        {
            try
            {
                var result = await _customerService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(Customer.FromModel((CustomerModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customers
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Customer>))]
        [HttpGet(Name = nameof(GetCustomerList))]
        public async Task<IActionResult> GetCustomerList()
        {
            try
            {
                var results = await _customerService.GetAllActiveAsync();

                return Ok(results.Select(x => Customer.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Customer
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteCustomer))]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            try
            {
                var result = await _customerService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Customer
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateCustomer))]
        public async Task<IActionResult> UpdateCustomer(int id, [FromBody] CustomerRequest request)
        {
            try
            {
                var result = await _customerService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region SendOTP
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("SendOTP", Name = nameof(SendOTP))]
        public async Task<IActionResult> SendOTP([FromBody] OTPRequest request)
        {
            try
            {
                var result = await _customerService.SendOtpAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Verify OTP and UpdateProfile
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("UpdateProfile/{id}", Name = nameof(UpdateProfile))]
        public async Task<IActionResult> UpdateProfile(int id, [FromBody] CustomerProfileRequest request)
        {
            try
            {
                var result = new ResponseModel();
                //Verify OTP
                if (await _customerService.VerifyOTPAsync(request.OTPRequest.Value, request!.OTPRequest!.OTP))
                {
                    //Update Profile
                    result = await _customerService.UpdateProfileAsync(request.ToUpdateModel(id));
                    if (result.Error)
                    {
                        if (result.StatusCode == HttpStatusCode.NotFound)
                        {
                            return NotFound(result.Response);
                        }
                        else
                        {
                            return BadRequest(result.Response);
                        }
                    }
                }
                else
                {
                    result.Response = "Invalid OTP";
                    return BadRequest(result.Response);
                }

                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Check Current Password
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("CheckPasswordValid/{id}", Name = nameof(CheckPasswordValid))]
        public async Task<IActionResult> CheckPasswordValid(int id, [FromBody] CustomerPasswordRequest request)
        {
            try
            {
                if (!await _customerService.CheckPasswordValidAsync(id, request.Password))
                {
                    var invalidPassword = new ResponseModel { Response = "Invalid Current Password" };
                    return BadRequest(invalidPassword.Response);
                }

                var result = await _customerService.SendOtpAsync(request.OTPRequest.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Change Password
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("ChangePassword/{id}", Name = nameof(ChangePassword))]
        public async Task<IActionResult> ChangePassword(int id, [FromBody] CustomerPasswordRequest request)
        {
            try
            {
                var result = new ResponseModel();
                //Verify OTP
                if (await _customerService.VerifyOTPAsync(request.OTPRequest.Value, request!.OTPRequest!.OTP))
                {
                    //Update Password
                    result = await _customerService.ChangePasswordAsync(id, request.Password);
                    if (result.Error)
                    {
                        if (result.StatusCode == HttpStatusCode.NotFound)
                        {
                            return NotFound(result.Response);
                        }
                        else
                        {
                            return BadRequest(result.Response);
                        }
                    }
                }
                else
                {
                    result.Response = "Invalid OTP";
                    return BadRequest(result.Response);
                }

                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Insert Profile Image
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("Profile/Image", Name = nameof(CreateProfileImage))]
        public async Task<IActionResult> CreateProfileImage([FromForm] CustomerProfileImageRequest request)
        {
            try
            {
                var result = await _customerProfileImageService.CreateImage(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Profile Image
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("Profile/Image", Name = nameof(UpdateProfileImage))]
        public async Task<IActionResult> UpdateProfileImage([FromForm] CustomerProfileImageRequest request)
        {
            try
            {

                var result = await _customerProfileImageService.UpdateImage(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Profile Image
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("Profile/Image/{customerId}", Name = nameof(GetProfileImage))]
        public async Task<IActionResult> GetProfileImage(int customerId)
        {
            try
            {
                var result = await _customerProfileImageService.GetCustomerProfileImage(customerId);

                if (!result.Error)
                {
                    return Ok(CustomerProfileImage.FromModel((CustomerProfileImageModel)result.Response!));
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Map Existing Customer Request OTP
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{CustomerId}/MapPoBoxRequestOTP", Name = nameof(MapExistingCutomerRequestOTP))]
        public async Task<IActionResult> MapExistingCutomerRequestOTP(int CustomerId, [FromBody] POBoxEnquiryRequest request)
        {
            try
            {
                var result = await _customerService.MapExistingRequestOTPAsync(CustomerId, request.ToModel());

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Map Existing Service
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{CustomerId}/MapPoBox", Name = nameof(MapExistingCutomer))]
        public async Task<IActionResult> MapExistingCutomer(int CustomerId, [FromBody] VerifyPOBoxEnquiryRequest request)
        {
            try
            {
                if (!await _customerService.VerifyOTPAsync(request.MobileNumber, request.OTP))
                {
                    return BadRequest(new ResponseModel { Error = true, Response = "Invalid OTP" });
                }

                var result = await _customerService.MapExistingServiceAsync(CustomerId, request.ToModel());

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Map Order Customer
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("MapOrder", Name = nameof(MapOrderCustomer))]
        public async Task<IActionResult> MapOrderCustomer(MapOrderCustomerRequest request)
        {
            try
            {
                var model = new MapOrderCustomerModel
                {
                    CustomerId = request.CustomerId,
                    SearchParameter = string.IsNullOrEmpty(request.POBoxNumber) ? request.PhoneNumber : request.POBoxNumber
                };

                var result = await _customerService.MapOrderCustomerAsync(model);

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update language preference
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}/language/{preferedLanguage}", Name = nameof(UpdateLanguagePreference))]
        public async Task<IActionResult> UpdateLanguagePreference(string preferedLanguage, int id)
        {
            try
            {

                var result = await _customerService.UpdateLanguagePreference(id, preferedLanguage);

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
