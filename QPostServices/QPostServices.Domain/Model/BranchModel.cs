﻿namespace QPostServices.Domain.Model
{
    public class BranchModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; } 
        public string Address { get; set; } = string.Empty;
        public string? AddressAr { get; set; }
        public string CostCenter { get; set; } = string.Empty;
        public string? GoogleCordinates { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? MobileNumber { get; set; }
        public string? Email { get; set; }
        public int? BranchTypeId { get; set; }
        public string? BranchTypeName { get; set; }
        public string? BranchTypeNameAr { get; set; }
        public string? WorkingHours { get; set; }
        public string? WorkingHoursAr { get; set; }
    }
}
