﻿namespace QPostServices.Domain.Helper
{
    public class AppSettings
    {
        public string SecretKey { get; set; } = string.Empty;
        public int AccessTokenExpireHours { get; set; }
        public string ExternalApiUrl { get; set; } = string.Empty;
        public string UATApiUrl { get; set; } = string.Empty;
        public string SoapServiceUrl { get; set; } = default!;
        public string PBXUsername { get; set; } = default!;
        public string PBXPassword { get; set; } = default!;
        public string PBXAgentId { get; set; } = default!;
    }
}
