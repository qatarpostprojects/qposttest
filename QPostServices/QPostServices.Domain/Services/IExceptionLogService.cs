﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IExceptionLogService
    {
        Task CreateAsync(LogModel model);
    }
}
