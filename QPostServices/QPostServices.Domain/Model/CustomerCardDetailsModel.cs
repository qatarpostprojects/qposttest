﻿namespace QPostServices.Domain.Model
{
    public class CustomerCardDetailsModel
    {
        public int Id { get; set; }
        public string CardNumber { get; set; } = string.Empty;
        public string CardProvider { get; set;} = string.Empty;
        public string CardType { get; set;} = string.Empty;
        public int CustomerId { get;set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set;}
    }
}
