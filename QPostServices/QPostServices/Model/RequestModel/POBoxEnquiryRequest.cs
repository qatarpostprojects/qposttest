﻿using QPostServices.Domain.Model;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class POBoxEnquiryRequest
    {
        [Required]
        public string? POBoxNo { get; set; }

        [AnyRequired(nameof(PassportNo), nameof(CompanyRegistrationNo), ErrorMessage = "QatarID is required if PassportNo or CompanyRegistrationNo is not provided.")]
        public string? QatarID { get; set; }

        [AnyRequired(nameof(QatarID), nameof(CompanyRegistrationNo), ErrorMessage = "PassportNo is required if QatarID or CompanyRegistrationNo is not provided.")]
        public string? PassportNo { get; set; }

        [AnyRequired(nameof(QatarID), nameof(PassportNo), ErrorMessage = "CompanyRegistrationNo is required if QatarID or PassportNo is not provided.")]
        public string? CompanyRegistrationNo { get; set; }

        public POBoxEnquiryModel ToModel()
        {
            return new POBoxEnquiryModel
            {
                CompanyRegistrationNo = CompanyRegistrationNo,
                PassportNo = PassportNo,
                POBoxNo = POBoxNo,
                QatarID = QatarID
            };
        }

    }
    public class VerifyPOBoxEnquiryRequest : POBoxEnquiryRequest
    {
        public string MobileNumber { get; set; } = string.Empty;
        public string OTP { get; set; } = string.Empty; 
    }
}
