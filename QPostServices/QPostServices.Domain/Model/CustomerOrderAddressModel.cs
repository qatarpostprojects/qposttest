﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CustomerOrderAddressModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string TrackingNumber { get; set; } = string.Empty;
        public string CustomerType { get; set; } = string.Empty;
        public string CustomerName { get; set; } = string.Empty;
        public string CustomerMobileNumber { get; set; } = string.Empty;
        public string? CustomerAddressZone { get; set; } 
        public string? CustomerAddressStreet { get; set; } 
        public string? CustomerAddressBuilding { get; set; } 
        public string? CustomerAddressUnit { get; set; }
        public string? CustomerLocationDetails { get; set; }
        public string CustomerPOBOXNumber { get; set; } = string.Empty;
        public string CustomerPostCode { get; set; } = string.Empty;
        public string? IsoCountryCode { get; set; } = string.Empty;
        public string CustomerEmail { get; set; } = string.Empty;
        public string CustomerIdentificationNumber { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? CustomerAddress1 { get; set; }
        public string? CustomerAddress2 { get; set; }
        public string? CustomerZipCode { get; set; }
    }
}
