﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class BoxTypeServices : IBoxTypeServices
    {
        private readonly IBoxTypeRepository _boxTypeRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Box type created successfully";
        private const string _successUpdate = "Box type updated successfully";
        private const string _failedCreation = "Box type creation failed";
        private const string _failedUpdation = "Box type updation failed";
        private const string _successDelete = "Box type deleted successfully";
        private const string _failedDelete = "Box type deletion failed";
        private const string _nameAlreadyExist = "Box type with name : {0} already exist";
        private const string _boxTypeDoesNotExist = "Box type with id : {0} Does not exist";


        public BoxTypeServices(IBoxTypeRepository boxTypeRepository, ILoginService loginService)
        {
            _boxTypeRepository = boxTypeRepository;
            _loginService = loginService;
        }

        #region Create Box Type
        public async Task<ResponseModel> CreateAsync(BoxTypeModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _boxTypeRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get Box Type by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _boxTypeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of Box Type
        public async Task<List<BoxTypeModel>> GetListAsync()
        {
            return await _boxTypeRepository.GetListAsync();
        }
        #endregion

        #region Update Box Type
        public async Task<ResponseModel> UpdateAsync(BoxTypeModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            var result = await _boxTypeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxTypeDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete Box Type
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _boxTypeRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
