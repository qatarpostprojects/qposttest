﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class FeedbackService : IFeedbackService
    {
        private const string _successCreation = "Feedback added successfully";
        private const string _failedCreation = "Feeback addition failed";
        private const string _customerDoesnotExist = "Customer with id {0} Does not exist";

        private readonly IFeedbackRepository _feedbackRepository;
        private readonly ILoginService _loginService;

        public FeedbackService(IFeedbackRepository feedbackRepository, ILoginService loginService)
        {
            _feedbackRepository = feedbackRepository;
            _loginService = loginService;
        }

        #region Add Feedback
        public async Task<ResponseModel> AddAsync(FeedbackModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInCustomer();
            var result = await _feedbackRepository.AddAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get Feedback By customerId
        public async Task<ResponseModel> GetByCustomerIdAsync()
        {
            var response = new ResponseModel();

            var customerId = _loginService.LoggedInCustomer();
            if (customerId != 0)
            {
                var result = await _feedbackRepository.GetByCustomerIdAsync(customerId);
                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                    response.StatusCode = System.Net.HttpStatusCode.OK;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_customerDoesnotExist, customerId);
                    response.StatusCode = System.Net.HttpStatusCode.NotFound;
                }
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_customerDoesnotExist, customerId);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }
            return response;
        }
        #endregion

        #region Get all
        public async Task<List<FeedbackModel>> GetAsync()
        {
            return await _feedbackRepository.GetAsync();
        } 
        #endregion
    }
}
