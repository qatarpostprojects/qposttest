﻿namespace QPostServices.Domain.Enums
{
    public enum EventStatus
    {
        ItemStored = 21,
        HomeDelivery = 27,
        HoldItem = 28,
        DiscardItem = 29,
        ReturnItem = 30,
        OrderPlaced = 31
    }
}
