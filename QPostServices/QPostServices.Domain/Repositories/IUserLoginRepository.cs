﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IUserLoginRepository
    {
        Task<int> CreateAsync(UserLoginModel model);
        Task<UserLoginModel?> GetByIdAsync(int id);
        Task<List<UserLoginModel>> GetAllActiveAsync();
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(UserLoginModel model);
    }
}
