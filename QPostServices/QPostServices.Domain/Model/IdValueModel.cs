﻿namespace QPostServices.Domain.Model
{
    public class IdValueModel
    {
        public int Id { get; set; }
        public string Value { get; set; } = string.Empty;
        public string? ValueAr { get; set; } 
    }
}
