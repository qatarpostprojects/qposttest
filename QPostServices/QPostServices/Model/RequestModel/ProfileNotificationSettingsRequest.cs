﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class ProfileNotificationSettingsRequest
    {
        public int UserId { get; set; }
        public bool RenewalReminders { get; set; }
        public bool OrderStatusUpdate { get; set; }
        public bool WalletbalanceAlert { get; set; }
        public bool DiscountOffers { get; set; }
        public ProfileNotificationSettingsModel ToModel()
        {
            return new ProfileNotificationSettingsModel()
            {
                UserId = UserId,
                WalletbalanceAlert = WalletbalanceAlert,
                DiscountOffers = DiscountOffers,
                OrderStatusUpdate = OrderStatusUpdate,
                RenewalReminders = RenewalReminders
            };
        }
    }
}
