﻿using QPostServices.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class WalletTransactionModel
    {
        public int CustomerId { get; set; }
        public int WalletId { get; set; }
        public string BillNo { get; set; } = default!;
        public decimal Amount { get; set; }
        public string ReferenceId { get; set; } = default!;
        public string BankTransactionId { get; set; } = default!;
        public DateTime TransactionDate { get; set; }
        public int PaymentId { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; } 
        public int UserId { get; set; }
    }
}
