﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ITermsAndConditionService
    {
        Task<string> GetAsync();
        Task<TermsAndConditionModel> GetDetailsAsync();
        Task<ResponseModel> CreateAsync(TermsAndConditionModel model);
        Task<ResponseModel> UpdateAsync(TermsAndConditionModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
