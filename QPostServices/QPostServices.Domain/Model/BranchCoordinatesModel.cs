﻿namespace QPostServices.Domain.Model
{
    public class BranchCoordinatesModel
    {
        public BranchCoordinatesModel(BranchModel bdetail, double dLat, double dLon)
        {
            try
            {
                BranchId = bdetail.Id;
                var googleCordinates = bdetail.GoogleCordinates;
                var coordinatesArray = googleCordinates!.Split('/');

                if (coordinatesArray.Length == 2)
                {
                    double latitude = Convert.ToDouble(coordinatesArray[0]);
                    double longitude = Convert.ToDouble(coordinatesArray[1]);

                    CalculatedDistance = Math.Abs(dLat - latitude) * 1000 +
                        Math.Abs(dLon - longitude) * 1000; 
                }
            }
            catch (Exception) { }
        }

        public int? BranchId { get; set; }
        public double CalculatedDistance { get; set; }
    }
}
