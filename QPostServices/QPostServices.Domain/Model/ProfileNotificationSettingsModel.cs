﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class ProfileNotificationSettingsModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool RenewalReminders { get;set; }
        public bool OrderStatusUpdate { get;set; }
        public bool WalletbalanceAlert { get;set; }
        public bool DiscountOffers { get;set; }
        public bool Status { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
