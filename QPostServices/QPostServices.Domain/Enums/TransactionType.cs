﻿namespace QPostServices.Domain.Enums
{
    public enum TransactionType
    {
        Credit = 1,
        Debit = 2
    }

    public enum SubscriptionTransactionType
    {
        New = 1,
        Renew = 2,
        Cancel = 3
    }
}
