﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class CustomerOrderDetailsRepository : ICustomerOrderDetailsRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;
        public CustomerOrderDetailsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }
        #region Create
        public async Task<int> CreateAsync(CustomerOrdersDetailsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@subOrderReference", model.SubOrderReference);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@deliveryProductCode", model.DeliveryProductCode);
                parameters.Add("@orderDate", model.OrderDate);
                parameters.Add("@orderAWBNumber", model.OrderAWBNumber);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@deliverySlot", model.DeliverySlot);
                parameters.Add("@pickupSlot", model.PickupSlot);
                parameters.Add("@deliveryDate", model.DeliveryDate);
                parameters.Add("@deliveryTypeId", model.DeliveryTypeId);
                parameters.Add("@source", model.Source);
                parameters.Add("@originCountry", model.OriginCountry);
                parameters.Add("@destinationCountry", model.DestinationCountry);
                parameters.Add("@mailFlow", model.MailFlow);
                parameters.Add("@merchantName", model.MerchantName);
                parameters.Add("@merchantTrackingNumber", model.MerchantTrackingNo);
                parameters.Add("@totalWeight", model.TotalWeight);
                parameters.Add("@volumetricWeight", model.VolumetricWeight);
                parameters.Add("@shipmentContent", model.ShipmentContent);
                parameters.Add("@branchId", model.BranchId);
                parameters.Add("@totalAmountToPaid", model.TotalAmountToPaid);
                parameters.Add("@totalAmountToCollect", model.TotalAmountToCollect);
                parameters.Add("@codRequired", model.CODRequired);
                parameters.Add("@quantity", model.Quantity);
                parameters.Add("@hscode", model.HSCode);
                parameters.Add("@currentStatusCode", model.CurrentStatusCode);
                parameters.Add("@currentStatusDescription", model.CurrentStatusDescription);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }


        #endregion

        #region CreateOrderAsync
        public async Task<int> CreateOrderAsync(CreateOrderModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_CreateOrder]";
                var parameters = new DynamicParameters();
                parameters.Add("@subOrderReference", model.SubOrderReference);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@deliveryProductCode", model.DeliveryProductCode);
                parameters.Add("@orderDate", model.OrderDate);
                parameters.Add("@orderAWBNumber", model.OrderAWBNumber);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@pickupSlot", model.PickupSlot);
                parameters.Add("@deliverySlot", model.DeliverySlot);
                parameters.Add("@deliveryDate", model.DeliveryDate);
                parameters.Add("@deliveryTypeId", model.DeliveryTypeId);
                parameters.Add("@source", model.Source);
                parameters.Add("@originCountry", model.OriginCountry);
                parameters.Add("@destinationCountry", model.DestinationCountry);
                parameters.Add("@mailFlow", model.MailFlow);
                parameters.Add("@merchantName", model.MerchantName);
                parameters.Add("@merchantTrackingNumber", model.MerchantTrackingNo);
                parameters.Add("@totalWeight", model.TotalWeight);
                parameters.Add("@volumetricWeight", model.VolumetricWeight);
                parameters.Add("@shipmentContent", model.ShipmentContent);
                parameters.Add("@branchId", model.BranchId);
                parameters.Add("@totalAmountToPaid", model.TotalAmountToPaid);
                parameters.Add("@totalAmountToCollect", model.TotalAmountToCollect);
                parameters.Add("@codRequired", model.CODRequired);
                parameters.Add("@quantity", model.Quantity);
                parameters.Add("@hscode", model.HSCode);
                parameters.Add("@currentStatusCode", model.CurrentStatusCode);
                parameters.Add("@currentStatusDescription", model.CurrentStatusDescription);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@isHDS", model.IsHDS);
                parameters.Add("@isHold", model.IsHold);
                parameters.Add("@isBranch", model.IsBranch);
                parameters.Add("@paymentId", model.PaymentId);
                parameters.Add("@walletTransactionId", model.WalletTransactionId);
                parameters.Add("@itemValue", model.ItemValue);
                parameters.Add("@itemDescription", model.ItemDescription);

                #region senderAddress
                parameters.Add("@senderCustomerType", AddressType.Sender.ToString());
                parameters.Add("@senderCustomerName", model.SenderOrderAddress.CustomerName);
                parameters.Add("@senderCustomerMobileNumber", model.SenderOrderAddress.CustomerMobileNumber);
                parameters.Add("@senderCustomerAddressZone", model.SenderOrderAddress.CustomerAddressZone);
                parameters.Add("@senderCustomerAddressStreet", model.SenderOrderAddress.CustomerAddressStreet);
                parameters.Add("@senderCustomerAddressBuilding", model.SenderOrderAddress.CustomerAddressBuilding);
                parameters.Add("@senderCustomerAddressUnit", model.SenderOrderAddress.CustomerAddressUnit);
                parameters.Add("@senderCustomerLocationDetails", model.SenderOrderAddress.CustomerLocationDetails);
                parameters.Add("@senderCustomerPOBOXNumber", model.SenderOrderAddress.CustomerPOBOXNumber);
                parameters.Add("@senderCustomerPostCode", model.SenderOrderAddress.CustomerPostCode);
                parameters.Add("@senderCustomerEmail", model.SenderOrderAddress.CustomerEmail);
                parameters.Add("@senderCustomerIdentificationNumber", model.SenderOrderAddress.CustomerIdentificationNumber);
                parameters.Add("@senderCustomerAddress1", model.SenderOrderAddress.CustomerAddress1);
                parameters.Add("@senderCustomerAddress2", model.SenderOrderAddress.CustomerAddress2);
                parameters.Add("@senderCustomerZipCode", model.SenderOrderAddress.CustomerZipCode);
                parameters.Add("@senderIsoCountryCode", model.SenderOrderAddress.IsoCountryCode);
                parameters.Add("@senderReference1", model.SenderOrderAddress.Reference1);
                parameters.Add("@senderReference2", model.SenderOrderAddress.Reference2);
                #endregion

                #region ReceiverAddress
                parameters.Add("@receiverCustomerType", AddressType.Receiver.ToString());
                parameters.Add("@receiverCustomerName", model.RecieverOrderAddress.CustomerName);
                parameters.Add("@receiverCustomerMobileNumber", model.RecieverOrderAddress.CustomerMobileNumber);
                parameters.Add("@receiverCustomerAddressZone", model.RecieverOrderAddress.CustomerAddressZone);
                parameters.Add("@receiverCustomerAddressStreet", model.RecieverOrderAddress.CustomerAddressStreet);
                parameters.Add("@receiverCustomerAddressBuilding", model.RecieverOrderAddress.CustomerAddressBuilding);
                parameters.Add("@receiverCustomerAddressUnit", model.RecieverOrderAddress.CustomerAddressUnit);
                parameters.Add("@receiverCustomerLocationDetails", model.RecieverOrderAddress.CustomerLocationDetails);
                parameters.Add("@receiverCustomerPOBOXNumber", model.RecieverOrderAddress.CustomerPOBOXNumber);
                parameters.Add("@receiverCustomerPostCode", model.RecieverOrderAddress.CustomerPostCode);
                parameters.Add("@receiverCustomerEmail", model.RecieverOrderAddress.CustomerEmail);
                parameters.Add("@receiverCustomerIdentificationNumber", model.RecieverOrderAddress.CustomerIdentificationNumber);
                parameters.Add("@receiverCustomerAddress1", model.RecieverOrderAddress.CustomerAddress1);
                parameters.Add("@receiverCustomerAddress2", model.RecieverOrderAddress.CustomerAddress2);
                parameters.Add("@receiverCustomerZipCode", model.RecieverOrderAddress.CustomerZipCode);
                parameters.Add("@receiverIsoCountryCode", model.RecieverOrderAddress.IsoCountryCode);
                parameters.Add("@receiverReference1", model.RecieverOrderAddress.Reference1);
                parameters.Add("@receiverReference2", model.RecieverOrderAddress.Reference2);
                #endregion

                #region PickupAddress
                parameters.Add("@pickupCustomerType", AddressType.PickUp.ToString());
                parameters.Add("@pickupCustomerName", model.PickUpOrderAddress.CustomerName);
                parameters.Add("@pickupCustomerMobileNumber", model.PickUpOrderAddress.CustomerMobileNumber);
                parameters.Add("@pickupCustomerAddressZone", model.PickUpOrderAddress.CustomerAddressZone);
                parameters.Add("@pickupCustomerAddressStreet", model.PickUpOrderAddress.CustomerAddressStreet);
                parameters.Add("@pickupCustomerAddressBuilding", model.PickUpOrderAddress.CustomerAddressBuilding);
                parameters.Add("@pickupCustomerAddressUnit", model.PickUpOrderAddress.CustomerAddressUnit);
                parameters.Add("@pickupCustomerLocationDetails", model.PickUpOrderAddress.CustomerLocationDetails);
                parameters.Add("@pickupCustomerPOBOXNumber", model.PickUpOrderAddress.CustomerPOBOXNumber);
                parameters.Add("@pickupCustomerPostCode", model.PickUpOrderAddress.CustomerPostCode);
                parameters.Add("@pickupCustomerEmail", model.PickUpOrderAddress.CustomerEmail);
                parameters.Add("@pickupCustomerIdentificationNumber", model.PickUpOrderAddress.CustomerIdentificationNumber);
                parameters.Add("@pickupCustomerAddress1", model.PickUpOrderAddress.CustomerAddress1);
                parameters.Add("@pickupCustomerAddress2", model.PickUpOrderAddress.CustomerAddress2);
                parameters.Add("@pickupCustomerZipCode", model.PickUpOrderAddress.CustomerZipCode);
                parameters.Add("@pickupIsoCountryCode", model.PickUpOrderAddress.IsoCountryCode);
                parameters.Add("@pickupReference1", model.PickUpOrderAddress.Reference1);
                parameters.Add("@pickupReference2", model.PickUpOrderAddress.Reference2);
                #endregion

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetOrderByIdOrTrackingNumberAsync
        public async Task<List<OrderDetailsModel>> GetOrderByIdOrTrackingNumberAsync(string reqInput)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_OrderDetails_GetByCustomerIdOrTrackingNumber]";
                var parameters = new DynamicParameters();
                parameters.Add("@reqInput", reqInput);
                var result = await connection.QueryMultipleAsync(procedure, parameters, commandType: CommandType.StoredProcedure);

                var orderDetails = await result.ReadAsync<OrderDetailsModel>();

                var addresses = await result.ReadAsync<CustomerOrderAddressModel>();

                foreach (var orderDetail in orderDetails)
                {
                    orderDetail.CustomerOrderAddress?.AddRange(addresses.Where(x => x.OrderId == orderDetail.Id));
                }

                return orderDetails.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region UpdateOrderAsync
        public async Task<int> UpdateOrderAsync(int id, CreateOrderModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_UpdateOrder]";
                var parameters = new DynamicParameters();
                parameters.Add("@orderId", id);
                parameters.Add("@subOrderReference", model.SubOrderReference);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@deliveryProductCode", model.DeliveryProductCode);
                parameters.Add("@orderDate", model.OrderDate);
                parameters.Add("@orderAWBNumber", model.OrderAWBNumber);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@pickupSlot", model.PickupSlot);
                parameters.Add("@deliverySlot", model.DeliverySlot);
                parameters.Add("@deliveryDate", model.DeliveryDate);
                parameters.Add("@deliveryTypeId", model.DeliveryTypeId);
                parameters.Add("@source", model.Source);
                parameters.Add("@originCountry", model.OriginCountry);
                parameters.Add("@destinationCountry", model.DestinationCountry);
                parameters.Add("@mailFlow", model.MailFlow);
                parameters.Add("@merchantName", model.MerchantName);
                parameters.Add("@merchantTrackingNumber", model.MerchantTrackingNo);
                parameters.Add("@totalWeight", model.TotalWeight);
                parameters.Add("@volumetricWeight", model.VolumetricWeight);
                parameters.Add("@shipmentContent", model.ShipmentContent);
                parameters.Add("@branchId", model.BranchId);
                parameters.Add("@totalAmountToPaid", model.TotalAmountToPaid);
                parameters.Add("@totalAmountToCollect", model.TotalAmountToCollect);
                parameters.Add("@codRequired", model.CODRequired);
                parameters.Add("@quantity", model.Quantity);
                parameters.Add("@hscode", model.HSCode);
                parameters.Add("@currentStatusCode", model.CurrentStatusCode);
                parameters.Add("@currentStatusDescription", model.CurrentStatusDescription);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@isHDS", model.IsHDS);
                parameters.Add("@isHold", model.IsHold);
                parameters.Add("@isBranch", model.IsBranch);
                parameters.Add("@paymentId", model.PaymentId);
                parameters.Add("@walletTransactionId", model.WalletTransactionId);

                #region senderAddress
                parameters.Add("@senderCustomerType", AddressType.Sender.ToString());
                parameters.Add("@senderCustomerName", model.SenderOrderAddress.CustomerName);
                parameters.Add("@senderCustomerMobileNumber", model.SenderOrderAddress.CustomerMobileNumber);
                parameters.Add("@senderCustomerAddressZone", model.SenderOrderAddress.CustomerAddressZone);
                parameters.Add("@senderCustomerAddressStreet", model.SenderOrderAddress.CustomerAddressStreet);
                parameters.Add("@senderCustomerAddressBuilding", model.SenderOrderAddress.CustomerAddressBuilding);
                parameters.Add("@senderCustomerAddressUnit", model.SenderOrderAddress.CustomerAddressUnit);
                parameters.Add("@senderCustomerLocationDetails", model.SenderOrderAddress.CustomerLocationDetails);
                parameters.Add("@senderCustomerPOBOXNumber", model.SenderOrderAddress.CustomerPOBOXNumber);
                parameters.Add("@senderCustomerPostCode", model.SenderOrderAddress.CustomerPostCode);
                parameters.Add("@senderCustomerEmail", model.SenderOrderAddress.CustomerEmail);
                parameters.Add("@senderCustomerIdentificationNumber", model.SenderOrderAddress.CustomerIdentificationNumber);
                parameters.Add("@senderCustomerAddress1", model.SenderOrderAddress.CustomerAddress1);
                parameters.Add("@senderCustomerAddress2", model.SenderOrderAddress.CustomerAddress2);
                parameters.Add("@senderCustomerZipCode", model.SenderOrderAddress.CustomerZipCode);
                #endregion

                #region ReceiverAddress
                parameters.Add("@receiverCustomerType", AddressType.Receiver.ToString());
                parameters.Add("@receiverCustomerName", model.RecieverOrderAddress.CustomerName);
                parameters.Add("@receiverCustomerMobileNumber", model.RecieverOrderAddress.CustomerMobileNumber);
                parameters.Add("@receiverCustomerAddressZone", model.RecieverOrderAddress.CustomerAddressZone);
                parameters.Add("@receiverCustomerAddressStreet", model.RecieverOrderAddress.CustomerAddressStreet);
                parameters.Add("@receiverCustomerAddressBuilding", model.RecieverOrderAddress.CustomerAddressBuilding);
                parameters.Add("@receiverCustomerAddressUnit", model.RecieverOrderAddress.CustomerAddressUnit);
                parameters.Add("@receiverCustomerLocationDetails", model.RecieverOrderAddress.CustomerLocationDetails);
                parameters.Add("@receiverCustomerPOBOXNumber", model.RecieverOrderAddress.CustomerPOBOXNumber);
                parameters.Add("@receiverCustomerPostCode", model.RecieverOrderAddress.CustomerPostCode);
                parameters.Add("@receiverCustomerEmail", model.RecieverOrderAddress.CustomerEmail);
                parameters.Add("@receiverCustomerIdentificationNumber", model.RecieverOrderAddress.CustomerIdentificationNumber);
                parameters.Add("@receiverCustomerAddress1", model.RecieverOrderAddress.CustomerAddress1);
                parameters.Add("@receiverCustomerAddress2", model.RecieverOrderAddress.CustomerAddress2);
                parameters.Add("@receiverCustomerZipCode", model.RecieverOrderAddress.CustomerZipCode);
                #endregion

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<CustomerOrdersDetailsModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<CustomerOrdersDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get All active
        public async Task<List<CustomerOrdersDetailsModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<CustomerOrdersDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get orders by customerId
        public async Task<List<CustomerOrdersDetailsModel>> GetOrdersByCustomerId(int customerId, string? type)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                parameters.Add("@type", type);
                var result = await connection.QueryAsync<CustomerOrdersDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Orders By Filter
        public async Task<List<CustomerOrdersDetailsModel>> GetOrdersByFilterAsync(string? phoneNumber, string? poBoxNo, string? type, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_GetByFilter]";
                var parameters = new DynamicParameters();
                parameters.Add("@phoneNumber", phoneNumber);
                parameters.Add("@type", type);
                parameters.Add("@poboxNumber", poBoxNo);
                parameters.Add("@startDate", startDate);
                parameters.Add("@endDate", endDate);
                var result = await connection.QueryAsync<CustomerOrdersDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Latest Order By CustomerId
        public async Task<CustomerOrdersDetailsModel?> GetLatestOrderByCustomerId(int customerId, string? type)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_GetLatestOrderByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                parameters.Add("@type", type);
                var result = await connection.QueryAsync<CustomerOrdersDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(CustomerOrdersDetailsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@subOrderReference", model.SubOrderReference);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@deliveryProductCode", model.DeliveryProductCode);
                parameters.Add("@orderDate", model.OrderDate);
                parameters.Add("@orderAWBNumber", model.OrderAWBNumber);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@pickupSlot", model.PickupSlot);
                parameters.Add("@deliverySlot", model.DeliverySlot);
                parameters.Add("@deliveryDate", model.DeliveryDate);
                parameters.Add("@deliveryTypeId", model.DeliveryTypeId);
                parameters.Add("@source", model.Source);
                parameters.Add("@originCountry", model.OriginCountry);
                parameters.Add("@destinationCountry", model.DestinationCountry);
                parameters.Add("@mailFlow", model.MailFlow);
                parameters.Add("@merchantName", model.MerchantName);
                parameters.Add("@merchantTrackingNumber", model.MerchantTrackingNo);
                parameters.Add("@totalWeight", model.TotalWeight);
                parameters.Add("@volumetricWeight", model.VolumetricWeight);
                parameters.Add("@shipmentContent", model.ShipmentContent);
                parameters.Add("@branchId", model.BranchId);
                parameters.Add("@totalAmountToPaid", model.TotalAmountToPaid);
                parameters.Add("@totalAmountToCollect", model.TotalAmountToCollect);
                parameters.Add("@codRequired", model.CODRequired);
                parameters.Add("@quantity", model.Quantity);
                parameters.Add("@hscode", model.HSCode);
                parameters.Add("@currentStatusCode", model.CurrentStatusCode);
                parameters.Add("@currentStatusDescription", model.CurrentStatusDescription);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@isHDS", model.IsHDS);
                parameters.Add("@isHold", model.IsHold);
                parameters.Add("@isBranch", model.IsBranch);
                parameters.Add("@paymentId", model.PaymentId);
                parameters.Add("@walletTransactionId", model.WalletTransactionId);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Get Details By TrackingNumber
        public async Task<CustomerOrderWithAddressModel?> GetByTrackingNumberAsync(string trackingNumber)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_GetOrderDetailsByTrackingNumber]";
                var parameters = new DynamicParameters();
                parameters.Add("@trackingNumber", trackingNumber);
                var result = await connection.QueryAsync<CustomerOrderWithAddressDBModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                var data = result.ToList();

                return MapCustomerOrderWithAddressModels(result.ToList());
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region MyRegion
        public async Task<CheckOrderModel> CheckOrderDetailAsync(string trackingNumber, string? phoneNumber, string? POBoxNumber)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderDetails_CheckOrderDetail]";
                var parameters = new DynamicParameters();
                parameters.Add("@trackingNumber", trackingNumber);
                parameters.Add("@phoneNumber", phoneNumber);
                parameters.Add("@POBoxNumber", POBoxNumber);
                var result = await connection.QueryAsync<CheckOrderModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region OrderGroupMthod
        private CustomerOrderWithAddressModel MapCustomerOrderWithAddressModels(List<CustomerOrderWithAddressDBModel> dbModels)
        {
            var groupedModel = dbModels
                .GroupBy(model => new { model.Id })
                .Select(group => new
                {
                    Id = group.Key.Id,
                    ReceiverAddress = group.FirstOrDefault(m => m.AddressCustomerType == AddressType.Receiver.ToString()),
                    SenderAddress = group.FirstOrDefault(m => m.AddressCustomerType == AddressType.Sender.ToString()),
                    PickupAddress = group.FirstOrDefault(m => m.AddressCustomerType == AddressType.PickUp.ToString()),
                    SubOrderReference = group.FirstOrDefault()?.SubOrderReference,
                    CustomerId = group.FirstOrDefault()?.CustomerId,
                    DeliveryProductCode = group.FirstOrDefault()?.DeliveryProductCode,
                    OrderDate = group.First().OrderDate,
                    OrderAWBNumber = group.FirstOrDefault()?.OrderAWBNumber,
                    TrackingNumber = group.FirstOrDefault()?.TrackingNumber,
                    PickupSlot = group.FirstOrDefault()?.PickupSlot,
                    DeliverySlot = group.FirstOrDefault()?.DeliverySlot,
                    DeliveryDate = group.First().DeliveryDate,
                    DeliveryTypeId = group.FirstOrDefault()?.DeliveryTypeId,
                    Source = group.FirstOrDefault()?.Source,
                    OriginCountry = group.FirstOrDefault()?.OriginCountry,
                    DestinationCountry = group.FirstOrDefault()?.DestinationCountry,
                    MailFlow = group.FirstOrDefault()?.MailFlow,
                    MerchantName = group.FirstOrDefault()?.MerchantName,
                    MerchantTrackingNo = group.FirstOrDefault()?.MerchantTrackingNo,
                    TotalWeight = group.FirstOrDefault()?.TotalWeight,
                    VolumetricWeight = group.FirstOrDefault()?.VolumetricWeight,
                    ShipmentContent = group.FirstOrDefault()?.ShipmentContent,
                    BranchId = group.FirstOrDefault()?.BranchId,
                    TotalAmountToPaid = group.FirstOrDefault()?.TotalAmountToPaid,
                    TotalAmountToCollect = group.FirstOrDefault()?.TotalAmountToCollect,
                    CODRequired = group.FirstOrDefault()?.CODRequired,
                    Quantity = group.FirstOrDefault()?.Quantity,
                    HSCode = group.FirstOrDefault()?.HSCode,

                    CurrentStatusCode = group.FirstOrDefault()?.CurrentStatusCode,
                    CurrentStatusDescription = group.FirstOrDefault()?.CurrentStatusDescription,
                    CurrentStatusDescriptionAr = group.FirstOrDefault()?.CurrentStatusDescriptionAr,
                    CurrentStatusCategory = group.FirstOrDefault()?.CurrentStatusCategory,
                    CurrentStatusCategoryAr = group.FirstOrDefault()?.CurrentStatusCategoryAr,
                    CurrentStatusLevel = group.FirstOrDefault()?.CurrentStatusLevel,

                    Remarks = group.FirstOrDefault()?.Remarks,
                    RecordStatus = group.First().RecordStatus,
                    Reference1 = group.FirstOrDefault()?.Reference1,
                    Reference2 = group.FirstOrDefault()?.Reference2,
                    CreatedUser = group.FirstOrDefault()?.CreatedUser,
                    CreatedUserName = group.FirstOrDefault()?.CreatedUserName,
                    CreatedDate = group.First().CreatedDate,
                    ModifiedUser = group.FirstOrDefault()?.ModifiedUser,
                    ModifiedUserName = group.FirstOrDefault()?.ModifiedUserName,
                    ModifiedDate = group.FirstOrDefault()?.ModifiedDate,
                    ReceiverBoxNumber = group.FirstOrDefault()?.ReceiverBoxNumber,
                    ReceiverAddressId = group.FirstOrDefault()?.ReceiverAddressId,
                    OrderImage = group.FirstOrDefault()?.OrderImage,
                    IsHDS = group.FirstOrDefault()?.IsHDS,
                    IsHold = group.FirstOrDefault()?.IsHold,
                    IsBranch = group.FirstOrDefault()?.IsBranch,
                    StampCode = group.FirstOrDefault()?.StampCode,
                    ItemValue = group.FirstOrDefault()?.ItemValue,
                    ItemDescription = group.FirstOrDefault()?.ItemDescription
                })
                .FirstOrDefault();

            if (groupedModel == null) return new CustomerOrderWithAddressModel();
            CustomerOrderWithAddressModel result = new CustomerOrderWithAddressModel
            {
                Id = groupedModel?.Id ?? 0,
                BranchId = groupedModel?.BranchId ?? string.Empty,
                OrderImage = groupedModel?.OrderImage,
                ReceiverAddressId = groupedModel?.ReceiverAddressId,
                ReceiverBoxNumber = groupedModel?.ReceiverBoxNumber,
                ModifiedDate = groupedModel?.ModifiedDate,
                ModifiedUserName = groupedModel?.ModifiedUserName,
                ModifiedUser = groupedModel?.ModifiedUser,
                CODRequired = groupedModel?.CODRequired ?? false,
                CreatedDate = groupedModel!.CreatedDate,
                CreatedUser = groupedModel?.CreatedUser ?? 0,
                CreatedUserName = groupedModel?.CreatedUserName ?? string.Empty,
                //CurrentStatusCode = groupedModel?.CurrentStatusCode ?? 0,
                //CurrentStatusDescription = groupedModel?.CurrentStatusDescription ?? string.Empty,
                CurrentStatus = new StatusModel
                {
                    CurrentStatusCategory = groupedModel?.CurrentStatusCategory,
                    CurrentStatusCategoryAr = groupedModel?.CurrentStatusCategoryAr,
                    CurrentStatusCode = groupedModel?.CurrentStatusCode ?? 0,
                    CurrentStatusDescription = groupedModel?.CurrentStatusDescription ?? string.Empty,
                    CurrentStatusDescriptionAr = groupedModel?.CurrentStatusDescriptionAr,
                    CurrentStatusLevel = groupedModel?.CurrentStatusLevel ?? 0
                },

                CustomerId = groupedModel?.CustomerId ?? 0,
                DeliveryDate = groupedModel!.DeliveryDate,
                DeliveryProductCode = groupedModel?.DeliveryProductCode ?? string.Empty,
                DeliverySlot = groupedModel?.DeliverySlot ?? string.Empty,
                DeliveryTypeId = groupedModel?.DeliveryTypeId ?? 0,
                DestinationCountry = groupedModel?.DestinationCountry ?? string.Empty,
                HSCode = groupedModel?.HSCode ?? string.Empty,
                MerchantName = groupedModel?.MerchantName ?? string.Empty,
                MailFlow = groupedModel?.MailFlow ?? string.Empty,
                MerchantTrackingNo = groupedModel?.MerchantTrackingNo ?? string.Empty,
                OrderDate = groupedModel!.OrderDate,
                OrderAWBNumber = groupedModel?.OrderAWBNumber ?? string.Empty,
                OriginCountry = groupedModel?.OriginCountry ?? string.Empty,
                PickupSlot = groupedModel?.PickupSlot ?? string.Empty,
                Quantity = groupedModel?.Quantity ?? 0,
                RecordStatus = groupedModel!.RecordStatus,
                Reference1 = groupedModel?.Reference1 ?? string.Empty,
                Reference2 = groupedModel?.Reference2 ?? string.Empty,
                Remarks = groupedModel?.Remarks ?? string.Empty,
                ShipmentContent = groupedModel?.ShipmentContent ?? string.Empty,
                Source = groupedModel?.Source ?? string.Empty,
                SubOrderReference = groupedModel?.SubOrderReference ?? string.Empty,
                TotalWeight = groupedModel?.TotalWeight ?? 0,
                VolumetricWeight = groupedModel?.VolumetricWeight ?? 0,
                TrackingNumber = groupedModel?.TrackingNumber ?? string.Empty,
                TotalAmountToPaid = groupedModel?.TotalAmountToPaid ?? 0,
                TotalAmountToCollect = groupedModel?.TotalAmountToCollect ?? 0,
                IsBranch = groupedModel?.IsBranch ?? false,
                StampCode = groupedModel?.StampCode,
                IsHold = groupedModel?.IsHold ?? false,
                IsHDS = groupedModel?.IsHDS ?? false,
                ItemDescription = groupedModel?.ItemDescription,
                ItemValue = groupedModel?.ItemValue,
                SenderAddress = groupedModel!.SenderAddress != null ? new CustomerOrderAddressModel
                {
                    CreatedDate = groupedModel!.SenderAddress!.AddressCreatedDate,
                    CreatedUser = groupedModel!.SenderAddress!.AddressCreatedUser,
                    CreatedUserName = groupedModel!.SenderAddress!.AddressCreatedUserName,
                    CustomerAddressBuilding = groupedModel!.SenderAddress!.CustomerAddressBuilding,
                    CustomerAddressStreet = groupedModel!.SenderAddress!.CustomerAddressStreet,
                    CustomerAddressUnit = groupedModel!.SenderAddress!.CustomerAddressUnit,
                    CustomerAddressZone = groupedModel!.SenderAddress!.CustomerAddressZone,
                    CustomerEmail = groupedModel!.SenderAddress!.AddressCustomerEmail,
                    CustomerIdentificationNumber = groupedModel!.SenderAddress!.AddressCustomerIdentificationNumber,
                    CustomerLocationDetails = groupedModel!.SenderAddress!.AddressCustomerLocationDetails,
                    CustomerMobileNumber = groupedModel!.SenderAddress!.AddressCustomerMobileNumber,
                    CustomerName = groupedModel!.SenderAddress!.AddressCustomerName,
                    CustomerPOBOXNumber = groupedModel!.SenderAddress!.AddressCustomerPOBOXNumber,
                    IsoCountryCode = groupedModel!.SenderAddress!.IsoCountryCode,
                    CustomerPostCode = groupedModel!.SenderAddress!.AddressCustomerPostCode,
                    CustomerType = groupedModel!.SenderAddress!.AddressCustomerType,
                    Id = groupedModel!.SenderAddress!.AddressId,
                    ModifiedDate = groupedModel!.SenderAddress!.AddressModifiedDate,
                    ModifiedUser = groupedModel!.SenderAddress!.AddressModifiedUser,
                    ModifiedUserName = groupedModel!.SenderAddress!.AddressModifiedUserName,
                    OrderId = groupedModel!.SenderAddress!.AddressOrderId,
                    RecordStatus = groupedModel!.SenderAddress!.AddressRecordStatus,
                    Reference1 = groupedModel!.SenderAddress!.AddressReference1,
                    Reference2 = groupedModel!.SenderAddress!.AddressReference2,
                    TrackingNumber = groupedModel!.SenderAddress!.AddressTrackingNumber,
                    CustomerAddress1 = groupedModel!.SenderAddress!.CustomerAddress1,
                    CustomerAddress2 = groupedModel!.SenderAddress!.CustomerAddress2,
                    CustomerZipCode = groupedModel!.SenderAddress!.CustomerZipCode
                } : null,
                ReceiverAddress = groupedModel!.ReceiverAddress != null ? new CustomerOrderAddressModel
                {
                    CreatedDate = groupedModel!.ReceiverAddress!.AddressCreatedDate,
                    CreatedUser = groupedModel!.ReceiverAddress!.AddressCreatedUser,
                    CreatedUserName = groupedModel!.ReceiverAddress!.AddressCreatedUserName,
                    CustomerAddressBuilding = groupedModel!.ReceiverAddress!.CustomerAddressBuilding,
                    CustomerAddressStreet = groupedModel!.ReceiverAddress!.CustomerAddressStreet,
                    CustomerAddressUnit = groupedModel!.ReceiverAddress!.CustomerAddressUnit,
                    CustomerAddressZone = groupedModel!.ReceiverAddress!.CustomerAddressZone,
                    CustomerEmail = groupedModel!.ReceiverAddress!.AddressCustomerEmail,
                    CustomerIdentificationNumber = groupedModel!.ReceiverAddress!.AddressCustomerIdentificationNumber,
                    CustomerLocationDetails = groupedModel!.ReceiverAddress!.AddressCustomerLocationDetails,
                    CustomerMobileNumber = groupedModel!.ReceiverAddress!.AddressCustomerMobileNumber,
                    CustomerName = groupedModel!.ReceiverAddress!.AddressCustomerName,
                    CustomerPOBOXNumber = groupedModel!.ReceiverAddress!.AddressCustomerPOBOXNumber,
                    IsoCountryCode = groupedModel!.ReceiverAddress!.IsoCountryCode,
                    CustomerPostCode = groupedModel!.ReceiverAddress!.AddressCustomerPostCode,
                    CustomerType = groupedModel!.ReceiverAddress!.AddressCustomerType,
                    Id = groupedModel!.ReceiverAddress!.AddressId,
                    ModifiedDate = groupedModel!.ReceiverAddress!.AddressModifiedDate,
                    ModifiedUser = groupedModel!.ReceiverAddress!.AddressModifiedUser,
                    ModifiedUserName = groupedModel!.ReceiverAddress!.AddressModifiedUserName,
                    OrderId = groupedModel!.ReceiverAddress!.AddressOrderId,
                    RecordStatus = groupedModel!.ReceiverAddress!.AddressRecordStatus,
                    Reference1 = groupedModel!.ReceiverAddress!.AddressReference1,
                    Reference2 = groupedModel!.ReceiverAddress!.AddressReference2,
                    TrackingNumber = groupedModel!.ReceiverAddress!.AddressTrackingNumber,
                    CustomerAddress1 = groupedModel!.ReceiverAddress!.CustomerAddress1,
                    CustomerAddress2 = groupedModel!.ReceiverAddress!.CustomerAddress2,
                    CustomerZipCode = groupedModel!.ReceiverAddress!.CustomerZipCode
                } : null,
                PickupAddress = groupedModel!.PickupAddress != null ? new CustomerOrderAddressModel
                {
                    CreatedDate = groupedModel!.PickupAddress!.AddressCreatedDate,
                    CreatedUser = groupedModel!.PickupAddress!.AddressCreatedUser,
                    CreatedUserName = groupedModel!.PickupAddress!.AddressCreatedUserName,
                    CustomerAddressBuilding = groupedModel!.PickupAddress!.CustomerAddressBuilding,
                    CustomerAddressStreet = groupedModel!.PickupAddress!.CustomerAddressStreet,
                    CustomerAddressUnit = groupedModel!.PickupAddress!.CustomerAddressUnit,
                    CustomerAddressZone = groupedModel!.PickupAddress!.CustomerAddressZone,
                    CustomerEmail = groupedModel!.PickupAddress!.AddressCustomerEmail,
                    CustomerIdentificationNumber = groupedModel!.PickupAddress!.AddressCustomerIdentificationNumber,
                    CustomerLocationDetails = groupedModel!.PickupAddress!.AddressCustomerLocationDetails,
                    CustomerMobileNumber = groupedModel!.PickupAddress!.AddressCustomerMobileNumber,
                    CustomerName = groupedModel!.PickupAddress!.AddressCustomerName,
                    CustomerPOBOXNumber = groupedModel!.PickupAddress!.AddressCustomerPOBOXNumber,
                    IsoCountryCode = groupedModel!.PickupAddress!.IsoCountryCode,
                    CustomerPostCode = groupedModel!.PickupAddress!.AddressCustomerPostCode,
                    CustomerType = groupedModel!.PickupAddress!.AddressCustomerType,
                    Id = groupedModel!.PickupAddress!.AddressId,
                    ModifiedDate = groupedModel!.PickupAddress!.AddressModifiedDate,
                    ModifiedUser = groupedModel!.PickupAddress!.AddressModifiedUser,
                    ModifiedUserName = groupedModel!.PickupAddress!.AddressModifiedUserName,
                    OrderId = groupedModel!.PickupAddress!.AddressOrderId,
                    RecordStatus = groupedModel!.PickupAddress!.AddressRecordStatus,
                    Reference2 = groupedModel!.PickupAddress!.AddressReference2,
                    Reference1 = groupedModel!.PickupAddress!.AddressReference1,
                    TrackingNumber = groupedModel!.PickupAddress!.AddressTrackingNumber,
                    CustomerAddress1 = groupedModel!.PickupAddress!.CustomerAddress1,
                    CustomerAddress2 = groupedModel!.PickupAddress!.CustomerAddress2,
                    CustomerZipCode = groupedModel!.PickupAddress!.CustomerZipCode
                } : null
            };

            return result;
        }

        #endregion

    }
}
