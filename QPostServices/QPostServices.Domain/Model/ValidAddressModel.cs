﻿namespace QPostServices.Domain.Model
{
    public class ValidAddressModel
    {
        public int BuildingNumber { get; set; }
        public int StreetNumber { get; set; }
        public int ZoneNumber { get; set; }
        public bool ValidAddress { get; set; }
    }
}
