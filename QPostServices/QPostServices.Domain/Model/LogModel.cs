﻿namespace QPostServices.Domain.Model
{
    public class LogModel
    {
        public string Method { get; set; } = string.Empty;
        public string RequestName { get; set; } = string.Empty;
        public string RequestUrl { get; set; } = string.Empty;
        public string? QueryString { get; set; }
        public string? Body { get; set; }
        public string? Response { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedUser { get; set; }
    }
}
