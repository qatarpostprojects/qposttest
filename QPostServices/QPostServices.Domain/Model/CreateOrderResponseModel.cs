﻿namespace QPostServices.Domain.Model
{
    public class CreateOrderResponseModel
    {
        public string Order_ID { get; set; }
        public string status { get; set; }
        public Tracking_No tracking_no { get; set; }
        public object remarks { get; set; }
        public object ErrorCode { get; set; }
        public object ErrorResponse { get; set; }
    }

    public class Tracking_No
    {
        public List<Label> Label { get; set; }
    }

    public class Label
    {
        public string output_type { get; set; }
        public string base64Data { get; set; }
        public string barcode_numbers { get; set; }
        public string status { get; set; }
        public Error Error { get; set; }
    }

    public class Error
    {
        public object ErrorCode { get; set; }
        public object ErrorReason { get; set; }
    }
}
