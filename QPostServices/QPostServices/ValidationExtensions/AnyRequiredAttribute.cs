﻿using System.ComponentModel.DataAnnotations;

namespace QPostServices.ValidationExtensions
{
    public class AnyRequiredAttribute : ValidationAttribute
    {
        private readonly string[] _propertyNames;

        public AnyRequiredAttribute(params string[] propertyNames)
        {
            _propertyNames = propertyNames;
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                var anyPropertySet = false;
                var propertyValues = new List<string?>();

                foreach (var propertyName in _propertyNames)
                {
                    var propertyValue = validationContext.ObjectInstance.GetType().GetProperty(propertyName)?.GetValue(validationContext.ObjectInstance, null)?.ToString();
                    propertyValues.Add(propertyValue);

                    if (!string.IsNullOrEmpty(propertyValue))
                    {
                        anyPropertySet = true;
                        break;
                    }
                }

                if (!anyPropertySet)
                {
                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }
    }
}
