﻿namespace QPostServices.Domain.Model
{
    public class HSCODEModel
    {
        public int Id { get; set; }
        public string HSCODE { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string DisplayKeyWord { get; set; } = string.Empty;
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? DescriptionAr { get; set; }
        public string? DisplayKeyWordAr { get; set; }
    }
}
