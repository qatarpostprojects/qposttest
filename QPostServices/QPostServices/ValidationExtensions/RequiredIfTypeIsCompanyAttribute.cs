﻿using QPostPBXService;
using QPostServices.Model.RequestModel;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.ValidationExtensions
{
    public class RequiredIfTypeIsCompanyAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            var subscribePOBoxRequest = (PBXSubscribePOBoxRequest)validationContext.ObjectInstance;

            if (subscribePOBoxRequest.HolderType == Domain.Enums.PBXHolderType.Company && string.IsNullOrEmpty(value?.ToString()))
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}
