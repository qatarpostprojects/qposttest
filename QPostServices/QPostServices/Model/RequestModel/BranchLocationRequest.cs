﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class BranchLocationRequest
    {
        [Required]
        public string Latitude { get; set; } = string.Empty;

        [Required]
        public string Longitude { get; set; } = string.Empty;

        public BranchLocationModel ToModel()
        {
            return new BranchLocationModel
            {
                Latitude = Latitude,
                Longitude = Longitude
            };
        }
    }
}
