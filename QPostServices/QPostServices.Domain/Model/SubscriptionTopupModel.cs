﻿namespace QPostServices.Domain.Model
{
    public class SubscriptionTopupModel
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; } = string.Empty;
        public int Amount { get; set; }
        public string Currency { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string ValidityDescription { get; set; } = string.Empty;
        public int Validity { get; set; }
        public int Type { get; set; } 
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? ColorCode { get; set; }
        public string? DescriptionAr { get; set; }
        public string? ValidityDescriptionAr { get; set; }
    }
}
