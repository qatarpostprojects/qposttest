﻿namespace QPostServices.Domain.Enums
{
    public enum PBXNumberOfEmployees
    {
        None = 1,
        From1To50 = 2,
        From51To100 = 3,
        From101To500 = 4,
        MoreThan500 = 5
    }
}
