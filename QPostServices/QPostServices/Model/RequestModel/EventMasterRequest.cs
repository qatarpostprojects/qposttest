﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class EventMasterRequest
    {
        public int EventCode { get; set; }
        public string EventDescription { get; set; } = string.Empty;
        public string Remarks { get; set; } = string.Empty;

        public EventMasterModel ToCreateModel()
        {
            return new EventMasterModel
            {
                EventCode = EventCode,
                EventDescription = EventDescription,
                Remarks = Remarks
            };
        }

        public EventMasterModel ToUpdateModel(int Id)
        {
            return new EventMasterModel
            {
                Id = Id,
                EventCode = EventCode,
                EventDescription = EventDescription,
                Remarks = Remarks
            };
        }
    }
}
