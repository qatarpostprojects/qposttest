﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ISubscriptionTransactionNumber
    {
        Task<TransactionNumberModel> GetLatestAsync(int serviceId);
    }
}
