﻿using Newtonsoft.Json;
using QPostServices.Domain.Helper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class TrackService : ITrackService
    {
        private readonly AppSettings _appSettings;
        private readonly ITrackRepository _trackRepository;

        public TrackService(AppSettings appSettings, ITrackRepository trackRepository)
        {
            _appSettings = appSettings;
            _trackRepository = trackRepository;
        }

        #region TrackAndTrace
        public async Task<object?> TrackAndTrace(TrackAndTraceModel model)
        {
            var client = new HttpClient();
            var requestModel = JsonConvert.SerializeObject(model);
            var request = new HttpRequestMessage(HttpMethod.Post, $"{_appSettings.ExternalApiUrl}DigitalService/rest/WrapperService/TrackAndTrace");
            var content = new StringContent(requestModel, System.Text.Encoding.UTF8, "application/json");
            request.Content = content;
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var result = JsonConvert.DeserializeObject<TrackAndTraceResponseModel>(await response.Content.ReadAsStringAsync());
                    return result;
                }
                catch (Exception)
                {

                    return await response.Content.ReadAsStringAsync();
                }
            }
            else
            {
                return await response.Content.ReadAsStringAsync();
            }
        }
        #endregion

        #region TrackAndTraceByMobile
        public async Task<object?> TrackAndTraceByMobile(TrackMobileModel model)
        {
            return await _trackRepository.TrackAndTraceByMobile(model);
        }
        #endregion

        #region AddLogsToTable
        public async Task AddLogAsync(LogModel log)
        {
            await _trackRepository.AddLogAsync(log);
        }
        #endregion

        #region Get MobileNumber
        public async Task<string?> GetMobileNumberAsync(string searchString)
        {
            return await _trackRepository.GetMobileNumberAsync(searchString);
        }
        #endregion
    }
}
