﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class HSCODERequest
    {
        [Required]
        public string HSCODE { get; set; } = string.Empty;
        [Required]
        public string Description { get; set; } = string.Empty;
        [Required]
        public string DisplayKeyWord { get; set; } = string.Empty;
        public string? DescriptionAr { get; set; }
        public string? DisplayKeyWordAr { get; set; }

        public HSCODEModel ToCreateModel()
        {
            return new HSCODEModel()
            {
                HSCODE = this.HSCODE,
                Description = this.Description,
                DisplayKeyWord = this.DisplayKeyWord,
                DescriptionAr = this.DescriptionAr,
                DisplayKeyWordAr = this.DisplayKeyWordAr
            };
        }
        public HSCODEModel ToUpdateModel(int id)
        {
            return new HSCODEModel()
            {
                HSCODE = this.HSCODE,
                Description = this.Description,
                DisplayKeyWord = this.DisplayKeyWord,
                Id = id,
                DescriptionAr = this.DescriptionAr,
                DisplayKeyWordAr = this.DisplayKeyWordAr
            };
        }
    }
}
