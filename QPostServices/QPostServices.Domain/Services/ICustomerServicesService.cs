﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerServicesService
    {
        Task<ResponseModel> CreateAsync(ServiceModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<ServiceModel>> GetAllActiveAsync();
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(ServiceModel model);
        Task<ResponseModel> GetServiceAmountAsync(int id);
        Task<ResponseModel> GetCalculatedServiceAmountAsync(CalculateServiceAmountModel model);
        Task<List<ServiceModel>> GetDetailsByCustomerIdAsync();
        Task<List<IdValueModel>> GetLookupAsync(ServiceType serviceType);
    }
}
