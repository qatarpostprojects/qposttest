﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPositionRepository
    {
        Task<List<PositionModel>> GetListAsync();
        Task<PositionModel> GetByIdAsync(int id);
        Task<int> CreateAsync(PositionModel model);
        Task<int> UpdateAsync(PositionModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
