﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class HSCODERepository : IHSCODERepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public HSCODERepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(HSCODEModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_HSCODE_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@hscode", model.HSCODE);
                parameters.Add("@description", model.Description);
                parameters.Add("@displayKeyWord", model.DisplayKeyWord);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@active", true);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                parameters.Add("@displayKeyWordAr", model.DisplayKeyWordAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get by Id
        public async Task<HSCODEModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_HSCODE_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<HSCODEModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault() ?? new HSCODEModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Search Text
        public async Task<List<HSCODEModel>> GetBySearchText(string searchText)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_HSCODE_GetBySearchText]";
                var parameters = new DynamicParameters();
                parameters.Add("@searchText", searchText);
                var result = await connection.QueryAsync<HSCODEModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get
        public async Task<List<HSCODEModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_HSCODE_Get]";
                var result = await connection.QueryAsync<HSCODEModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(HSCODEModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_HSCODE_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@hscode", model.HSCODE);
                parameters.Add("@description", model.Description);
                parameters.Add("@displayKeyWord", model.DisplayKeyWord);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                parameters.Add("@displayKeyWordAr", model.DisplayKeyWordAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, bool status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_HSCODE_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
