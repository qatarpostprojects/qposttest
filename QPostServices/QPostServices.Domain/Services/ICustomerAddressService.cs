﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerAddressService
    {
        Task<ResponseModel> CreateAsync(CustomerAddressModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<CustomerAddressModel>> GetAddressesListByCustomerId(int customerId);
        Task<CustomerAddressModel?> GetPrimaryAddressByType(AddressType type);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(CustomerAddressModel model);
    }
}
