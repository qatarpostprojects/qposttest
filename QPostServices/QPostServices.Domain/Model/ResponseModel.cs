﻿using System.Net;

namespace QPostServices.Domain.Model
{
    public class ResponseModel
    {
        public bool Error { get; set; }
        public HttpStatusCode? StatusCode { get; set; }
        public object? Response { get; set; }
    }

   


}
