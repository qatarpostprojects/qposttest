﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class ProductWeightRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        [Required]
        public int DeliveryProductId { get; set; }
        [Required]
        public decimal Weight { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public ProductWeightModel ToCreateModel()
        {
            return new ProductWeightModel()
            {
                Name = Name,
                Description = Description,
                DeliveryProductId = DeliveryProductId,
                Weight = Weight,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
        public ProductWeightModel ToUpdateModel(int id)
        {
            return new ProductWeightModel()
            {
                Name = Name,
                Description = Description,
                //DeliveryProductId = DeliveryProductId,
                Weight = Weight,
                Id = id,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
