﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class TimeSlotService : ITimeSlotService
    {
        private const string _successCreation = "Time Slot created successfully";
        private const string _successUpdate = "Time Slot updated successfully";
        private const string _failedCreation = "Time Slot creation failed";
        private const string _failedUpdation = "Time Slot updation failed";
        private const string _successDelete = "Time Slot deleted successfully";
        private const string _failedDelete = "Time Slot deletion failed";
        private const string _TimeSlotDoesNotExist = "Time Slot with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly ITimeSlotRepository _timeSlotRepository;
        public TimeSlotService(ITimeSlotRepository timeSlotRepository, ILoginService loginService)
        {
            _timeSlotRepository = timeSlotRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(TimeSlotModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();

            var result = await _timeSlotRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _timeSlotRepository.UpdateStatusAsync(id, Domain.Enums.RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_TimeSlotDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get
        public async Task<List<TimeSlotModel>> GetAsync()
        {
            return await _timeSlotRepository.GetAsync();
        }
        #endregion

        #region Get by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _timeSlotRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_TimeSlotDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(TimeSlotModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();

            var result = await _timeSlotRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_TimeSlotDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
