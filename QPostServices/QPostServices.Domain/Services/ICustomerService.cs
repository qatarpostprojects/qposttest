﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerService
    {
        Task<ResponseModel> RegistrationAsync(CustomerRegistraionModel model);
        Task<ResponseModel> CreateAsync(CustomerModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<CustomerModel>> GetAllActiveAsync();
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(CustomerModel model);
        Task<ResponseModel> SendOtpAsync(OtpModel model);
        Task<ResponseModel> UpdateProfileAsync(CustomerProfileModel model);
        Task<bool> VerifyOTPAsync(string value, string otp);
        Task<bool> CheckPasswordValidAsync(int id, string password);
        Task<ResponseModel> ChangePasswordAsync(int id, string password);
        Task<ResponseModel> MapExistingRequestOTPAsync(int customerId,POBoxEnquiryModel model);
        Task<ResponseModel> MapExistingServiceAsync(int customerId,POBoxEnquiryModel model);
        Task<ResponseModel> MapOrderCustomerAsync(MapOrderCustomerModel model);
        Task<ResponseModel> UpdateLanguagePreference(int customerId,string language);
    }
}
