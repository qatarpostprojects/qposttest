﻿namespace QPostServices.Domain.Model
{
    public class CustomerRegistraionModel
    {
        public string FirstName { get; set; } = default!;
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
        public string CustomerUserName { get; set; } = string.Empty;
        public string CustomerPassword { get; set; } = string.Empty;
        public string SecurityQuestion { get; set; } = string.Empty;
        public string SecurityAnswer { get; set; } = string.Empty;
        public string DocumentType { get; set; } = default!;
        public string? DocumentReference { get; set; }
        public string MobileNumber { get; set; } = default!;
        public string? AlternativeMobileNumber { get; set; }
        public string Nationality { get; set; } = default!;
        public DateTime? DateOfBirth { get; set; }
        public string PoBoxNumber { get; set; } = default!;
        public string ConnectedVpNumber { get; set; } = default!;
        public string? SmartBoxNumber { get; set; }
        public DateTime? PoBoxExpiry { get; set; }
        public DateTime? SmartBoxExpiry { get; set; }
        public int? WalletId { get; set; }
        public decimal? WalletBalanceAmount { get; set; }
        public string? WalletReferenceNumber { get; set; }
        public string? HDS { get; set; }
        public int? HDSPackageId { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? Reference3 { get; set; }
        public int CreatedUser { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; } = string.Empty;
        public string EmailAddress { get; set; } = string.Empty;
        public string? PreferedLanguage { get; set; }
        public string? FirstNameAr { get; set; }
        public string? MiddleNameAr { get; set; }
        public string? LastNameAr { get; set; }
    }
}
