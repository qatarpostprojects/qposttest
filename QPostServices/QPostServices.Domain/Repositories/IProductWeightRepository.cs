﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IProductWeightRepository
    {
        Task<List<ProductWeightModel>> GetListAsync();
        Task<List<ProductWeightModel>> GetByDeliveryProductId(int deliveryProductId);
        Task<int> CreateAsync(ProductWeightModel model);
        Task<int> UpdateAsync(ProductWeightModel model);
        Task<int> UpdateStatusAsync(int id, bool status, int userId);
        Task<ProductWeightModel> GetByIdAsync(int id);
    }
}
