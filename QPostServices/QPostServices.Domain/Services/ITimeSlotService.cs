﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ITimeSlotService
    {
        Task<ResponseModel> CreateAsync(TimeSlotModel model);
        Task<ResponseModel> UpdateAsync(TimeSlotModel model);
        Task<ResponseModel> DeleteAsync(int id);
        Task<List<TimeSlotModel>> GetAsync();
        Task<ResponseModel> GetByIdAsync(int id);
    }
}
