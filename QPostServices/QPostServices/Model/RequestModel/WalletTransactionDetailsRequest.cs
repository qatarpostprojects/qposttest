﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class WalletTransactionDetailsRequest
    {
        [Required]
        public int CustomerId { get; set; }
        
        [Required]
        public string BillNo { get; set; } = default!;
        
        [Required]
        public decimal Amount { get; set; }
        
        [Required]
        public TransactionType Type { get; set; }
        
        [Required]
        public string ReferenceId { get; set; } = default!;
        
        [Required]
        public string BankTransactionId { get; set; } = default!;
        
        [Required]
        public DateTime Date { get; set; }
        public int PaymentId { get; set; }
        
        public string? Remarks { get; set; }
        
        public string? Reference1 { get; set; }

        public WalletTransactionDetailsModel ToCreateModel()
        {
            return new WalletTransactionDetailsModel
            {
                CustomerId= CustomerId,
                BillNo= BillNo,
                Amount= Amount,
                Type= Type,
                ReferenceId= ReferenceId,
                BankTransactionId= BankTransactionId,
                Date = Date,
                PaymentId = PaymentId,
                Remarks= Remarks,
                Reference1 = Reference1
            };
        }
        public WalletTransactionDetailsModel ToUpdateModel(int id)
        {
            return new WalletTransactionDetailsModel
            {
                Id= id,
                CustomerId = CustomerId,
                BillNo = BillNo,
                Amount = Amount,
                Type = Type,
                ReferenceId = ReferenceId,
                BankTransactionId = BankTransactionId,
                Date = Date,
                PaymentId = PaymentId,
                Remarks = Remarks,
                Reference1 = Reference1
            };
        }
    }
}
