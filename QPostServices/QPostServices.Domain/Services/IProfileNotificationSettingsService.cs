﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IProfileNotificationSettingsService
    {
        Task<ResponseModel> CreateAsync(ProfileNotificationSettingsModel model);
        Task<ResponseModel> UpdateAsync(ProfileNotificationSettingsModel model);
        Task<ResponseModel> GetByUserId(int userId);
    }
}
