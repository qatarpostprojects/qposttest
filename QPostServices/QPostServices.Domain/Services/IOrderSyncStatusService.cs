﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IOrderSyncStatusService
    {
        Task<List<OrderSyncStatusModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(OrderSyncStatusModel model);
    }
}
