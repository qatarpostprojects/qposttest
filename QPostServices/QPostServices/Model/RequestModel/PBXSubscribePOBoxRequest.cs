﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PBXSubscribePOBoxRequest
    {
        [Required]
        //[StringLength(6, MinimumLength = 1)]
        public string POBoxNo { get; set; } = default!;

        [Required]
        //[StringLength(6, MinimumLength = 6)]
        public string CostCenter { get; set; } = default!;

        [Required]
        //[StringLength(9, MinimumLength = 9)]
        public string HoldKey { get; set; } = default!;

        [Required]
        [EnumRequired(typeof(PBXBoxCategory))]
        public PBXBoxCategory Category { get; set; }

        //[Required]
        [EnumRequired(typeof(PBXSubType))]
        public PBXSubType SubType { get; set; }

        [Required]
        [EnumRequired(typeof(PBXHolderType))]
        public PBXHolderType HolderType { get; set; }

        //[Required]
        public string POBoxLocationID { get; set; } = default!;
        public string? POBoxLocationName { get; set; }

        //[Required]
        public DateTime? StartDate { get; set; }

        //[Required]
        public DateTime? PaidUntil { get; set; }
        [Required]
        public int MonthValue { get; set; }

        [RequiredIfTypeIsCompany(ErrorMessage = "Company Name is required when Holder Type is Company.")]
        [StringLength(40)]
        public string? CompanyName { get; set; }

        [RequiredIfTypeIsCompany(ErrorMessage = "Company Reg Number is required when Holder Type is Company.")]
        [StringLength(10)]
        public string? CompanyRegNumber { get; set; }

        //[Required]
        public PBXCustomerRequest Customer { get; set; } = new();
        //public InternalRequest InternalRequest { get; set; } = new();
        public PBXSubscribeModel ToModel()
        {
            return new PBXSubscribeModel()
            {
                Category = Category,
                CompanyName = CompanyName,
                CompanyRegNumber = CompanyRegNumber,
                CostCenter = CostCenter,
                Customer = Customer.ToModel(),
                StartDate = DateTime.Now,
                PaidUntil = DateTime.Now.AddMonths(MonthValue),
                HolderType = HolderType,
                HoldKey = HoldKey,
                POBoxLocationID = POBoxLocationID,
                POBoxLocationName = POBoxLocationName,
                POBoxNo = POBoxNo,
                SubType = SubType
            };
        }
    }

    //public class InternalRequest
    //{
    //    public int POBoxTypeId { get; set; }
    //    public int BranchId { get; set; }
    //    public int Rented { get; set; }
    //    public DateTime LastPaidUntil { get; set; }
    //    public string? GeoLocationX { get; set; }
    //    public string? GeoLocationY { get; set; }
    //    public string? SmartBoxNumber { get; set; }
    //    public int IsSmartBox { get; set; }
    //    public int POBoxPositionID { get; set; }
    //    public int POBoxLockTypeID { get; set; }
    //    public string? Location { get; set; }
    //    public string? Reserved { get; set; }
    //    public bool Blocked { get; set; }
    //    public string? Remarks { get; set; }
    //    public string? GroupId { get; set; }
    //    public string? SmartCardNumber { get; set; }
    //}
}
