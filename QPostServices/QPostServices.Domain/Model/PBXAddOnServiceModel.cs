﻿namespace QPostServices.Domain.Model
{
    public class PBXAddOnServiceModel
    {
        public string Description { get; set; } = default!;

        public string Value { get; set; } = default!;

        public double Amount { get; set; } = default!;

        public double BaseRate { get; set; } = default!;
    }
}
