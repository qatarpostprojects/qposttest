﻿namespace QPostServices.Domain.Model
{
    public class TransactionNumberModel
    {
        public int Id { get; set; }
        public string Prefix { get; set; } = string.Empty;
        public int ServiceId { get; set; }
        public int UniqueSequenceNumber { get; set; }
    }
}
