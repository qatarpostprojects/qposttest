﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPickupTypeService
    {
        Task<List<PickupTypeModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(PickupTypeModel model);
        Task<ResponseModel> UpdateAsync(PickupTypeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
