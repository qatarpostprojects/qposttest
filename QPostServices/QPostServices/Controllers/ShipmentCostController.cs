﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.ResponseModel;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentCostController : ControllerBase
    {
        private readonly IShipmentCostService _shipmentCostService;
        public ShipmentCostController(IShipmentCostService shipmentCostService)
        {
            _shipmentCostService = shipmentCostService;
        }

        #region Get Service by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<PriceCalculatorModel>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet(Name = nameof(GetShipmentCost))]
        public async Task<IActionResult> GetShipmentCost([FromQuery] PriceCalculatorModel request)
        {
            if (!(request.ShipmentType.ToLower() == "domestic" || request.ShipmentType.ToLower() == "overseas"))
            {
                return NotFound();
            }
            try
            {
                var result = await _shipmentCostService.GetCostDetailsAsync(request);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                var response = ((List<ShipmentCostModel>)result.Response!).Select(x => ShipmentCost.FromModel(x));
                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
