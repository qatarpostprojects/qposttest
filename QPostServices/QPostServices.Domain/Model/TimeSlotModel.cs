﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class TimeSlotModel
    {
        public int Id { get; set; }
        public string TimeSlot { get; set; } = string.Empty;
        public string? TimeSlotAr { get; set; }
        public string? StartTime { get; set; }
        public string? EndTime { get; set;}
        public RecordStatus RecordStatus { get;set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set;}
        public DateTime ModifiedDate { get; set;}
    }
}
