﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPaymentMethodRepository
    {
        Task<List<PaymentMethodModel>> GetListAsync();
        Task<int> CreateAsync(PaymentMethodModel model);
        Task<int> UpdateAsync(PaymentMethodModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
        Task<PaymentMethodModel> GetByIdAsync(int id);
    }
}
