﻿namespace QPostServices.Domain.Enums
{
    public enum PBXAddOnServiceType
    {
        OnePerWeek = 1,
        TwoPerWeek = 2,
        ThreePerWeek = 3
    }
}
