﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HSCODEController : ControllerBase
    {
        private readonly IHSCODEService _hscodeService;
        public HSCODEController(IHSCODEService hscodeService)
        {
            _hscodeService = hscodeService;
        }

        #region Get HSCODE List
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<HSCODEResponse>))]
        [HttpGet(Name = nameof(GetHSCODEList))]
        public async Task<IActionResult> GetHSCODEList()
        {
            try
            {
                var results = await _hscodeService.GetListAsync();
                return Ok(results.Select(x => HSCODEResponse.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get HSCODE By Id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(HSCODEResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetHSCODE))]
        public async Task<IActionResult> GetHSCODE(int id)
        {
            try
            {
                var result = await _hscodeService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(HSCODEResponse.FromModel((HSCODEModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get HSCODE By SearchText 
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(HSCODESearchResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("search/{searchText}", Name = nameof(GetHSCODEBySearchText))]
        public async Task<IActionResult> GetHSCODEBySearchText(string searchText)
        {
            try
            {
                var results = await _hscodeService.GetBySearchText(searchText);
                return Ok(results.Select(x => HSCODESearchResponse.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateHSCODE))]
        public async Task<IActionResult> CreateHSCODE([FromBody] HSCODERequest request)
        {
            try
            {
                var result = await _hscodeService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateHSCODE))]
        public async Task<IActionResult> UpdateHSCODE(int id, [FromBody] HSCODERequest request)
        {
            try
            {
                var result = await _hscodeService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteHSCODE))]
        public async Task<IActionResult> DeleteHSCODE(int id)
        {
            try
            {
                var result = await _hscodeService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
