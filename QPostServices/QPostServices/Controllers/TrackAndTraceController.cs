﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using System.Security.Claims;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TrackAndTraceController : ControllerBase
    {
        private readonly ITrackService _trackService;
        private readonly ICustomerService _customerService;

        public TrackAndTraceController(ITrackService trackService, ICustomerService customerService)
        {
            _trackService = trackService;
            _customerService = customerService;
        }

        #region GetTracking
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("GetTracking", Name = nameof(GetTracking))]
        public async Task<IActionResult> GetTracking([FromBody] TrackAndTraceRequest request)
        {
            object? result = null;
            var logData = SetupLogAsync(request);
            try
            {
                if (await _customerService.VerifyOTPAsync(request.OTPRequest.Value, request!.OTPRequest!.OTP))
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(request.req_Input, "^[0-9]+$")) //If request contains only numbers then check for mobile
                    {
                        result = await _trackService.TrackAndTraceByMobile(request.ToMobileModel());
                    }
                    else                                                                             //Else check for TrackingNumber
                    {
                        result = await _trackService.TrackAndTrace(request.ToModel());
                    }
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Invalid OTP");
                }
            }
            catch (Exception ex)
            {
                var errorResponse = new
                {
                    ErrorMessage = "Internal Error, please try again.",
                    IsSuccess = false,
                    ErrorDescription = ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : "")
                };
                result = errorResponse;
                return StatusCode(500, errorResponse.ErrorMessage);
            }
            finally
            {
                logData.Response = JsonConvert.SerializeObject(result);
                await _trackService.AddLogAsync(logData);
            }
        }
        #endregion

        #region Setting up Logs
        private LogModel SetupLogAsync(object? request = null)
        {
            var userData = HttpContext.User.Claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier)?.Value;
            int? userId = string.IsNullOrEmpty(userData) ? null : int.Parse(userData);

            var requestLog = new LogModel
            {
                Method = HttpContext.Request.Method,
                RequestName = HttpContext.Request.Path,
                RequestUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.Path}",
                QueryString = HttpContext.Request.QueryString.ToString(),
                Body = JsonConvert.SerializeObject(request),
                CreatedDate = DateTime.Now,
                CreatedUser = userId
            };
            return requestLog;
        }
        #endregion

        #region SendOTP
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("SendOTP", Name = nameof(SendTrackAndTraceOTP))]
        public async Task<IActionResult> SendTrackAndTraceOTP([FromBody] TrackAndTraceOTPRequest request)
        {
            try
            {
                var mob = request.Value;
                if (!request.IsMobileNumber)
                {
                    mob = await _trackService.GetMobileNumberAsync(request.Value);
                }
                if (!string.IsNullOrEmpty(mob))
                {
                    var result = await _customerService.SendOtpAsync(new OtpModel { Note = request.Note, Value = mob, Type = request.Type });
                    if (!result.Error)
                    {
                        return Ok(mob);
                    }

                    return BadRequest(result.Response);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get MobileNumber
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("GetMobileNumber", Name = nameof(GetMobileNumber))]
        public async Task<IActionResult> GetMobileNumber(string searchString)
        {
            try
            {
                object? result = await _trackService.GetMobileNumberAsync(searchString);

                return Ok(result);
            }
            catch (Exception ex)
            {
                var errorResponse = new
                {
                    ErrorMessage = "Internal Error, please try again.",
                    IsSuccess = false,
                    ErrorDescription = ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : "")
                };
                return StatusCode(500, errorResponse.ErrorMessage);
            }
        }
        #endregion
    }
}
