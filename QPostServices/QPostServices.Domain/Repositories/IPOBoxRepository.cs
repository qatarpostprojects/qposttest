﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPOBoxRepository
    {
        Task<List<POBoxModel>> GetListAsync();
        Task<int> CreateAsync(POBoxModel model);
        Task<int> UpdateAsync(POBoxModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status,int userId);
        Task<POBoxModel> GetByIdAsync(int id);
        Task<List<POBoxServiceModel>> GetAllLandingPageServicesAsync();
    }
}
