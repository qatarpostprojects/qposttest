﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class PaymentService : IPaymentService
    {
        private const string _paymentDoesNotExist = "Payment with id {0} does not exists";
        private const string _successPaymentCreation = "Payment created successfully";
        private const string _failedPaymentCreation = "Payment creation failed";
        private const string _successPaymentDeleted = "Payment with id {0} deleted successfully";
        private const string _successPaymentUpdate = "Payment with id {0} updated successfully";        

        private readonly ILoginService _loginService;
        private readonly IPaymentRepository _paymentRepository;
        public PaymentService(IPaymentRepository paymentRepository,ILoginService loginService)
        {
            _paymentRepository = paymentRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(PaymentModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _paymentRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedPaymentCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _paymentRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successPaymentDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_paymentDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _paymentRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_paymentDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get By CustomerId
        public Task<List<PaymentModel>> GetPaymentListByCustomerId(int customerId)
        {
            return _paymentRepository.GetAllActiveByCustomerIdAsync(customerId);
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(PaymentModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _paymentRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successPaymentUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_paymentDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Recent Transactions By Customer
        public async Task<List<RecentTransactionModel>> GetRecentTransactionsByCustomerIdAsync(int customerId)
        {
            return await _paymentRepository.GetRecentTransactionsByCustomerIdAsync(customerId);
        } 
        #endregion
    }
}
