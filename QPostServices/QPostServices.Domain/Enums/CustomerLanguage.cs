﻿namespace QPostServices.Domain.Enums
{
    public enum CustomerLanguage
    {
        English = 1,
        Arabic = 2
    }
}
