﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class PaymentMethod
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? Image { get; set; }
        public int ListingOrder { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? RedirectingURL { get; set; }
        public string? NameAr { get; set; }

        public static PaymentMethod FromModel(PaymentMethodModel model)
        {
            return new PaymentMethod
            {
                Id = model.Id,
                Name = model.Name,
                Image = model.Image,
                ListingOrder = model.ListingOrder,
                RecordStatus = model.RecordStatus,
                CreatedUser = model.CreatedUser,
                CreatedDate = model.CreatedDate,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                RedirectingURL = model.RedirectingURL,
                NameAr = model.NameAr
            };
        }
    }
}
