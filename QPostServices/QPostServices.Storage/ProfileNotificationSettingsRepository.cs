﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class ProfileNotificationSettingsRepository : IProfileNotificationSettingsRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public ProfileNotificationSettingsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(ProfileNotificationSettingsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProfileNotificationSettings_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@userId", model.UserId);
                parameters.Add("@discountOffers", model.DiscountOffers);
                parameters.Add("@walletBalanceAlert", model.WalletbalanceAlert);
                parameters.Add("@renewalReminders", model.RenewalReminders);
                parameters.Add("@orderStatusUpdate", model.OrderStatusUpdate);
                parameters.Add("@status", true);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By User Id
        public async Task<ProfileNotificationSettingsModel?> GetByUserId(int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProfileNotificationSettings_GetByUserId]";
                var parameters = new DynamicParameters();
                parameters.Add("@userId", userId);
                var result = await connection.QueryAsync<ProfileNotificationSettingsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region Update
        public async Task<int> UpdateAsync(ProfileNotificationSettingsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ProfileNotificationSettings_UpdateByUserId]";
                var parameters = new DynamicParameters();
                parameters.Add("@userId", model.UserId);
                parameters.Add("@orderStatusUpdate", model.OrderStatusUpdate);
                parameters.Add("@renewalReminders", model.RenewalReminders);
                parameters.Add("@walletBalanceAlert", model.WalletbalanceAlert);
                parameters.Add("@discountOffers", model.DiscountOffers);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch { throw; }
        } 
        #endregion
    }
}
