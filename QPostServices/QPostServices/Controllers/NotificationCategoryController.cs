﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationCategoryController : ControllerBase
    {
        private readonly INotificationCategoryServices _notificationCategoryServices;

        public NotificationCategoryController(INotificationCategoryServices notificationCategoryServices)
        {
            _notificationCategoryServices = notificationCategoryServices;
        }

        #region Create Notification Category
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateNotificationCategory))]
        public async Task<IActionResult> CreateNotificationCategory([FromBody] NotificationCategoryRequest request)
        {
            try
            {
                var result = await _notificationCategoryServices.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Notification Categories
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<NotificationCategory>))]
        [HttpGet("GetNotifications",Name = nameof(GetNotificationCategoryList))]
        public async Task<IActionResult> GetNotificationCategoryList()
        {
            try
            {
                var results = await _notificationCategoryServices.GetListAsync();

                return Ok(results.Select(x => NotificationCategory.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
