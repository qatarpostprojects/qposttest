﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ILocationService
    {
        Task<List<string>> GetZonesAsync(int zoneNumber);
        Task<List<string>> GetStreetsAsync(int zoneNumber);
        Task<List<string>> GetBuildingsAsync(int zoneNumber, int streetNumber);
        Task<ResponseModel> ValidateAddressAsync(ValidAddressModel model);
    }
}
