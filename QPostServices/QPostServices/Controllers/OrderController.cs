﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderSyncStatusService _orderService;
        private readonly ICustomerOrderDetailsService _customerOrderDetailsService;
        private readonly IStampCodeService _stampCodeService;

        public OrderController(IOrderSyncStatusService orderService, ICustomerOrderDetailsService customerOrderDetailsService,
            IStampCodeService stampCodeService)
        {
            _orderService = orderService;
            _customerOrderDetailsService = customerOrderDetailsService;
            _stampCodeService = stampCodeService;
        }

        #region Create Order
        [AllowAnonymous]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("CreateOrder", Name = nameof(CreateOrder))]
        public async Task<IActionResult> CreateOrder([FromBody] CreateOrderRequest request)
        {
            try
            {
                var result = await _customerOrderDetailsService.CreateOrderAsync(request.ToModel());
                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get OrderDetails by TrackingNumber Or CustomerID
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("GetOrderDetails/{reqInput}", Name = nameof(GetOrderDetailsByIdOrTrackingNumber))]
        public async Task<IActionResult> GetOrderDetailsByIdOrTrackingNumber(string reqInput)
        {
            try
            {
                var result = await _customerOrderDetailsService.GetOrderByIdOrTrackingNumberAsync(reqInput);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                var orders = (List<OrderDetailsModel>)result.Response!;
                return Ok(orders.Select(x => OrderDetails.FromModel(x)));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Order
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPut("UpdateOrder/{id}", Name = nameof(UpdateOrder))]
        public async Task<IActionResult> UpdateOrder(int id, [FromBody] CreateOrderRequest request)
        {
            try
            {
                var result = await _customerOrderDetailsService.UpdateOrderAsync(id, request.ToModel());
                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create OrderSyncStatus
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("SyncStatus", Name = nameof(CreateOrderSyncStatus))]
        public async Task<IActionResult> CreateOrderSyncStatus([FromBody] OrderSyncStatusRequest request)
        {
            try
            {
                var result = await _orderService.CreateAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get OrderSyncStatus by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OrderSyncStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetOrderSyncStatus))]
        public async Task<IActionResult> GetOrderSyncStatus(int id)
        {
            try
            {
                var result = await _orderService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(OrderSyncStatus.FromModel((OrderSyncStatusModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region GetOrderSyncStatusList
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<OrderSyncStatus>))]
        [HttpGet(Name = nameof(GetOrderSyncStatusList))]
        public async Task<IActionResult> GetOrderSyncStatusList()
        {
            try
            {
                var results = await _orderService.GetListAsync();

                return Ok(results.Select(x => OrderSyncStatus.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create Stamp code
        [AllowAnonymous]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("CreateStampCode", Name = nameof(CreateStampCode))]
        public async Task<IActionResult> CreateStampCode([FromBody] StampCodeRequest request)
        {
            try
            {
                var result = await _stampCodeService.CreateAsync(request.ToModel());
                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Stamp code
        [AllowAnonymous]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("StampCode/Order/{id}", Name = nameof(GetStampCode))]
        public async Task<IActionResult> GetStampCode(int id)
        {
            try
            {
                var result = await _stampCodeService.GetByOrderIdAsync(id);
                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
