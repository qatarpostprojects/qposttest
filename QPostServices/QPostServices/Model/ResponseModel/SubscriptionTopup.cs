﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class SubscriptionTopup
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; } = string.Empty;
        public int Amount { get; set; }
        public string Currency { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string ValidityDescription { get; set; } = string.Empty;
        public int Validity { get; set; }
        public int Type { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? ColorCode { get; set; }
        public string? DescriptionAr { get; set; }
        public string? ValidityDescriptionAr { get; set; }

        public static SubscriptionTopup FromModel(SubscriptionTopupModel model)
        {
            return new SubscriptionTopup
            {
                Id = model.Id,
                Amount = model.Amount,
                Currency = model.Currency,
                Description = model.Description,
                ValidityDescription = model.ValidityDescription,
                Validity = model.Validity,
                Type = model.Type,
                Active = model.Active,
                CreatedDate = model.CreatedDate,
                CreatedUserId = model.CreatedUserId,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserId = model.ModifiedUserId,
                ModifiedUserName = model.ModifiedUserName,
                ServiceId = model.ServiceId,
                ServiceName = model.ServiceName,
                ColorCode = model.ColorCode,
                DescriptionAr = model.DescriptionAr,
                ValidityDescriptionAr = model.ValidityDescriptionAr
            };
        }
    }
}
