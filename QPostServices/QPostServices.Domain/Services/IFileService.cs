﻿using Microsoft.AspNetCore.Http;

namespace QPostServices.Domain.Services
{
    public interface IFileService
    {
        Task<string> ConvertImageToBase64(IFormFile image);
    }
}
