﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerCardDetails
    {
        public int Id { get; set; }
        public string CardNumber { get; set; } = string.Empty;
        public string CardProvider { get; set; } = string.Empty;
        public string CardType { get; set; } = string.Empty;
        public int CustomerId { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public static CustomerCardDetails FromModel(CustomerCardDetailsModel model)
        {
            return new CustomerCardDetails
            {
                Id = model.Id,
                CardType = model.CardType,
                CardNumber = model.CardNumber,
                CardProvider = model.CardProvider,
                CreatedDate = model.CreatedDate,
                ModifiedDate = model.ModifiedDate,
                CustomerId = model.CustomerId,
                Active = model.Active
            };
        }
    }
}
