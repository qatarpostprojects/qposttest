﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class NotificationCategoryServices : INotificationCategoryServices
    {
        private readonly INotificationCategoryRepository _notificationCategoryRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Notification category created successfully";
        private const string _successUpdate = "Notification category updated successfully";
        private const string _failedCreation = "Notification category creation failed";
        private const string _failedUpdation = "Notification category updation failed";
        private const string _successDelete = "Notification category deleted successfully";
        private const string _failedDelete = "Notification category deletion failed";
        private const string _nameAlreadyExist = "Notification category with name : {0} already exist";
        private const string _notificationCategoryDoesNotExist = "Notification category with id : {0} Does not exist";


        public NotificationCategoryServices(INotificationCategoryRepository notificationCategoryRepository, ILoginService loginService)
        {
            _notificationCategoryRepository = notificationCategoryRepository;
            _loginService = loginService;
        }

        #region Create NotificationCategory
        public async Task<ResponseModel> CreateAsync(NotificationCategoryModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _notificationCategoryRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get list of NotificationCategory
        public async Task<List<NotificationCategoryModel>> GetListAsync()
        {
            return await _notificationCategoryRepository.GetListAsync();
        }
        #endregion
    }
}
