﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryName { get; set; } = string.Empty;
        public string? CountryCode { get; set; }
        public string? Flag { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? CountryNameAr { get; set; }

        public static Country FromModel(CountryModel model)
        {
            return new Country
            {
                Id = model.Id,
                CountryName = model.CountryName,
                CountryCode = model.CountryCode,
                Flag = model.Flag,
                RecordStatus = model.RecordStatus.ToString(),
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                CountryNameAr = model.CountryNameAr,
            };
        }
    }
}
