﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IOrderContentRepository
    {
        Task<List<OrderContentModel>> GetListAsync();
        Task<int> CreateAsync(OrderContentModel model);
        Task<int> UpdateAsync(OrderContentModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
        Task<OrderContentModel> GetByIdAsync(int id);
    }
}
