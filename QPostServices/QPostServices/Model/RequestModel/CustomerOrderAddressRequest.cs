﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerOrderAddressRequest
    {
        [Required]
        public int OrderId { get; set; }
        [Required]
        public string TrackingNumber { get; set; } = string.Empty;
        [Required]
        public string CustomerType { get; set; } = string.Empty;
        [Required]
        public string CustomerName { get; set; } = string.Empty;
        [Required]
        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string CustomerMobileNumber { get; set; } = string.Empty;
        //[Required]
        public string? CustomerAddressZone { get; set; } 
        //[Required]
        public string? CustomerAddressStreet { get; set; } 
        //[Required]
        public string? CustomerAddressBuilding { get; set; } 
        //[Required]
        public string? CustomerAddressUnit { get; set; }
        public string? CustomerLocationDetails { get; set; }
        [Required]
        public string CustomerPOBOXNumber { get; set; } = string.Empty;
        public string CustomerPostCode { get; set; } = string.Empty;
        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string CustomerEmail { get; set; } = string.Empty;
        [Required]
        public string CustomerIdentificationNumber { get; set; } = string.Empty;
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? CustomerAddress1 { get; set; }
        public string? CustomerAddress2 { get; set; }
        public string? CustomerZipCode { get; set; }

        public CustomerOrderAddressModel ToCreateModel()
        {
            return new CustomerOrderAddressModel()
            {
                CustomerAddressBuilding = CustomerAddressBuilding,
                CustomerAddressUnit = CustomerAddressUnit,
                CustomerLocationDetails = CustomerLocationDetails,
                CustomerPostCode = CustomerPostCode,
                CustomerEmail = CustomerEmail,
                CustomerIdentificationNumber = CustomerIdentificationNumber,
                Reference1 = Reference1,
                Reference2 = Reference2,
                CustomerAddressStreet = CustomerAddressStreet,
                CustomerAddressZone = CustomerAddressZone,
                CustomerMobileNumber = CustomerMobileNumber,
                CustomerName = CustomerName,
                CustomerPOBOXNumber = CustomerPOBOXNumber,
                CustomerType = CustomerType,
                OrderId = OrderId,
                TrackingNumber = TrackingNumber,
                CustomerAddress1 = CustomerAddress1,
                CustomerAddress2 = CustomerAddress2,
                CustomerZipCode= CustomerZipCode
            };
        }
        public CustomerOrderAddressModel ToUpdateModel(int id)
        {
            return new CustomerOrderAddressModel()
            {
                CustomerAddressZone=CustomerAddressZone,
                CustomerMobileNumber=CustomerMobileNumber,
                CustomerName=CustomerName,
                CustomerPOBOXNumber=CustomerPOBOXNumber,
                CustomerType=CustomerType,
                OrderId=OrderId,
                TrackingNumber=TrackingNumber,
                CustomerAddressBuilding=CustomerAddressBuilding,
                CustomerAddressUnit=CustomerAddressUnit,
                CustomerLocationDetails=CustomerLocationDetails,
                CustomerPostCode=CustomerPostCode,
                CustomerEmail=CustomerEmail,
                CustomerIdentificationNumber=CustomerIdentificationNumber,
                Reference1=Reference1,
                Reference2 = Reference2,
                CustomerAddressStreet=CustomerAddressStreet,
                Id=id,
                CustomerAddress1 = CustomerAddress1,
                CustomerAddress2 = CustomerAddress2,
                CustomerZipCode = CustomerZipCode
            };
        }
    }
}
