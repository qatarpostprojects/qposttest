﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace QPostServices.Storage
{
    public class StampCodeRepository : IStampCodeRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public StampCodeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<string?> CreateStampCodeAsync(StampCodeModel model)
        {
            var stampCode = GenerateUniqueCode(model.OrderId);
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_OrderStampCode_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@orderId", model.OrderId);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@stampCode", stampCode);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<string>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return !string.IsNullOrEmpty(result.ToString()) ? stampCode : null;
            }
            catch
            {
                throw;
            }
        }

        public static string GenerateUniqueCode(int orderId)
        {
            string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            StringBuilder code = new StringBuilder();
            Random random = new Random(orderId);

            code.Append(orderId.ToString());

            while (code.Length < 8)
            {
                code.Append(characters[random.Next(characters.Length)]);
            }

            return code.ToString();
        }

        public async Task<StampCodeModel?> GetByOrderId(int orderId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_OrderStampCode_GetByOrderId]";
                var parameters = new DynamicParameters();
                parameters.Add("@orderId", orderId);
                var result = await connection.QueryAsync<StampCodeModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
    }
}
