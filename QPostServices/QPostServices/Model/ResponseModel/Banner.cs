﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Banner
    {
        public int Id { get; set; }
        public string? BannerAr { get; set; }
        public string? BannerEng { get; set; }
        public string Device { get; set; } = string.Empty;
        public int ListingOrder { get; set; }
        public bool Active { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static Banner FromModel(BannerModel model)
        {
            return new Banner()
            {
                Id = model.Id,
                BannerAr = model.BannerAr,
                BannerEng = model.BannerEng,
                Device = model.Device,
                ListingOrder = model.ListingOrder,
                Active = model.Active,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                CreatedDate = model.CreatedDate
            };
        }
    }
}
