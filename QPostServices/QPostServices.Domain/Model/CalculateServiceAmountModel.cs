﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class CalculateServiceAmountModel
    {
        public string? Range { get; set; }
        public string Type { get; set; } = string.Empty;
        public int Duration { get; set; }
        public int ServiceId { get; set; }
    }
}
