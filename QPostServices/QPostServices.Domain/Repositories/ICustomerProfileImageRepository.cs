﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerProfileImageRepository
    {
        Task<CustomerProfileImageModel> GetCustomerProfileImageAsync(int customerId);
        Task<int> CreateImageAsync(CustomerProfileImageModel model);
        Task<int> UpdateImageAsync(CustomerProfileImageModel model);
    }
}
