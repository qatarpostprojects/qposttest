﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerOrdersDetailsController : ControllerBase
    {
        private readonly ICustomerOrderDetailsService _customerOrderDetailsService;
        private readonly ICustomerService _customerService;
        public CustomerOrdersDetailsController(ICustomerOrderDetailsService customerOrderDetailsService, ICustomerService customerService)
        {
            _customerOrderDetailsService = customerOrderDetailsService;
            _customerService = customerService;
        }

        #region Create Customer OrdersDetails
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateCustomerOrdersDetails))]
        public async Task<IActionResult> CreateCustomerOrdersDetails([FromBody] CustomerOrdersDetailsRequest request)
        {
            try
            {
                var result = await _customerOrderDetailsService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer OrdersDetails by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerOrdersDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetCustomerOrdersDetails))]
        public async Task<IActionResult> GetCustomerOrdersDetails(int id)
        {
            try
            {
                var result = await _customerOrderDetailsService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(CustomerOrdersDetails.FromModel((CustomerOrdersDetailsModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer OrdersDetails
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerOrdersDetails>))]
        [HttpGet(Name = nameof(GetCustomerOrdersDetailsList))]
        public async Task<IActionResult> GetCustomerOrdersDetailsList()
        {
            try
            {
                var results = await _customerOrderDetailsService.GetAllActiveAsync();

                return Ok(results.Select(x => CustomerOrdersDetails.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer OrdersDetails by customerId
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerOrdersDetails>))]
        [HttpGet("Customer/{customerId}/Detail", Name = nameof(GetCustomerOrderDetailsByCustomerIdList))]
        public async Task<IActionResult> GetCustomerOrderDetailsByCustomerIdList(int customerId)
        {
            try
            {
                var results = await _customerOrderDetailsService.GetByCustomerIdAsync(customerId);

                return Ok(results.Select(x => CustomerOrdersDetails.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Customer OrdersDetails
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteCustomerOrdersDetails))]
        public async Task<IActionResult> DeleteCustomerOrdersDetails(int id)
        {
            try
            {
                var result = await _customerOrderDetailsService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Customer OrdersDetails
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateCustomerOrdersDetails))]
        public async Task<IActionResult> UpdateCustomerOrdersDetails(int id, [FromBody] CustomerOrdersDetailsRequest request)
        {
            try
            {
                var result = await _customerOrderDetailsService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get My OrdersDetails
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerOrdersDetails>))]
        [HttpGet("Customer/{customerId}", Name = nameof(GetMyOrders))]
        public async Task<IActionResult> GetMyOrders(int customerId, string? type)
        {
            try
            {
                var results = await _customerOrderDetailsService.GetMyOrdersAsync(customerId, type);

                return Ok(results);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get order by filter
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerOrdersDetails>))]
        [HttpGet("Filter", Name = nameof(GetByFilter))]
        public async Task<IActionResult> GetByFilter(string? phoneNumber, string? poBoxNo, string? type, Domain.Enums.FilterDateTypes? filterDateTypes)
        {
            try
            {
                var results = await _customerOrderDetailsService.GetOrdersByFilterAsync(phoneNumber, poBoxNo, type, filterDateTypes);

                return Ok(results);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create Inbound Request
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("Inbound", Name = nameof(CreateInboundRequest))]
        public async Task<IActionResult> CreateInboundRequest([FromBody] InboundRequest request)
        {
            try
            {
                var result = await _customerOrderDetailsService.CreateInboundRequestAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }
                if (result.StatusCode == HttpStatusCode.NotFound)
                {
                    return NotFound(result.Response);
                }
                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Latest Tracking Details
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetOrderDetails))]
        [HttpGet("Customer/GetLatestTrackingDetails", Name = nameof(GetLatestTrackingDetails))]
        public async Task<IActionResult> GetLatestTrackingDetails(string? type)
        {
            try
            {
                var result = await _customerOrderDetailsService.GetLatestTrackingDetails(type);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(result.Response!);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer OrdersDetails by TrackingNumber
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("OrderDetail/{trackingNumber}", Name = nameof(GetByTrackingNumber))]
        public async Task<IActionResult> GetByTrackingNumber(string trackingNumber)
        {
            try
            {
                var result = await _customerOrderDetailsService.GetByTrackingNumberAsync(trackingNumber);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(result.Response!);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region CheckOrderDetail
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("CheckOrderDetail/{trackingNumber}", Name = nameof(CheckOrderDetail))]
        public async Task<IActionResult> CheckOrderDetail(string trackingNumber, [FromQuery] PhoneNoPoBoxRequest request)
        {
            try
            {
                var result = await _customerOrderDetailsService.CheckOrderDetailAsync(trackingNumber, request.PhoneNumber, request.POBoxNumber);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(result.Response!);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region VerifyOrderDetail
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("VerifyOrderDetail/{trackingNumber}", Name = nameof(VerifyOrderDetail))]
        public async Task<IActionResult> VerifyOrderDetail([Required] string trackingNumber, [Required] string phoneNumber, [Required] string otp)
        {
            try
            {
                var result = new ResponseModel();

                if (await _customerService.VerifyOTPAsync(phoneNumber, otp))
                {
                    result = await _customerOrderDetailsService.GetByTrackingNumberAsync(trackingNumber);

                    if (result.Error)
                    {
                        return NotFound(result.Response);
                    }

                    return Ok(result.Response!);
                }
                else
                {
                    result.Response = "Invalid OTP";
                    return BadRequest(result.Response);
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
