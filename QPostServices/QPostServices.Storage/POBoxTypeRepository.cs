﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class POBoxTypeRepository : IPOBoxTypeRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public POBoxTypeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(POBoxTypeModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxType_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@logo", model.Logo);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@active", true);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<POBoxTypeModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxType_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<POBoxTypeModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault() ?? new POBoxTypeModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get
        public async Task<List<POBoxTypeModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxType_Get]";
                var result = await connection.QueryAsync<POBoxTypeModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(POBoxTypeModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxType_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@logo", model.Logo);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, bool status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxType_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
