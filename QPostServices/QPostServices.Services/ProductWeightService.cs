﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class ProductWeightService : IProductWeightService
    {
        private const string _successCreation = "Product weight created successfully";
        private const string _successUpdate = "Product weight updated successfully";
        private const string _failedCreation = "Product weight creation failed";
        private const string _failedUpdation = "Product weight updation failed";
        private const string _successDelete = "Product weight deleted successfully";
        private const string _failedDelete = "Product weight deletion failed";
        private const string _ProductWeightDoesNotExist = "Product weight with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly IProductWeightRepository _productWeightRepository;
        public ProductWeightService(IProductWeightRepository productWeightRepository, ILoginService loginService)
        {
            _productWeightRepository = productWeightRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(ProductWeightModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();

            var result = await _productWeightRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _productWeightRepository.UpdateStatusAsync(id, false, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_ProductWeightDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Delivery Product Id
        public async Task<List<ProductWeightModel>> GetByDeliveryProductId(int deliveryProductId)
        {
            return await _productWeightRepository.GetByDeliveryProductId(deliveryProductId);
        } 
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _productWeightRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_ProductWeightDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All
        public async Task<List<ProductWeightModel>> GetListAsync()
        {
            return await _productWeightRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(ProductWeightModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();

            var result = await _productWeightRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_ProductWeightDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
