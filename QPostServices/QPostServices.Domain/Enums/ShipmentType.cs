﻿namespace QPostServices.Domain.Enums
{
    public enum ShipmentType
    {
        Domestic = 1,
        Overseas = 2
    }
}
