﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPOBoxDurationRepository
    {
        Task<List<POBoxDurationModel>> GetListByServiceIdAsync(int serviceId);
        Task<int> CreateAsync(POBoxDurationModel model);
        Task<int> UpdateAsync(POBoxDurationModel model);
        Task<int> UpdateStatusAsync(int id, bool status, int userId);
        Task<POBoxDurationModel> GetByIdAsync(int id);
        Task<List<POBoxDurationModel>> GetListAsync();
    }
}
