﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerOrderDetailsRepository
    {
        Task<List<CustomerOrdersDetailsModel>> GetListAsync();
        Task<List<CustomerOrdersDetailsModel>> GetOrdersByCustomerId(int customerId, string? type);
        Task<CheckOrderModel> CheckOrderDetailAsync(string trackingNumber, string? phoneNumber, string? POBoxNumber);
        Task<List<CustomerOrdersDetailsModel>> GetOrdersByFilterAsync(string? phoneNumber, string? poBoxNo, string? type, DateTime? startDate, DateTime? endDate);
        Task<CustomerOrdersDetailsModel?> GetLatestOrderByCustomerId(int customerId, string? type);
        Task<CustomerOrdersDetailsModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(CustomerOrdersDetailsModel model);
        Task<int> UpdateAsync(CustomerOrdersDetailsModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
        Task<CustomerOrderWithAddressModel?> GetByTrackingNumberAsync(string trackingNumber);
        Task<int> CreateOrderAsync(CreateOrderModel model);
        Task<List<OrderDetailsModel>> GetOrderByIdOrTrackingNumberAsync(string reqInput);
        Task<int> UpdateOrderAsync(int id, CreateOrderModel model);
    }
}
