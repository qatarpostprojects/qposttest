﻿using Dapper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace QPostServices.Storage
{
    public class CustomerCardDetailsRepository:ICustomerCardDetailsRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerCardDetailsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create Card Details
        public async Task<int> CreateCardDetailsAsync(CustomerCardDetailsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerCardDetails_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@cardNumber", model.CardNumber);
                parameters.Add("@cardProvider", model.CardProvider);
                parameters.Add("@cardType", model.CardType);
                parameters.Add("@active", true);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Card Details By Customer Id
        public async Task<List<CustomerCardDetailsModel>> GetCardDetailsByCustomerIdAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerCardDetails_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<CustomerCardDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Card Details By Id
        public async Task<CustomerCardDetailsModel?> GetCardDetailsByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerCardDetails_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<CustomerCardDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Card Details
        public async Task<int> UpdateCardDetailAsync(CustomerCardDetailsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerCardDetails_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@cardNumber", model.CardNumber);
                parameters.Add("@cardType", model.CardType);
                parameters.Add("@cardProvider", model.CardProvider);
                parameters.Add("@id", model.Id);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update Card status
        public async Task<int> UpdateCardStatusAsync(int id, bool status)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerCardDetails_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", status);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
