﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Security.Authentication.ExtendedProtection;

namespace QPostServices.Services
{
    public class CustomerSubscriptionService : ICustomerSubscriptionService
    {
        private const string _subscriptionDoesNotExist = "Customer subscription with id {0} does not exists";
        private const string _successCreation = "Customer subscription created successfully";
        private const string _failedCreation = "Customer subscription creation failed";
        private const string _successCustomerDeleted = "Customer subscription with id {0} deleted successfully";
        private const string _successCustomerRenewed = "Customer subscription with id {0} Renewed successfully";
        private const string _successUpdate = "Customer subscription with id {0} updated successfully";

        private readonly ICustomerSubscriptionRepository _customerSubscriptionRepository;
        private readonly ILoginService _loginService;
        private readonly IServiceRepository _serviceRepository;
        private readonly ISubscriptionTransactionRepository _subscriptionTransactionRepository;
        private readonly ISubscriptionTransactionNumber _subscriptionTransactionNumber;
        private readonly ISubscriptionTopupRepository _subscriptionTopupRepository;
        private readonly IPOBoxService _poBoxService;
        private readonly IFileService _fileService;

        public CustomerSubscriptionService(ICustomerSubscriptionRepository customerSubscriptionRepository,
            ILoginService loginService, IServiceRepository serviceRepository,
            ISubscriptionTransactionRepository subscriptionTransactionRepository, ISubscriptionTransactionNumber subscriptionTransactionNumber,
            ISubscriptionTopupRepository subscriptionTopupRepository, IPOBoxService poBoxService, IFileService fileService)
        {
            _customerSubscriptionRepository = customerSubscriptionRepository;
            _loginService = loginService;
            _serviceRepository = serviceRepository;
            _subscriptionTransactionRepository = subscriptionTransactionRepository;
            _subscriptionTransactionNumber = subscriptionTransactionNumber;
            _subscriptionTopupRepository = subscriptionTopupRepository;
            _poBoxService = poBoxService;
            _fileService = fileService;
        }


        #region Create Customer subscription
        public async Task<ResponseModel> CreateAsync(CustomerSubscriptionModel model)
        {
            try
            {
                var response = new ResponseModel();

                if (model.QatarIdFrontSideFile != null)
                {
                    model.QatarIdFrontSide = await _fileService.ConvertImageToBase64(model.QatarIdFrontSideFile);
                }
                if (model.QatarIdBackSideFile != null)
                {
                    model.QatarIdBackSide = await _fileService.ConvertImageToBase64(model.QatarIdBackSideFile);
                }

                if (model.CompanyDocFrontSideFile != null)
                {
                    model.CompanyDocFrontSide = await _fileService.ConvertImageToBase64(model.CompanyDocFrontSideFile);
                }
                if (model.CompanyDocBackSideFile != null)
                {
                    model.CompanyDocBackSide = await _fileService.ConvertImageToBase64(model.CompanyDocBackSideFile);
                }
                var service = await _serviceRepository.GetByIdAsync(model.ServiceId);
                if (service == null)
                {
                    response.Error = true;
                    response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                }

                model = PlanForNewSubscription(model, service!);

                model.CreatedUser = _loginService.LoggedInUser();

                //if (model.ExternalSubscriptionModel != null)
                //{
                //    model.ExternalSubscriptionModel.StartDate = model.StartDate;
                //    model.ExternalSubscriptionModel.PaidUntil = model.EndDate;
                //    var result = await _poBoxService.SubscribePOBoxAsync(model.ExternalSubscriptionModel);
                //}

                var subscriptionId = await _customerSubscriptionRepository.CreateAsync(model);

                if (subscriptionId > 0)
                {
                    var last = await _subscriptionTransactionNumber.GetLatestAsync(model.ServiceId);
                    var transaction = new SubscriptionTransactionModel
                    {
                        SubscriptionId = subscriptionId,
                        CardPaymentId = model.CardPaymentId,
                        CreatedUser = model.CreatedUser,
                        PaymentType = model.PaymentType,
                        TransactionAmount = service!.SubscriptionAmount * model.SubscriptionTypeValue,
                        WalletTransactioId = model.WalletTransactioId,
                        TransactionType = SubscriptionTransactionType.New.ToString(),
                        ServiceId = model.ServiceId,
                        TransactionDate = DateTime.Now,
                        TransactionReferenceNumber = GenerateReferenceNumber(last.Prefix, model.ServiceId, last.UniqueSequenceNumber),
                        AWBNumber = model.AWBNumber
                    };
                    await _subscriptionTransactionRepository.CreateAsync(transaction);

                    response.Response = subscriptionId;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Create Customer Add On subscription
        public async Task<ResponseModel> CreateAddOnAsync(CustomerSubscriptionModel model)
        {
            try
            {
                var response = new ResponseModel();

                var service = await _serviceRepository.GetByIdAsync(model.ServiceId);
                if (service == null)
                {
                    response.Error = true;
                    response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                }

                model = PlanForNewSubscription(model, service!);

                model.CreatedUser = _loginService.LoggedInUser();

                var subscriptionId = await _customerSubscriptionRepository.CreateAsync(model);

                if (subscriptionId > 0)
                {
                    var last = await _subscriptionTransactionNumber.GetLatestAsync(model.ServiceId);
                    var transaction = new SubscriptionTransactionModel
                    {
                        SubscriptionId = subscriptionId,
                        CardPaymentId = model.CardPaymentId,
                        CreatedUser = model.CreatedUser,
                        PaymentType = model.PaymentType,
                        TransactionAmount = service!.SubscriptionAmount * model.SubscriptionTypeValue,
                        WalletTransactioId = model.WalletTransactioId,
                        TransactionType = SubscriptionTransactionType.New.ToString(),
                        ServiceId = model.ServiceId,
                        TransactionDate = DateTime.Now,
                        TransactionReferenceNumber = GenerateReferenceNumber(last.Prefix, model.ServiceId, last.UniqueSequenceNumber),
                        AWBNumber = model.AWBNumber
                    };
                    await _subscriptionTransactionRepository.CreateAsync(transaction);

                    response.Response = new
                    {
                        SubscriptionId = subscriptionId,
                        Date = transaction.CreatedDate,
                        PaymentMethod = model.PaymentType,
                        TransactionId = transaction.TransactionReferenceNumber,
                        TransactionAmount= transaction.TransactionAmount,
                        NoOfDeliveries = model.NumberOfTransactionPending,
                        EndDate = model.EndDate
                    };
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        public async Task MapExistingCustomer(CustomerSubscriptionModel model)
        {
            var serviceModel = await _serviceRepository.GetByIdAsync(100) ?? new ServiceModel();
            model.ServiceId = serviceModel.Id;

            model.MaximumNumberOfTransaction = serviceModel.ServiceBaseOn == ServiceBasedOn.Transaction.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.NumberOfTransactionPending = serviceModel.ServiceBaseOn == ServiceBasedOn.Transaction.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.NumberOfTransactionCompleted = 0;
            model.ServiceAmount = serviceModel.ServiceBaseOn == ServiceBasedOn.Amount.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.ServiceAmountConsumed = serviceModel.ServiceBaseOn == ServiceBasedOn.Amount.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.ServiceAmountPending = 0;
            await _customerSubscriptionRepository.CreateAsync(model);
        }

        #region Get All Active Customer subscription
        public Task<List<CustomerSubscriptionDetailModel>> GetAllActiveByCustomerIdAsync(int customerId)
        {
            return _customerSubscriptionRepository.GetAllActiveByCustomerIdAsync(customerId);
        }
        #endregion

        #region Get Nearby Expiry Subscriptions
        public Task<List<CustomerSubscriptionModel>> GetNearbyExpirySubscriptions()
        {
            return _customerSubscriptionRepository.GetNearbyExpirySubscriptions();
        }
        #endregion

        #region Get Customer subscription By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _customerSubscriptionRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_subscriptionDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetAddOnServices
        public async Task<List<IdValueModel>> GetAddOnServicesAsync()
        {
            try
            {
                var customerId = _loginService.LoggedInCustomer();

                var services = await _customerSubscriptionRepository.GetServiceIdsByCustomerId(customerId);

                var addOnServices = await _serviceRepository.GetAddOnServicesByIds(services);

                return addOnServices;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update Customer subscription
        public async Task<ResponseModel> UpdateAsync(CustomerSubscriptionModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _customerSubscriptionRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_subscriptionDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Customer subscription
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _customerSubscriptionRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successCustomerDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_subscriptionDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region RenewAsync
        public async Task<ResponseModel> RenewAsync(SubscriptionRenewModel model)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            try
            {
                var subscription = await _customerSubscriptionRepository.GetByIdAsync(model.SubscriptionId);

                if (subscription != null)
                {
                    var service = await _serviceRepository.GetByIdAsync(subscription!.ServiceId);
                    var renew = PlanForRenewSubscription(subscription, service!, model.SubscriptionTypeValue);
                    renew.ModifiedUser = userId;
                    var result = await _customerSubscriptionRepository.RenewAsync(renew);

                    // await _poBoxService.RenewPostBoxAsync(model.ExternalRenewModel.POBoxNo,model.ExternalRenewModel.Period);

                    if (result > 0)
                    {
                        var last = await _subscriptionTransactionNumber.GetLatestAsync(subscription!.ServiceId);
                        var transaction = new SubscriptionTransactionModel
                        {
                            SubscriptionId = model.SubscriptionId,
                            CardPaymentId = model.CardPaymentId,
                            CreatedUser = userId,
                            PaymentType = model.PaymentType,
                            TransactionAmount = service!.RenewAmount * model.SubscriptionTypeValue,
                            WalletTransactioId = model.WalletTransactioId,
                            TransactionType = SubscriptionTransactionType.Renew.ToString(),
                            TransactionDate = DateTime.Now,
                            ServiceId = subscription!.ServiceId,
                            AWBNumber = model.AWBNumber,
                            TransactionReferenceNumber = GenerateReferenceNumber(last.Prefix, subscription!.ServiceId, last.UniqueSequenceNumber)
                        };
                        await _subscriptionTransactionRepository.CreateAsync(transaction);

                        var data = new
                        {
                            SubscriptionId = model.SubscriptionId,
                            ServiceId = service.Id,
                            ServiceName = service.Name,
                            PoBox = model.ExternalRenewModel.POBoxNo,
                            Date = transaction.TransactionDate,
                            EndDate = renew.EndDate,
                            TransactionReferenceNumber = transaction.TransactionReferenceNumber,
                            TransactionAmount = transaction.TransactionAmount
                        };

                        response.Response = data;
                        response.Error = false;
                        return response;
                    }
                }

                response.Error = true;
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
                response.Response = string.Format(_subscriptionDoesNotExist, model.SubscriptionId);

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region PlanForNewSubscription
        private CustomerSubscriptionModel PlanForNewSubscription(CustomerSubscriptionModel model, ServiceModel serviceModel)
        {
            var now = DateTime.Now;
            DateTime enddate;

            var type = (ServiceSubscriptionType)Enum.Parse(typeof(ServiceSubscriptionType), serviceModel.SubscriptionType);


            switch (type)
            {
                case ServiceSubscriptionType.Daily:
                    enddate = now.AddDays(model.SubscriptionTypeValue);
                    break;
                case ServiceSubscriptionType.Weekly:
                    enddate = now.AddDays(7 * model.SubscriptionTypeValue);
                    break;
                case ServiceSubscriptionType.Monthly:
                    enddate = now.AddMonths(model.SubscriptionTypeValue);
                    break;
                case ServiceSubscriptionType.Yearly:
                    enddate = now.AddYears(model.SubscriptionTypeValue);
                    break;
                default:
                    throw new ArgumentException("Invalid renewal type");
            }
            model.StartDate = now;
            model.EndDate = enddate;

            model.MaximumNumberOfTransaction = serviceModel.ServiceBaseOn == ServiceBasedOn.Transaction.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.NumberOfTransactionPending = serviceModel.ServiceBaseOn == ServiceBasedOn.Transaction.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.NumberOfTransactionCompleted = 0;
            model.ServiceAmount = serviceModel.ServiceBaseOn == ServiceBasedOn.Amount.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.ServiceAmountConsumed = serviceModel.ServiceBaseOn == ServiceBasedOn.Amount.ToString() ? (serviceModel.ServiceBaseValue * model.SubscriptionTypeValue) : 0;
            model.ServiceAmountPending = 0;

            return model;
        }
        #endregion

        #region PlanForRenewSubscription
        private CustomerSubscriptionRenewModel PlanForRenewSubscription(CustomerSubscriptionModel subscription, ServiceModel service, int subscriptionTypeValue)
        {
            var initEndDate = subscription.EndDate > DateTime.Now ? subscription.EndDate : DateTime.Now;
            DateTime endDate;
            var type = (ServiceSubscriptionType)Enum.Parse(typeof(ServiceSubscriptionType), service.SubscriptionType);


            switch (type)
            {
                case ServiceSubscriptionType.Daily:
                    endDate = initEndDate.AddDays(subscriptionTypeValue);
                    break;
                case ServiceSubscriptionType.Weekly:
                    endDate = initEndDate.AddDays(7 * subscriptionTypeValue);
                    break;
                case ServiceSubscriptionType.Monthly:
                    endDate = initEndDate.AddMonths(subscriptionTypeValue);
                    break;
                case ServiceSubscriptionType.Yearly:
                    endDate = initEndDate.AddYears(subscriptionTypeValue);
                    break;
                default:
                    throw new ArgumentException("Invalid renewal type");
            }
            var renew = new CustomerSubscriptionRenewModel
            {
                EndDate = endDate,
                CustomerId = subscription.CustomerId,
                SubscriptionId = subscription.Id,
                MaximumNumberOfTransaction = service.ServiceBaseOn == ServiceBasedOn.Transaction.ToString() ? subscription.MaximumNumberOfTransaction + (service.ServiceBaseValue * subscriptionTypeValue) : subscription.MaximumNumberOfTransaction,
                NumberOfTransactionPending = service.ServiceBaseOn == ServiceBasedOn.Transaction.ToString() ? subscription.NumberOfTransactionPending + (service.ServiceBaseValue * subscriptionTypeValue) : subscription.NumberOfTransactionPending,
                ServiceAmount = service.ServiceBaseOn == ServiceBasedOn.Amount.ToString() ? subscription.ServiceAmount + (service.ServiceBaseValue * subscriptionTypeValue) : subscription.ServiceAmount,
                ServiceAmountPending = service.ServiceBaseOn == ServiceBasedOn.Amount.ToString() ? subscription.ServiceAmountPending + (service.ServiceBaseValue * subscriptionTypeValue) : subscription.ServiceAmountPending
            };

            return renew;
        }

        #endregion

        #region GenerateReferenceNumber
        private static string GenerateReferenceNumber(string Prefix, int serviceId, int UniqueSequenecNnumber)
        {
            var refNumb = $"{Prefix}{serviceId}{UniqueSequenecNnumber + 1:D4}{DateTime.Now:yy}";
            return refNumb;
        }
        #endregion

        #region Get renew page data
        public async Task<ResponseModel> GetCurrentSubscriptionDetailsForRenew(int serviceId, int customerId)
        {
            var currentSubscriptionData = await _customerSubscriptionRepository.GetByServiceAndCustomerAsync(serviceId, customerId);
            var availableSubscriptionOptions = await _subscriptionTopupRepository.GetListByServiceIdAsync(serviceId);
            var response = new ResponseModel()
            {
                Response = new { availableSubscriptionOptions, currentSubscriptionData },
                Error = false
            };
            return response;
        }


        #endregion
    }
}
