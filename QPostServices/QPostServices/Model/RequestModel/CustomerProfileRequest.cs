﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerProfileRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string EmailAddress { get; set; } = string.Empty;

        [Required]
        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string MobileNumber { get; set; } = default!;

        [Required]
        public OTPRequest OTPRequest { get; set; } = new OTPRequest();

        public string? NameAr { get; set; }

        public CustomerProfileModel ToUpdateModel(int id)
        {
            return new CustomerProfileModel
            {
                Id = id,
                Name = Name,
                EmailAddress = EmailAddress,
                MobileNumber = MobileNumber,
                OTP = OTPRequest.ToModel(),
                NameAr = NameAr
            };
        }
    }
}
