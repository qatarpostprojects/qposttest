﻿using QPostServices.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Services
{
    public interface IStampCodeService
    {
        Task<ResponseModel> GetByOrderIdAsync(int orderId);
        Task<ResponseModel> CreateAsync(StampCodeModel model);
    }
}
