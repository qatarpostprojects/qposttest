﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IOrderSyncStatusRepository
    {
        Task<List<OrderSyncStatusModel>> GetListAsync();
        Task<OrderSyncStatusModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(OrderSyncStatusModel model);
    }
}
