﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CustomerOrdersDetailsModel
    {
        public int Id { get; set; }
        public string? SubOrderReference { get; set; }
        public int CustomerId { get; set; }
        public string DeliveryProductCode { get; set; } = string.Empty;
        public DateTime OrderDate { get; set; }
        public string OrderAWBNumber { get; set; } = string.Empty;
        public string TrackingNumber { get; set; } = string.Empty;
        public string PickupSlot { get; set; } = string.Empty;
        public string DeliverySlot { get; set; } = string.Empty;
        public DateTime DeliveryDate { get; set; }
        public int DeliveryTypeId { get; set; }
        public string Source { get; set; } = string.Empty;
        public string OriginCountry { get; set; } = string.Empty;
        public string DestinationCountry { get; set; } = string.Empty;
        public string MailFlow { get; set; } = string.Empty;
        public string MerchantName { get; set; } = string.Empty;
        public string MerchantTrackingNo { get; set; } = string.Empty;
        public decimal TotalWeight { get; set; }
        public decimal VolumetricWeight { get; set; }
        public string ShipmentContent { get; set; } = string.Empty;
        public string BranchId { get; set; } = string.Empty;
        public decimal TotalAmountToPaid { get; set; }
        public decimal TotalAmountToCollect { get; set; }
        public bool CODRequired { get; set; }
        public int Quantity { get; set; }
        public string? HSCode { get; set; }

        public int CurrentStatusCode { get; set; }
        public string CurrentStatusDescription { get; set; } = string.Empty;
        public string? CurrentStatusDescriptionAr { get; set; }
        public string? CurrentStatusCategory { get; set; }
        public string? CurrentStatusCategoryAr { get; set; }
        public int CurrentStatusLevel { get; set; }


        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ReceiverBoxNumber { get; set; }
        public int? ReceiverAddressId { get; set; }
        public string? OrderImage { get; set; }
        public string? BranchName { get; set; }
        public bool? IsHDS { get; set; }
        public bool? IsHold { get; set; }
        public bool? IsBranch { get; set; }
        public int PaymentId { get; set; }
        public int? WalletTransactionId { get; set; }
        public string? StampCode { get; set; }
        public int? ItemValue { get; set; }
        public string? ItemDescription { get; set; }
    }

    public class GetOrderDetails
    {
        public int OrderId { get; set; }
        public string MailFlow { get; set; } = string.Empty;
        public bool CODRequired { get; set; }

        public int CurrentStatusCode { get; set; }
        public string CurrentStatusDescription { get; set; } = string.Empty;
        public string? CurrentStatusDescriptionAr { get; set; }
        public string? CurrentStatusCategory { get; set; }
        public string? CurrentStatusCategoryAr { get; set; }
        public int CurrentStatusLevel { get; set; }

        public decimal TotalAmountToPaid { get; set; }
        public decimal TotalAmountToCollect { get; set; }
        public string? ReceiverBoxNumber { get; set; }
        public int? ReceiverAddressId { get; set; }
        public object? OtherDetails { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }

        public string MerchantTrackingNo { get; set; } = string.Empty;

        public string TrackingNumber { get; set; } = string.Empty;
        public string? OrderImage { get; set; }
        public string? BranchName { get; set; }
        public int? ItemValue { get; set; }
        public string? ItemDescription { get; set; }
    }
}
