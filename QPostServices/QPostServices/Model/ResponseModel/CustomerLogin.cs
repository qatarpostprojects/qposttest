﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerLogin
    {

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public int RoleId { get; set; }
        public string RoleName { get; set; } = string.Empty;
        public string RecordStatus { get; set; } = string.Empty;
        public DateTime? CreatedDate { get; set; }
        public string? Token { get; set; }
        public string? POBOXNumber { get; set; }
        public int? ServiceId { get; set; }
        public string MobileNumber { get; set; } = string.Empty;
        public string EmailAddress { get; set; } = string.Empty;
        public string? PreferedLanguage { get; set; }

        public static CustomerLogin FromModel(UserLoginModel model)
        {
            return new CustomerLogin()
            {
                Id = model.Id,
                CreatedDate = model.CreatedDate,
                CustomerId = model.CustomerId,
                UserName = model.UserName,
                Name = model.Name,
                RecordStatus = model.RecordStatus.ToString(),
                RoleId = model.RoleId,
                RoleName = model.RoleName,
                Token = model.Token,
                POBOXNumber = model.POBOXNumber,
                ServiceId = model.ServiceId,
                MobileNumber = model.MobileNumber,
                EmailAddress = model.EmailAddress,
                PreferedLanguage = model.PreferedLanguage
            };
        }
    }
}
