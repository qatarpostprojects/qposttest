﻿using Microsoft.AspNetCore.Hosting;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace QPostServices.Services
{
    public class CustomerService : ICustomerService
    {
        private const string _customerDoesNotExist = "Customer with id {0} does not exists";
        private const string _successCustomerCreation = "Customer created successfully";
        private const string _failedCustomerCreation = "Customer creation failed";
        private const string _successCustomerDeleted = "Customer with id {0} deleted successfully";
        private const string _successCustomerUpdate = "Customer with id {0} updated successfully";
        private const string _sendOtpError = "Error sending OTP to {0}";
        private const string _sendOtpSuccess = "OTP sent successfully to {0}";

        private readonly IPOBoxService _pOBoxService;
        private readonly ICustomerRepository _customerRepository;
        private readonly ILoginService _loginService;
        private readonly IConfigParameterRepository _configParameterRepository;
        private readonly IHostingEnvironment _hostEnvironment;
        private readonly ICustomerSubscriptionService _customerSubscription;
        private readonly IWalletTransactionSummaryRepository _walletTransactionSummaryRepository;

        public CustomerService(ICustomerRepository customerRepository,
            ILoginService loginService, IConfigParameterRepository configParameterRepository, IHostingEnvironment hostEnvironment,
            IPOBoxService pOBoxService, ICustomerSubscriptionService customerSubscription, IWalletTransactionSummaryRepository walletTransactionSummaryRepository)
        {
            _customerRepository = customerRepository;
            _loginService = loginService;
            _configParameterRepository = configParameterRepository;
            _hostEnvironment = hostEnvironment;
            _pOBoxService = pOBoxService;
            _customerSubscription = customerSubscription;
            _walletTransactionSummaryRepository = walletTransactionSummaryRepository;
        }


        #region Create customer
        public async Task<ResponseModel> CreateAsync(CustomerModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _customerRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successCustomerCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCustomerCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get All Active customers
        public Task<List<CustomerModel>> GetAllActiveAsync()
        {
            return _customerRepository.GetAllActiveAsync();
        }
        #endregion

        #region Get Customer By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _customerRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_customerDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update customer
        public async Task<ResponseModel> UpdateAsync(CustomerModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _customerRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successCustomerUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_customerDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Customer
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _customerRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successCustomerDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_customerDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region Customer Registration
        public async Task<ResponseModel> RegistrationAsync(CustomerRegistraionModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var customerId = await _customerRepository.RegistrationAsync(model);

                if (customerId > 0)
                {
                    var summaryModel = new WalletTransactionSummaryModel
                    {
                        Amount = 0,
                        CreatedDate = DateTime.Now,
                        CreatedUser = model.CreatedUser,
                        CustomerId = customerId,
                        LastTransactionDate = DateTime.Now,
                        LastTransactionId = null,
                        RecordStatus = RecordStatus.Active,
                        WalletId = model.WalletId ?? 0,
                        Remarks = "initiate"
                    };
                    await _walletTransactionSummaryRepository.CreateAsync(summaryModel, TransactionType.Credit);
                    response.Response = customerId;
                    response.Error = false;
                    await _customerRepository.MapOrderCustomerAsync(new MapOrderCustomerModel
                    {
                        CustomerId = customerId,
                        SearchParameter = string.IsNullOrEmpty(model.PoBoxNumber) ?
                        model.MobileNumber : model.PoBoxNumber
                    });
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCustomerCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region SendOTP
        public async Task<ResponseModel> SendOtpAsync(OtpModel model)
        {
            var response = new ResponseModel();
            string? messageToSend = await _configParameterRepository.GetByConfigKeyAsync(model.Note);

            if (model.Type == OtpType.Email.ToString())
            {
                await SendOTPToEmailAsync(model, messageToSend ?? string.Empty);
                response.Error = false;
                response.Response = string.Format(_sendOtpSuccess, model.Value);
            }
            else if (model.Type == OtpType.SMS.ToString())
            {
                if (await SendOTPToMobileAsync(model, messageToSend ?? string.Empty))
                {
                    response.Error = false;
                    response.Response = string.Format(_sendOtpSuccess, model.Value);
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_sendOtpError, model.Value);
                }
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_sendOtpError, model.Value);
            }
            return response;
        }
        #endregion

        #region Send Otp to Mobile Number
        private async Task<bool> SendOTPToMobileAsync(OtpModel model, string messageToSend)
        {
            model.OTP = GenerateOTP();
            await _customerRepository.SaveCustomerVerificationOTP(model);
            string message = String.Concat(messageToSend, model.OTP);

            var moduleName = "BulkSMS";
            var username = "bulksmssuser";
            var login = "bulksmssuser";
            var password = "bulksmssuser@1122";
            var itemID = "item";
            var transType = "trans";

            var apiUrl = $"https://web.qatarpost.qa/QSMSGW/api/SendSMS/1?mobileNumber={model.Value}&moduleName={moduleName}&ItemID={itemID}&TransType={transType}&username={username}&message={message}&login={login}&password={password}";

            using var client = new HttpClient();
            var response = await client.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Send Otp to Email Address
        private async Task SendOTPToEmailAsync(OtpModel model, string messageToSend)
        {
            string emailServer = "10.202.13.133";

            model.OTP = GenerateOTP();

            await _customerRepository.SaveCustomerVerificationOTP(model);

            if (!string.IsNullOrEmpty(messageToSend))
            {
                string relativeImagePath = Path.Combine("Images", "OtpImage.png");
                string wwwrootPath = _hostEnvironment.WebRootPath;
                string imagePath = Path.Combine(wwwrootPath, relativeImagePath);
                LinkedResource inlineImage = new(imagePath, MediaTypeNames.Image.Jpeg)
                {
                    ContentId = Guid.NewGuid().ToString()
                };

                string body = @"<html>
                            <body>
                            <div style='padding:5%;'>
                                <div style='border: 5px solid maroon;'> 
                                    <img src='cid:" + inlineImage.ContentId + @"'/> 
                                </div>

                                <div style='padding:1rem 2rem; border:1px solid grey'>
                                    <p>" + messageToSend + "<b> " + model.OTP + "</b>" + @"</p>
                                    <br/>
                                    <div>Sincerely,</div>
                                    <div><b>Qatar Post</b></div>
                                </div>
                            </div>
                            </body>
                            </html>";

                SmtpClient client = new SmtpClient(emailServer);
                //SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

                MailMessage oMail = new()
                {
                    From = new MailAddress("<no-reply@qatarposts.com>", "QPostApp Order POBOX")
                };
                {
                    {
                        oMail.To.Add(model.Value);
                    }
                }
                oMail.Subject = "OTP QPostApp Order POBOX";

                oMail.IsBodyHtml = true;

                oMail.Priority = MailPriority.Normal;

                oMail.Body = body;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
                htmlView.LinkedResources.Add(inlineImage);
                oMail.AlternateViews.Add(htmlView);

                client.UseDefaultCredentials = false;
                //client.EnableSsl = true;


                client.Credentials = new NetworkCredential("no-reply@qatarposts.com", "staff"); //Username and password
                //client.Credentials = new NetworkCredential("sreelakzmee@gmail.com", "wnlakifofniitric");
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                try
                {
                    client.Send(oMail);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region GenerateOTP
        private string GenerateOTP()
        {
            string otp = "";
            int otpSize = 6;
            Random rand = new();
            for (int i = 0; i < otpSize; i++)
            {
                otp += rand.Next(0, 9).ToString();
            }
            return otp;
        }
        #endregion

        #region VerifyOTP
        public Task<bool> VerifyOTPAsync(string value, string otp)
        {
            return _customerRepository.VerifyOTPAsync(value, otp);
        }
        #endregion

        #region Update Customer Profile
        public async Task<ResponseModel> UpdateProfileAsync(CustomerProfileModel model)
        {
            var response = new ResponseModel();

            //Update Profile
            var customerProfile = GenerateCustomerName(model);

            var result = await _customerRepository.UpdateProfileAsync(customerProfile);

            if (result > 0)
            {
                response.Response = string.Format(_successCustomerUpdate, model.Id);
                response.Error = false;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_customerDoesNotExist, model.Id);
            }

            return response;
        }
        #endregion

        #region GenerateCustomerName
        private CustomerProfileModel GenerateCustomerName(CustomerProfileModel model)
        {
            string[] nameParts = model.Name.Split(' ');
            if (nameParts.Length >= 1)
            {
                model.FirstName = nameParts[0];
            }

            if (nameParts.Length >= 2)
            {
                model.LastName = nameParts[^1];
            }

            if (nameParts.Length > 2)
            {
                model.MiddleName = string.Join(" ", nameParts.Skip(1).Take(nameParts.Length - 2));
            }

            if (!string.IsNullOrEmpty(model.NameAr))
            {
                string[] namePartsAr = model.NameAr.Split(' ');
                if (namePartsAr.Length >= 1)
                {
                    model.FirstNameAr = namePartsAr[0];
                }

                if (namePartsAr.Length >= 2)
                {
                    model.LastNameAr = namePartsAr[^1];
                }

                if (namePartsAr.Length > 2)
                {
                    model.MiddleNameAr = string.Join(" ", namePartsAr.Skip(1).Take(namePartsAr.Length - 2));
                }
            }

            return model;
        }
        #endregion

        #region Check Current Password Valid
        public Task<bool> CheckPasswordValidAsync(int id, string password)
        {
            //var id = _loginService.LoggedInCustomer();
            return _customerRepository.CheckPasswordValidAsync(id, password);
        }
        #endregion

        #region Update Password
        public async Task<ResponseModel> ChangePasswordAsync(int id, string password)
        {
            var response = new ResponseModel();
            var result = await _customerRepository.ChangePasswordAsync(id, password);

            if (result > 0)
            {
                response.Response = _successCustomerCreation;
                response.Error = false;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCustomerCreation;
            }

            return response;
        }


        #endregion

        #region Map Existing Customer Request OTP
        public async Task<ResponseModel> MapExistingRequestOTPAsync(int customerId, POBoxEnquiryModel model)
        {
            try
            {
                var result = await _pOBoxService.GetPOBoxEnqiryAsync(model);

                if (result.ErrorCode == "00")
                {
                    var response = (QPostPBXService.PostBox)result.Entry!;

                    //SendOTP
                    var otpModel = new OtpModel
                    {
                        Note = OtpNote.MapExisting.ToString(),
                        Type = OtpType.SMS.ToString(),
                        Value = response.Customer.MobileNumber
                    };

                    if (!string.IsNullOrEmpty(response.Customer.MobileNumber))
                    {
                        await SendOtpAsync(otpModel);
                        return new ResponseModel
                        {
                            Error = false,
                            Response = response.Customer.MobileNumber
                        };
                    }
                    return new ResponseModel
                    {
                        Error = true,
                        Response = response.Customer.MobileNumber
                    };
                }

                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest,
                    Response = "Mapping failed"
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Map Existing Service
        public async Task<ResponseModel> MapExistingServiceAsync(int customerId, POBoxEnquiryModel model)
        {
            try
            {
                var result = await _pOBoxService.GetPOBoxEnqiryAsync(model);

                if (result.ErrorCode == "00")
                {
                    var response = (QPostPBXService.PostBox)result.Entry!;
                    var subScriptionModel = new CustomerSubscriptionModel
                    {
                        CustomerId = customerId,
                        CreatedUser = _loginService.LoggedInUser(),
                        IsEligibleForRenew = true,
                        //  PaymentChannel = PaymentChannel,
                        // ServiceProductId = ServiceProductId,
                        Reference1 = "Mapping existing customer",
                        StartDate = response.StartDate,
                        EndDate = response.PaidUntil
                        //SubscriptionTypeValue = SubscriptionTypeValue,
                        //CardPaymentId = CardPaymentId,
                        //PaymentType = PaymentType,
                    };
                    await _customerSubscription.MapExistingCustomer(subScriptionModel);

                    //await _customerRepository.MapOrderCustomerAsync(new MapOrderCustomerModel
                    //{
                    //    CustomerId = customerId,
                    //    SearchParameter = string.IsNullOrEmpty(response.PostBoxNumber) ?
                    //    response.Customer.MobileNumber : response.PostBoxNumber
                    //});

                    return new ResponseModel
                    {
                        Error = false,
                        Response = "Mapping success"
                    };
                }

                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest,
                    Response = "Mapping failed"
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Map Order Customer
        public async Task<ResponseModel> MapOrderCustomerAsync(MapOrderCustomerModel model)
        {
            try
            {
                var result = await _customerRepository.MapOrderCustomerAsync(model);

                if (result > 0)
                {
                    return new ResponseModel
                    {
                        Error = false,
                        StatusCode = HttpStatusCode.OK,
                        Response = "Success"
                    };
                }

                return new ResponseModel
                {
                    Error = true,
                    StatusCode = HttpStatusCode.BadRequest,
                    Response = "Mapping failed"
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update language preference
        public async Task<ResponseModel> UpdateLanguagePreference(int customerId, string language)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _customerRepository.UpdateLanguagePreference(customerId, language, _loginService.LoggedInUser());

                if (result > 0)
                {
                    response.Response = "Prefered Language updated";
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = "Updation failed";
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
