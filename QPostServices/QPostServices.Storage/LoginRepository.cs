﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class LoginRepository : ILoginRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public LoginRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Check Login
        public async Task<UserLoginModel?> CheckLoginAsync(string username, string password)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Users_CheckLogin]";
                var parameters = new DynamicParameters();
                parameters.Add("@userName", username);
                parameters.Add("@password", password);
                var result = await connection.QueryAsync<UserLoginModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Create Login Log
        public async Task CreateLogAsync(LoginLogsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_LoginLogs_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@ipAddress", model.IPAddress);
                parameters.Add("@deviceName", model.DeviceName ?? "");
                parameters.Add("@loginDate", DateTime.Now);
                parameters.Add("@loginStatus", "Success");
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                var result = await connection.QueryAsync<UserLoginModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
