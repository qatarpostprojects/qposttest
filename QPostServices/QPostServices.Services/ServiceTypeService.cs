﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class ServiceTypeService : IServiceTypeService
    {
        private readonly IServiceTypeRepository _serviceTypeRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Service Type created successfully";
        private const string _successUpdate = "Service Type updated successfully";
        private const string _failedCreation = "Service Type creation failed";
        private const string _failedUpdation = "Service Type updation failed";
        private const string _successDelete = "Service Type deleted successfully";
        private const string _failedDelete = "Service Type deletion failed";
        private const string _nameAlreadyExist = "Service Type with name : {0} already exist";
        private const string _DoesNotExist = "Service Type with id : {0} Does not exist";

        public ServiceTypeService(IServiceTypeRepository serviceTypeRepository, ILoginService loginService)
        {
            _serviceTypeRepository = serviceTypeRepository;
            _loginService = loginService;
        }

        #region Get
        public async Task<List<ServiceTypeModel>> GetListAsync()
        {
            return await _serviceTypeRepository.GetListAsync();
        }
        #endregion

        #region Get Id Value List
        public async Task<List<IdValueModel>> GetIdValueListAsync()
        {
            return await _serviceTypeRepository.GetIdValueListAsync();
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _serviceTypeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Create
        public async Task<ResponseModel> CreateAsync(ServiceTypeModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _serviceTypeRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(ServiceTypeModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUser = _loginService.LoggedInUser();
            var result = await _serviceTypeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _serviceTypeRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
