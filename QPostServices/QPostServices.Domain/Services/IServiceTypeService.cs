﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IServiceTypeService
    {
        Task<List<ServiceTypeModel>> GetListAsync();
        Task<List<IdValueModel>> GetIdValueListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(ServiceTypeModel model);
        Task<ResponseModel> UpdateAsync(ServiceTypeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
