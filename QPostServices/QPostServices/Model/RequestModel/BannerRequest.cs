﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class BannerRequest
    {
        public IFormFile? BannerArFile { get; set; }
        public IFormFile? BannerEngFile { get; set; }
        public string? BannerAr { get; set; }
        public string? BannerEng { get; set; }
        public string Device { get; set; } = string.Empty;
        public int ListingOrder { get; set; }
        public BannerModel ToCreateModel()
        {
            return new BannerModel()
            {
                ListingOrder = ListingOrder,
                BannerArFile = BannerArFile,
                BannerEngFile = BannerEngFile,               
                Device = Device
            };
        }
        public BannerModel ToUpdateModel(int id)
        {
            return new BannerModel()
            {
                ListingOrder = ListingOrder,
                BannerArFile = BannerArFile,
                BannerEngFile = BannerEngFile,
                BannerAr = BannerAr,
                BannerEng = BannerEng,
                Device = Device,
                Id = id
            };
        }
    }
}
