﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class TrackAndTraceRequest
    {
        public string req_Input { get; set; } = string.Empty;
        public OTPRequest OTPRequest { get; set; } = new OTPRequest();

        public TrackAndTraceModel ToModel()
        {
            return new TrackAndTraceModel()
            {
                req_Input = req_Input
            };
        }

        public TrackMobileModel ToMobileModel()
        {
            return new TrackMobileModel()
            {
                RecipientMobileNumber = req_Input
            };
        }
    }
}
