﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class InboundRequestService : IInboundRequestService
    {
        private readonly IInboundRequestRepository _inboundRequestRepository;
        public InboundRequestService(IInboundRequestRepository inboundRequestRepository)
        {
            _inboundRequestRepository = inboundRequestRepository;
        }

        #region GetMasterLookupAsync
        public Task<List<IdValueModel>> GetMasterLookupAsync()
        {
            return _inboundRequestRepository.GetMasterLookUpAsync();
        } 
        #endregion
    }
}
