﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class WalletTransactionDetailsService : IWalletTransactionDetailsService
    {
        private const string _detailsDoesNotExist = "Wallet transaction details with id {0} does not exists";
        private const string _successDetailsCreation = "Wallet transaction details created successfully";
        private const string _failedDetailsCreation = "Wallet transaction details creation failed";
        private const string _successDetailsDeleted = "Wallet transaction details with id {0} deleted successfully";
        private const string _successDetailsUpdate = "Wallet transaction details with id {0} updated successfully";

        private readonly IWalletTransactionDetailsRepository _walletTransactionDetailsRepository;
        private readonly IWalletTransactionSummaryRepository _walletTransactionSummaryRepository;
        private readonly ILoginService _loginService;
        public WalletTransactionDetailsService(IWalletTransactionDetailsRepository walletTransactionDetailsRepository,
            ILoginService loginService, IWalletTransactionSummaryRepository walletTransactionSummaryRepository)
        {
            _walletTransactionDetailsRepository = walletTransactionDetailsRepository;
            _loginService = loginService;
            _walletTransactionSummaryRepository = walletTransactionSummaryRepository;
        }

        #region Create wallet transaction details
        public async Task<ResponseModel> CreateAsync(WalletTransactionDetailsModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _walletTransactionDetailsRepository.CreateAsync(model);

                if (result > 0)
                {
                    var transactionSummaryId = await _walletTransactionSummaryRepository.CreateAsync(new WalletTransactionSummaryModel()
                    {
                        CustomerId = model.CustomerId,
                        Amount = model.Amount,
                        CreatedUser = _loginService.LoggedInUser(),
                        LastTransactionId = result,
                        Remarks = model.Remarks
                    }, model.Type);
                    response.Response = _successDetailsCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedDetailsCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Create wallet transaction details
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _walletTransactionDetailsRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successDetailsDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_detailsDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get wallet transaction details by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _walletTransactionDetailsRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_detailsDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get list of wallet transaction details by customer id
        public Task<List<WalletTransactionDetailsModel>> GetListByCustomerId(int customerId)
        {
            return _walletTransactionDetailsRepository.GetListByCustomerId(customerId);
        }
        #endregion

        #region Update wallet transaction details
        public async Task<ResponseModel> UpdateAsync(WalletTransactionDetailsModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _walletTransactionDetailsRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successDetailsUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_detailsDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get REcent wallet transaction details by customer id
        public Task<List<WalletTransactionDetailsModel>> GetRecentTransactionsAsync(int customerId)
        {
            return _walletTransactionDetailsRepository.GetRecentTransactionsAsync(customerId);
        }
        #endregion
    }
}
