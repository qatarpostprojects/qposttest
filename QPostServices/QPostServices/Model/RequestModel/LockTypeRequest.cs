﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class LockTypeRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        
        [Required]
        public string Description { get; set; } = string.Empty;

        public LockTypeModel ToCreateModel()
        {
            return new LockTypeModel
            {
                Name = Name,
                Description = Description
            };
        }

        public LockTypeModel ToUpdateModel(int id)
        {
            return new LockTypeModel
            {
                Id = id,
                Name = Name,
                Description = Description
            };
        }
    }
}
