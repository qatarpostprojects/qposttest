﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class SubscriptionTransactionRepository : ISubscriptionTransactionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public SubscriptionTransactionRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create Subscription Transaction
        public async Task<int> CreateAsync(SubscriptionTransactionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransaction_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@subscriptionId", model.SubscriptionId);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@transactionReferenceNumber", model.TransactionReferenceNumber);
                parameters.Add("@transactionAmount", model.TransactionAmount);
                parameters.Add("@transactionDate", model.TransactionDate);
                parameters.Add("@paymentType", model.PaymentType);
                parameters.Add("@walletTransactionId", model.WalletTransactioId);
                parameters.Add("@cardPaymentId", model.CardPaymentId);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@awbNumber", model.AWBNumber);
                parameters.Add("@transactionType", model.TransactionType);
                parameters.Add("@serviceId", model.ServiceId);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get all Subscription Transaction
        public async Task<List<SubscriptionTransactionModel>> GetAllActiveAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransaction_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<SubscriptionTransactionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get  Subscription Transactions by Filter
        public async Task<List<SubscriptionTransactionModel>> GetByFilterAsync(int? customerId, int? serviceId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransaction_GetByFilter]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                parameters.Add("@serviceId", serviceId);
                var result = await connection.QueryAsync<SubscriptionTransactionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Subscription Transaction By id
        public async Task<SubscriptionTransactionModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransaction_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<SubscriptionTransactionModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Subscription Transaction
        public async Task<int> UpdateAsync(SubscriptionTransactionModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransaction_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@transactionReferenceNumber", model.TransactionReferenceNumber);
                parameters.Add("@transactionAmount", model.TransactionAmount);
                parameters.Add("@transactionDate", model.TransactionDate);
                parameters.Add("@paymentType", model.PaymentType);
                parameters.Add("@walletTransactionId", model.WalletTransactioId);
                parameters.Add("@cardPaymentId", model.CardPaymentId);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update status of Subscription Transaction 
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_SubscriptionTransaction_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
