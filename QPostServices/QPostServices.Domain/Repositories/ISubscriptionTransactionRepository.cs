﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ISubscriptionTransactionRepository
    {
        Task<int> CreateAsync(SubscriptionTransactionModel model);
        Task<SubscriptionTransactionModel?> GetByIdAsync(int id);
        Task<List<SubscriptionTransactionModel>> GetAllActiveAsync();
        Task<List<SubscriptionTransactionModel>> GetByFilterAsync(int? customerId, int? serviceId);
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(SubscriptionTransactionModel model);
    }
}
