﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class BoxTypeRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        
        [Required]
        public string Description { get; set; } = string.Empty;

        public BoxTypeModel ToCreateModel()
        {
            return new BoxTypeModel
            {
                Name = Name,
                Description = Description
            };
        }

        public BoxTypeModel ToUpdateModel(int id)
        {
            return new BoxTypeModel
            {
                Id = id,
                Name = Name,
                Description = Description
            };
        }
    }
}
