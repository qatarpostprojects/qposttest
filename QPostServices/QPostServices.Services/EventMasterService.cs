﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class EventMasterService : IEventMasterService
    {
        private readonly IEventMasterRepository _eventMasterRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Event created successfully";
        private const string _successUpdate = "Event updated successfully";
        private const string _failedCreation = "Event creation failed";
        private const string _failedUpdation = "Event updation failed";
        private const string _successDelete = "Event deleted successfully";
        private const string _failedDelete = "Event deletion failed";
        private const string _nameAlreadyExist = "Event with name : {0} already exist";
        private const string _eventDoesNotExist = "Event with id : {0} Does not exist";

        public EventMasterService(IEventMasterRepository eventMasterRepository, ILoginService loginService)
        {
            _eventMasterRepository = eventMasterRepository;
            _loginService = loginService;
        }

        #region Create Event
        public async Task<ResponseModel> CreateAsync(EventMasterModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _eventMasterRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.EventDescription);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get Event by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _eventMasterRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_eventDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of Event
        public async Task<List<EventMasterModel>> GetListAsync()
        {
            return await _eventMasterRepository.GetListAsync();
        }
        #endregion

        #region Update Event
        public async Task<ResponseModel> UpdateAsync(EventMasterModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUser = _loginService.LoggedInUser();
            var result = await _eventMasterRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.EventDescription);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_eventDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete Event
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _eventMasterRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_eventDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
