﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class UserSessionService : IUserSessionService
    {
        private readonly IUserSessionRepository _userSessionRepository;
        private readonly ILoginService _loginService;

        public UserSessionService(IUserSessionRepository userSessionRepository, ILoginService loginService)
        {
            _userSessionRepository = userSessionRepository;
            _loginService = loginService;
        }
        public async Task<ResponseModel> CreateAsync(UserSessionModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _userSessionRepository.CreateAsync(model);

            if (!string.IsNullOrEmpty(result))
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }

            else
            {
                response.Error = true;
                response.Response = "Request failed";
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }

        public async Task<ResponseModel> GetBySessionValueAsync(string sessionValue)
        {
            var response = new ResponseModel();

            var result = await _userSessionRepository.GetBySessionValueAsync(sessionValue);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }

        public async Task<ResponseModel> UpdateStatusAsync(string sessionValue, UserSessionStatus status)
        {
            var response = new ResponseModel();

            var userId =  _loginService.LoggedInUser();
            var result = await _userSessionRepository.UpdateStatusAsync(sessionValue, status, userId);

            if (result > 0)
            {
                response.Response = "Update success";
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
           
            else
            {
                response.Error = true;
                response.Response = "Request failed";
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
    }
}
