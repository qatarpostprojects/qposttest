﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPOBoxTypeService
    {
        Task<List<POBoxTypeModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(POBoxTypeModel model);
        Task<ResponseModel> UpdateAsync(POBoxTypeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
