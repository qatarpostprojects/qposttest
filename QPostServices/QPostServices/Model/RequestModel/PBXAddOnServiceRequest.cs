﻿using QPostPBXService;
using QPostServices.Domain.Enums;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PBXAddOnServiceRequest
    {
        [Required]
        //[StringLength(6, MinimumLength = 1)]
        public string POBoxNo { get; set; } = default!;

        [Required]
        [EnumRequired(typeof(PBXAddOnPeriod))]
        public PBXAddOnPeriod Period { get; set; } = default!;
    }

    public class PBXAddOnServiceCreateRequest : PBXAddOnServiceRequest
    {
        [Required]
        [EnumRequired(typeof(PBXAddOnServiceType))]
        public PBXAddOnServiceType AddOnServiceType { get; set; }
    }
}
