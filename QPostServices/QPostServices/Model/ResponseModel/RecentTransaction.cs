﻿using QPostServices.Domain.Model;
using System.Linq.Expressions;

namespace QPostServices.Model.ResponseModel
{
    public class RecentTransaction
    {
        public int PaymentId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal? PaymentRequestAmount { get; set; }
        public decimal? PaymentReceiveAmount { get; set; }
        public int? OrderId { get; set; }
        public int? SubscriptionTransactionId { get; set; }
        public int? WalletTransactionDetailsId { get; set; }
        public string? SubscriptionReferenceNumber { get; set; }
        public string? TransactionReferenceId { get; set; }
        public string? OrderTrackingNumber { get; set; }
        public string? TransactionMessage { get; set; }
        public static RecentTransaction FromModel(RecentTransactionModel model)
        {
            string transactionMessage;
            if (model.OrderId > 0)
            {
                transactionMessage = "Order Placed";
            }
            else if (model.SubscriptionTransactionId > 0)
            {
                transactionMessage = "Subscribed Service";
            }
            else if(model.WalletTransactionDetailsId > 0)
            {
                transactionMessage = "Transaction completed";
            }
            else
            {
                transactionMessage = "Payment success";
            }
            return new RecentTransaction()
            {
                PaymentId = model.PaymentId,
                PaymentDate = model.PaymentDate,
                PaymentRequestAmount = model.PaymentRequestAmount,
                PaymentReceiveAmount = model.PaymentReceiveAmount,
                OrderId = model.OrderId,
                SubscriptionTransactionId = model.SubscriptionTransactionId,
                SubscriptionReferenceNumber = model.SubscriptionReferenceNumber,
                TransactionMessage = transactionMessage,
                OrderTrackingNumber = model.OrderTrackingNumber,
                TransactionReferenceId = model.TransactionReferenceId,
                WalletTransactionDetailsId = model.WalletTransactionDetailsId,
            };
        }
    }
}
