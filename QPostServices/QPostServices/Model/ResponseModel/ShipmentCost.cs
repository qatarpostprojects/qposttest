﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class ShipmentCost
    {
        public string SLA { get; set; } = string.Empty;
        public string Price { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        public string? TypeAr { get; set; }

        public static ShipmentCost FromModel(ShipmentCostModel model)
        {
            return new ShipmentCost
            {
                Price = model.Price,
                SLA = model.SLA,
                Type = model.Type,
                TypeAr = model.TypeAr,
            };
        }
    }
}
