﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class SubscriptionTopupRequest
    {
        [Required]
        public int Amount { get; set; }
        [Required]
        public string Currency { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string ValidityDescription { get; set; } = string.Empty;
        [Required]
        public int Validity { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public int ServiceId { get; set; }
        public string? ColorCode { get; set; }
        public string? DescriptionAr { get; set; }
        public string? ValidityDescriptionAr { get; set; }

        public SubscriptionTopupModel ToCreateModel()
        {
            return new SubscriptionTopupModel
            {
                Amount = Amount,
                Currency = Currency,
                Description = Description,
                ValidityDescription = ValidityDescription,
                Validity = Validity,
                Type = Type,
                ServiceId = ServiceId,
                ColorCode = ColorCode,
                DescriptionAr = DescriptionAr,
                ValidityDescriptionAr = ValidityDescriptionAr
            };
        }

        public SubscriptionTopupModel ToUpdateModel(int id)
        {
            return new SubscriptionTopupModel
            {
                Id = id,
                Amount = Amount,
                Currency = Currency,
                Description = Description,
                ValidityDescription = ValidityDescription,
                Validity = Validity,
                Type = Type,
                ColorCode = ColorCode,
                DescriptionAr = DescriptionAr,
                ValidityDescriptionAr = ValidityDescriptionAr
            };
        }
    }
}
