﻿using QPostPBXService;
using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class PBXSubscribeModel
    {

        public string POBoxNo { get; set; } = default!;

        public string CostCenter { get; set; } = default!;

        public string HoldKey { get; set; } = default!;

        public PBXBoxCategory Category { get; set; }

        public Enums.PBXSubType SubType { get; set; }

        public Enums.PBXHolderType HolderType { get; set; }

        public string POBoxLocationID { get; set; } = default!;
        public string? POBoxLocationName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime PaidUntil { get; set; }
        public string? CompanyName { get; set; }
        public string? CompanyRegNumber { get; set; }
        public PBXCustomerModel Customer { get; set; } = new();
        public PBXSubscribeDetails ToModel()
        {
            return new PBXSubscribeDetails()
            {
                POBoxNo = POBoxNo,
                CostCenter = CostCenter,
                HoldKey = HoldKey,
                Category = (QPostPBXService.BoxCategory)Category,
                SubType = (QPostPBXService.SubType)SubType,
                CompanyName = CompanyName,
                CompanyRegNumber = CompanyRegNumber,
                HolderType = (QPostPBXService.HolderType)HolderType,
                PaidUntil = PaidUntil,
                POBoxLocationID = POBoxLocationID,
                POBoxLocationName = POBoxLocationName,
                StartDate = StartDate,
                Customer = Customer.ToModel()
            };
        }
    }

    public class PBXCustomerModel
    {
        public string CompanyActivity { get; set; } = default!;

        public Enums.PBXCompanySector Sector { get; set; } = default!;

        public DateTime DateOfBirth { get; set; }

        public Enums.PBXCustomerIdType CustomerIDType { get; set; } = default!;

        public string QatarID { get; set; } = default!;

        public string PassportNo { get; set; } = default!;

        public DateTime PassportExpiryDate { get; set; }

        public string PassportIssueCountry { get; set; } = default!;

        public int AdditionalNumberOfKeys { get; set; }

        public PBXNumberOfEmployees NumOfEmployees { get; set; } = default!;

        public Enums.CustomerLanguage CustomerLanguage { get; set; } = default!;

        public string CustomerName { get; set; } = default!;

        public string Address { get; set; } = default!;

        public string BuildingNumber { get; set; } = default!;

        public string ZoneNumber { get; set; } = default!;

        public string StreetNumber { get; set; } = default!;

        public string City { get; set; } = default!;

        public string MobileNumber { get; set; } = default!;

        public string OfficeTelephoneNo { get; set; } = default!;

        public string Email { get; set; } = default!;
        public List<string> SmartCardNumber { get; set; }
        public PBXCustomerDetails ToModel()
        {
            return new PBXCustomerDetails()
            {
                AdditionalNumberOfKeys = AdditionalNumberOfKeys,
                Address = Address,
                CompanyActivity = CompanyActivity,
                BuildingNumber = BuildingNumber,
                City = City,
                CustomerIDType = (QPostPBXService.CustomerIDType)CustomerIDType,
                CustomerLanguage = (QPostPBXService.CustomerLanguage)CustomerLanguage,
                CustomerName = CustomerName,
                DateOfBirth = DateOfBirth,
                Email = Email,
                MobileNumber = MobileNumber,
                OfficeTelephoneNo = OfficeTelephoneNo,
                PassportExpiryDate = PassportExpiryDate,
                PassportIssueCountry = PassportIssueCountry,
                QatarID = QatarID,
                StreetNumber = StreetNumber,
                ZoneNumber = ZoneNumber,
                NumOfEmployees = (NoOfEmployees)NumOfEmployees,
                PassportNo = PassportNo,
                Sector = (QPostPBXService.CompanySector)Sector,
                SmartCardNumbers = SmartCardNumber.ToArray()
            };
        }

    }
}
