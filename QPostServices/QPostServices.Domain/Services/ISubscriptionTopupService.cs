﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ISubscriptionTopupService
    {
        Task<List<SubscriptionTopupModel>> GetListByServiceIdAsync(int serviceId);
        Task<List<SubscriptionTopupModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(SubscriptionTopupModel model);
        Task<ResponseModel> UpdateAsync(SubscriptionTopupModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
