﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IBannerRepository
    {
        Task<List<BannerModel>> GetListAsync();
        Task<List<BannerModel>> GetByDevice(string device);
        Task<int> CreateAsync(BannerModel model);
        Task<int> UpdateAsync(BannerModel model);
        Task<int> UpdateStatusAsync(int id, bool status, int userId);
        Task<BannerModel> GetByIdAsync(int id);
    }
}
