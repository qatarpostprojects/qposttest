﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerSubscriptionController : ControllerBase
    {
        private readonly ICustomerSubscriptionService _customerSubscriptionService;
        private readonly ICustomerServicesService _customerServicesService;
        public CustomerSubscriptionController(ICustomerSubscriptionService customerSubscriptionService, ICustomerServicesService customerServicesService)
        {
            _customerSubscriptionService = customerSubscriptionService;
            _customerServicesService = customerServicesService;
        }

        #region Create Customer Subscription
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateCustomerSubscription))]
        public async Task<IActionResult> CreateCustomerSubscription([FromForm] CustomerSubscriptionRequest request)
        {
            try
            {
                var result = await _customerSubscriptionService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create Customer Addon Subscription
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("AddOnSubscription",Name = nameof(CreateCustomerAddonSubscription))]
        public async Task<IActionResult> CreateCustomerAddonSubscription([FromBody] CustomerAddonSubscriptionRequest request)
        {
            try
            {
                var result = await _customerSubscriptionService.CreateAddOnAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer Subscription by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerSubscription))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetCustomerSubscription))]
        public async Task<IActionResult> GetCustomerSubscription(int id)
        {
            try
            {
                var result = await _customerSubscriptionService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(CustomerSubscription.FromModel((CustomerSubscriptionModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Customer Subscriptions
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerSubscriptionDetailModel>))]
        [HttpGet("Customer/{customerId}", Name = nameof(GetCustomerSubscriptionList))]
        public async Task<IActionResult> GetCustomerSubscriptionList(int customerId)
        {
            try
            {
                var results = await _customerSubscriptionService.GetAllActiveByCustomerIdAsync(customerId);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Nearby Expiry Subscriptions
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<CustomerSubscription>))]
        [HttpGet("NearbyExpiry", Name = nameof(GetNearbyExpirySubscriptions))]
        public async Task<IActionResult> GetNearbyExpirySubscriptions()
        {
            try
            {
                var results = await _customerSubscriptionService.GetNearbyExpirySubscriptions();

                return Ok(results.Select(x => CustomerSubscription.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete Customer Subscription
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteCustomerSubscription))]
        public async Task<IActionResult> DeleteCustomerSubscription(int id)
        {
            try
            {
                var result = await _customerSubscriptionService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Customer Subscription
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateCustomerSubscription))]
        public async Task<IActionResult> UpdateCustomerSubscription(int id, [FromBody] CustomerSubscriptionRequest request)
        {
            try
            {
                var result = await _customerSubscriptionService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Renew Customer Subscription
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPatch("RenewSubscription", Name = nameof(RenewCustomerSubscription))]
        public async Task<IActionResult> RenewCustomerSubscription(SubscriptionRenewRequest request)
        {
            try
            {
                var result = await _customerSubscriptionService.RenewAsync(request.ToModel());

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get AddOnService
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<IdValue>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("GetAddOnServices", Name = nameof(GetAddOnServices))]
        public async Task<IActionResult> GetAddOnServices()
        {
            try
            {
                var result = await _customerSubscriptionService.GetAddOnServicesAsync();

                return Ok(result.Select(x => IdValue.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region GetCurrentSubscriptionDetailsForRenew 
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("Customer/{customerId}/Service/{serviceId}", Name = nameof(GetCurrentSubscriptionDetailsForRenew))]
        public async Task<IActionResult> GetCurrentSubscriptionDetailsForRenew(int customerId, int serviceId)
        {
            try
            {
                var result = await _customerSubscriptionService.GetCurrentSubscriptionDetailsForRenew(serviceId, customerId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get calculated Subscription Amount
        // need to remove
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        [HttpPost("CalculateAmount/Type/{type}", Name = nameof(GetSubscriptionAmount))]
        public async Task<IActionResult> GetSubscriptionAmount(string type,[FromBody] CalculateSubscriptionAmountRequest request)
        {
            try
            {
                return Ok(50);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get calculated Home Delivery Amount
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        [HttpGet("CalculateAmount/Service/{serviceId}/HomeDelivery", Name = nameof(GetHomeDeliveryAmount))]
        public async Task<IActionResult> GetHomeDeliveryAmount( int serviceId)
        {
            try
            {
                return Ok(50);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get calculated Hold Delivery Amount
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        [HttpPost("CalculateAmount/HoldDelivery", Name = nameof(GetHoldDeliveryAmount))]
        public async Task<IActionResult> GetHoldDeliveryAmount([FromBody] CalculateHoldDeliveryAmountRequest requests)
        {
            try
            {
                return Ok(50);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get calculated Branch Amount
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        [HttpGet("CalculateAmount/Service/{serviceId}/Branch", Name = nameof(GetBranchAmount))]
        public async Task<IActionResult> GetBranchAmount( int serviceId)
        {
            try
            {
                return Ok(50);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get calculated Subscription Amount
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        [HttpGet("CalculateServiceAmount", Name = nameof(GetSubscriptionServiceAmount))]
        public async Task<IActionResult> GetSubscriptionServiceAmount([FromQuery] CalculateServiceAmountRequest request)
        {
            try
            {
               var result = await _customerServicesService.GetCalculatedServiceAmountAsync(request.ToModel());
                if (result.Error)
                {
                    return NotFound();
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
