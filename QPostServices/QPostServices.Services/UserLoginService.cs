﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class UserLoginService : IUserLoginService
    {
        private const string _userLoginDoesNotExist = "User login with id {0} does not exists";
        private const string _successUserLoginCreation = "User login created successfully";
        private const string _failedUserLoginCreation = "User login creation failed";
        private const string _successUserLoginDeleted = "User login with id {0} deleted successfully";
        private const string _successUserLoginUpdate = "User login with id {0} updated successfully";


        private readonly IUserLoginRepository _userLoginRepository;
        private readonly ILoginService _loginService;
        public UserLoginService(IUserLoginRepository userLoginRepository, ILoginService loginService)
        {
            _userLoginRepository = userLoginRepository;
            _loginService = loginService;
        }


        #region Create user Login
        public async Task<ResponseModel> CreateAsync(UserLoginModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.CreatedUser = _loginService.LoggedInUser();
                var result = await _userLoginRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successUserLoginCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedUserLoginCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get All Active user Logins
        public Task<List<UserLoginModel>> GetAllActiveAsync()
        {
            return _userLoginRepository.GetAllActiveAsync();
        }
        #endregion

        #region Get UserLogin By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var result = await _userLoginRepository.GetByIdAsync(id);

                if (result != null)
                {
                    response.Response = result;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_userLoginDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update user Login
        public async Task<ResponseModel> UpdateAsync(UserLoginModel model)
        {
            try
            {
                var response = new ResponseModel();
                model.ModifiedUser = _loginService.LoggedInUser();
                var result = await _userLoginRepository.UpdateAsync(model);

                if (result > 0)
                {
                    response.Response = string.Format(_successUserLoginUpdate, model.Id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_userLoginDoesNotExist, model.Id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete User Login
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            try
            {
                var response = new ResponseModel();

                var userId = _loginService.LoggedInUser();


                var result = await _userLoginRepository.UpdateStatusAsync(id, userId, RecordStatus.Inactive);

                if (result > 0)
                {
                    response.Response = string.Format(_successUserLoginDeleted, id);
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = string.Format(_userLoginDoesNotExist, id);
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
