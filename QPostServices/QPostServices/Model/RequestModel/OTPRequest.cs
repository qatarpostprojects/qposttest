﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class OTPRequest
    {
        [Required]
        [EnumRequired(typeof(OtpType))]
        public string Type { get; set; } = string.Empty;

        [Required]
        public string Value { get; set; } = string.Empty;

        [Required]
        [EnumRequired(typeof(OtpNote))]
        public string Note { get; set; } = string.Empty;

        public string? OTP { get; set; }

        public OtpModel ToModel()
        {
            return new OtpModel
            {
                Note = Note,
                Type = Type,
                Value = Value,
                OTP = OTP
            };
        }
    }

    public class TrackAndTraceOTPRequest
    {
        [Required]
        public bool IsMobileNumber { get; set; }

        [Required]
        [EnumRequired(typeof(OtpType))]
        public string Type { get; set; } = string.Empty;
        [Required]
        public string Value { get; set; } = string.Empty;

        [Required]
        [EnumRequired(typeof(OtpNote))]
        public string Note { get; set; } = string.Empty;
    }
}
