﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class NotificationCategoryRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        public NotificationCategoryModel ToCreateModel()
        {
            return new NotificationCategoryModel
            {
                Name = Name
            };
        }

        public NotificationCategoryModel ToUpdateModel(int id)
        {
            return new NotificationCategoryModel
            {
                Id = id,
                Name = Name
            };
        }
    }
}
