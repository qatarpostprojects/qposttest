﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class SubscriptionTransactionRequest
    {
        [Required]
        public int SubscriptionId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public string TransactionReferenceNo { get; set; } = string.Empty;

        [Required]
        public decimal TransactionAmount { get; set; }

        [Required]
        public DateTime TransactionDate { get; set; }
        public string? PaymentType { get; set; }

        [Required]
        public int? WalletTransID { get; set; }
        public int? CardPaymentID { get; set; }
        public string? Remarks { get; set; }

        public SubscriptionTransactionModel ToCreateModel()
        {
            return new SubscriptionTransactionModel()
            {
                SubscriptionId = SubscriptionId,
                CustomerId = CustomerId,
                TransactionReferenceNumber = TransactionReferenceNo,
                TransactionAmount = TransactionAmount,
                TransactionDate = TransactionDate,
                PaymentType = PaymentType,
                WalletTransactioId = WalletTransID,
                CardPaymentId = CardPaymentID,
                Remarks = Remarks
            };
        }

        public SubscriptionTransactionModel ToUpdateModel(int id)
        {
            return new SubscriptionTransactionModel()
            {
                Id = id,
                SubscriptionId = SubscriptionId,
                CustomerId = CustomerId,
                TransactionReferenceNumber = TransactionReferenceNo,
                TransactionAmount = TransactionAmount,
                TransactionDate = TransactionDate,
                PaymentType = PaymentType,
                WalletTransactioId = WalletTransID,
                CardPaymentId = CardPaymentID,
                Remarks = Remarks
            };
        }
    }
}
