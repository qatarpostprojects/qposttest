﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IBannerService
    {
        Task<List<BannerModel>> GetListAsync();
        Task<List<BannerModel>> GetByDevice(string device);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(BannerModel model);
        Task<ResponseModel> UpdateAsync(BannerModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
