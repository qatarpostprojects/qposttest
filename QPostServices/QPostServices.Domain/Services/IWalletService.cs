﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IWalletService
    {
        Task<ResponseModel> CreateAsync(WalletModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<WalletModel>> GetAllActiveAsync();
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(WalletModel model);
        Task<ResponseModel> DepositAsync(WalletTransactionModel model);
        Task<ResponseModel> PaymentAsync(WalletTransactionModel model);
    }
}
