﻿using QPostServices.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class PBXRenewPostBoxModel
    {
        public string POBoxNo { get; set; } = default!;

        public POBoxRenewalPeriod Period { get; set; }
    }
}
