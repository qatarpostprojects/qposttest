﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class ConfigParameterRepository : IConfigParameterRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public ConfigParameterRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region GetByConfigKeyAsync
        public async Task<string?> GetByConfigKeyAsync(string configKey)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_ConfigParameter_GetByConfigKey]";
                var parameters = new DynamicParameters();
                parameters.Add("@configKey", configKey);
                var result = await connection.QueryAsync<string>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault() ?? "Your verification OTP is : ";
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
