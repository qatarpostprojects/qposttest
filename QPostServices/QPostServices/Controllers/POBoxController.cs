﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class POBoxController : ControllerBase
    {
        private readonly IPOBoxService _poBoxService;
        public POBoxController(IPOBoxService poBoxService)
        {
            _poBoxService = poBoxService;
        }
        #region Create
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreatePOBox))]
        public async Task<IActionResult> CreatePOBox([FromBody] POBoxRequest request)
        {
            try
            {
                var result = await _poBoxService.CreateAsync(request.ToModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(POBox))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetPOBox))]
        public async Task<IActionResult> GetPOBox(int id)
        {
            try
            {
                var result = await _poBoxService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(POBox.FromModel((POBoxModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get POBox list
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<POBox>))]
        [HttpGet(Name = nameof(GetPOBoxList))]
        public async Task<IActionResult> GetPOBoxList()
        {
            try
            {
                var results = await _poBoxService.GetListAsync();

                return Ok(results.Select(x => POBox.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get POBox landing page service list
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<POBOxService>))]
        [HttpGet("landingpageservices", Name = nameof(GetAllLandingPageServices))]
        public async Task<IActionResult> GetAllLandingPageServices()
        {
            try
            {
                var results = await _poBoxService.GetAllLandingPageServicesAsync();

                return Ok(results.Select(x => POBOxService.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeletePOBox))]
        public async Task<IActionResult> DeletePOBox(int id)
        {
            try
            {
                var result = await _poBoxService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update POBox
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdatePOBox))]
        public async Task<IActionResult> UpdatePOBox(int id, [FromBody] POBoxRequest request)
        {
            try
            {
                var result = await _poBoxService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region POBox enquiry
        /// <summary>
        /// POBox enquiry
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("poboxenquiry", Name = nameof(GetPOBoxEnqiry))]
        public async Task<IActionResult> GetPOBoxEnqiry([FromQuery] POBoxEnquiryRequest request)
        {
            var result = await _poBoxService.GetPOBoxEnqiryAsync(request.ToModel());
            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };

            return Ok(response);
        }
        #endregion

        #region Get AddOn Service
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("addonservice", Name = nameof(GetAddOnService))]
        public async Task<IActionResult> GetAddOnService([FromQuery] PBXAddOnServiceRequest request)
        {
            var result = await _poBoxService.GetAddOnServicesAsync(request.POBoxNo, request.Period);

            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };
            return Ok(response);
        }
        #endregion

        #region Add AddOn Service
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("addonservice", Name = nameof(AddAddOnService))]
        public async Task<IActionResult> AddAddOnService([FromBody] PBXAddOnServiceCreateRequest request)
        {
            var result = await _poBoxService.AddAddOnServicesAsync(request.POBoxNo, request.Period, request.AddOnServiceType);

            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };

            return Ok(response);
        }
        #endregion

        #region RenewPostBox
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("renew", Name = nameof(RenewPostBox))]
        public async Task<IActionResult> RenewPostBox([FromBody] PBXRenewPostBoxRequest request)
        {
            //var result = await _poBoxService.RenewPostBoxAsync(request.POBoxNo, request.Period);
            var result = new PBXResponseModel();

            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };

            return Ok(response);
        }
        #endregion

        #region UpdatePBXAddress
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPut("address", Name = nameof(UpdatePBXAddress))]
        public async Task<IActionResult> UpdatePBXAddress([FromBody] PBXUpdateAddressRequestModel request)
        {
            var result = await _poBoxService.UpdateAddressAsync(request.Address.ToModel(), request.POBoxNumber);

            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };

            return Ok(response);
        }
        #endregion

        #region GetAvailablePOBoxes
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("available", Name = nameof(GetAvailablePOBoxes))]
        public async Task<IActionResult> GetAvailablePOBoxes([FromQuery] PBXBoxNoCostCenterRequest request)
        {
            var result = await _poBoxService.GetAvailablePOBoxesAsync(request.POBoxNo ?? string.Empty, request.CostCenter);

            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };

            return Ok(response);
        }
        #endregion

        #region SubscribePOBox
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("subscribe", Name = nameof(SubscribePOBox))]
        public async Task<IActionResult> SubscribePOBox([FromBody] PBXSubscribePOBoxRequest request)
        {
            //var result = await _poBoxService.SubscribePOBoxAsync(request.ToModel());
            var result = new PBXResponseModel();
            //if (!string.IsNullOrEmpty(result.ErrorCode))
            //{
            //    var model = new POBoxModel()
            //    {
            //        Blocked = request.InternalRequest.Blocked,
            //        Remarks = request.InternalRequest.Remarks,
            //        GroupId = request.InternalRequest.GroupId,
            //        BranchId = request.InternalRequest.BranchId,
            //        GeoLocationX = request.InternalRequest.GeoLocationX,
            //        GeoLocationY = request.InternalRequest.GeoLocationY,
            //        SmartBoxNumber = request.InternalRequest.SmartBoxNumber,
            //        POBoxCategoryID = (int)request.Category,
            //        IsSmartBox = request.InternalRequest.IsSmartBox,
            //        POBoxPositionID = request.InternalRequest.POBoxPositionID,
            //        POBoxLockTypeID = request.InternalRequest.POBoxLockTypeID,
            //        LastPaidUntil = request.InternalRequest.LastPaidUntil,
            //        Location = request.InternalRequest.Location,
            //        Reserved = request.InternalRequest.Reserved,
            //        PaidUntil = request.PaidUntil,
            //        POBoxLocationId = request.POBoxLocationID,
            //        POBoxLocationName = request.POBoxLocationName,
            //        POBoxNumber = request.POBoxNo,
            //        POBoxSubType = request.SubType.ToString(),
            //        POBoxTypeId = request.InternalRequest.POBoxTypeId,
            //        Rented = request.InternalRequest.Rented,
            //        SmartCardNumber = request.InternalRequest.SmartCardNumber,
            //        StartDate = request.StartDate
            //    };
            //    var data = await _poBoxService.CreateAsync(model);

            //    if (!data.Error)
            //    {
            //        return Ok(data.Response);
            //    }

            //    return BadRequest(data.Response);
            //}
            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };

            return Ok(response);
        }
        #endregion

        #region HoldPOBox
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("hold", Name = nameof(GetHoldPoBox))]
        public async Task<IActionResult> GetHoldPoBox([FromQuery] PBXBoxNoCostCenterRequest request)
        {
            var result = await _poBoxService.HoldPOBoxAsync(request.POBoxNo, request.CostCenter);
           // var addOnServices = (List<PBXAddOnServiceModel>)result.Entry!;

            var response = new PBXResponse
            {
                Entry = result.Entry,
                ErrorCode = result.ErrorCode,
                Message = result.Message,
            };
            return Ok(response);
        }
        #endregion
    }
}
