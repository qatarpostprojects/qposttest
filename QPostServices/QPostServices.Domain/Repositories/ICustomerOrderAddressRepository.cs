﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerOrderAddressRepository
    {
        Task<List<CustomerOrderAddressModel>> GetListAsync();
        Task<List<CustomerOrderAddressModel>> GetByOrderIdAsync(int orderId);
        Task<CustomerOrderAddressModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(CustomerOrderAddressModel model);
        Task<int> UpdateAsync(CustomerOrderAddressModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
