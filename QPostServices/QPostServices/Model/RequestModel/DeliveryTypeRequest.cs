﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class DeliveryTypeRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string Description { get; set; } = string.Empty;

        public string? Image { get; set; }

        public IFormFile? ImageFile { get; set; }

        public int ListingOrder { get; set; }

        public int DeliveryProductId { get; set; }
        [Required]
        public string PriceType { get; set; } = string.Empty;
        public string? ProductServiceCategory { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public DeliveryTypeModel ToCreateModel()
        {
            return new DeliveryTypeModel
            {
                Name = Name,
                Description = Description,
                Image = Image,
                ImageFile = ImageFile,
                ListingOrder = ListingOrder,
                DeliveryProductId = DeliveryProductId,
                PriceType = PriceType,
                ProductServiceCategory = ProductServiceCategory,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }

        public DeliveryTypeModel ToUpdateModel(int id)
        {
            return new DeliveryTypeModel
            {
                Id = id,
                Name = Name,
                Description = Description,
                Image = Image,
                ImageFile = ImageFile,
                ListingOrder = ListingOrder,
                DeliveryProductId = DeliveryProductId,
                PriceType = PriceType,
                ProductServiceCategory = ProductServiceCategory,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
