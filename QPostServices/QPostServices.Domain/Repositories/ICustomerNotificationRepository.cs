﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ICustomerNotificationRepository
    {
        Task<int> CreateNotificationAsync(CustomerNotificationModel model);
        Task<int> UpdateReadStatusAsync(int id, bool isRead);
        Task<List<CustomerNotificationModel>> GetAllNotificationsAsync(CustomerNotificationFetchModel model);
    }
}
