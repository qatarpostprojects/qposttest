﻿namespace QPostServices.Domain.Enums
{
    public enum ServiceSubscriptionType
    {
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Yearly = 4
    }
}
