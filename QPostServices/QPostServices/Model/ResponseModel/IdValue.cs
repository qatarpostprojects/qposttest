﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class IdValue
    {
        public int Id { get; set; }
        public string Value { get; set; } = string.Empty;
        public string? ValueAr { get; set; }

        public static IdValue FromModel(IdValueModel model)
        {
            return new IdValue
            {
                Id = model.Id,
                Value = model.Value,
                ValueAr = model.ValueAr,
            };
        }
    }
}
