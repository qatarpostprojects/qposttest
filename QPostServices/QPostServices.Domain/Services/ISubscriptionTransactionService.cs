﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ISubscriptionTransactionService
    {
        Task<ResponseModel> CreateAsync(SubscriptionTransactionModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<SubscriptionTransactionModel>> GetAllActiveAsync();
        Task<List<SubscriptionTransactionModel>> GetByFilterAsync(int? customerId, int? serviceId);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(SubscriptionTransactionModel model);
    }
}
