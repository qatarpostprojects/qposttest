﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Reflection;

namespace QPostServices.Services
{
    public class TermsAndConditionService : ITermsAndConditionService
    {
        private const string _successCreation = "Terms and condition created successfully";
        private const string _successUpdate = "Terms and condition updated successfully";
        private const string _failedCreation = "Terms and condition creation failed";
        private const string _failedUpdation = "Terms and condition updation failed";
        private const string _successDelete = "Terms and condition deleted successfully";
        private const string _failedDelete = "Terms and condition deletion failed";
        private const string _TermsAndConditionDoesNotExist = "Terms and condition with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly ITermsAndConditionRepository _termsAndConditionRepository;
        public TermsAndConditionService(ITermsAndConditionRepository termsAndConditionRepository, ILoginService loginService)
        {
            _termsAndConditionRepository = termsAndConditionRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(TermsAndConditionModel model)
        {
            var response = new ResponseModel();
            model.CreatedBy = _loginService.LoggedInUser();
            var result = await _termsAndConditionRepository.CreateAsync(model);
            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();
            var result = await _termsAndConditionRepository.UpdateStatusAsync(id, Domain.Enums.RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_TermsAndConditionDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get value
        public async Task<string> GetAsync()
        {
            return await _termsAndConditionRepository.GetAsync();
        }
        #endregion
        
        #region Get details
        public async Task<TermsAndConditionModel> GetDetailsAsync()
        {
            return await _termsAndConditionRepository.GetDetailsAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(TermsAndConditionModel model)
        {
            var response = new ResponseModel();
            model.ModifiedBy = _loginService.LoggedInUser();
            var result = await _termsAndConditionRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_TermsAndConditionDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
