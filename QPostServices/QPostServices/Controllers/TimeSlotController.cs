﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TimeSlotController : ControllerBase
    {
        private readonly ITimeSlotService _timeSlotService;
        public TimeSlotController(ITimeSlotService timeSlotService)
        {
            _timeSlotService = timeSlotService;
        }
        #region Create 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateTimeSlot))]
        public async Task<IActionResult> CreateTimeSlot([FromBody] TimeSlotRequest request)
        {
            try
            {
                var result = await _timeSlotService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Time slot by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TimeSlotResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetTimeSlotById))]
        public async Task<IActionResult> GetTimeSlotById(int id)
        {
            try
            {
                var result = await _timeSlotService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(TimeSlotResponse.FromModel((TimeSlotModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get 
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TimeSlotResponse>))]
        [HttpGet(Name = nameof(GetTimeSlotList))]
        public async Task<IActionResult> GetTimeSlotList()
        {
            try
            {
                var results = await _timeSlotService.GetAsync();

                return Ok(results.Select(x => TimeSlotResponse.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteTimeSlot))]
        public async Task<IActionResult> DeleteTimeSlot(int id)
        {
            try
            {
                var result = await _timeSlotService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateTimeSlot))]
        public async Task<IActionResult> UpdateTimeSlot(int id, [FromBody] TimeSlotRequest request)
        {
            try
            {
                var result = await _timeSlotService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
