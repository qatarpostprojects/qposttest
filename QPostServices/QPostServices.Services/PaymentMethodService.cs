﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class PaymentMethodService : IPaymentMethodService
    {
        private const string _successCreation = "Payment method created successfully";
        private const string _successUpdate = "Payment method updated successfully";
        private const string _failedCreation = "Payment method creation failed";
        private const string _failedUpdation = "Payment method updation failed";
        private const string _successDelete = "Payment method deleted successfully";
        private const string _failedDelete = "Payment method deletion failed";
        private const string _PaymentMethodDoesNotExist = "Payment method with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly IPaymentMethodRepository _paymentMethodRepository;
        private readonly IFileService _fileService;
        public PaymentMethodService(IPaymentMethodRepository paymentMethodRepository,ILoginService loginService, IFileService fileService)
        {
            _paymentMethodRepository = paymentMethodRepository;
            _loginService = loginService;
            _fileService = fileService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(PaymentMethodModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();
            if (model.ImageFile != null)
            {
                model.Image = await _fileService.ConvertImageToBase64(model.ImageFile);
            }
            var result = await _paymentMethodRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _paymentMethodRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_PaymentMethodDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _paymentMethodRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_PaymentMethodDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All
        public async Task<List<PaymentMethodModel>> GetListAsync()
        {
            return await _paymentMethodRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(PaymentMethodModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();
            if (model.ImageFile != null)
            {
                model.Image = await _fileService.ConvertImageToBase64(model.ImageFile);
            }
            var result = await _paymentMethodRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_PaymentMethodDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
