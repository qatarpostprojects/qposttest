﻿namespace QPostServices.Domain.Enums
{
    public enum PBXCompanySector
    {
        None = 1,
        Government = 2,
        OilOrGas = 3,
        Financial = 4,
        Services = 5,
        Others = 6
    }
}
