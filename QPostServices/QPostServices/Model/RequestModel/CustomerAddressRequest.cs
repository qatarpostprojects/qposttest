﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerAddressRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        [Required]
        [RegularExpression(@"^[^\s@]+@[^\s@]+\.[^\s@]+$", ErrorMessage = "Invalid email format.")]
        public string Email { get; set; } = string.Empty;
        [Required]
        [RegularExpression(@"^\+?[0-9]{8,14}$", ErrorMessage = "Invalid mobile number.")]
        public string Phone { get; set; } = string.Empty;
        [ConditionalRequired(nameof(AddressType), "Sender", ErrorMessage = "OTP is required if address type is sender")]
        public OTPRequest? OTPRequest { get; set; }
        [Required]
        public int CustomerId { get; set; }

        public bool IsPrimary { get; set; }

        [Required]
        public string City { get; set; } = default!;

        public string? PoBox { get; set; } 

        public string? SmartBox { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", true, ErrorMessage = "Required if Qatar is selected")]
        public string? ZoneNumber { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", true, ErrorMessage = "Required if Qatar is selected")]
        public string? StreetNumber { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", true, ErrorMessage = "Required if Qatar is selected")]
        public string? BuildingNumber { get; set; }

        //[Required]
        //[ConditionalRequired(nameof(Country), "Qatar", true,ErrorMessage ="Required if Qatar is selected")]

        public string? FloorNumber { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", true, ErrorMessage = "Required if Qatar is selected")]
        //[Required]
        public string? UnitNumber { get; set; }
        public string? GeoLocationX { get; set; }

        //[Required]
        public string? GeoLatitude { get; set; }

        //[Required]
        public string? GeoLongitude { get; set; }

        [EnumRequired(typeof(AddressType))]
        public AddressType AddressType { get; set; }
        public string? AddressDescription { get; set; }

        [Required]
        public string Country { get; set; } = default!;
        public string? Reference1 { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", false, ErrorMessage = "Required if Qatar is not selected")]
        public string? Address1 { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", false, ErrorMessage = "Required if Qatar is not selected")]
        public string? Address2 { get; set; }
        [ConditionalRequired(nameof(Country), "Qatar", false, ErrorMessage = "Required if Qatar is not selected")]
        public string? ZipCode { get; set; }

        public CustomerAddressModel ToCreateModel()
        {
            return new CustomerAddressModel
            {
                CustomerId = CustomerId,
                IsPrimary = IsPrimary,
                City = City,
                PoBox = PoBox,
                SmartBox = SmartBox,
                ZoneNumber = ZoneNumber,
                StreetNumber = StreetNumber,
                BuildingNumber = BuildingNumber,
                FloorNumber = FloorNumber,
                UnitNumber = UnitNumber,
                GeoLatitude = GeoLatitude,
                GeoLongitude = GeoLongitude,
                GeoLocationX = GeoLocationX,
                AddressDescription = AddressDescription,
                Country = Country,
                Reference1 = Reference1,
                Name = Name,
                Email = Email,
                Phone = Phone,
                AddressType = AddressType,
                Address1 = Address1,
                Address2 = Address2,
                ZipCode = ZipCode
            };
        }

        public CustomerAddressModel ToUpdateModel(int id)
        {
            return new CustomerAddressModel
            {
                Id = id,
                CustomerId = CustomerId,
                IsPrimary = IsPrimary,
                City = City,
                PoBox = PoBox,
                SmartBox = SmartBox,
                ZoneNumber = ZoneNumber,
                StreetNumber = StreetNumber,
                BuildingNumber = BuildingNumber,
                FloorNumber = FloorNumber,
                UnitNumber = UnitNumber,
                GeoLatitude = GeoLatitude,
                GeoLongitude = GeoLongitude,
                GeoLocationX = GeoLocationX,
                AddressDescription = AddressDescription,
                Country = Country,
                Reference1 = Reference1,
                Name = Name,
                Email = Email,
                Phone = Phone,
                AddressType = AddressType,
                Address1 = Address1,
                Address2 = Address2,
                ZipCode = ZipCode
            };
        }
    }
}
