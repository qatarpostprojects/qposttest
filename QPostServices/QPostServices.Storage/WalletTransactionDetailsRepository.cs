﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using System.Data.SqlClient;
using System.Data;
using QPostServices.Domain.Repositories;

namespace QPostServices.Storage
{
    public class WalletTransactionDetailsRepository : IWalletTransactionDetailsRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public WalletTransactionDetailsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create wallet transaction details
        public async Task<int> CreateAsync(WalletTransactionDetailsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionDetails_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@walletTransactionBillNo", model.BillNo);
                parameters.Add("@walletTransactionAmount", model.Amount);
                parameters.Add("@walletTransactionType", (int)model.Type);
                parameters.Add("@transactionReferenceId", model.ReferenceId);
                parameters.Add("@bankTransactionId", model.BankTransactionId);
                parameters.Add("@walletTransactionDate ", model.Date);
                parameters.Add("@walletTransactionPaymentId", model.PaymentId);
                parameters.Add("@walletReference1", model.Reference1);
                parameters.Add("@walletRemarks", model.Remarks);
                parameters.Add("@recordStatus ", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get wallet transaction details by Customer Id
        public async Task<List<WalletTransactionDetailsModel>> GetListByCustomerId(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionDetails_GetByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<WalletTransactionDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get  wallet transaction details
        public async Task<WalletTransactionDetailsModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionDetails_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<WalletTransactionDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update  wallet transaction details
        public async Task<int> UpdateAsync(WalletTransactionDetailsModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionDetails_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@walletTransactionBillNo", model.BillNo);
                parameters.Add("@walletTransactionAmount", model.Amount);
                parameters.Add("@walletTransactionType", (int)model.Type);
                parameters.Add("@transactionReferenceId", model.ReferenceId);
                parameters.Add("@bankTransactionId", model.BankTransactionId);
                parameters.Add("@walletTransactionDate", model.Date);
                parameters.Add("@walletTransactionPaymentId", model.PaymentId);
                parameters.Add("@walletRemarks", model.Remarks);
                parameters.Add("@walletReference1", model.Reference1);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update  wallet transaction details status
        public async Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionDetails_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)recordStatus);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Recent wallet transaction details by Customer Id
        public async Task<List<WalletTransactionDetailsModel>> GetRecentTransactionsAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_WalletTransactionDetails_GetRecentTransactionsByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<WalletTransactionDetailsModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
