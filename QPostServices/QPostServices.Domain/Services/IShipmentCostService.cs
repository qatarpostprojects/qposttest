﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IShipmentCostService
    {
        Task<ResponseModel> GetCostDetailsAsync(PriceCalculatorModel model);
    }
}
