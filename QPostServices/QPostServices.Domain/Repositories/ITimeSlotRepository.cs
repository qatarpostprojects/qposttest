﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ITimeSlotRepository
    {
        Task<int> CreateAsync(TimeSlotModel model);
        Task<int> UpdateAsync(TimeSlotModel model);
        Task<int> UpdateStatusAsync(int id,RecordStatus status,int userId);
        Task<List<TimeSlotModel>> GetAsync();
        Task<TimeSlotModel> GetByIdAsync(int id);
    }
}
