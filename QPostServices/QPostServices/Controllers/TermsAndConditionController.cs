﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TermsAndConditionController : ControllerBase
    {
        private readonly ITermsAndConditionService _termsAndConditionService;
        public TermsAndConditionController(ITermsAndConditionService termsAndConditionService)
        {
            _termsAndConditionService = termsAndConditionService;
        }
        //#region Create 
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[HttpPost(Name = nameof(CreateTermsAndCondition))]
        //public async Task<IActionResult> CreateTermsAndCondition([FromBody] TermsAndConditionRequest request)
        //{
        //    try
        //    {
        //        var result = await _termsAndConditionService.CreateAsync(request.ToCreateModel());

        //        if (!result.Error)
        //        {
        //            return Ok(result.Response);
        //        }

        //        return BadRequest(result.Response);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
        //    }
        //}
        //#endregion

        //#region Get by id
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TermsAndConditionModel))]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[HttpGet("{id}", Name = nameof(GetTermsAndConditionById))]
        //public async Task<IActionResult> GetTermsAndConditionById(int id)
        //{
        //    try
        //    {
        //        var result = await _termsAndConditionService.GetByIdAsync(id);

        //        if (result.Error)
        //        {
        //            return NotFound(result.Response);
        //        }

        //        return Ok((TermsAndConditionModel)result.Response!);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
        //    }
        //}
        //#endregion

        #region Get 
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [HttpGet(Name = nameof(GetTermsAndConditionValue))]
        public async Task<IActionResult> GetTermsAndConditionValue()
        {
            try
            {
                var results = await _termsAndConditionService.GetAsync();

                return Ok(results);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get details
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TermsAndConditionModel))]
        [HttpGet("Details",Name = nameof(GetTermsAndConditionDetails))]
        public async Task<IActionResult> GetTermsAndConditionDetails()
        {
            try
            {
                var results = await _termsAndConditionService.GetDetailsAsync();

                return Ok(results);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateTermsAndCondition))]
        public async Task<IActionResult> UpdateTermsAndCondition(int id, [FromBody] TermsAndConditionRequest model)
        {
            try
            {
                var result = await _termsAndConditionService.UpdateAsync(model.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }

}
