﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class CalculateSubscriptionAmountRequest
    {
        public string Range { get; set; } = string.Empty;
        public int DurationId { get; set; }
        public int ServiceId { get; set; }
    }

    public class CalculateServiceAmountRequest
    {
        public string? Range { get; set; }
        public string Type { get; set; } = string.Empty;
        public int Duration { get; set; }
        public int ServiceId { get; set; }

        public CalculateServiceAmountModel ToModel()
        {
            return new CalculateServiceAmountModel
            {
                Range = Range,
                Type = Type,
                Duration = Duration,
                ServiceId = ServiceId
            };
        }
    }

    public class CalculateHoldDeliveryAmountRequest
    {
        public int DurationId { get; set; }
        public List<string> TrackingNumber { get; set; } = new List<string>();
        public int ItemCount { get; set; }
        public int NumberOfDays { get; set; }
    }
}
