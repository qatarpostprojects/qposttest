﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class OrderSyncStatusModel
    {
        public int Id { get; set; }
        public string TrackingNumber { get; set; } = string.Empty;
        public DateTime SyncDate { get; set; }
        public string SyncStatus { get; set; } = string.Empty;
        public string? ErrorDescription { get; set; } 
        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
    }
}
