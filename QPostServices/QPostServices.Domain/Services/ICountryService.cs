﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICountryService
    {
        Task<List<CountryModel>> GetListAsync(string? searchString);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(CountryModel model);
        Task<ResponseModel> UpdateAsync(CountryModel model);
        Task<ResponseModel> DeleteAsync(int id);

    }
}