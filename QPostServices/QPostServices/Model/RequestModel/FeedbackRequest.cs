﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class FeedbackRequest
    {
        [Required]
        public string Review { get; set; } = string.Empty;
        public string? Comment { get; set; }

        public FeedbackModel ToModel()
        {
            return new FeedbackModel
            {
                Review = Review,
                Comment = Comment
            };
        }
    }
}
