﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Services;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerNotificationController : ControllerBase
    {
        private readonly ICustomerNotificationService _customerNotificationService;
        public CustomerNotificationController(ICustomerNotificationService customerNotificationService)
        {
            _customerNotificationService = customerNotificationService;
        }

        #region Create Notification
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateCustomerNotification))]
        public async Task<IActionResult> CreateCustomerNotification([FromBody] CustomerNotificationRequest request)
        {
            try
            {
                var result = await _customerNotificationService.CreateNotificationAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
        
        #region Get Notification
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet(Name = nameof(GetCustomerNotificationByFilter))]
        public async Task<IActionResult> GetCustomerNotificationByFilter([FromQuery] CustomerNotificationFetchRequest request)
        {
            try
            {
                var result = await _customerNotificationService.GetAllNotificationsAsync(request.ToModel());
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
        
        #region Update Notification status
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("Customer/{id}",Name = nameof(UpdateCustomerNotificationReadStatus))]
        public async Task<IActionResult> UpdateCustomerNotificationReadStatus(int id, bool isRead)
        {
            try
            {
                var result = await _customerNotificationService.UpdateReadStatusAsync(id,isRead);

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

    }
}
