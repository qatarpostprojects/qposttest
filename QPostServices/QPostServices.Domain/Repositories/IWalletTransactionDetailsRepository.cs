﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IWalletTransactionDetailsRepository
    {
        Task<int> CreateAsync(WalletTransactionDetailsModel model);
        Task<WalletTransactionDetailsModel?> GetByIdAsync(int id);
        Task<List<WalletTransactionDetailsModel>> GetListByCustomerId(int customerId);
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(WalletTransactionDetailsModel model);
        Task<List<WalletTransactionDetailsModel>> GetRecentTransactionsAsync(int customerId);
    }
}
