﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IConfigParameterRepository
    {
        Task<string?> GetByConfigKeyAsync(string configKey);
    }
}
