﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class Wallet
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string Description { get; set; } = string.Empty;
        public string? DescriptionAr { get; set; } 
        public string? Reference1 { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public static Wallet FromModel(WalletModel model)
        {
            return new Wallet
            {
                Id= model.Id,
                Name= model.Name,
                Description= model.Description,
                CreatedDate= model.CreatedDate,
                CreatedUser= model.CreatedUser,
                CreatedUserName= model.CreatedUserName,
                ModifiedDate= model.ModifiedDate,
                ModifiedUser= model.ModifiedUser,
                ModifiedUserName= model.ModifiedUserName,
                Reference1= model.Reference1,
                RecordStatus = model.RecordStatus.ToString(),
                NameAr= model.NameAr,
                DescriptionAr= model.DescriptionAr,
            };
        }
    }
}
