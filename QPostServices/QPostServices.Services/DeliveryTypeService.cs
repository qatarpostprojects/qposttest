﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class DeliveryTypeService : IDeliveryTypeService
    {
        private const string _successCreation = "Delivery Type created successfully";
        private const string _successUpdate = "Delivery Type updated successfully";
        private const string _failedCreation = "Delivery Type creation failed";
        private const string _failedUpdation = "Delivery Type updation failed";
        private const string _successDelete = "Delivery Type deleted successfully";
        private const string _failedDelete = "Delivery Type deletion failed";
        private const string _nameAlreadyExist = "Delivery Type with name : {0} already exist";
        private const string _deliveryTypeDoesNotExist = "Delivery Type with id : {0} Does not exist";

        private readonly IDeliveryTypeRepository _deliveryTypeRepository;
        private readonly ILoginService _loginService;
        private readonly IFileService _fileService;

        public DeliveryTypeService(IDeliveryTypeRepository deliveryTypeRepository, ILoginService loginService, IFileService fileService)
        {
            _deliveryTypeRepository = deliveryTypeRepository;
            _loginService = loginService;
            _fileService = fileService;
        }

        #region Create DeliveryType
        public async Task<ResponseModel> CreateAsync(DeliveryTypeModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            if (model.ImageFile != null)
            {
                model.Image = await _fileService.ConvertImageToBase64(model.ImageFile);
            }
            
            var result = await _deliveryTypeRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete DeliveryType
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _deliveryTypeRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_deliveryTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get DeliveryType by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _deliveryTypeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
               response.Response = string.Format(_deliveryTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of DeliveryType
        public async Task<List<DeliveryTypeModel>> GetListAsync()
        {
            return await _deliveryTypeRepository.GetListAsync();
        }
        #endregion

        #region Get list of DeliveryType By ProductId
        public async Task<List<DeliveryTypeModel>> GetByProductIdAsync(int productId)
        {
            return await _deliveryTypeRepository.GetByProductIdAsync(productId);
        }
        #endregion

        #region Update DeliveryType
        public async Task<ResponseModel> UpdateAsync(DeliveryTypeModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            if (model.ImageFile != null)
            {
                model.Image = await _fileService.ConvertImageToBase64(model.ImageFile);
            }

            var result = await _deliveryTypeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_deliveryTypeDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
