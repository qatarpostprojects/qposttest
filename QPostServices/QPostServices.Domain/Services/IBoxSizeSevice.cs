﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IBoxSizeServices
    {
        Task<List<BoxSizeModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(BoxSizeModel model);
        Task<ResponseModel> UpdateAsync(BoxSizeModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
