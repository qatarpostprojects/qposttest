﻿using Konscious.Security.Cryptography;
using System.Collections;
using System.Text;

namespace QPostServices.Domain.Helper
{
    public class PasswordHasher
    {
        private const int SaltSize = 16;
        private const int HashLength = 32;

        public static string HashPassword(string password)
        {
            byte[] salt = new byte[SaltSize];
            using (var rng = new System.Security.Cryptography.RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            using (var hasher = new Argon2id(Encoding.UTF8.GetBytes(password)))
            {
                hasher.Salt = salt;
                hasher.DegreeOfParallelism = 8; 
                hasher.MemorySize = 65536;      
                hasher.Iterations = 4;          

                return Convert.ToBase64String(hasher.GetBytes(HashLength));
            }
        }

        public static bool VerifyPassword(string enteredPassword, string storedHash)
        {
            byte[] hashedBytes = Convert.FromBase64String(storedHash);

            byte[] salt = new byte[SaltSize];
            Array.Copy(hashedBytes, 0, salt, 0, SaltSize);

            using (var hasher = new Argon2id(Encoding.UTF8.GetBytes(enteredPassword)))
            {
                hasher.Salt = salt;
                hasher.DegreeOfParallelism = 8; 
                hasher.MemorySize = 65536;
                hasher.Iterations = 4;

                byte[] enteredHash = hasher.GetBytes(HashLength);

                return StructuralComparisons.StructuralEqualityComparer.Equals(hashedBytes, enteredHash);
            }
        }
    }
}
