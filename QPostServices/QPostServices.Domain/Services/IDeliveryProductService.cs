﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IDeliveryProductService
    {
        Task<List<DeliveryProductModel>> GetListAsync();
        Task<List<DeliveryProductModel>> GetByTypeAsync(bool isOverseas);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(DeliveryProductModel model);
        Task<ResponseModel> UpdateAsync(DeliveryProductModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
