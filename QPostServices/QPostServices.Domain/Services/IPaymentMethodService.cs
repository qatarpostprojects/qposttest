﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPaymentMethodService
    {
        Task<List<PaymentMethodModel>> GetListAsync();
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(PaymentMethodModel model);
        Task<ResponseModel> UpdateAsync(PaymentMethodModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
