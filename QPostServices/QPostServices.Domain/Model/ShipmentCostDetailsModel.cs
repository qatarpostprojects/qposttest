﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class ShipmentCostDetailsModel
    {
        public int Id { get; set; }
        public string Type { get; set; } = string.Empty;
        public string? TypeAr { get; set; }
        public string SLA { get; set; } = string.Empty;
        public string TypeName { get; set; } = string.Empty;
        public ShipmentType ShipmentType { get; set; }
        public string? ShipmentTypeAr { get; set; }
        public string? Remark1 { get; set; } 
        public string? Remark2 { get; set; } 
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
