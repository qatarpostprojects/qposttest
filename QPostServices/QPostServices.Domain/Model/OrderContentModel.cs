﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class OrderContentModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty; 
        public string? NameAr { get; set; } 
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public IFormFile? IconFile { get; set; }
        public string? Icon { get; set; }
        public int ListingOrder { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
