﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class OrderDetails
    {
        public int Id { get; set; }
        public string? SubOrderReference { get; set; }
        public int CustomerId { get; set; }
        public string DeliveryProductCode { get; set; } = string.Empty;
        public DateTime OrderDate { get; set; }
        public string OrderAWBNumber { get; set; } = string.Empty;
        public string TrackingNumber { get; set; } = string.Empty;
        public string PickupSlot { get; set; } = string.Empty;
        public string DeliverySlot { get; set; } = string.Empty;
        public DateTime DeliveryDate { get; set; }
        public int DeliveryTypeId { get; set; }
        public string Source { get; set; } = string.Empty;
        public string OriginCountry { get; set; } = string.Empty;
        public string DestinationCountry { get; set; } = string.Empty;
        public string MailFlow { get; set; } = string.Empty;
        public string MerchantName { get; set; } = string.Empty;
        public string MerchantTrackingNo { get; set; } = string.Empty;
        public decimal TotalWeight { get; set; }
        public decimal VolumetricWeight { get; set; }
        public string ShipmentContent { get; set; } = string.Empty;
        public string BranchId { get; set; } = string.Empty;
        public decimal TotalAmountToPaid { get; set; }
        public decimal TotalAmountToCollect { get; set; }
        public bool CODRequired { get; set; }
        public int Quantity { get; set; }
        public string? HSCode { get; set; }
        public int CurrentStatusCode { get; set; } 

        public string CurrentStatusDescription { get; set; } = string.Empty;
        public string? Remarks { get; set; }
        public string RecordStatus { get; set; } = string.Empty;
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? StampCode { get; set; }

        public List<CustomerOrderAddress>? CustomerOrderAddresses { get; set; }

        public static OrderDetails FromModel(OrderDetailsModel model)
        {
            return new OrderDetails
            {
                Id = model.Id,
                CustomerId = model.CustomerId,
                DeliveryTypeId = model.DeliveryTypeId,
                DeliverySlot = model.DeliverySlot,
                PickupSlot = model.PickupSlot,
                DeliveryProductCode = model.DeliveryProductCode,
                DeliveryDate = model.DeliveryDate,
                CurrentStatusDescription = model.CurrentStatusDescription,
                CurrentStatusCode = model.CurrentStatusCode,
                BranchId = model.BranchId,
                CODRequired = model.CODRequired,
                DestinationCountry = model.DestinationCountry,
                HSCode = model.HSCode,
                MailFlow = model.MailFlow,
                MerchantName = model.MerchantName,
                MerchantTrackingNo = model.MerchantTrackingNo,
                OrderAWBNumber = model.OrderAWBNumber,
                OrderDate = model.OrderDate,
                OriginCountry = model.OriginCountry,
                Quantity = model.Quantity,
                Reference1 = model.Reference1,
                Reference2 = model.Reference2,
                Remarks = model.Remarks,
                ShipmentContent = model.ShipmentContent,
                Source = model.Source,
                SubOrderReference = model.SubOrderReference,
                TotalAmountToCollect = model.TotalAmountToCollect,
                TotalAmountToPaid = model.TotalAmountToPaid,
                TotalWeight = model.TotalWeight,
                VolumetricWeight = model.VolumetricWeight,
                TrackingNumber = model.TrackingNumber,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                RecordStatus = model.RecordStatus.ToString(),
                StampCode= model.StampCode,
                CustomerOrderAddresses = model.CustomerOrderAddress.Select(x=> CustomerOrderAddress.FromModel(x)).ToList()
            };
        }

    }
}
