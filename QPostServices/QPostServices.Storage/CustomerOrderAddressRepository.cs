﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class CustomerOrderAddressRepository : ICustomerOrderAddressRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public CustomerOrderAddressRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }
        #region Create
        public async Task<int> CreateAsync(CustomerOrderAddressModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderAddress_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@orderId", model.OrderId);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@customerType", model.CustomerType);
                parameters.Add("@customerName", model.CustomerName);
                parameters.Add("@customerMobileNumber", model.CustomerMobileNumber);
                parameters.Add("@customerAddressZone", model.CustomerAddressZone);
                parameters.Add("@customerAddressStreet", model.CustomerAddressStreet);
                parameters.Add("@customerAddressBuilding", model.CustomerAddressBuilding);
                parameters.Add("@customerAddressUnit", model.CustomerAddressUnit);
                parameters.Add("@customerLocationDetails", model.CustomerLocationDetails);
                parameters.Add("@customerPOBOXNumber", model.CustomerPOBOXNumber);
                parameters.Add("@customerPostCode", model.CustomerPostCode);
                parameters.Add("@customerEmail", model.CustomerEmail);
                parameters.Add("@customerIdentificationNumber", model.CustomerIdentificationNumber);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@customerAddress1", model.CustomerAddress1);
                parameters.Add("@customerAddress2", model.CustomerAddress2);
                parameters.Add("@customerZipCode", model.CustomerZipCode);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<CustomerOrderAddressModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderAddress_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<CustomerOrderAddressModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get All active
        public async Task<List<CustomerOrderAddressModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderAddress_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<CustomerOrderAddressModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(CustomerOrderAddressModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderAddress_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@orderId", model.OrderId);
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@customerType", model.CustomerType);
                parameters.Add("@customerName", model.CustomerName);
                parameters.Add("@customerMobileNumber", model.CustomerMobileNumber);
                parameters.Add("@customerAddressZone", model.CustomerAddressZone);
                parameters.Add("@customerAddressStreet", model.CustomerAddressStreet);
                parameters.Add("@customerAddressBuilding", model.CustomerAddressBuilding);
                parameters.Add("@customerAddressUnit", model.CustomerAddressUnit);
                parameters.Add("@customerLocationDetails", model.CustomerLocationDetails);
                parameters.Add("@customerPOBOXNumber", model.CustomerPOBOXNumber);
                parameters.Add("@customerPostCode", model.CustomerPostCode);
                parameters.Add("@customerEmail", model.CustomerEmail);
                parameters.Add("@customerIdentificationNumber", model.CustomerIdentificationNumber);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@customerAddress1", model.CustomerAddress1);
                parameters.Add("@customerAddress2", model.CustomerAddress2);
                parameters.Add("@customerZipCode", model.CustomerZipCode);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderAddress_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Get By OrderId
        public async Task<List<CustomerOrderAddressModel>> GetByOrderIdAsync(int orderId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_CustomerOrderAddress_GetByOrderId]";
                var parameters = new DynamicParameters();
                parameters.Add("@orderId", orderId);
                var result = await connection.QueryAsync<CustomerOrderAddressModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
