﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class FeedbackModel
    {
        public int Id { get; set; }
        public string Review { get; set; } = string.Empty;
        public string? Comment { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
