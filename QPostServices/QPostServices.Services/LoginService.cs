﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using QPostServices.Domain.Helper;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace QPostServices.Services
{
    public class LoginService : ILoginService
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly ILoginRepository _loginRepository;
        private readonly AppSettings _appSettings;
        public LoginService(AppSettings appSettings, ILoginRepository loginRepository, IHttpContextAccessor httpContext)
        {
            _loginRepository = loginRepository;
            _appSettings = appSettings;
            _httpContext = httpContext;
        }

        #region GenerateTokenAsync
        public async Task<string?> GenerateTokenAsync(string username, string password, string ip, string deviceName)
        {
            try
            {
                var user = await _loginRepository.CheckLoginAsync(username, password);
                if (user != null)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                            new Claim(ClaimTypes.Role, user.RoleName)
                        }),
                        Expires = DateTime.UtcNow.AddMonths(_appSettings.AccessTokenExpireHours),
                        SigningCredentials = new SigningCredentials(
                            new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    CreateLog(user.CustomerId, ip, deviceName);

                    return tokenHandler.WriteToken(token);
                }
                return null;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region LoginCustomerAsync
        public async Task<UserLoginModel?> LoginCustomerAsync(string username, string password, string ip, string deviceName)
        {
            try
            {
                var user = await _loginRepository.CheckLoginAsync(username, password);
                if (user != null)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                            new Claim(ClaimTypes.Role, user.RoleName)
                        }),
                        Expires = DateTime.UtcNow.AddMonths(_appSettings.AccessTokenExpireHours),
                        SigningCredentials = new SigningCredentials(
                            new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha256Signature)
                    };
                    if (user.RoleName == "Customer")
                    {
                        tokenDescriptor.Subject.AddClaim(new Claim("customerId", user.CustomerId.ToString()));
                    }
                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    CreateLog(user.CustomerId, ip, deviceName);
                    user.Token = tokenHandler.WriteToken(token);
                    return user;
                }
                return null;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region LoggedInUser
        public int LoggedInUser()
        {
            var userId = _httpContext.HttpContext.User.Claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier)?.Value;
            return string.IsNullOrEmpty(userId) ? 0 : int.Parse(userId);
        }
        #endregion

        #region LoggedInCustomer
        public int LoggedInCustomer()
        {
            var user = _httpContext.HttpContext.User;
            var isCustomer = user.IsInRole("Customer");

            if (isCustomer)
            {
                var customerIdClaim = user.Claims.FirstOrDefault(i => i.Type == "customerId")?.Value;
                if (!string.IsNullOrEmpty(customerIdClaim) && int.TryParse(customerIdClaim, out var customerId))
                {
                    return customerId;
                }
            }

            return 0;
        }
        #endregion

        #region CreateLog
        private async void CreateLog(int customerId, string ip, string deviceName)
        {
            var log = new LoginLogsModel
            {
                CustomerId = customerId,
                DeviceName = deviceName,
                IPAddress = ip,
            };
            await _loginRepository.CreateLogAsync(log);
        } 
        #endregion
    }
}
