﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PBXRenewPostBoxRequest
    {
        [Required]
        //[StringLength(6, MinimumLength = 1)]
        public string POBoxNo { get; set; } = default!;

        [Required]
        //[EnumRequired(typeof(POBoxRenewalPeriod))]
        public POBoxRenewalPeriod Period { get; set; }

        public PBXRenewPostBoxModel ToModel()
        {
            return new PBXRenewPostBoxModel
            {
                Period = Period,
                POBoxNo = POBoxNo
            };
        }
    }
}
