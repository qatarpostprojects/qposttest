﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class InboundRequestRepository : IInboundRequestRepository
    {

        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public InboundRequestRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }
        #region Create
        public async Task<int> CreateAsync(InboundRequestModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_InboundRequest_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@boxNumber", model.BoxNumber);
                parameters.Add("@customerId", model.CustomerId);
                parameters.Add("@requestedDate", DateTime.Now);
                parameters.Add("@addressId", model.AddressId);
                parameters.Add("@requestTypeId", model.RequestTypeId);
                parameters.Add("@holdDays", model.HoldDays);
                parameters.Add("@specialInstructions", model.SpecialInstructions);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@currentStatus", model.CurrentStatus);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@codRequired", model.CodRequired);
                parameters.Add("@paidAmount", model.PaidAmount);
                parameters.Add("@paymentType", model.PaymentType);
                parameters.Add("@codAmount", model.CODAmount);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region GetMasterLookUpAsync
        public async Task<List<IdValueModel>> GetMasterLookUpAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_InboundRequestMaster_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<IdValueModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
