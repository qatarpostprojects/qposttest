﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class CustomerAddress
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Phone { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public bool IsPrimary { get; set; }
        public string City { get; set; } = default!;
        public string? PoBox { get; set; } 
        public string? SmartBox { get; set; } 
        public string? ZoneNumber { get; set; }
        public string? StreetNumber { get; set; }
        public string? BuildingNumber { get; set; }
        public string? FloorNumber { get; set; }
        public string? UnitNumber { get; set; }
        public string? GeoLocationX { get; set; }
        public string? GeoLatitude { get; set; }
        public string? GeoLongitude { get; set; }
        public string AddressType { get; set; } = default!;
        public string? AddressDescription { get; set; }
        public string Country { get; set; } = default!;
        public string? Reference1 { get; set; }
        public string RecordStatus { get; set; } = default!;
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? ZipCode { get; set; }

        public static CustomerAddress FromModel(CustomerAddressModel model)
        {
            return new CustomerAddress
            {
                Id = model.Id,
                Country = model.Country,
                AddressDescription = model.AddressDescription,
                AddressType = model.AddressType.ToString(),
                RecordStatus = model.RecordStatus.ToString(),
                BuildingNumber = model.BuildingNumber,
                City = model.City,
                CreatedDate = model.CreatedDate,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CustomerId = model.CustomerId,
                FloorNumber = model.FloorNumber,
                GeoLatitude = model.GeoLatitude,
                GeoLongitude = model.GeoLongitude,
                GeoLocationX = model.GeoLocationX,
                IsPrimary = model.IsPrimary,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                PoBox = model.PoBox,
                Reference1 = model.Reference1,
                SmartBox = model.SmartBox,
                StreetNumber = model.StreetNumber,
                UnitNumber = model.UnitNumber,
                ZoneNumber = model.ZoneNumber,
                Name = model.Name,
                Email = model.Email,
                Phone = model.Phone,
                Address1 = model.Address1,
                Address2 = model.Address2,
                ZipCode = model.ZipCode
            };
        }
    }
}
