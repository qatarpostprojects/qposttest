﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class PickupTypeService : IPickupTypeService
    {
        private const string _pickupTypeDoesNotExist = "Pickup Type with id {0} does not exists";
        private const string _successCreation = "Pickup Type created successfully";
        private const string _failedCreation = "Pickup Type creation failed";
        private const string _successDeleted = "Pickup Type deleted successfully";
        private const string _successUpdate = "Pickup Type updated successfully";

        private readonly IPickupTypeRepository _pickupTypeRepository;
        private readonly ILoginService _loginService;
        private readonly IFileService _fileService;

        public PickupTypeService(IPickupTypeRepository pickupTypeRepository, ILoginService loginService, IFileService fileService)
        {
            _pickupTypeRepository = pickupTypeRepository;
            _loginService = loginService;
            _fileService = fileService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(PickupTypeModel model)
        {
            try
            {
                model.CreatedUser = _loginService.LoggedInUser();
                var response = new ResponseModel();
                if (model.LogoImageFile != null)
                {
                    model.Logo = await _fileService.ConvertImageToBase64(model.LogoImageFile);
                }

                var result = await _pickupTypeRepository.CreateAsync(model);

                if (result > 0)
                {
                    response.Response = _successCreation;
                    response.Error = false;
                }
                else
                {
                    response.Error = true;
                    response.Response = _failedCreation;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _pickupTypeRepository.UpdateStatusAsync(id, false, userId);

            if (result > 0)
            {
                response.Response = _successDeleted;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_pickupTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _pickupTypeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_pickupTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get all
        public async Task<List<PickupTypeModel>> GetListAsync()
        {
            return await _pickupTypeRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(PickupTypeModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();

            if (model.LogoImageFile != null)
            {
                model.Logo = await _fileService.ConvertImageToBase64(model.LogoImageFile);
            }

            var result = await _pickupTypeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_pickupTypeDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
