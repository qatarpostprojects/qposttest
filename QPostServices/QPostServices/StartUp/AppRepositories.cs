﻿using QPostServices.Domain.Repositories;
using QPostServices.Services;
using QPostServices.Storage;

namespace QPostServices.StartUp
{
    public static class AppRepositories
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IExceptionLogRepository, ExceptionLogRepository>();

            services.AddScoped<ILockTypeRepository, LockTypeRepository>();
            services.AddScoped<ILoginRepository, LoginRepository>();
            services.AddScoped<IPositionRepository, PositionRepository>();
            services.AddScoped<IBoxCategoryRepository, BoxCategoryRepository>();
            services.AddScoped<IBoxTypeRepository, BoxTypeRepository>();
            services.AddScoped<IPOBoxRepository, POBoxRepository>();
            services.AddScoped<IBoxSizeRepository, BoxSizeRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICustomerAddressRepository, CustomerAddressRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            services.AddScoped<IWalletTransactionSummaryRepository, WalletTransactionSummaryRepository>();
            services.AddScoped<IWalletTransactionDetailsRepository, WalletTransactionDetailsRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<IServiceRepository, ServiceRepository>();
            services.AddScoped<ICustomerSubscriptionRepository, CustomerSubscriptionRepository>();
            services.AddScoped<ICustomerOrderAddressRepository, CustomerOrderAddressRepository>();
            services.AddScoped<ICustomerOrderDetailsRepository, CustomerOrderDetailsRepository>();
            services.AddScoped<IUserLoginRepository, UserLoginRepository>();
            services.AddScoped<ISubscriptionTransactionRepository, SubscriptionTransactionRepository>();
            services.AddScoped<IOrderSyncStatusRepository, OrderSyncStatusRepository>();
            services.AddScoped<IEventMasterRepository, EventMasterRepository>();
            services.AddScoped<IShipmentCostRepository, ShipmentCostRepository>();
            services.AddScoped<ITrackRepository, TrackRepository>();
            services.AddScoped<ISubscriptionTransactionNumber, SubscriptionTransactionNumber>();
            services.AddScoped<IInboundRequestRepository,InboundRequestRepository>();
            services.AddScoped<IPaymentMethodRepository,PaymentMethodRepository>();
            services.AddScoped<IOrderContentRepository,OrderContentRepository>();
            services.AddScoped<IDeliveryProductRepository,DeliveryProductRepository>();
            services.AddScoped<IDeliveryTypeRepository, DeliveryTypeRepository>();
            services.AddScoped<IBranchRepository, BranchRepository>();
            services.AddScoped<ISubscriptionTopupRepository, SubscriptionTopupRepository>();
            services.AddScoped<IPOBoxDurationRepository, POBoxDurationRepository>();
            services.AddScoped<IPOBoxTypeRepository, POBoxTypeRepository>();
            services.AddScoped<IProductWeightRepository, ProductWeightRepository>();
            services.AddScoped<IHSCODERepository, HSCODERepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<ICustomerCardDetailsRepository, CustomerCardDetailsRepository>();
            services.AddScoped<IConfigParameterRepository, ConfigParameterRepository>();
            services.AddScoped<IPickupTypeRepository, PickupTypeRepository>();
            services.AddScoped<ICustomerProfileImageRepository, CustomerProfileImageRepository>();
            services.AddScoped<IBannerRepository, BannerRepository>();
            services.AddScoped<IFeedbackRepository, FeedbackRepository>();
            services.AddScoped<IUserSessionRepository, UserSessionRepository>();
            services.AddScoped<IProfileNotificationSettingsRepository, ProfileNotificationSettingsRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IServiceTypeRepository, ServiceTypeRepository>();
            services.AddScoped<INotificationCategoryRepository, NotificationCategoryRepository>();
            services.AddScoped<IStampCodeRepository, StampCodeRepository>();
            services.AddScoped<ICustomerNotificationRepository, CustomerNotificationRepository>();
            services.AddScoped<ITimeSlotRepository, TimeSlotRepository>();
            services.AddScoped<ITermsAndConditionRepository, TermsAndConditionRepository>();

            return services;
        }
    }
}
