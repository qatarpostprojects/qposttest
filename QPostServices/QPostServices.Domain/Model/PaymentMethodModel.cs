﻿using Microsoft.AspNetCore.Http;
using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class PaymentMethodModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public IFormFile? ImageFile { get; set; }
        public string? Image { get; set; }
        public int ListingOrder { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public string? RedirectingURL { get; set; }
        public string? NameAr { get; set; }
    }
}
