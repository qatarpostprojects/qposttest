﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/Wallet/TransactionSummary")]
    [ApiController]
    public class WalletTransactionSummaryController : ControllerBase
    {
        private readonly IWalletTransactionSummaryService _walletTransactionSummaryService;
        public WalletTransactionSummaryController(IWalletTransactionSummaryService walletTransactionSummaryService)
        {
            _walletTransactionSummaryService = walletTransactionSummaryService;
        }

        #region Create Wallet Transaction Summary
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateWalletTransactionSummary))]
        public async Task<IActionResult> CreateWalletTransactionSummary([FromBody] WalletTransactionSummaryRequest request)
        {
            try
            {
                var result = await _walletTransactionSummaryService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Wallet Transaction Summary by id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(WalletTransactionSummary))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetWalletTransactionSummary))]
        public async Task<IActionResult> GetWalletTransactionSummary(int id)
        {
            try
            {
                var result = await _walletTransactionSummaryService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(WalletTransactionSummary.FromModel((WalletTransactionSummaryModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Wallet Transaction Summary by walletid
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<WalletTransactionSummary>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("Wallet/{id}", Name = nameof(GetWalletTransactionSummaryByWalletId))]
        public async Task<IActionResult> GetWalletTransactionSummaryByWalletId(int id)
        {
            try
            {
                var results = await _walletTransactionSummaryService.GetByWalletIdAsync(id);

                return Ok(results.Select(x => WalletTransactionSummary.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Wallet Transaction Summary by customer Id
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<WalletTransactionSummary>))]
        [HttpGet("Customer/{customerId}", Name = nameof(GetWalletTransactionSummaryList))]
        public async Task<IActionResult> GetWalletTransactionSummaryList(int customerId)
        {
            try
            {
                var results = await _walletTransactionSummaryService.GetListByCustomerId(customerId);

                return Ok(results.Select(x => WalletTransactionSummary.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Wallet Balance Amount
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        [HttpGet("Customer/{customerId}/Wallet/{walletId}/Balance", Name = nameof(GetWalletBalanceAmount))]
        public async Task<IActionResult> GetWalletBalanceAmount(int customerId, int walletId)
        {
            try
            {
                var results = await _walletTransactionSummaryService.GetWalletBalanceAmountAsync(customerId, walletId);

                return Ok(results);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        // #region Get Wallet Summary by customer and wallet
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
        //[HttpGet("Customer/{customerId}/Wallet/{walletId}", Name = nameof(GetWalletDetailsByCustomerAndWallet))]
        //public async Task<IActionResult> GetWalletDetailsByCustomerAndWallet(int customerId, int walletId)
        //{
        //    try
        //    {
        //        var results = await _walletTransactionSummaryService.GetWalletBalanceAmountAsync(customerId, walletId);

        //        return Ok(results);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
        //    }
        //}
        //#endregion

        #region Delete Wallet Transaction Summary
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteWalletTransactionSummary))]
        public async Task<IActionResult> DeleteWalletTransactionSummary(int id)
        {
            try
            {
                var result = await _walletTransactionSummaryService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update Wallet Transaction Summary
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateWalletTransactionSummary))]
        public async Task<IActionResult> UpdateWalletTransactionSummary(int id, [FromBody] WalletTransactionSummaryRequest request)
        {
            try
            {
                var result = await _walletTransactionSummaryService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
