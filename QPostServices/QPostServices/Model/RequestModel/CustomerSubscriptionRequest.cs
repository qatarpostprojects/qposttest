﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerSubscriptionRequest
    {
        [Required]
        public int CustomerId { get; set; }

        [Required]
        public int ServiceId { get; set; }

        //[Required]
        //public DateTime StartDate { get; set; }

        //[Required]
        //public DateTime EndDate { get; set; }

        public string? QatarIdFrontSide { get; set; }
        public IFormFile? QatarIdFrontSideFile { get; set; }
        public string? QatarIdBackSide { get; set; }
        public IFormFile? QatarIdBackSideFile { get; set; }

        public string? CompanyDocFrontSide { get; set; }
        public IFormFile? CompanyDocFrontSideFile { get; set; }
        public string? CompanyDocBackSide { get; set; }
        public IFormFile? CompanyDocBackSideFile { get; set; }

        [Required]
        public int? ServiceProductId { get; set; }
        [Required]
        public int SubscriptionTypeValue { get; set; }
        public string PaymentChannel { get; set; } = string.Empty;
        public bool IsEligibleForRenew { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public int? LastPaymentTransactionId { get; set; }
        public bool ActiveStatus { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }

        public int? CardPaymentId { get; set; }
        public int? WalletTransactioId { get; set; }
        public string? PaymentType { get; set; }

        public string AWBNumber { get; set; } = string.Empty;

        public PBXSubscribePOBoxRequest ExternalSubscriptionRequest { get; set; } = new PBXSubscribePOBoxRequest();
        //public int? MaximumNumberOfTransaction { get; set; }
        //public int? NumberOfTransactionPending { get; set; }
        //public int? NumberOfTransactionCompleted { get; set; }
        //public decimal? ServiceAmount { get; set; }
        //public decimal? ServiceAmountPending { get; set; }
        //public decimal? ServiceAmountConsumed { get; set; }

        public CustomerSubscriptionModel ToCreateModel()
        {
            return new CustomerSubscriptionModel
            {
                ActiveStatus = ActiveStatus,
                LastPaymentDate = LastPaymentDate,
                LastPaymentTransactionId = LastPaymentTransactionId,
                CustomerId = CustomerId,
                //EndDate = EndDate,
                IsEligibleForRenew = IsEligibleForRenew,
                PaymentChannel = PaymentChannel,
                ServiceProductId = ServiceProductId,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Remarks = Remarks,
                ServiceId = ServiceId,
                StartDate = DateTime.Now,
                SubscriptionTypeValue = SubscriptionTypeValue,
                CardPaymentId = CardPaymentId,
                PaymentType = PaymentType,
                AWBNumber = AWBNumber,
                QatarIdBackSide = QatarIdBackSide,
                QatarIdBackSideFile = QatarIdBackSideFile,
                QatarIdFrontSide = QatarIdFrontSide,
                QatarIdFrontSideFile = QatarIdFrontSideFile,
                CompanyDocBackSide = CompanyDocBackSide,
                CompanyDocBackSideFile = CompanyDocBackSideFile,
                CompanyDocFrontSide = CompanyDocFrontSide,
                CompanyDocFrontSideFile = CompanyDocFrontSideFile,
                ExternalSubscriptionModel = ExternalSubscriptionRequest.ToModel()
                //ServiceAmount = ServiceAmount,
                //ServiceAmountPending = ServiceAmountPending,
                //ServiceAmountConsumed = ServiceAmountConsumed,
                //MaximumNumberOfTransaction = MaximumNumberOfTransaction,
                //NumberOfTransactionPending = NumberOfTransactionPending,
                //NumberOfTransactionCompleted = NumberOfTransactionCompleted
            };
        }
        public CustomerSubscriptionModel ToUpdateModel(int id)
        {
            return new CustomerSubscriptionModel
            {
                Id = id,
                ActiveStatus = ActiveStatus,
                //LastPaymentDate = LastPaymentDate,
                //LastPaymentTransactionId = LastPaymentTransactionId,
                //CustomerId = CustomerId,
                //EndDate = EndDate,
                IsEligibleForRenew = IsEligibleForRenew,
                PaymentChannel = PaymentChannel,
                ServiceProductId = ServiceProductId,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Remarks = Remarks,
                //ServiceId = ServiceId
                //StartDate = StartDate,
                //ServiceAmount = ServiceAmount,
                //ServiceAmountPending = ServiceAmountPending,
                //ServiceAmountConsumed = ServiceAmountConsumed,
                //MaximumNumberOfTransaction = MaximumNumberOfTransaction,
                //NumberOfTransactionCompleted= NumberOfTransactionCompleted,
                //NumberOfTransactionPending = NumberOfTransactionPending
            };
        }
    }

    public class CustomerAddonSubscriptionRequest
    {
        [Required]
        public int CustomerId { get; set; }

        [Required]
        public int ServiceId { get; set; }

        [Required]
        public int? ServiceProductId { get; set; }
        [Required]
        public int SubscriptionTypeValue { get; set; }
        public string PaymentChannel { get; set; } = string.Empty;
        public bool IsEligibleForRenew { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public int? LastPaymentTransactionId { get; set; }
        public bool ActiveStatus { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }

        public int? CardPaymentId { get; set; }
        public int? WalletTransactioId { get; set; }
        public string PaymentType { get; set; }

        public string AWBNumber { get; set; } = string.Empty;

        public CustomerSubscriptionModel ToCreateModel()
        {
            return new CustomerSubscriptionModel
            {
                ActiveStatus = ActiveStatus,
                LastPaymentDate = LastPaymentDate,
                LastPaymentTransactionId = LastPaymentTransactionId,
                CustomerId = CustomerId,
                IsEligibleForRenew = IsEligibleForRenew,
                PaymentChannel = PaymentChannel,
                ServiceProductId = ServiceProductId,
                Reference1 = Reference1,
                Reference2 = Reference2,
                Remarks = Remarks,
                ServiceId = ServiceId,
                StartDate = DateTime.Now,
                SubscriptionTypeValue = SubscriptionTypeValue,
                CardPaymentId = CardPaymentId,
                PaymentType = PaymentType,
                AWBNumber = AWBNumber,
                ExternalSubscriptionModel = null
            };
        }
    }
}
