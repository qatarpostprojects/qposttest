﻿using QPostServices.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class CurrentServiceDetailsModel
    {
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public int ServiceId { get; set; }
        public int SubscriptionTypeValue { get; set; }
        public DateTime SubscriptionStartDate { get; set; }
        public DateTime SubscriptionEndDate { get; set; }
        public int? ServiceProductId { get; set; }
        public string PaymentChannel { get; set; } = string.Empty;
        public bool IsEligibleForRenew { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public int? LastPaymentTransactionId { get; set; }
        public bool ActiveStatus { get; set; }
        public int? MaximumNumberOfTransaction { get; set; }
        public int? NumberOfTransactionPending { get; set; }
        public int? NumberOfTransactionCompleted { get; set; }
        public decimal? ServiceAmount { get; set; }
        public decimal? ServiceAmountPending { get; set; }
        public decimal? ServiceAmountConsumed { get; set; }
        public string ServiceName { get; set; } = string.Empty;
        public string ServiceSubscriptionType { get; set; } = string.Empty;
        public string ServiceSubscriptionPaymentType { get; set; } = string.Empty;
        public bool ServiceUpgradeEligible { get; set; }
        public int ServiceBaseValue { get; set; }
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }
        public string? ServiceBaseOn { get; set; }
    }
}
