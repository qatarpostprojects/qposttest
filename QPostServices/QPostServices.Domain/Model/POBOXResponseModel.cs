﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QPostServices.Domain.Model
{
    public class PBXResponseModel
    {
        public string? ErrorCode { get; set; }
        public object? Entry { get; set; }
        public string? Message { get; set; }
    }

    public class PBXCustomer
    {
        public string companyActivity { get; set; }
        public int sector { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int customerIDType { get; set; }
        public string qatarID { get; set; }
        public string passportNo { get; set; }
        public object passportExpiryDate { get; set; }
        public string passportIssueCountry { get; set; }
        public int additionalNumberOfKeys { get; set; }
        public List<string> smartCardNumbers { get; set; }
        public int numOfEmployees { get; set; }
        public int customerLanguage { get; set; }
        public string customerName { get; set; }
        public string address { get; set; }
        public string buildingNumber { get; set; }
        public string zoneNumber { get; set; }
        public string streetNumber { get; set; }
        public string city { get; set; }
        public string mobileNumber { get; set; }
        public string officeTelephoneNo { get; set; }
        public string email { get; set; }
    }

    public class PoBoxEnquiryEntry
    {
        public string postBoxNumber { get; set; }
        public int status { get; set; }
        public DateTime startDate { get; set; }
        public DateTime paidUntil { get; set; }
        public int category { get; set; }
        public int customerType { get; set; }
        public string poBoxLocation { get; set; }
        public string boxPosition { get; set; }
        public PBXCustomer customer { get; set; }
        public object addService { get; set; }
        public List<object> companyList { get; set; }
        public string remarks { get; set; }
        public bool reserved { get; set; }
    }
}
