﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPaymentRepository
    {
        Task<int> CreateAsync(PaymentModel model);
        Task<PaymentModel?> GetByIdAsync(int id);
        Task<List<PaymentModel>> GetAllActiveByCustomerIdAsync(int customerId); 
        Task<List<RecentTransactionModel>> GetRecentTransactionsByCustomerIdAsync(int customerId);
        Task<int> UpdateStatusAsync(int id, int userId, RecordStatus recordStatus);
        Task<int> UpdateAsync(PaymentModel model);       
    }
}
