﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class CustomerOrderAddressService: ICustomerOrderAddressService
    {
        private const string _successCreation = "Address created successfully";
        private const string _successUpdate = "Address updated successfully";
        private const string _failedCreation = "Address creation failed";
        private const string _failedUpdation = "Address updation failed";
        private const string _successDelete = "Address deleted successfully";
        private const string _failedDelete = "Address deletion failed";
        private const string _nameAlreadyExist = "Address with id : {0} already exist";
        private const string _addressDoesNotExist = "Address with id : {0} Does not exist";

        private readonly ICustomerOrderAddressRepository _customerOrderAddressRepository;
        private readonly ILoginService _loginService;
        public CustomerOrderAddressService(ICustomerOrderAddressRepository customerOrderAddressRepository, ILoginService loginService)
        {
            _customerOrderAddressRepository = customerOrderAddressRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(CustomerOrderAddressModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _customerOrderAddressRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _customerOrderAddressRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_addressDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get All
        public async Task<List<CustomerOrderAddressModel>> GetAllActiveAsync()
        {
            return await _customerOrderAddressRepository.GetListAsync();
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _customerOrderAddressRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_addressDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }        
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(CustomerOrderAddressModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUser = _loginService.LoggedInUser();
            var result = await _customerOrderAddressRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_addressDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Order Id
        public async Task<List<CustomerOrderAddressModel>> GetByOrderIdAsync(int orderId)
        {
            return await _customerOrderAddressRepository.GetByOrderIdAsync(orderId);
        }
        #endregion
    }
}
