﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class ServiceRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string Description { get; set; } = string.Empty;

        [Required]
        public string SubscriptionType { get; set; } = string.Empty;

        [Required]
        public string? ApplicableFor { get; set; }

        [Required]
        public string SubscriptionPaymentType { get; set; } = string.Empty;
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }
        public string? SubscriptionTypeAr { get; set; }
        public string? ApplicableForAr { get; set; }
        public string? SubscriptionPaymentTypeAr { get; set; }
        public bool IsUpgradeEligible { get; set; }
        public int? UpgradeID { get; set; }
        public bool IsAddonServiceAvailable { get; set; }
        public int? AddonServiceID { get; set; }
        public int? LoyaltyID { get; set; }
        public string? Remarks { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public string? ServiceBaseOn { get; set; }
        public int ServiceBaseValue { get; set; }
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }
        public bool IsAddonService { get; set; }
        public int? ParentServiceId { get; set; }
        public int? ServiceTypeId { get; set; }

        public ServiceModel ToCreateModel()
        {
            return new ServiceModel
            {
                Name = Name,
                Description = Description,
                SubscriptionType = SubscriptionType,
                ApplicableFor = ApplicableFor,
                SubscriptionPaymentType = SubscriptionPaymentType,
                IsUpgradeEligible = IsUpgradeEligible,
                UpgradeID = UpgradeID,
                IsAddonServiceAvailable = IsAddonServiceAvailable,
                LoyaltyID = LoyaltyID,
                Remarks = Remarks,
                Reference1 = Reference1,
                Reference2 = Reference2,
                ServiceBaseOn = ServiceBaseOn,
                ServiceBaseValue = ServiceBaseValue,
                SubscriptionAmount = SubscriptionAmount,
                RenewAmount = RenewAmount,
                CancelAmount = CancelAmount,
                IsAddonService = IsAddonService,
                ParentServiceId = ParentServiceId,
                ServiceTypeId = ServiceTypeId,
                NameAr = NameAr,
                SubscriptionPaymentTypeAr = SubscriptionPaymentTypeAr,
                SubscriptionTypeAr = SubscriptionTypeAr,
                ApplicableForAr = ApplicableForAr,
                DescriptionAr = DescriptionAr,
            };
        }

        public ServiceModel ToUpdateModel(int id)
        {
            return new ServiceModel
            {
                Id = id,
                Name = Name,
                Description = Description,
                SubscriptionType = SubscriptionType,
                ApplicableFor = ApplicableFor,
                SubscriptionPaymentType = SubscriptionPaymentType,
                IsUpgradeEligible = IsUpgradeEligible,
                UpgradeID = UpgradeID,
                IsAddonServiceAvailable = IsAddonServiceAvailable,
                LoyaltyID = LoyaltyID,
                Remarks = Remarks,
                Reference1 = Reference1,
                Reference2 = Reference2,
                ServiceBaseOn = ServiceBaseOn,
                ServiceBaseValue = ServiceBaseValue,
                SubscriptionAmount = SubscriptionAmount,
                RenewAmount = RenewAmount,
                CancelAmount = CancelAmount,
                IsAddonService = IsAddonService,
                ParentServiceId = ParentServiceId,
                ServiceTypeId = ServiceTypeId,
                NameAr = NameAr,
                SubscriptionPaymentTypeAr = SubscriptionPaymentTypeAr,
                SubscriptionTypeAr = SubscriptionTypeAr,
                ApplicableForAr = ApplicableForAr,
                DescriptionAr = DescriptionAr,
            };
        }
    }
}
