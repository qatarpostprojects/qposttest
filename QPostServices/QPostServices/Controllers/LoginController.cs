﻿using DeviceDetectorNET;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;

namespace QPostServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;
        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        #region Check Login
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [HttpPost(Name = nameof(CheckLogin))]
        public async Task<IActionResult> CheckLogin([FromBody] LoginRequest request)
        {
            try
            {
                var deviceName = GetDevice();

                var myIP = Request.HttpContext.Connection.RemoteIpAddress?.ToString() ?? string.Empty;

                var token = await _loginService.GenerateTokenAsync(request.UserName, request.Password, myIP, deviceName);

                if (token != null)
                {
                    return Ok(token);
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
        
        #region Check Customer Login
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [HttpPost("Customer",Name = nameof(CheckCustomerLogin))]
        public async Task<IActionResult> CheckCustomerLogin([FromBody] LoginRequest request)
        {
            try
            {
                var deviceName = GetDevice();

                var myIP = Request.HttpContext.Connection.RemoteIpAddress?.ToString() ?? string.Empty;

                var userDetails = await _loginService.LoginCustomerAsync(request.UserName, request.Password, myIP, deviceName);

                if (userDetails != null)
                {
                    return Ok(CustomerLogin.FromModel(userDetails));
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region GetDevice
        private string GetDevice()
        {
            string userAgent = HttpContext.Request.Headers["User-Agent"].ToString();
            var dd = new DeviceDetector(userAgent);
            dd.Parse();
            return dd.GetDeviceName();
        } 
        #endregion
    }
}
