﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class TimeSlotResponse
    {
        public int Id { get; set; }
        public string TimeSlot { get; set; } = string.Empty;
        public string? TimeSlotAr { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public static TimeSlotResponse FromModel(TimeSlotModel model)
        {
            return new TimeSlotResponse()
            {
                Id = model.Id,
                TimeSlot = model.TimeSlot,
                TimeSlotAr = model.TimeSlotAr,
                CreatedUser = model.CreatedUser,
                CreatedUserName = model.CreatedUserName,
                CreatedDate = model.CreatedDate,
                ModifiedUser = model.ModifiedUser,
                ModifiedUserName = model.ModifiedUserName,
                ModifiedDate = model.ModifiedDate,
                RecordStatus = model.RecordStatus,
                StartTime =  DateTime.Today.Add(TimeSpan.Parse(model.StartTime)),
                EndTime = DateTime.Today.Add(TimeSpan.Parse(model.EndTime)),
            };
        }
    }
}
