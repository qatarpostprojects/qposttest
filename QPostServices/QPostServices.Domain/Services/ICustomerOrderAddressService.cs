﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerOrderAddressService
    {
        Task<ResponseModel> CreateAsync(CustomerOrderAddressModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<CustomerOrderAddressModel>> GetAllActiveAsync();
        Task<List<CustomerOrderAddressModel>> GetByOrderIdAsync(int orderId);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(CustomerOrderAddressModel model);
    }
}
