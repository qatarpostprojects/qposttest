﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class UserLoginRequest
    {
        [Required]
        public int CustomerId { get; set; }

        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string UserName { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;

        [Required]
        public string SecurityQuestion { get; set; } = string.Empty;

        [Required]
        public string SecurityAnswer { get; set; } = string.Empty;

        [Required]
        public int RoleId { get; set; }
        public UserLoginModel ToCreateModel()
        {
            return new UserLoginModel
            {
                CustomerId = CustomerId,
                Name = Name,
                UserName = UserName,
                Password = Password,
                SecurityQuestion = SecurityQuestion,
                SecurityAnswer = SecurityAnswer,
                RoleId = RoleId
            };
        }

        public UserLoginModel ToUpdateModel(int id)
        {
            return new UserLoginModel
            {
                Id = id,
                Name = Name,
                UserName = UserName,
                Password = Password,
                SecurityQuestion = SecurityQuestion,
                SecurityAnswer = SecurityAnswer
            };
        }
    }
}
