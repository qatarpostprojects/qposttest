﻿namespace QPostServices.Domain.Model
{
    public class MapOrderCustomerModel
    {
        public int CustomerId { get; set; }
        public string? SearchParameter { get; set; }
    }
}
