﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IHSCODERepository
    {
        Task<List<HSCODEModel>> GetListAsync();
        Task<List<HSCODEModel>> GetBySearchText(string searchText);
        Task<int> CreateAsync(HSCODEModel model);
        Task<int> UpdateAsync(HSCODEModel model);
        Task<int> UpdateStatusAsync(int id, bool status, int userId);
        Task<HSCODEModel> GetByIdAsync(int id);
    }
}
