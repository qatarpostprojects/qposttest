﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ILoginRepository
    {
        Task<UserLoginModel?> CheckLoginAsync(string username, string password);
        Task CreateLogAsync(LoginLogsModel model);
    }
}
