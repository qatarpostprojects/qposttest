﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IFeedbackService
    {
        Task<ResponseModel> AddAsync(FeedbackModel model);
        Task<ResponseModel> GetByCustomerIdAsync();
        Task<List<FeedbackModel>> GetAsync();
    }
}
