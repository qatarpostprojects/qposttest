﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class BoxCategoryServices : IBoxCategoryServices
    {
        private readonly IBoxCategoryRepository _boxCategoryRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Box category created successfully";
        private const string _successUpdate = "Box category updated successfully";
        private const string _failedCreation = "Box category creation failed";
        private const string _failedUpdation = "Box category updation failed";
        private const string _successDelete = "Box category deleted successfully";
        private const string _failedDelete = "Box category deletion failed";
        private const string _nameAlreadyExist = "Box category with name : {0} already exist";
        private const string _boxCategoryDoesNotExist = "Box category with id : {0} Does not exist";


        public BoxCategoryServices(IBoxCategoryRepository boxCategoryRepository, ILoginService loginService)
        {
            _boxCategoryRepository = boxCategoryRepository;
            _loginService = loginService;
        }

        #region Create BoxCategory
        public async Task<ResponseModel> CreateAsync(BoxCategoryModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _boxCategoryRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get BoxCategory by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _boxCategoryRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxCategoryDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of BoxCategory
        public async Task<List<BoxCategoryModel>> GetListAsync()
        {
            return await _boxCategoryRepository.GetListAsync();
        }
        #endregion

        #region Update BoxCategory
        public async Task<ResponseModel> UpdateAsync(BoxCategoryModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            var result = await _boxCategoryRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxCategoryDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete BoxCategory
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _boxCategoryRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_boxCategoryDoesNotExist,id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
