﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public ServiceRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create Service
        public async Task<int> CreateAsync(ServiceModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@subscriptionType", model.SubscriptionType);
                parameters.Add("@applicableFor", model.ApplicableFor);
                parameters.Add("@subscriptionPaymentType", model.SubscriptionPaymentType);
                parameters.Add("@upgradeEligible", model.IsUpgradeEligible);
                parameters.Add("@upgradeId", model.UpgradeID);
                parameters.Add("@addonServiceAvailable", model.IsAddonServiceAvailable);
                parameters.Add("@serviceLoyaltyId", model.LoyaltyID);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@serviceBaseOn", model.ServiceBaseOn); 
                parameters.Add("@serviceBaseValue", model.ServiceBaseValue);
                parameters.Add("@subscriptionAmount", model.SubscriptionAmount);
                parameters.Add("@renewAmount", model.RenewAmount);
                parameters.Add("@cancelAmount", model.CancelAmount);
                parameters.Add("@isAddOnService", model.IsAddonService);
                parameters.Add("@parentServiceId", model.ParentServiceId);
                parameters.Add("@serviceTypeId", model.ServiceTypeId);

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Service by id
        public async Task<ServiceModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<ServiceModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Service by name
        public async Task<ServiceModel?> GetByNameAsync(string name)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetByname]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", name);
                var result = await connection.QueryAsync<ServiceModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Service list
        public async Task<List<ServiceModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<ServiceModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Service
        public async Task<int> UpdateAsync(ServiceModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@subscriptionType", model.SubscriptionType);
                parameters.Add("@applicableFor", model.ApplicableFor);
                parameters.Add("@subscriptionPaymentType", model.SubscriptionPaymentType);
                parameters.Add("@upgradeEligible", model.IsUpgradeEligible);
                parameters.Add("@upgradeId", model.UpgradeID);
                parameters.Add("@addonServiceAvailable", model.IsAddonServiceAvailable);
                parameters.Add("@serviceLoyaltyId", model.LoyaltyID);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@reference1", model.Reference1);
                parameters.Add("@reference2", model.Reference2);
                parameters.Add("@serviceBaseOn", model.ServiceBaseOn); 
                parameters.Add("@serviceBaseValue", model.ServiceBaseValue); 
                parameters.Add("@subscriptionAmount", model.SubscriptionAmount); 
                parameters.Add("@renewAmount", model.RenewAmount); 
                parameters.Add("@cancelAmount", model.CancelAmount); 
                parameters.Add("@isAddOnService", model.IsAddonService); 
                parameters.Add("@parentServiceId", model.ParentServiceId);
                parameters.Add("@serviceTypeId", model.ServiceTypeId);


                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Service record status
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", (int)status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Get Service Amount
        public async Task<ServiceAmountModel?> GetServiceAmountAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetServiceAmount]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<ServiceAmountModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get AddOnServices By ServiceIds
        public async Task<List<IdValueModel>> GetAddOnServicesByIds(List<int> services)
        {
            try
            {
                DataTable table = new DataTable();
                table.Columns.Add("Value", typeof(int));
                foreach (int service in services)
                {
                    table.Rows.Add(service);
                }

                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetAddOnServicesByServiceIds]";
                var parameters = new DynamicParameters();

                parameters.Add("@ParentServiceIds", table.AsTableValuedParameter("IntList"));

                var result = await connection.QueryAsync<IdValueModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetDetailsByCustomerId 
        public async Task<List<ServiceModel>> GetDetailsByCustomerIdAsync(int customerId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetParentServiceByCustomerId]";
                var parameters = new DynamicParameters();
                parameters.Add("@customerId", customerId);
                var result = await connection.QueryAsync<ServiceModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get AddOn Service By ServiceID
        public async Task<List<ServiceModel>> GetAddOnServicesById(int serviceId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetAddOnServicesById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", serviceId);
                var result = await connection.QueryAsync<ServiceModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get Lookup 
        public async Task<List<IdValueModel>> GetLookupAsync(ServiceType serviceType)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_Service_GetIdValue]";
                var parameters = new DynamicParameters();
                parameters.Add("@type", (int)serviceType);
                var result = await connection.QueryAsync<IdValueModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
