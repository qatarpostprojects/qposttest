﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class TrackRepository : ITrackRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;
        private readonly string? _defaultDB;

        public TrackRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("UnifiedMessaging");
            _defaultDB = _configuration.GetConnectionString("DefaultConnection");
        }

        #region TrackAndTraceByMobile
        public async Task<List<TrackMobileResponseModel>> TrackAndTraceByMobile(TrackMobileModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);

                var query = $@"select TRACKING_NUMBER TrackingNumber, LAST_EVENT_STATUS CurrentStatus,
                                    LAST_EVENT_DATE CurrentStatusDate, '' Origin,
                            PRODUCT_SERVICE_CODE ProductServiceCode,
				            ORDER_CREATED_DATE ReceivedDate
                                from [UnifiedMessaging].[dbo].[QPS_WHATSAPP_SEARCH] 
                            where RECIPIENT_MOBILE_NO = '{model.RecipientMobileNumber}' and
                            ORDER_CREATED_DATE  > DateAdd(dd, -20, GetDate()) 
                            order by ORDER_CREATED_DATE desc";

                var result = await connection.QueryAsync<TrackMobileResponseModel>(query);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region AddLogsToTable
        public async Task AddLogAsync(LogModel log)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_defaultDB);
                var procedure = "[POBox_APILogs_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@method", log.Method);
                parameters.Add("@requestName", log.RequestName);
                parameters.Add("@requestUrl", log.RequestUrl);
                parameters.Add("@queryString", log.QueryString);
                parameters.Add("@body", log.Body);
                parameters.Add("@response", log.Response);
                parameters.Add("@createdDate", log.CreatedDate);
                parameters.Add("@createdUser", log.CreatedUser);
                await connection.QueryAsync(procedure, parameters, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetMobileNumber
        public async Task<string?> GetMobileNumberAsync(string searchString)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_defaultDB);
                var procedure = "[POBox_TrackAndTrace_GetMobileNumber]";
                var parameters = new DynamicParameters();
                parameters.Add("@searchString", searchString);
                var result = await connection.QueryAsync<string>(procedure, parameters, commandType: CommandType.StoredProcedure);

                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
