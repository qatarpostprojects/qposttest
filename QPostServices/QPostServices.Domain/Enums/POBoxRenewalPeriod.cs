﻿namespace QPostServices.Domain.Enums
{
    public enum POBoxRenewalPeriod
    {
        OneYear = 0,
        TwoYear = 1
    }
}
