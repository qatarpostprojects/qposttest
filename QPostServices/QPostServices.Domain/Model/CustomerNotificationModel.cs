﻿namespace QPostServices.Domain.Model
{
    public class CustomerNotificationModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string? TrackingNumber { get; set; }
        public string? MobileNumber { get; set; }
        public string? Message { get; set; }
        public bool IsRead { get; set; }
        public int CategoryId { get; set; }
        public string? CategoryName { get; set; }
        public int RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
    public class CustomerNotificationFetchModel
    {
        public int? CustomerId { get; set; }
        public string? TrackingNumber { get; set; }
        public string? MobileNumber { get; set; }
        public bool? IsRead { get; set; }
        public int? CategoryId { get; set; }
    }
}
