﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IHSCODEService
    {
        Task<List<HSCODEModel>> GetListAsync();
        Task<List<HSCODEModel>> GetBySearchText(string searchText);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<ResponseModel> CreateAsync(HSCODEModel model);
        Task<ResponseModel> UpdateAsync(HSCODEModel model);
        Task<ResponseModel> DeleteAsync(int id);
    }
}
