﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class LocationRepository : ILocationRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _metrashConnection;

        public LocationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _metrashConnection = _configuration.GetConnectionString("Metrash");
        }

        #region GetBuildings
        public async Task<List<string>> GetBuildingsAsync(int zoneNumber, int streetNumber)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_metrashConnection);

                var query = $@"SELECT DISTINCT [BUILDING_N]
                    FROM [Metrash_TEST].[dbo].[ZSB]
                     WHERE [ZONE_NO] = {zoneNumber} and [STREET_NO] = {streetNumber}
                     ORDER BY [BUILDING_N] ASC ";

                var result = await connection.QueryAsync<string>(query);
                return result.ToList() ?? new List<string>();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetStreets
        public async Task<List<string>> GetStreetsAsync(int zoneNumber)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_metrashConnection);

                var query = $@"SELECT DISTINCT [STREET_NO]
                    FROM [Metrash_TEST].[dbo].[ZSB]
                    WHERE [ZONE_NO] = {zoneNumber} 
                    ORDER BY [STREET_NO] ASC";

                var result = await connection.QueryAsync<string>(query);
                return result.ToList() ?? new List<string>();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GetZones
        public async Task<List<string>> GetZonesAsync(int zoneNumber)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_metrashConnection);

                var query = $@"SELECT DISTINCT [ZONE_NO]
                    FROM [Metrash_TEST].[dbo].[ZSB]
                    WHERE [ZONE_NO] LIKE '%' + '{zoneNumber}' + '%'
                    ORDER BY [ZONE_NO] ASC";

                var result = await connection.QueryAsync<string>(query);
                return result.ToList() ?? new List<string>();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region ValidateAddress
        public async Task<ValidAddressModel> ValidateAddressAsync(ValidAddressModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_metrashConnection);

                var query = $@"SELECT [ZONE_NO] As ZoneNumber
                              ,[STREET_NO] As StreetNumber
                              ,[BUILDING_N] As BuildingNumber
	                          , 1 as ValidAddress
                            FROM [Metrash_TEST].[dbo].[ZSB] 
                            where [ZONE_NO] = {model.ZoneNumber} and [STREET_NO]= {model.StreetNumber} and [BUILDING_N] = {model.BuildingNumber} ";

                var result = await connection.QueryAsync<ValidAddressModel>(query);
                return result.FirstOrDefault() ?? new ValidAddressModel { ValidAddress = false };
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
