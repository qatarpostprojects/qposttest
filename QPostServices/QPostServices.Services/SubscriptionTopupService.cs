﻿using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class SubscriptionTopupService : ISubscriptionTopupService
    {
        private const string _successCreation = "Subscription Topup created successfully";
        private const string _successUpdate = "Subscription Topup updated successfully";
        private const string _failedCreation = "Subscription Topup creation failed";
        private const string _failedUpdation = "Subscription Topup updation failed";
        private const string _successDelete = "Subscription Topup deleted successfully";
        private const string _failedDelete = "Subscription Topup deletion failed";
        private const string _subscriptionTopupDoesNotExist = "Subscription Topup with id : {0} Does not exist";

        private readonly ISubscriptionTopupRepository _subscriptionTopupRepository;
        private readonly ILoginService _loginService;

        public SubscriptionTopupService(ISubscriptionTopupRepository subscriptionTopupRepository, ILoginService loginService)
        {
            _subscriptionTopupRepository = subscriptionTopupRepository;
            _loginService = loginService;
        }

        #region Create SubscriptionTopup
        public async Task<ResponseModel> CreateAsync(SubscriptionTopupModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _subscriptionTopupRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete SubscriptionTopup
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _subscriptionTopupRepository.UpdateStatusAsync(id, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_subscriptionTopupDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get SubscriptionTopup by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _subscriptionTopupRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_subscriptionTopupDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of SubscriptionTopup By ServiceId
        public async Task<List<SubscriptionTopupModel>> GetListByServiceIdAsync(int serviceId)
        {
            return await _subscriptionTopupRepository.GetListByServiceIdAsync(serviceId);
        }
        #endregion

        #region Get list of SubscriptionTopup
        public async Task<List<SubscriptionTopupModel>> GetListAsync()
        {
            return await _subscriptionTopupRepository.GetListAsync();
        }
        #endregion

        #region Update SubscriptionTopup
        public async Task<ResponseModel> UpdateAsync(SubscriptionTopupModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            var result = await _subscriptionTopupRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_subscriptionTopupDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
