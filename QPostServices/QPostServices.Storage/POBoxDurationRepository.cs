﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data.SqlClient;
using System.Data;

namespace QPostServices.Storage
{
    public class POBoxDurationRepository : IPOBoxDurationRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public POBoxDurationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(POBoxDurationModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxDuration_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@description", model.Description);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                parameters.Add("@logo", model.Logo);
                parameters.Add("@monthValue", model.MonthValue);
                parameters.Add("@createdUser", model.CreatedUser);
                parameters.Add("@serviceId", model.ServiceId);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@active", true);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<POBoxDurationModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxDuration_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<POBoxDurationModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault() ?? new POBoxDurationModel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get by service id
        public async Task<List<POBoxDurationModel>> GetListByServiceIdAsync(int serviceId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxDuration_GetByServiceId]";
                var parameters = new DynamicParameters();
                parameters.Add("@serviceId", serviceId);
                var result = await connection.QueryAsync<POBoxDurationModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get
        public async Task<List<POBoxDurationModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxDuration_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<POBoxDurationModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(POBoxDurationModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxDuration_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@name", model.Name);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@description", model.Description);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                parameters.Add("@logo", model.Logo);
                parameters.Add("@monthValue", model.MonthValue);
                parameters.Add("@modifiedUser", model.ModifiedUser);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update Status
        public async Task<int> UpdateStatusAsync(int id, bool status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBoxDuration_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@active", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
