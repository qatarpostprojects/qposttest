﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IPaymentService
    {
        Task<List<PaymentModel>> GetPaymentListByCustomerId(int customerId);
        Task<List<RecentTransactionModel>> GetRecentTransactionsByCustomerIdAsync(int customerId);
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(PaymentModel model);
        Task<ResponseModel> CreateAsync(PaymentModel model);
        Task<ResponseModel> GetByIdAsync(int id);        
    }
}
