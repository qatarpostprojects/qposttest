﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Model.ResponseModel
{
    public class OrderContent
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? NameAr { get; set; } 
        public string? Description { get; set; }
        public string? DescriptionAr { get; set; }
        public string? Icon { get; set; }
        public int ListingOrder { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedUserName { get; set; }
        public static OrderContent FromModel(OrderContentModel model)
        {
            return new OrderContent()
            {
                Name = model.Name,
                Description = model.Description,
                Icon = model.Icon,
                ListingOrder = model.ListingOrder,
                RecordStatus = model.RecordStatus,
                CreatedUser = model.CreatedUser,
                CreatedDate = model.CreatedDate,
                CreatedUserName = model.CreatedUserName,
                ModifiedUser = model.ModifiedUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUserName = model.ModifiedUserName,
                Id = model.Id,
                NameAr = model.NameAr,
                DescriptionAr = model.DescriptionAr,
            };
        }
    }
}
