﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IServiceRepository
    {
        Task<List<ServiceModel>> GetListAsync();
        Task<ServiceModel?> GetByIdAsync(int id);
        Task<ServiceModel?> GetByNameAsync(string name);
        Task<int> CreateAsync(ServiceModel model);
        Task<int> UpdateAsync(ServiceModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
        Task<ServiceAmountModel?> GetServiceAmountAsync(int id);
        Task<List<IdValueModel>> GetAddOnServicesByIds(List<int> services);
        Task<List<IdValueModel>> GetLookupAsync(ServiceType serviceType);
        Task<List<ServiceModel>> GetDetailsByCustomerIdAsync(int customerId);
        Task<List<ServiceModel>> GetAddOnServicesById(int serviceId);
    }
}
