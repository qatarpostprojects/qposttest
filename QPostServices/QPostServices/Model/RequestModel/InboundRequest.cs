﻿using QPostServices.Domain.Model;
using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class InboundRequest
    {
        [Required]
        public int OrderId { get; set; }
        [Required]
        public int TypeId { get; set; } 

        [ConditionalRequired(nameof(TypeId),"101")]
        public int? Holddays { get; set; }
        public string? SpecialInstructions { get; set; }
        public string? Remarks { get; set; }

        [Required]
        public string PaymentType { get; set; } = string.Empty;

        public InboundModel ToModel()
        {
            return new InboundModel
            {
                OrderId = OrderId,
                TypeId = TypeId,
                Holddays= Holddays,
                SpecialInstructions= SpecialInstructions,
                PaymentType= PaymentType,
                Remarks = Remarks
            };
        }
    }
}
