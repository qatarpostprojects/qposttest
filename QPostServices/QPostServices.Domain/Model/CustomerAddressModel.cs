﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CustomerAddressModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Phone { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public bool IsPrimary { get; set; }
        public string City { get; set; } = default!;
        public string? PoBox { get; set; } 
        public string? SmartBox { get; set; } 
        public string? ZoneNumber { get; set; } 
        public string? StreetNumber { get; set; } 
        public string? BuildingNumber { get; set; } 
        public string? FloorNumber { get; set; } 
        public string? UnitNumber { get; set; } 
        public string? GeoLocationX { get; set; }
        public string? GeoLatitude { get; set; } 
        public string? GeoLongitude { get; set; } 
        public AddressType AddressType { get; set; }
        public string? AddressDescription { get; set; }
        public string Country { get; set; } = default!;
        public string? Reference1 { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? ZipCode { get; set; }
    }
}
