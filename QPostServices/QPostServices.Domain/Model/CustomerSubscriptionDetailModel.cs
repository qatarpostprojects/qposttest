﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class CustomerSubscriptionDetailModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceType { get; set; } = string.Empty;
        public int SubscriptionTypeValue { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? ServiceProductId { get; set; }
        public string PaymentChannel { get; set; } = string.Empty;
        public bool IsEligibleForRenew { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public int? LastPaymentTransactionId { get; set; }
        public bool ActiveStatus { get; set; }
        public string? Remarks { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public string? Reference1 { get; set; }
        public string? Reference2 { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? MaximumNumberOfTransaction { get; set; }
        public int? NumberOfTransactionPending { get; set; }
        public int? NumberOfTransactionCompleted { get; set; }
        public decimal? ServiceAmount { get; set; }
        public decimal? ServiceAmountPending { get; set; }
        public decimal? ServiceAmountConsumed { get; set; }
        public int? CardPaymentId { get; set; }
        public string PaymentType { get; set; }
        public int? WalletTransactioId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string SubscriptionType { get; set; } = string.Empty;
        public string? ApplicableFor { get; set; }
        public string SubscriptionPaymentType { get; set; } = string.Empty;
        public bool IsUpgradeEligible { get; set; }
        public int? UpgradeID { get; set; }
        public bool IsAddonServiceAvailable { get; set; }
        public bool IsAddonService { get; set; }
        public int? ParentServiceId { get; set; }
        public int? LoyaltyID { get; set; }
        public int ServiceBaseValue { get; set; }
        public decimal SubscriptionAmount { get; set; }
        public decimal RenewAmount { get; set; }
        public decimal CancelAmount { get; set; }
        public string? ServiceBaseOn { get; set; }
    }
}
