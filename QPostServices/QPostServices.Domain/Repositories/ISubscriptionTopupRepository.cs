﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ISubscriptionTopupRepository
    {
        Task<List<SubscriptionTopupModel>> GetListByServiceIdAsync(int serviceId);
        Task<List<SubscriptionTopupModel>> GetListAsync();
        Task<SubscriptionTopupModel?> GetByIdAsync(int id);
        Task<int> CreateAsync(SubscriptionTopupModel model);
        Task<int> UpdateAsync(SubscriptionTopupModel model);
        Task<int> UpdateStatusAsync(int id, int userId);
    }
}
