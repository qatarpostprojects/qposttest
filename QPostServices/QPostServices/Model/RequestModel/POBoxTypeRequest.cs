﻿using QPostServices.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class POBoxTypeRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        public string? Logo { get; set; }
        public string? NameAr { get; set; }
        public string? DescriptionAr { get; set; }

        public POBoxTypeModel ToCreateModel()
        {
            return new POBoxTypeModel() 
            { 
                Name = Name, 
                Description = Description, 
                Logo = Logo,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
        public POBoxTypeModel ToUpdateModel(int id)
        {
            return new POBoxTypeModel() 
            { 
                Name = Name, 
                Description = Description, 
                Logo = Logo,
                Id = id,
                NameAr = NameAr,
                DescriptionAr = DescriptionAr
            };
        }
    }
}
