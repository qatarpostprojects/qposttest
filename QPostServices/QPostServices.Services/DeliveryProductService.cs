﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class DeliveryProductService : IDeliveryProductService
    {
        private const string _successCreation = "Delivery product created successfully";
        private const string _successUpdate = "Delivery product updated successfully";
        private const string _failedCreation = "Delivery product creation failed";
        private const string _failedUpdation = "Delivery product updation failed";
        private const string _successDelete = "Delivery product deleted successfully";
        private const string _failedDelete = "Delivery product deletion failed";
        private const string _DeliveryProductDoesNotExist = "Delivery product with id : {0} Does not exist";

        private readonly ILoginService _loginService;
        private readonly IDeliveryProductRepository _deliveryProductRepository;
        public DeliveryProductService(IDeliveryProductRepository deliveryProductRepository, ILoginService loginService)
        {
            _deliveryProductRepository = deliveryProductRepository;
            _loginService = loginService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(DeliveryProductModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();

            var result = await _deliveryProductRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _deliveryProductRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DeliveryProductDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _deliveryProductRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DeliveryProductDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Type
        public async Task<List<DeliveryProductModel>> GetByTypeAsync(bool isOverseas)
        {
            return await _deliveryProductRepository.GetByTypeAsync(isOverseas);
        }
        #endregion

        #region Get All
        public async Task<List<DeliveryProductModel>> GetListAsync()
        {
            return await _deliveryProductRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(DeliveryProductModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();

            var result = await _deliveryProductRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_DeliveryProductDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
