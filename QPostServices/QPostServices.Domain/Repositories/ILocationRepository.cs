﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ILocationRepository
    {
        Task<List<string>> GetZonesAsync(int id);
        Task<List<string>> GetStreetsAsync(int zoneId);
        Task<List<string>> GetBuildingsAsync(int zoneId, int streetId);
        Task<ValidAddressModel> ValidateAddressAsync(ValidAddressModel model);
    }
}
