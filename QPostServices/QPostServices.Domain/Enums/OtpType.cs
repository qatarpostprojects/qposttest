﻿namespace QPostServices.Domain.Enums
{
    public enum OtpType
    {
        SMS = 1,
        Email = 2
    }
}
