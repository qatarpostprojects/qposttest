﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ICustomerProfileImageService
    {
        Task<ResponseModel> GetCustomerProfileImage(int customerId);
        Task<ResponseModel> CreateImage(CustomerProfileImageModel model);
        Task<ResponseModel> UpdateImage(CustomerProfileImageModel model);
    }
}
