﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class StampCodeRequest
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public string? TrackingNumber { get; set; }

        public StampCodeModel ToModel()
        {
            return new StampCodeModel
            {
                OrderId = OrderId,
                CustomerId = CustomerId,
                TrackingNumber = TrackingNumber,
            };
        }
    }
}
