﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IPOBoxTypeRepository
    {
        Task<List<POBoxTypeModel>> GetListAsync();
        Task<int> CreateAsync(POBoxTypeModel model);
        Task<int> UpdateAsync(POBoxTypeModel model);
        Task<int> UpdateStatusAsync(int id, bool status, int userId);
        Task<POBoxTypeModel> GetByIdAsync(int id);
    }
}
