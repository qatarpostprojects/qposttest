﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IUserLoginService
    {
        Task<ResponseModel> CreateAsync(UserLoginModel model);
        Task<ResponseModel> GetByIdAsync(int id);
        Task<List<UserLoginModel>> GetAllActiveAsync();
        Task<ResponseModel> DeleteAsync(int id);
        Task<ResponseModel> UpdateAsync(UserLoginModel model);
    }
}
