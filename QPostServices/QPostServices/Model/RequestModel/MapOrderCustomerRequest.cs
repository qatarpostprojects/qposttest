﻿using QPostServices.ValidationExtensions;
using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class MapOrderCustomerRequest : PhoneNoPoBoxRequest
    {
        [Required]
        public int CustomerId { get; set; }
    }
}
