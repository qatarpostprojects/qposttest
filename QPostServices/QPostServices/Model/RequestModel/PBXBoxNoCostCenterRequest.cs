﻿using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class PBXBoxNoCostCenterRequest
    {
        //[StringLength(6, MinimumLength = 1)]
        public string? POBoxNo { get; set; } = default!;

        [Required]
        //[StringLength(6, MinimumLength = 6)]
        public string CostCenter { get; set; } = default!;
    }
}
