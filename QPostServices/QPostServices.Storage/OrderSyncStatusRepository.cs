﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class OrderSyncStatusRepository : IOrderSyncStatusRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public OrderSyncStatusRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create OrderSyncStatus
        public async Task<int> CreateAsync(OrderSyncStatusModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_OrderSyncStatus_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@trackingNumber", model.TrackingNumber);
                parameters.Add("@syncDate", model.SyncDate);
                parameters.Add("@syncStatus", model.SyncStatus);
                parameters.Add("@errorDescription", model.ErrorDescription);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@recordStatus", RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUserId);
                parameters.Add("@createdDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get OrderSyncStatus by id
        public async Task<OrderSyncStatusModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_OrderSyncStatus_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<OrderSyncStatusModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get list of OrderSyncStatus
        public async Task<List<OrderSyncStatusModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_OrderSyncStatus_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<OrderSyncStatusModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
