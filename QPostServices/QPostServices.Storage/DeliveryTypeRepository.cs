﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class DeliveryTypeRepository : IDeliveryTypeRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;

        public DeliveryTypeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create DeliveryType
        public async Task<int> CreateAsync(DeliveryTypeModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_DeliveryType_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@image", model.Image);
                parameters.Add("@priceType", model.PriceType);
                parameters.Add("@listingOrder", model.ListingOrder);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUserId);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@deliveryProductId", model.DeliveryProductId);
                parameters.Add("@productServiceCategory", model.ProductServiceCategory);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get DeliveryType by Id
        public async Task<DeliveryTypeModel?> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_DeliveryType_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<DeliveryTypeModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get DeliveryType List
        public async Task<List<DeliveryTypeModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_DeliveryType_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<DeliveryTypeModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get DeliveryType List By ProductId
        public async Task<List<DeliveryTypeModel>> GetByProductIdAsync(int productId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_DeliveryType_GetByProductId]";
                var parameters = new DynamicParameters();
                parameters.Add("@deliveryProductId", productId);
                var result = await connection.QueryAsync<DeliveryTypeModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update DeliveryType
        public async Task<int> UpdateAsync(DeliveryTypeModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_DeliveryType_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@deliveryProductId", model.DeliveryProductId);
                parameters.Add("@name", model.Name);
                parameters.Add("@description", model.Description);
                parameters.Add("@priceType", model.PriceType);
                parameters.Add("@image", model.Image);
                parameters.Add("@listingOrder", model.ListingOrder);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@modifiedUser", model.ModifiedUserId);
                parameters.Add("@id", model.Id);
                parameters.Add("@productServiceCategory", model.ProductServiceCategory);
                parameters.Add("@nameAr", model.NameAr);
                parameters.Add("@descriptionAr", model.DescriptionAr);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update DeliveryType RecordStatus
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_DeliveryType_UpdateStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
