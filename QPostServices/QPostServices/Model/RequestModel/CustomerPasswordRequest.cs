﻿using System.ComponentModel.DataAnnotations;

namespace QPostServices.Model.RequestModel
{
    public class CustomerPasswordRequest
    {
        [Required]
        public string Password { get; set; } = string.Empty;
        public OTPRequest OTPRequest { get; set; } = new OTPRequest();
    }
}
