﻿using QPostServices.Domain.Enums;

namespace QPostServices.Domain.Model
{
    public class EventMasterModel
    {
        public int Id { get; set; }
        public int EventCode { get; set; }
        public string EventDescription { get; set; } = string.Empty;
        public string Remarks { get; set; } = string.Empty;
        public RecordStatus RecordStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUser { get; set; }
        public string CreatedUserName { get; set; } = string.Empty;
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public string? ModifiedUserName { get; set; }
    }
}
