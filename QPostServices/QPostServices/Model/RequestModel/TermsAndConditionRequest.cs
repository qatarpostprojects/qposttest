﻿using QPostServices.Domain.Model;

namespace QPostServices.Model.RequestModel
{
    public class TermsAndConditionRequest
    {
        public string? Value { get; set; }
        public string? ValueAr { get; set; }
        public TermsAndConditionModel ToCreateModel()
        {
            return new TermsAndConditionModel()
            {
                Value = Value,
                ValueAr = ValueAr
            };
        }
        public TermsAndConditionModel ToUpdateModel(int id)
        {
            return new TermsAndConditionModel()
            {
                Value = Value,
                Id = id,
                ValueAr = ValueAr
            };
        }
    }
}
