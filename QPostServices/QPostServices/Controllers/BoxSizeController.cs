﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BoxSizeController : ControllerBase
    {
        private readonly IBoxSizeServices _boxSizeServices;
        public BoxSizeController(IBoxSizeServices boxSizeServices)
        {
            _boxSizeServices = boxSizeServices;
        }

        #region Create BoxSize
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateBoxSize))]
        public async Task<IActionResult> CreateBoxSize([FromBody] BoxSizeRequest request)
        {
            try
            {
                var result = await _boxSizeServices.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get BoxSize by id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoxSize))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetBoxSize))]
        public async Task<IActionResult> GetBoxSize(int id)
        {
            try
            {
                var result = await _boxSizeServices.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(BoxSize.FromModel((BoxSizeModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get BoxSizes
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<BoxSize>))]
        [HttpGet(Name = nameof(GetBoxSizeList))]
        public async Task<IActionResult> GetBoxSizeList()
        {
            try
            {
                var results = await _boxSizeServices.GetListAsync();

                return Ok(results.Select(x => BoxSize.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete BoxSize
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteBoxSize))]
        public async Task<IActionResult> DeleteBoxSize(int id)
        {
            try
            {
                var result = await _boxSizeServices.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update BoxSize
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateBoxSize))]
        public async Task<IActionResult> UpdateBoxSize(int id, [FromBody] BoxSizeRequest request)
        {
            try
            {
                var result = await _boxSizeServices.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
