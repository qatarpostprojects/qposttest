﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface IStampCodeRepository
    {
        Task<string?> CreateStampCodeAsync(StampCodeModel model);
        Task<StampCodeModel?> GetByOrderId(int orderId);
    }
}
