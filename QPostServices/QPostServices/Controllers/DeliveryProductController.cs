﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QPostServices.Domain.Model;
using QPostServices.Domain.Services;
using QPostServices.Model.RequestModel;
using QPostServices.Model.ResponseModel;
using System.Net;

namespace QPostServices.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryProductController : ControllerBase
    {
        private readonly IDeliveryProductService _deliveryProductService;
        public DeliveryProductController(IDeliveryProductService deliveryProductService)
        {
            _deliveryProductService = deliveryProductService;
        }

        #region Get Delivery product List
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<DeliveryProduct>))]
        [HttpGet(Name = nameof(GetDeliveryProductList))]
        public async Task<IActionResult> GetDeliveryProductList()
        {
            try
            {
                var results = await _deliveryProductService.GetListAsync();
                return Ok(results.Select(x => DeliveryProduct.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Delivery product By Id
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DeliveryProduct))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}", Name = nameof(GetDeliveryProduct))]
        public async Task<IActionResult> GetDeliveryProduct(int id)
        {
            try
            {
                var result = await _deliveryProductService.GetByIdAsync(id);

                if (result.Error)
                {
                    return NotFound(result.Response);
                }

                return Ok(DeliveryProduct.FromModel((DeliveryProductModel)result.Response!));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Get Delivery product By type
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DeliveryProduct))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("type/{isOverSeas}", Name = nameof(GetDeliveryProductByType))]
        public async Task<IActionResult> GetDeliveryProductByType(bool isOverSeas)
        {
            try
            {
                var result = await _deliveryProductService.GetByTypeAsync(isOverSeas);
                return Ok(result.Select(x => DeliveryProduct.FromModel(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Create
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost(Name = nameof(CreateDeliveryProduct))]
        public async Task<IActionResult> CreateDeliveryProduct([FromBody] DeliveryProductRequest request)
        {
            try
            {
                var result = await _deliveryProductService.CreateAsync(request.ToCreateModel());

                if (!result.Error)
                {
                    return Ok(result.Response);
                }

                return BadRequest(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Update
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}", Name = nameof(UpdateDeliveryProduct))]
        public async Task<IActionResult> UpdateDeliveryProduct(int id, [FromBody] DeliveryProductRequest request)
        {
            try
            {
                var result = await _deliveryProductService.UpdateAsync(request.ToUpdateModel(id));

                if (result.Error)
                {
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        return NotFound(result.Response);
                    }
                    else
                    {
                        return BadRequest(result.Response);
                    }
                }
                return Ok(result.Response);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion

        #region Delete 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = nameof(DeleteDeliveryProduct))]
        public async Task<IActionResult> DeleteDeliveryProduct(int id)
        {
            try
            {
                var result = await _deliveryProductService.DeleteAsync(id);
                if (result.Error)
                {
                    return NotFound(result.Response);
                }
                return Ok(result.Response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message + ((ex.InnerException != null) ? ex.InnerException.Message : ""));
            }
        }
        #endregion
    }
}
