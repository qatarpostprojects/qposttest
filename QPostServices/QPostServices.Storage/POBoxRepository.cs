﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace QPostServices.Storage
{
    public class POBoxRepository : IPOBoxRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string? _dbCon;
        public POBoxRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _dbCon = _configuration.GetConnectionString("DefaultConnection");
        }

        #region Create
        public async Task<int> CreateAsync(POBoxModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBox_Create]";
                var parameters = new DynamicParameters();
                parameters.Add("@poBoxNumber", model.POBoxNumber);
                parameters.Add("@recordStatus", (int)RecordStatus.Active);
                parameters.Add("@createdUser", model.CreatedUserId);
                parameters.Add("@createdDate", DateTime.Now);
                parameters.Add("@poBoxType", model.POBoxTypeId);
                parameters.Add("@poBoxSubType", model.POBoxSubType);
                parameters.Add("@branchId", model.BranchId);
                parameters.Add("@rented", model.Rented);
                parameters.Add("@poBoxLocationId", model.POBoxLocationId);
                parameters.Add("@poBoxLocationName", model.POBoxLocationName);
                parameters.Add("@startDate", model.StartDate);
                parameters.Add("@paidUntil", model.PaidUntil);
                parameters.Add("@lastPaidUntil", model.LastPaidUntil);
                parameters.Add("@geoLocationX", model.GeoLocationX);
                parameters.Add("@geoLocationY", model.GeoLocationY);
                parameters.Add("@smartBoxNumber", model.SmartBoxNumber);
                parameters.Add("@poBoxCategoryId", model.POBoxCategoryID);
                parameters.Add("@isSmartBox", model.IsSmartBox);
                parameters.Add("@poBoxPositionId", model.POBoxPositionID);
                parameters.Add("@poBoxLockTypeId", model.POBoxLockTypeID);
                parameters.Add("@smartCardNumber", model.SmartCardNumber);
                parameters.Add("@location", model.Location);
                parameters.Add("@reserved", model.Reserved);
                parameters.Add("@blocked", model.Blocked);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@groupId", model.GroupId);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get By Id
        public async Task<POBoxModel> GetByIdAsync(int id)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBox_GetById]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var result = await connection.QueryAsync<POBoxModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get All
        public async Task<List<POBoxModel>> GetListAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBox_Get]";
                var parameters = new DynamicParameters();
                var result = await connection.QueryAsync<POBoxModel>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Update
        public async Task<int> UpdateAsync(POBoxModel model)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBox_Update]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", model.Id);
                parameters.Add("@modifiedDate", DateTime.Now);
                parameters.Add("@modifiedUser", model.ModifiedUserId);
                parameters.Add("@poBoxNumber", model.POBoxNumber);
                parameters.Add("@recordStatus", model.RecordStatus);
                parameters.Add("@poBoxType", model.POBoxTypeId);
                parameters.Add("@poBoxSubType", model.POBoxSubType);
                parameters.Add("@branchId", model.BranchId);
                parameters.Add("@rented", model.Rented);
                parameters.Add("@poBoxLocationId", model.POBoxLocationId);
                parameters.Add("@poBoxLocationName", model.POBoxLocationName);
                parameters.Add("@startDate", model.StartDate);
                parameters.Add("@paidUntil", model.PaidUntil);
                parameters.Add("@lastPaidUntil", model.LastPaidUntil);
                parameters.Add("@geoLocationX", model.GeoLocationX);
                parameters.Add("@geoLocationY", model.GeoLocationY);
                parameters.Add("@smartBoxNumber", model.SmartBoxNumber);
                parameters.Add("@poBoxCategoryId", model.POBoxCategoryID);
                parameters.Add("@isSmartBox", model.IsSmartBox);
                parameters.Add("@poBoxPositionId", model.POBoxPositionID);
                parameters.Add("@poBoxLockTypeId", model.POBoxLockTypeID);
                parameters.Add("@smartCardNumber", model.SmartCardNumber);
                parameters.Add("@location", model.Location);
                parameters.Add("@reserved", model.Reserved);
                parameters.Add("@blocked", model.Blocked);
                parameters.Add("@remarks", model.Remarks);
                parameters.Add("@groupId", model.GroupId);

                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();

            }
            catch { throw; }
        }
        #endregion

        #region Update status
        public async Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId)
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBox_UpdateRecordStatus]";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@recordStatus", status);
                parameters.Add("@modifiedUser", userId);
                parameters.Add("@modifiedDate", DateTime.Now);
                var result = await connection.QueryAsync<int>(procedure, parameters, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Get All landing page services
        public async Task<List<POBoxServiceModel>> GetAllLandingPageServicesAsync()
        {
            try
            {
                IDbConnection connection = new SqlConnection(_dbCon);
                var procedure = "[POBox_POBOxService_GetAllServicesForLandingPage]";
                var result = await connection.QueryAsync<POBoxServiceModel>(procedure, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
