﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class LockTypeServices : ILockTypeServices
    {
        private readonly ILockTypeRepository _lockTypeRepository;
        private readonly ILoginService _loginService;

        private const string _successCreation = "Lock type created successfully";
        private const string _successUpdate = "Lock type updated successfully";
        private const string _failedCreation = "Lock type creation failed";
        private const string _failedUpdation = "Lock type updation failed";
        private const string _successDelete = "Lock type deleted successfully";
        private const string _failedDelete = "Lock type deletion failed";
        private const string _nameAlreadyExist = "Lock type with name : {0} already exist";
        private const string _lockTypeDoesNotExist = "Lock type with id : {0} Does not exist";


        public LockTypeServices(ILockTypeRepository lockTypeRepository, ILoginService loginService)
        {
            _lockTypeRepository = lockTypeRepository;
            _loginService = loginService;
        }

        #region Create lock type
        public async Task<ResponseModel> CreateAsync(LockTypeModel model)
        {
            var response = new ResponseModel();

            model.CreatedUserId = _loginService.LoggedInUser();
            var result = await _lockTypeRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Get lock type by id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _lockTypeRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_lockTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get list of lock type
        public async Task<List<LockTypeModel>> GetListAsync()
        {
            return await _lockTypeRepository.GetListAsync();
        }
        #endregion

        #region GetLookUpAsync
        public Task<List<IdValueModel>> GetLookUpAsync()
        {
            throw new NotImplementedException();
        } 
        #endregion

        #region Update Lock Type
        public async Task<ResponseModel> UpdateAsync(LockTypeModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUserId = _loginService.LoggedInUser();
            var result = await _lockTypeRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_lockTypeDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Delete Lock type
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _lockTypeRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_lockTypeDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion
    }
}
