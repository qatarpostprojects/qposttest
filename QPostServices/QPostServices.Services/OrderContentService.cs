﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class OrderContentService : IOrderContentService
    {
        private const string _successCreation = "Order Content created successfully";
        private const string _successUpdate = "Order Content updated successfully";
        private const string _failedCreation = "Order Content creation failed";
        private const string _failedUpdation = "Order Content updation failed";
        private const string _successDelete = "Order Content deleted successfully";
        private const string _failedDelete = "Order Content deletion failed";
        private const string _OrderContentDoesNotExist = "Order Content with id : {0} Does not exist";

        private readonly IFileService _fileService;
        private readonly ILoginService _loginService;
        private readonly IOrderContentRepository _orderContentRepository;
        public OrderContentService(IOrderContentRepository orderContentRepository,ILoginService loginService, IFileService fileService)
        {
            _orderContentRepository = orderContentRepository;
            _loginService = loginService;
            _fileService = fileService;
        }

        #region Create
        public async Task<ResponseModel> CreateAsync(OrderContentModel model)
        {
            model.CreatedUser = _loginService.LoggedInUser();
            var response = new ResponseModel();
            if (model.IconFile != null)
            {
                model.Icon = await _fileService.ConvertImageToBase64(model.IconFile);
            }
            var result = await _orderContentRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();
            var userId = _loginService.LoggedInUser();

            var result = await _orderContentRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_OrderContentDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get By Id
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _orderContentRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_OrderContentDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region Get List
        public async Task<List<OrderContentModel>> GetListAsync()
        {
            return await _orderContentRepository.GetListAsync();
        }
        #endregion

        #region Update
        public async Task<ResponseModel> UpdateAsync(OrderContentModel model)
        {
            var response = new ResponseModel();
            model.ModifiedUser = _loginService.LoggedInUser();
            if (model.IconFile != null)
            {
                model.Icon = await _fileService.ConvertImageToBase64(model.IconFile);
            }
            var result = await _orderContentRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_OrderContentDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        } 
        #endregion
    }
}
