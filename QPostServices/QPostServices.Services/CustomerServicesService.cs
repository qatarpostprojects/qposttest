﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;
using QPostServices.Domain.Repositories;
using QPostServices.Domain.Services;

namespace QPostServices.Services
{
    public class CustomerServicesService : ICustomerServicesService
    {
        private const string _successCreation = "Service created successfully";
        private const string _successUpdate = "Service updated successfully";
        private const string _failedCreation = "Service creation failed";
        private const string _failedUpdation = "Service updation failed";
        private const string _successDelete = "Service deleted successfully";
        private const string _failedDelete = "Service deletion failed";
        private const string _nameAlreadyExist = "Service with name : {0} already exist";
        private const string _serviceDoesNotExist = "Service with id : {0} Does not exist";

        private readonly IServiceRepository _serviceRepository;
        private readonly ILoginService _loginService;
        public CustomerServicesService(IServiceRepository subscriptionRepository, ILoginService loginService)
        {
            _serviceRepository = subscriptionRepository;
            _loginService = loginService;
        }

        #region Create Service
        public async Task<ResponseModel> CreateAsync(ServiceModel model)
        {
            var response = new ResponseModel();

            model.CreatedUser = _loginService.LoggedInUser();
            var result = await _serviceRepository.CreateAsync(model);

            if (result > 0)
            {
                response.Response = _successCreation;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = _failedCreation;
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            return response;
        }
        #endregion

        #region Delete subscription
        public async Task<ResponseModel> DeleteAsync(int id)
        {
            var response = new ResponseModel();

            var userId = _loginService.LoggedInUser();
            var result = await _serviceRepository.UpdateStatusAsync(id, RecordStatus.Inactive, userId);

            if (result > 0)
            {
                response.Response = _successDelete;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_serviceDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region GetAllActiveAsync
        public async Task<List<ServiceModel>> GetAllActiveAsync()
        {
            return await _serviceRepository.GetListAsync();
        }
        #endregion

        #region GetByIdAsync
        public async Task<ResponseModel> GetByIdAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _serviceRepository.GetByIdAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_serviceDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region UpdateAsync
        public async Task<ResponseModel> UpdateAsync(ServiceModel model)
        {
            var response = new ResponseModel();

            model.ModifiedUser = _loginService.LoggedInUser();
            var result = await _serviceRepository.UpdateAsync(model);

            if (result > 0)
            {
                response.Response = _successUpdate;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else if (result < 0)
            {
                response.Error = true;
                response.Response = string.Format(_nameAlreadyExist, model.Name);
                response.StatusCode = System.Net.HttpStatusCode.BadRequest;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_serviceDoesNotExist, model.Id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }

        #endregion

        #region GetServiceAmountAsync
        public async Task<ResponseModel> GetServiceAmountAsync(int id)
        {
            var response = new ResponseModel();

            var result = await _serviceRepository.GetServiceAmountAsync(id);

            if (result != null)
            {
                response.Response = result;
                response.Error = false;
                response.StatusCode = System.Net.HttpStatusCode.OK;
            }
            else
            {
                response.Error = true;
                response.Response = string.Format(_serviceDoesNotExist, id);
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
            }

            return response;
        }
        #endregion

        #region GetDetailsByCustomerId
        public async Task<List<ServiceModel>> GetDetailsByCustomerIdAsync()
        {
            int customerId = _loginService.LoggedInCustomer();
            var parentServices = await _serviceRepository.GetDetailsByCustomerIdAsync(customerId);

            //var addOnServices = await _serviceRepository.GetAddOnServicesByIds(parentServices.Select(x=>x.Id).ToList());

            if (parentServices != null)
            {
                //parentServices.Select(async x => x.AddOnServices = await _serviceRepository.GetAddOnServicesById(x.Id)); 
                var tasks = parentServices.Select(async x => await _serviceRepository.GetAddOnServicesById(x.Id));
                var results = await Task.WhenAll(tasks);
                for (int i = 0; i < parentServices.Count; i++)
                {
                    parentServices.ElementAt(i).AddOnServices = results[i];
                }
            }

            return parentServices ?? new List<ServiceModel>();

        }
        #endregion

        #region Get Lookop
        public Task<List<IdValueModel>> GetLookupAsync(ServiceType serviceType)
        {
            return _serviceRepository.GetLookupAsync(serviceType);
        }

        public async Task<ResponseModel> GetCalculatedServiceAmountAsync(CalculateServiceAmountModel model)
        {
            var service = await _serviceRepository.GetByIdAsync(model.ServiceId);
            if (service == null)
            {
                return new ResponseModel 
                { 
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    Error = true
                };
            }

            decimal netRate = 0;
            decimal thisTermpayble = 0;

            DateTime currentDate = DateTime.Now;

            if (service.SubscriptionType == ServiceSubscriptionType.Yearly.ToString())
            {

                decimal perDayAmount = service.SubscriptionAmount / 360;
                int remainingMonths = 12 - DateTime.Now.Month;
                int remainingDaysinMonth = (31 - DateTime.Now.Day) < 1 ? 1 : 31 - DateTime.Now.Day;
                int totalRemainingDays = remainingDaysinMonth + (remainingMonths * 30);
                thisTermpayble = perDayAmount * totalRemainingDays;
            }

            else if (service.SubscriptionType == ServiceSubscriptionType.Monthly.ToString())
            {
                decimal perDayAmount = service.SubscriptionAmount / 30;

                int daysInMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
                int remainingDaysInMonth = daysInMonth - currentDate.Day;

                thisTermpayble = perDayAmount * remainingDaysInMonth;

            }
            else if (service.SubscriptionType == ServiceSubscriptionType.Weekly.ToString())
            {
                decimal perDayAmount = service.SubscriptionAmount / 7;

                DayOfWeek currentDayOfWeek = currentDate.DayOfWeek;
                int remainingDaysInWeek = DayOfWeek.Saturday - currentDayOfWeek;

                if (remainingDaysInWeek < 0)
                {
                    remainingDaysInWeek = 0;
                }

                thisTermpayble = perDayAmount * remainingDaysInWeek;
            }
            else
            {
                thisTermpayble = service.SubscriptionAmount;
            }
            netRate = thisTermpayble + ((model.Duration - 1) * service.SubscriptionAmount);
            return new ResponseModel 
            { 
                Error = false,
                Response= netRate,
                StatusCode = System.Net.HttpStatusCode.OK
            };
        }
        #endregion

    }
}
