﻿namespace QPostServices.Domain.Model
{
    public class POBoxEnquiryModel
    {
        public string? POBoxNo { get; set; }

        public string? QatarID { get; set; }

        public string? PassportNo { get; set; }

        public string? CompanyRegistrationNo { get; set; }
    }
}
