﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface ILoginService
    {
        Task<string?> GenerateTokenAsync(string username, string password,string ip,string deviceName);
        Task<UserLoginModel?> LoginCustomerAsync(string username, string password, string ip, string deviceName);
        int LoggedInUser();
        int LoggedInCustomer();
    }
}
