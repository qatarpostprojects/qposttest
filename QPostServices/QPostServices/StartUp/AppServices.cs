﻿using QPostServices.Domain.Services;
using QPostServices.Services;

namespace QPostServices.StartUp
{
    public static class AppServices
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IExceptionLogService, ExceptionLogService>();

            services.AddScoped<ILockTypeServices, LockTypeServices>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IPositionServices, PositionServices>();
            services.AddScoped<IBoxTypeServices, BoxTypeServices>();
            services.AddScoped<IBoxCategoryServices, BoxCategoryServices>();
            services.AddScoped<IBoxSizeServices, BoxSizeServices>();
            services.AddScoped<IPOBoxService, POBoxService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICustomerAddressService, CustomerAddressService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<IWalletTransactionSummaryService, WalletTransactionSummaryService>();
            services.AddScoped<IWalletTransactionDetailsService, WalletTransactionDetailsService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<ICustomerServicesService, CustomerServicesService>();
            services.AddScoped<ICustomerOrderAddressService, CustomerOrderAddressService>();
            services.AddScoped<ICustomerSubscriptionService, CustomerSubscriptionService>();
            services.AddScoped<ICustomerOrderDetailsService, CustomerOrderDetailsService>();
            services.AddScoped<IUserLoginService, UserLoginService>();
            services.AddScoped<ISubscriptionTransactionService, SubscriptionTransactionService>();
            services.AddScoped<IOrderSyncStatusService, OrderSyncStatusService>();
            services.AddScoped<IEventMasterService, EventMasterService>();
            services.AddScoped<IShipmentCostService, ShipmentCostService>();
            services.AddScoped<ITrackService, TrackService>();
            services.AddScoped<IInboundRequestService, InboundRequestService>();
            services.AddScoped<IPaymentMethodService, PaymentMethodService>();
            services.AddScoped<IOrderContentService, OrderContentService>();
            services.AddScoped<IDeliveryProductService, DeliveryProductService>();
            services.AddScoped<IDeliveryTypeService, DeliveryTypeService>();
            services.AddScoped<IBranchService, BranchService>();
            services.AddScoped<ISubscriptionTopupService, SubscriptionTopupService>();
            services.AddScoped<IPOBoxDurationService, POBoxDurationService>();
            services.AddScoped<IPOBoxTypeService, POBoxTypeService>();
            services.AddScoped<IProductWeightService, ProductWeightService>();
            services.AddScoped<IHSCODEService, HSCODEService>();
            services.AddScoped<ILocationService, LocationService>();
            services.AddScoped<ICustomerCardDetailsService, CustomerCardDetailsService>();
            services.AddScoped<IPickupTypeService, PickupTypeService>();
            services.AddScoped<ICustomerProfileImageService, CustomerProfileImageService>();
            services.AddScoped<IFileService, FileServices>();
            services.AddScoped<IBannerService, BannerService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<IUserSessionService, UserSessionService>();
            services.AddScoped<IProfileNotificationSettingsService, ProfileNotificationsService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IServiceTypeService, ServiceTypeService>();
            services.AddScoped<INotificationCategoryServices, NotificationCategoryServices>();
            services.AddScoped<IStampCodeService, StampCodeService>();
            services.AddScoped<ICustomerNotificationService, CustomerNotificationService>();
            services.AddScoped<ITimeSlotService, TimeSlotService>();
            services.AddScoped<ITermsAndConditionService, TermsAndConditionService>();

            return services;
        }
    }
}
