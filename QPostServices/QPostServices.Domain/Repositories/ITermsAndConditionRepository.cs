﻿using QPostServices.Domain.Enums;
using QPostServices.Domain.Model;

namespace QPostServices.Domain.Repositories
{
    public interface ITermsAndConditionRepository
    {
        Task<string> GetAsync();
        Task<TermsAndConditionModel> GetDetailsAsync();
        Task<int> CreateAsync(TermsAndConditionModel model);
        Task<int> UpdateAsync(TermsAndConditionModel model);
        Task<int> UpdateStatusAsync(int id, RecordStatus status, int userId);
    }
}
