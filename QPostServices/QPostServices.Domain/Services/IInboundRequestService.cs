﻿using QPostServices.Domain.Model;

namespace QPostServices.Domain.Services
{
    public interface IInboundRequestService
    {
        Task<List<IdValueModel>> GetMasterLookupAsync();
    }
}
